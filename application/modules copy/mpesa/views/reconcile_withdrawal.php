<?php
$row = $query->row();

$serial_number = strtoupper($row->receipt_number);
$sender_name = $row->sender_name;
$sender_phone = $row->sender_phone;
$sender_name = str_replace('%20', ' ', $sender_name);
$account_number = strtoupper($row->account_number);
$amount = $mpesa_amount = $row->withdrawn;
$created = $row->created;
$completion_time  = $transaction_date = $row->completion_time;
// $mpesa_id = $row->mpesa_id;
$account_from_id = $row->account_id;
$paying_account = $row->paying_account;
$account_name = $row->account_name;

// var_dump($paying_account);die();


$completion_time = date('jS M Y H:i',strtotime($completion_time));

$date_paid = date('Y-m-d',strtotime($completion_time));



// var_dump($mpesa_id);die();
$mpesa_balance = $this->accounts_model->get_unallocated_withdrawal($mpesa_id);
$mpesa_balance = $mpesa_amount - $mpesa_balance;
?>

<div class="col-md-12">
	
</div>
<div class="row">
	<!-- <div class="col-md-12"> -->
		<div class="col-md-12">
			<section class="panel panel-warning">
			    <header class="panel-heading">
			      <h3 class="panel-title">Transaction Detail</h3>
			    </header>
			    <div class="panel-body">
			    	<div class="col-md-12">
			    		<div class="col-md-2">
			    			<strong>ACCOUNT FROM :</strong> <br> <?php echo $account_name;?>
			    		</div>
			    		<div class="col-md-2">
			    			<strong>SERIAL NUMBER :</strong><br>  <?php echo $serial_number;?>
			    		</div>
			    		<div class="col-md-2">
			    			<strong>AMOUNT TRANSFERED :</strong><br>  <?php echo $amount;?>
			    		</div>
			    		<div class="col-md-2">
			    			<strong>COMPLETION DATE :</strong><br>  <?php echo $completion_time;?>
			    		</div>
			    		<div class="col-md-3">
			    			<a href="<?php echo site_url();?>mpesa-transactions/unreconcilled-withdrawals" class="btn btn-sm pull-right btn-warning"  ><i class="fa fa-arrow-left"></i> Back to unreconcilled list</a>
			    		</div>
			    	</div>
					
				</div>
			</section>
		</div>

		<div class="col-md-6">
			<section class="panel panel-warning">
			    <header class="panel-heading">
			      <h3 class="panel-title">Tenancy Info</h3>
			    </header>
			    <div class="panel-body">
			    	<div class="row">
			            <div class="col-md-12">
			              <?php echo form_open("mpesa/transactions/confirm_transfer/".$mpesa_id."/".$account_from_id, array("class" => "form-horizontal"));?>
			              
			                <div class="col-md-12">
			                    <!-- <h2 class="pull-right"> KES. <?php echo number_format($total_amount,2);?></h2> -->

			                <input type="hidden" name="account_from_id" value="<?php echo $account_from_id?>">
			                 <input type="hidden" name="transaction_date" value="<?php echo $transaction_date?>">
			                  
			                  
	                        <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">


			                    
							<input type="hidden" name="mpesa_id" id="mpesa_id" value="<?php echo $mpesa_id?>">
							<div class="form-group">
									<label class="col-md-4 control-label">Account To: </label>

									<div class="col-md-7">

										<select id='account_to_id' name='account_to_id' class='form-control' required="required">
											<option value="">-- Select account --</option>
				                                <?php
				                                 $query = $this->accounts_model->get_bank_accounts_types(6);

				                                $options2 = $query;
				                                $bank_list = '';
				                                $bank_total = 0;
				                                foreach($options2->result() AS $key_old) 
				                                { 

				                                    $account_id = $key_old->account_id;
				                                    $account_name = $key_old->account_name;
				                                    ?>
				                                    <option value="<?php echo $account_id;?>"><?php echo $account_name;?></option>
				                                    <?php
				                                    
				                                }
				                                ?>
										</select>
									</div>
							</div>
			                  <div class="form-group">
			                    <label class="col-md-4 control-label">Payment Date: </label>

			                    <div class="col-md-7">
			                       <div class="input-group">
			                            <span class="input-group-addon">
			                                <i class="fa fa-calendar"></i>
			                            </span>
			                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" id="datepicker" value="<?php echo $date_paid?>" required>
			                        </div>
			                    </div>
			                  </div>
			                 
			                   <div class="form-group" >
			                      <label class="col-md-4 control-label"> Reference No: </label>
			                      <div class="col-md-7">
			                        <input type="text" class="form-control" name="reference_number" placeholder="" value="<?php echo $serial_number?>">
			                      </div>
			                    </div>
			                  <div class="form-group">
			                    <label class="col-md-4 control-label">Total Amount: </label>

			                    <div class="col-md-7">
			                      <input type="number" class="form-control" name="total_amount" placeholder=""  autocomplete="off" value="<?php echo $amount;?>" readonly>
			                    </div>
			                  </div>
			                  <div class="form-group">
			                    <label class="col-md-4 control-label">Paid in By: </label>

			                    <div class="col-md-7">
			                      <input type="text" class="form-control" name="paid_by" placeholder="<?php echo $sender_name;?>" value="<?php echo $sender_name;?>" autocomplete="off" >
			                    </div>
			                  </div>
			                  <div class="col-md-12">
			                      <div class="text-center">
			                        <button class="btn btn-info btn-sm " type="submit" onclick="return confirm('Are you sure you want to complete this payment ? ')">Complete Payment </button>
			                      </div>
			                  </div>
			                </div>
			              <?php echo form_close();?>
			            </div>
			          </div>
						
				
				</div>
			</section>
		</div>
		<div class="col-md-6">
			    <div class="col-md-12">
			     <section class="panel panel-warning">
				    <header class="panel-heading">
				      <h3 class="panel-title">Latest Payment</h3>
				    </header>
				    <div class="panel-body">
			              <table class="table table-hover table-bordered col-md-12">
			                <thead>
			                  <tr>
			                    <th>#</th>
			                    <th>Payment Date</th>
			                    <th>Accounts</th>
			                    <th>Amount Paid</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  <?php
			                  // var_dump($mpesa_id);die();
			                  $lease_payments = $this->accounts_model->get_lease_mpesa_withdrawals($mpesa_id,10);
			                    // var_dump($tenant_query);die();
			                  if($lease_payments->num_rows() > 0)
			                  {
			                    $y = 0;
			                    foreach ($lease_payments->result() as $key) {
			                      # code...
			                      $receipt_number = $key->transaction_number;
			                      $amount_paid = $key->transacted_amount;
			                      $transaction_date = $key->transaction_date;
			                      $account_to_name = $key->account_name;
			                      $confirmation_status = $key->mpesa_transfer_status;

			                      if($confirmation_status == 0)
			                      {
			                        $confirmation_status_text = "Reconciled";
			                        $button = '<a class="btn btn-sm btn-danger" href="'.site_url().'mpesa/transactions/deactivate_payments/'.$mpesa_id.'" onclick="return confirm(\'Do you want to Deactivate this payment Kes.'.$amount_paid.'?\');" title="Deactivate For Company Return'.$amount_paid.'"> Deactivate </a>';


			                      }
			                      else
			                      {
			                        $confirmation_status_text = "Not Reconciled";
			                        $button = '<a class="btn btn-sm btn-info" href="'.site_url().'mpesa/transactions/activate_payments/'.$mpesa_id.'" onclick="return confirm(\'Do you want to Activate this payment Kes.'.$amount_paid.'?\');" title="Activate For Company Return'.$amount_paid.'"> Activate</a>';


			                      }
			                      $payment_explode = explode('-', $payment_date);

			                      $transaction_date = date('jS M Y',strtotime($transaction_date));
			                      // $payment_created = date('jS M Y',strtotime($payment_created));
			                      $y++;

			                      ?>
			                      <tr>
			                        <td><?php echo $y?></td>
			                        <td><?php echo $transaction_date;?></td>
			                        <td><?php echo $account_name?> - <?php echo $account_to_name?></td>
			                        <td><?php echo number_format($amount_paid,2);?></td>

			                      </tr>
			                      <?php

			                    }
			                  }
			                  ?>

			                </tbody>
			              </table>

			          </div>
			        </div>
			      </section>
			    
			
		</div>
		
	<!-- </div> -->

</div>


<script type="text/javascript">
	$(function() {
		 $("#lease_id").customselect();
	    $("#invoice_type_id").customselect();
	    $("#billing_schedule_id").customselect();

	});


	function check_payment_type(payment_type_id){

	  var myTarget1 = document.getElementById("cheque_div");

	  var myTarget2 = document.getElementById("mpesa_div");

	  var myTarget3 = document.getElementById("insuarance_div");

	  if(payment_type_id == 1)
	  {
	    // this is a cash

	    myTarget1.style.display = 'none';
	    myTarget2.style.display = 'none';
	    myTarget3.style.display = 'none';
	  }
	  else if(payment_type_id == 2 || payment_type_id == 3 || payment_type_id == 5)
	  {
	    // cheque
	    myTarget1.style.display = 'block';
	    myTarget2.style.display = 'none';
	    myTarget3.style.display = 'none';
	  }

	  else
	  {
	    myTarget1.style.display = 'none';
	    myTarget2.style.display = 'none';
	    myTarget3.style.display = 'none';
	  }

	}
</script>
