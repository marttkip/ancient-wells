<?php echo $this->load->view('search/search_mpesa_completed_records','', true); ?>
<?php

$result = '';

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;

	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Date Paid</th>
				<th>Serial Numbers</th>
				<th>Phone Number</th>
				<th>Sender Name</th>
				<th>Account </th>
				<th>Amount </th>
				<th>Recon </th>
				<th colspan="1">Actions</th>
			</tr>
		</thead>
		  <tbody>

	';


	foreach ($query->result() as $leases_row)
	{
		$created = $leases_row->created;
		$mpesa_id = $leases_row->mpesa_id;
		$serial_number = $leases_row->receipt_number;
		$account_number = $leases_row->account_number;
		$sender_name = $leases_row->sender_name;
		$sender_phone = $leases_row->sender_phone;
		$amount = $leases_row->paid_in;
		$account_number = $leases_row->account_number;
		$mpesa_status = $leases_row->mpesa_status;
    	$amount_recon = $leases_row->recon_amount;

		// $amount_recon = $this->accounts_model->get_amount_reconcilled($mpesa_id);

		$sender_name = str_replace('%20', ' ', $sender_name);

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$date_sent  = date('jS M Y', strtotime($created));
		$count++;

		if($mpesa_status == 0)
		{
			if($amount_recon > 0)
			{
				$button = '<td><a class="btn btn-sm btn-info" href="'.site_url().'reconcile-payment/'.$mpesa_id.'" > <i class="fa fa-folder"></i> Detail</a></td>';
			}
			else {
					$button = '	<td><a class="btn btn-sm btn-info" href="'.site_url().'reconcile-payment/'.$mpesa_id.'" > <i class="fa fa-folder"></i> Detail</a></td>';
			}

			if($amount == $amount_recon)
			{
				$highight= 'success';
			}

			else if($amount != $amount_recon)
			{
				$highight = 'warning';
			}
		}
    else
		{
			$button = '<td></td><td></td>';
			$highight = 'danger';
		}
		$result .=
					'
						<tr >
							<td class="'.$highight.'">'.$count.'</td>
							<td class="'.$highight.'">'.$date_sent.'</td>
							<td class="'.$highight.'">'.$serial_number.'</td>
							<td class="'.$highight.'">'.$sender_phone.'</td>
							<td class="'.$highight.'">'.$sender_name.'</td>
							<td class="'.$highight.'">'.$account_number.'</td>
							<td class="'.$highight.'">'.number_format($amount ,2).'</td>
							<td class="'.$highight.'">'.number_format($amount_recon ,2).'</td>
							'.$button.'

						</tr>
					';


	}

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}


$accounts_search_title = $this->session->userdata('accounts_search_title');
?>
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->
<section class="panel panel-success">
    <header class="panel-heading">
      <h3 class="panel-title"><?php echo $title;?></h3>
      <div class="widget-tools pull-right" style="margin-top:-25px;">
		
	</div>
    </header>
    <div class="panel-body">

        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}

			$error = $this->session->userdata('error_message');

			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$search =  $this->session->userdata('search_completed_mpesa');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'mpesa/transactions/close_completed_mpesa_search" class="btn btn-sm btn-warning">Close Search</a>';
			}

			?>
			<div class="table-responsive">

				<?php echo $result;?>

            </div>
             <div class="panel-footer">
	        	<?php if(isset($links)){echo $links;}?>
	        </div>
		</div>

	</section>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>
