<?php echo $this->load->view('search/search_reconcilled_withdrawals','', true); ?>
<?php

$result = '';

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;

	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Date Paid</th>
				<th>Serial Numbers</th>
				<th>Sender Name</th>
				<th>Account From</th>
				<th>Amount </th>
				<th>Recon </th>
				<th colspan="2">Actions</th>
			</tr>
		</thead>
		  <tbody>

	';


	foreach ($query->result() as $leases_row)
	{
		// var_dump($leases_row);die();
		$created = $leases_row->completion_time;
		$mpesa_id = $leases_row->mpesa_id;
		$serial_number = $leases_row->receipt_number;
		$account_number = $leases_row->account_number;
		$sender_name = $leases_row->sender_name;
		$sender_phone = $leases_row->sender_phone;
		$amount = $leases_row->withdrawn;
		$account_number = $leases_row->account_number;
		$mpesa_status = $leases_row->mpesa_status;
    	$amount_recon = $leases_row->recon_amount;
    	$account_from = $leases_row->account_name;

		// $amount_recon = $this->accounts_model->get_amount_reconcilled($mpesa_id);

		$sender_name = str_replace('%20', ' ', $sender_name);

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$date_sent  = date('jS M Y', strtotime($created));

		// var_dump($created);die();
		$count++;

		
			// if($amount_recon > 0)
			// {
			// 	$button = '<td><a class="btn btn-xs btn-info" href="'.site_url().'reconcile-withdrawal/'.$mpesa_id.'" > <i class="fa fa-recycle"></i> Reconcile</a></td> <td></td>';
			// }
			// else {
			// 	$button = '	<td><a class="btn btn-xs btn-info" href="'.site_url().'reconcile-withdrawal/'.$mpesa_id.'" > <i class="fa fa-recycle"></i> Reconcile</a></td>
			// 				';
			// }
		if($amount_recon == $amount)
		{
		  $highight = 'success';
		}
    	else
		{
			$button = '<td></td><td></td>';
			$highight = 'danger';
		}
		$result .=
					'
						<tr class="'.$highight.'">
							<td>'.$count.'</td>
							<td>'.$date_sent.'</td>
							<td>'.strtoupper($serial_number).'</td>
							<td>'.$sender_name.'</td>
							<td>'.strtoupper($account_from).'</td>
							<td>'.number_format($amount ,2).'</td>
							<td>'.number_format($amount_recon ,2).'</td>
							'.$button.'

						</tr>
					';


	}

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}


$accounts_search_title = $this->session->userdata('accounts_search_title');
?>
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->

<section class="panel panel-success">
    <header class="panel-heading">
      <h3 class="panel-title"><?php echo $title;?></h3>
      <div class="widget-tools pull-right" style="margin-top:-25px;">
			
	</div>
    </header>
    <div class="panel-body">

        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}

			$error = $this->session->userdata('error_message');

			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$search =  $this->session->userdata('search_mpesa_reconcilled_transfered');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'mpesa/transactions/close_reconcilled_mpesa_withdrawals_search" class="btn btn-sm btn-warning">Close Search</a>';
			}

			?>
			 <div class="modal fade" id="upload_tenants" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Upload payments</h4>
                                </div>
                                <div class="modal-body">  
                                    <!-- Widget content -->
                                    <div class="panel-body">
                                        <div class="padd">
                                              <?php echo form_open_multipart('import/import-payments', array("class" => "form-horizontal", "role" => "form"));?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>Download the import template <a href="<?php echo site_url().'import/payments-template';?>">here.</a></li>
                                                    
                                                    <li>Save your file as a <strong>CSV (Comma Delimited)</strong> file before importing</li>
                                                    <li>After adding tenants data to the import template please import them using the button below</li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <select class="form-control" name="account_id" required="required">
                                                        	<option value=""> --- SELECT AN ACCOUNT ---  </option>
                                                            <?php
                                                             $account_rs = $this->accounts_model->get_bank_accounts();

                                                             if($account_rs->num_rows() > 0)
                                                             {
                                                             	foreach ($account_rs->result() as $key => $value) {
                                                             		# code...
                                                             		$account_id = $value->account_id;
                                                             		$account_name = $value->account_name;

                                                             		echo '<option value="'.$account_id.'"> '.$account_name.' </option>';
                                                             	}
                                                             }

                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br>
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="fileUpload btn btn-primary">
                                                            <span>Import tenants</span>
                                                            <input type="file" class="upload"  name="import_csv"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row ">
                                            <div class="center-align">
                                                <input type="submit" class="btn btn-success" value="Import payments" onChange="this.form.submit();" onclick="return confirm('Do you really want to upload the selected file?')">
                                            </div>
                                        </div>
                                        <?php echo form_close();?>
                                          </br>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
			<div class="table-responsive">

				<?php echo $result;?>

            </div>
             <div class="panel-footer">
	        	<?php if(isset($links)){echo $links;}?>
	        </div>
		</div>
	</section>
	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>
