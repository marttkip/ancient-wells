<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Content-type: application/json;');
class Mpesa  extends MX_Controller
{
		function __construct()
	{
		parent:: __construct();
		$this->load->model('accounts/accounts_model');
		$this->load->model('mpesa/mpesa_model');
		// $this->load->model('messaging/messaging_model');
		$this->load->model('real_estate_administration/leases_model');
	}

	 public function mpesa_receive_transaction($type=null)
	{
		$json = file_get_contents('php://input');


		$insert_array['test'] = $json;
		$this->db->insert('mpesa_test',$insert_array);

		// $json = '{
		//             "TransactionType": "Pay Bill",
		//             "TransID": "SGI11JPQI7",
		//             "TransTime": "20240718082957",
		//             "TransAmount": "5.00",
		//             "BusinessShortCode": "4022029",
		//             "BillRefNumber": "AWLB1011946",
		//             "InvoiceNumber": "",
		//             "OrgAccountBalance": "1337.00",
		//             "ThirdPartyTransID": "",
		//             "MSISDN": "e627220a4bf374cbcf224e81d15ed90ee97e59a0e495cc5ababcf6f2b479c195",
		//             "FirstName": "MARTIN",
		//             "MiddleName": "",
		//             "LastName": ""
		//         }';

		$decoded = json_decode($json);
	    $serial_number = null;
	    if(isset($decoded))
	    {
	      $serial_number = $decoded->TransID;
	    }

		if(!empty($serial_number))
		{
			$TransAmount = $decoded->TransAmount;
			$TransTime = $decoded->TransTime;
			$TransID = $decoded->TransID;
			$BusinessShortCode = $decoded->BusinessShortCode;
			$BillRefNumber = $decoded->BillRefNumber;
			$MSISDN = $decoded->MSISDN;
			$FirstName = $decoded->FirstName;
			// $MiddleName = $decoded->MiddleName;
			// $LastName = $decoded->LastName;
			$OrgAccountBalance = $decoded->OrgAccountBalance;
			$completion_time = date('Y-m-d H:i:s',strtotime($TransTime));
			
			$sender_name = $FirstName;
	    	$sender_name = str_replace('%20', ' ', $sender_name);
			$insert_array2['receipt_number'] = $TransID;
			$insert_array2['account_number'] = $BillRefNumber;
			$insert_array2['mpesa_status'] = $type;
			$insert_array2['transaction_status'] = $type;
			$insert_array2['sender_name'] = $sender_name;
			$insert_array2['sender_phone'] = $MSISDN;
			$insert_array2['paid_in'] = $TransAmount;
			$insert_array2['transaction_type'] = 0;
			$insert_array2['completion_time'] = $completion_time;
			$insert_array2['initiation_time'] = $completion_time;

			$insert_array2['created'] = date('Y-m-d H:i:s');


			if($this->db->insert('mpesa_transactions',$insert_array2))
			{
				$mpesa_id = $this->db->insert_id();

				if(!empty($BillRefNumber))
				$this->update_account($BillRefNumber,$mpesa_id,$TransAmount,$TransTime,$TransID,$FirstName);

				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'The service was accepted successfully';
				$response['ThirdPartyTransID'] = date('YmdHis');

				$tenant_phone_number = '+'.$MSISDN;

			}
			else
			{
				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'Could not process payment';
				$response['ThirdPartyTransID'] = date('YmdHis');
			}
		}
		else
		{
			$response['ResultCode'] = 1;
			$response['ResultDesc'] = 'Could not process payment';
			$response['ThirdPartyTransID'] = date('YmdHis');
		}
		echo json_encode($response);
	}

	public function update_account($account_number,$mpesa_id,$total_mpesa_paid,$transaction_time,$transaction_code,$paid_by)
	{


		// convert the time to date time

		$date  = date('Y-m-d',strtotime($transaction_time));
		$month  = date('m',strtotime($transaction_time));
		$year  = date('Y',strtotime($transaction_time));

		// var_dump($date);die();

		// check the format of the account number and lease that is active account_number is the tenant number HXXXXXX
		$lease_id = $this->mpesa_model->get_active_lease_id_from_tenant_number($account_number);

		if($lease_id > 0) // check if the value returns a lease id
		{
			// check if the account has a balance
			$all_leases = $this->leases_model->get_lease_detail($lease_id);
			foreach ($all_leases->result() as $leases_row)
			{
				$tenant_unit_id = $leases_row->tenant_unit_id;
				$property_name = $leases_row->property_name;
				$property_id = $leases_row->property_id;
				$rental_unit_name = $leases_row->rental_unit_name;
				$rental_unit_id = $leases_row->rental_unit_id;
				$tenant_name = $leases_row->tenant_name;
				$tenant_email = $leases_row->tenant_email;
				$lease_start_date = $leases_row->lease_start_date;
				$lease_duration = $leases_row->lease_duration;
				$tenant_id = $leases_row->tenant_id;
				$lease_number = $leases_row->lease_number;
				$lease_status = $leases_row->lease_status;
				$tenant_status = $leases_row->tenant_status;
				$tenant_number = $leases_row->tenant_number;
				$tenant_phone_number = $leases_row->tenant_phone_number;
			}

        
			$total_arrears = $this->accounts_model-> get_tenants_statements($lease_id,1);
			// $total_arrears = $tenants_response['balance'];


			if($total_arrears > 0 and $total_arrears <= $total_arrears) // if the account has a balance  and also the total_mpesa_paid
			{
				// get all the invoice types based on the priority of the balances

				$item_list = $this->accounts_model->get_all_invoice_items($lease_id);


				if($item_list->num_rows() > 0)
				{
						$amount_paid_balance = $total_mpesa_paid;
						foreach ($item_list->result() as $key => $value) 
						{
							// code...
								$invoice_type_name = $value->invoice_type_name;
								$invoice_type_id = $value->invoice_type_id;
								$total_amount = $value->total_amount;



								if($total_amount > 0 AND $total_amount <= $amount_paid_balance)
								{

									// allocate the payment items based on step 3

									$amount_to_pay = $total_amount;

									$amount = $amount_to_pay;
						  		$invoice_type_id=$invoice_type_id;
						  		// $invoice_id=$this->input->post('invoice_id');
						  		$payment_month=$month;
						  		$payment_year=$year;
						  		$lease_number=$lease_number;
						  		$rental_unit_id=$rental_unit_id;
						  		$tenant_id=$tenant_id;

						  		$item_date = date('M Y',strtotime($date));

						  		$remarks = 'Payment for  '.$invoice_type_name;

						  		$service = array(
						  							'payment_id'=>0,
						  							'amount_paid'=> $amount,
						  							'payment_month'=> $payment_month,
						  							'rental_unit_id'=> $rental_unit_id,
						  							'tenant_id'=> $tenant_id,
						  							'lease_number'=> $lease_number,
						  							'payment_year'=> $payment_year,
						  							'invoice_type_id' => $invoice_type_id,
						  							'payment_item_status' => 0,
						  							'lease_id' => $lease_id,
						  							'mpesa_id' => $mpesa_id,
						  							'personnel_id' => $this->session->userdata('personnel_id'),
						  							'payment_item_created' => $date,
						  							'remarks'=>$remarks
						  						);	

						  

						  		// var_dump($service); die();
						  		$this->db->insert('payment_item',$service);

									// reduce the total amount to the balance


									$amount_paid_balance = $amount_paid_balance - $amount_to_pay;




								}else if($total_amount > 0 AND $total_amount > $amount_paid_balance)
								{
									// pay the difference 
									$amount_to_pay = $amount_paid_balance; 



									$amount = $amount_to_pay;
									$invoice_type_id=$invoice_type_id;
									// $invoice_id=$this->input->post('invoice_id');
									$payment_month=$month;
									$payment_year=$year;
									$lease_number=$lease_number;
									$rental_unit_id=$rental_unit_id;
									$tenant_id=$tenant_id;

									$item_date = date('M Y',strtotime($date));

									$remarks = 'Payment for  '.$invoice_type_name;

									$service = array(
													'payment_id'=>0,
													'amount_paid'=> $amount,
													'payment_month'=> $payment_month,
													'rental_unit_id'=> $rental_unit_id,
													'tenant_id'=> $tenant_id,
													'lease_number'=> $lease_number,
													'payment_year'=> $payment_year,
													'invoice_type_id' => $invoice_type_id,
													'payment_item_status' => 0,
													'lease_id' => $lease_id,
													'mpesa_id' => $mpesa_id,
													'personnel_id' => $this->session->userdata('personnel_id'),
													'payment_item_created' => $date,
													'remarks'=>$remarks
												);

									// var_dump($service); die();
									$this->db->insert('payment_item',$service);

									// reduce the total amount to the balance

									$amount_paid_balance = $amount_paid_balance - $amount_to_pay;


								}

								// Create payment and update the payment items with the payment id

						}


					$amount_paid = $total_mpesa_paid;
					$payment_method=9;
					$type_of_account=1;
					$rental_unit_id = $rental_unit_id;
					$lease_number=$lease_number;
					$tenant_id=$tenant_id;

					$transaction_code = $transaction_code;
					$bank_id = 1;


					// calculate the points to get
					$payment_date = $date;

					$date_check = explode('-', $payment_date);
					$month = $date_check[1];
					$year = $date_check[0];


			  		$receipt_number = $this->accounts_model->create_receipt_number();




			  		$data = array(
								  			'payment_method_id'=>$payment_method,
								  			'bank_id'=>$bank_id,
								  			'amount_paid'=>$amount_paid,
								  			'personnel_id'=>$this->session->userdata("personnel_id"),
								  			'transaction_code'=>$transaction_code,
								  			'payment_date'=>$payment_date,
								  			'receipt_number'=>$transaction_code,
								  			'document_number'=>$receipt_number,
								  			'paid_by'=>$paid_by,
								  			'payment_created'=>date("Y-m-d"),
								  			'rental_unit_id'=>$rental_unit_id,
								  			'year'=>$year,
								  			'month'=>$month,
									      'account_id'=>$lease_number,
									      'tenant_id'=>$tenant_id,
								  			'confirm_number'=> $receipt_number,
								  			'payment_created_by'=>$this->session->userdata("personnel_id"),
								  			'approved_by'=>$this->session->userdata("personnel_id"),
								  			'date_approved'=>date('Y-m-d')
								  		);

			  			$data['lease_id'] = $lease_id;

			  			if($this->db->insert('payments', $data))
			  			{
			  				$payment_id = $this->db->insert_id();
			  				$service = array(
			  									'payment_id'=>$payment_id,
			  									'payment_item_created' =>$date,
			  									'payment_item_status'=>1,
			  									'document_no'=>$receipt_number
			  								);
			  				$this->db->where('payment_item_status = 0 AND lease_id = '.$lease_id.' and mpesa_id = '.$mpesa_id);
			  				$this->db->update('payment_item',$service);


			  				$this->mpesa_model->update_mpesa_recon_status($mpesa_id,$total_mpesa_paid,$payment_id);
			  				// $add_mpesa['local_id'] = $payment_id;
			  				// $this->db->where('mpesa_id = '.$mpesa_id);
			  				// $this->db->update('mpesa_transactions',$add_mpesa);
			  				// send receipt to the tenant for the transaction 
			  				

			  				// return TRUE;
			  			}
			  			else{
			  				// return FALSE;
			  			}
			  		

		  	}
			}

			

		}

		


		// send sms for the payment done

	}
	public function update_all_mpesa()
	{
		$this->db->where('recon_amount = 0 OR recon_amount IS NULL OR recon_status = 0');
		$query = $this->db->get('mpesa_transactions');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$mpesa_id = $value->mpesa_id;
				$paid_in  = $value->paid_in;

			 	$this->mpesa_model->update_mpesa_recon_status($mpesa_id,$paid_in,0);

			}
		}
	}


  public function mpesa_receive_transaction_old($type)
	{
		$json = file_get_contents('php://input');

		$insert_array['test'] = $json;
		$this->db->insert('mpesa_test',$insert_array);
		$decoded = json_decode($json);
    $serial_number = null;
    if(isset($decoded))
    {
      $serial_number = $decoded->TransID;
    }

		if(!empty($serial_number))
		{
			$TransAmount = $decoded->TransAmount;
			$TransTime = $decoded->TransTime;
			$TransID = $decoded->TransID;
			$BusinessShortCode = $decoded->BusinessShortCode;
			$BillRefNumber = $decoded->BillRefNumber;
			$MSISDN = $decoded->MSISDN;
			$FirstName = $decoded->FirstName;
			$MiddleName = $decoded->MiddleName;
			$LastName = $decoded->LastName;
			$OrgAccountBalance = $decoded->OrgAccountBalance;
			$sender_name = $FirstName.' '.$MiddleName.' '.$LastName;
	    $sender_name = str_replace('%20', ' ', $sender_name);
			$insert_array2['serial_number'] = $TransID;
			$insert_array2['account_number'] = $BillRefNumber;
			$insert_array2['mpesa_status'] = $type;
			$insert_array2['sender_name'] = $sender_name;
			$insert_array2['sender_phone'] = $MSISDN;
			$insert_array2['amount'] = $TransAmount;
			$insert_array2['created'] = date('Y-m-d H:i:s');
			if($this->db->insert('mpesa_transactions',$insert_array2))
			{

				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'The service was accepted successfully';
				$response['ThirdPartyTransID'] = date('YmdHis');

				$tenant_phone_number = '+'.$MSISDN;

			}
			else
			{
				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'Could not process payment';
				$response['ThirdPartyTransID'] = date('YmdHis');
			}
		}
		else
		{
			$response['ResultCode'] = 1;
			$response['ResultDesc'] = 'Could not process payment';
			$response['ThirdPartyTransID'] = date('YmdHis');
		}
		echo json_encode($response);
	}

}
?>
