<?php

class Mpesa_model extends CI_Model
{

  /*
  *	Count all items from a table
  *	@param string $table
  * 	@param string $where
  *
  */
  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  *	Retrieve all tenants
  *	@param string $table
  * 	@param string $where
  *
  */
  public function get_unreconcilled_payments($table, $where, $per_page=null, $page=null, $order=null, $order_method = 'ASC')
  {
    //retrieve all tenants
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('account', 'account.account_id = mpesa_transactions.account_id','LEFT');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }

   public function get_unreconcilled_withdrawals($table, $where, $per_page=null, $page=null, $order=null, $order_method = 'ASC')
  {
    //retrieve all tenants
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('account', 'account.account_id = v_withdrawals_transactions.account_id','LEFT');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }




	public function get_cancel_actions()
	{
		$this->db->where('cancel_action_status', 1);
		$this->db->order_by('cancel_action_name');

		return $this->db->get('cancel_action');
	}

  public function cancel_mpesa_payment($mpesa_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d"),
			"mpesa_status" => 1
		);

		$this->db->where('mpesa_id', $mpesa_id);
		if($this->db->update('mpesa_transactions', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}



  	public function payment_item_add($lease_id,$paying_account)
  	{
  		$amount = $this->input->post('amount');
  		$invoice_type_id=$this->input->post('invoice_type_id');
  		// $invoice_id=$this->input->post('invoice_id');
  		$payment_month=$this->input->post('payment_month');
  		$payment_year=$this->input->post('payment_year');
  		$lease_number=$this->input->post('lease_number');
  		$rental_unit_id=$this->input->post('rental_unit_id');
  		$tenant_id=$this->input->post('tenant_id');
  		// insert for service charge

  		// $this->db->where('lease_invoice_id',$invoice_id);
  		// $query = $this->db->get('lease_invoice');

  		// $row = $query->row();

  		// $invoice_date = $row->invoice_date;


  		$this->db->where('invoice_type_id',$invoice_type_id);
  		$query_type = $this->db->get('invoice_type');
  		$row_two = $query_type->row();
  		$invoice_type_name = $row_two->invoice_type_name;
  		$concate = $payment_year.'-'.$payment_month;
  		$date = date('M Y',strtotime($concate));

  		$remarks = 'Payment for '.$date.' '.$invoice_type_name;

  		$service = array(
  							'payment_id'=>0,
  							'amount_paid'=> $amount,
  							'payment_month'=> $payment_month,
  							'rental_unit_id'=> $rental_unit_id,
  							'tenant_id'=> $tenant_id,
  							'lease_number'=> $lease_number,
  							'payment_year'=> $payment_year,
  							'invoice_type_id' => $invoice_type_id,
  							'payment_item_status' => 0,
  							'lease_id' => $lease_id,
  							'mpesa_id' => $this->input->post('mpesa_id'),
  							'personnel_id' => $this->session->userdata('personnel_id'),
  							'payment_item_created' => date('Y-m-d'),
  							'remarks'=>$remarks
  						);

  		// var_dump($service); die();
      if($invoice_type_id != 2)
      {
        $this->db->insert('payment_item',$service);
      }
      else
      {
        $this->db->insert('water_payment_item',$service);
      }
  		
  		return TRUE;

  	}


  	public function confirm_payment($lease_id,$paying_account,$personnel_id = NULL)
  	{
  		$amount = $this->input->post('total_amount');
  		$payment_method=$this->input->post('payment_method');
  		$type_of_account=$this->input->post('type_of_account');
  		$rental_unit_id = $this->input->post('rental_unit_id');
      $lease_number=$this->input->post('lease_number');
      $tenant_id=$this->input->post('tenant_id');
  		if($payment_method == 2 || $payment_method == 3 || $payment_method == 5)
  		{
  			$transaction_code = $this->input->post('reference_number');
  			$bank_id = $this->input->post('bank_id');
  		}
  		else
  		{
  			$transaction_code = '';
  			$bank_id = 1;
  		}
      $total_visits = $_POST['water_income'];
      // var_dump($total_visits[0]);die();
  		// calculate the points to get
  		$payment_date = $this->input->post('payment_date');


      $transaction_date = $this->input->post('transaction_date');

  		$date_check = explode('-', $payment_date);
  		$month = $date_check[1];
  		$year = $date_check[0];


  		$receipt_number = $this->accounts_model->create_receipt_number();

  		$data = array(
  			'payment_method_id'=>$payment_method,
  			'bank_id'=>$bank_id,
  			'personnel_id'=>$this->session->userdata("personnel_id"),
  			'transaction_code'=>$transaction_code,
  			'payment_date'=>$this->input->post('payment_date'),
  			'receipt_number'=>$transaction_code,
  			'document_number'=>$receipt_number,
  			'paid_by'=>$this->input->post('paid_by'),
  			'payment_created'=>date("Y-m-d"),
  			'rental_unit_id'=>$rental_unit_id,
  			'year'=>$year,
  			'month'=>$month,
        'account_id'=>$lease_number,
        'tenant_id'=>$tenant_id,
  			'confirm_number'=> $receipt_number,
        'transaction_date'=> $transaction_date,
  			'payment_created_by'=>$this->session->userdata("personnel_id"),
  			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
  		);

      if(!empty($_POST['rental_income']))
      {
        $data['amount_paid'] = $this->input->post('rental_total');
        $parent = 'payments';
        $child = 'payment_item';

        $data['lease_id'] = $lease_id;

        if($this->db->insert($parent, $data))
        {
          $payment_id = $this->db->insert_id();
          

          $total_visits = sizeof($_POST['rental_income']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['rental_income'];
              $payment_item_id = $visit[$r];
              //check if card is held
              $service = array(
                                'payment_id'=>$payment_id,
                                'payment_item_created' =>$this->input->post('payment_date'),
                                'payment_item_status'=>1,
                                'rental_unit_id'=>$rental_unit_id,
                                'lease_number'=>$lease_number,
                                'tenant_id'=>$tenant_id,
                                'lease_id'=>$lease_id,
                                'payment_year'=>$year,
                                'payment_month'=>$month,
                                'document_no'=>$receipt_number
                              );
              $this->db->where('payment_item_id',$payment_item_id);
              $this->db->update('payment_item',$service);
            }
          }
        


        }
        else{

        }
      }
      else if(!empty($_POST['water_income']))
      {
        $data['amount_paid'] = $this->input->post('water_total');
        $parent = 'water_payments';
        $child = 'water_payment_item';
        $data['lease_id'] = $lease_id;

        // var_dump($data);die();

        if($this->db->insert($parent, $data))
        {
          $payment_id = $this->db->insert_id();
          

          $total_visits = sizeof($_POST['water_income']);
          //check if any checkboxes have been ticked
          if($total_visits > 0)
          {
            for($r = 0; $r < $total_visits; $r++)
            {
              $visit = $_POST['water_income'];
              $payment_item_id = $visit[$r];
              //check if card is held
              $service = array(
                                'payment_id'=>$payment_id,
                                'payment_item_created' =>$this->input->post('payment_date'),
                                'payment_item_status'=>1,
                                'rental_unit_id'=>$rental_unit_id,
                                'lease_number'=>$lease_number,
                                'tenant_id'=>$tenant_id,
                                'lease_id'=>$lease_id,
                                'payment_year'=>$year,
                                'payment_month'=>$month,
                                'document_no'=>$receipt_number
                              );
              $this->db->where('payment_item_id',$payment_item_id);
              $this->db->update('water_payment_item',$service);
            }
          }
        


        }
        else{

        }


        
      }
      return TRUE;
  	
  	}


    public function confirm_withdrawal($mpesa_id)
    {
      $amount = $this->input->post('total_amount');
      $account_from_id=$this->input->post('account_from_id');
      $transaction_date=$this->input->post('transaction_date');
      $account_to_id = $this->input->post('account_to_id');
      $payment_date=$this->input->post('payment_date');
      $reference_number=$this->input->post('reference_number');
     

      // $receipt_number = $this->accounts_model->create_receipt_number();

      $data = array(
                    'transacted_amount'=>$amount,
                    'account_from_id'=>$account_from_id,
                    'account_to_id'=>$account_to_id,
                    'transaction_date'=>$transaction_date,
                    'payment_date'=>$payment_date,
                    'transaction_number'=>$reference_number,
                    'mpesa_id'=>$mpesa_id,
                    'created_by'=>$this->session->userdata('personnel_id')
                  );

      

        if($this->db->insert('mpesa_transfers', $data))
        {
          $payment_id = $this->db->insert_id();
          $service = array(
                    'mpesa_status'=>1,
                    'lease_id'=>$payment_id,
                   
                  );
          $this->db->where('mpesa_id',$mpesa_id);
          $this->db->update('mpesa_transactions',$service);


          return TRUE;
        }
        else{
          return FALSE;
        }
      
    }
    public function get_active_lease_id_from_tenant_number($tenant_number)
    {
      $this->db->from('leases,tenant_unit,tenants');
      $this->db->select('*');
      $this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND leases.lease_number = "'.$tenant_number.'"');

      $query = $this->db->get();

      $result = $query->result();
      $lease_id = 0;

      if($query->num_rows() == 1 )
      {
        $result = $query->result();
        $lease_id = $result[0]->lease_id;
      }

      return $lease_id;
    }

    public function get_all_payments($mpesa_id=0)
    {
      $add = '';
      if($mpesa_id > 0)
        $add = ' AND data.mpesa_id ='.$mpesa_id;

      $select = "
                SELECT
                    SUM(data.amount_paid) AS amount_paid,
                    data.mpesa_id AS mpesa_id
                FROM
                (

                  SELECT 
                    payment_item.payment_item_id AS payment_item_id, 
                    payment_item.payment_id AS payment_id, 
                    payment_item.amount_paid AS amount_paid, 
                      payment_item.invoice_type_id AS invoice_type_id,
                      payments.bank_id AS bank_id,
                      payment_item.mpesa_id AS mpesa_id,
                      payments.transaction_code AS transaction_code,
                      payments.transaction_date AS transaction_date,
                      payments.payment_date AS payment_date,
                      'Rental Income' AS payment_type,
                      'rental_income' AS checked_type,
                      payments.year AS year,
                      payments.month AS month,
                      payments.lease_id AS lease_id
                  FROM payments,payment_item
                  WHERE 
                    payments.payment_id = payment_item.payment_id 
                    AND payments.cancel = 0 

                  UNION ALL

                  SELECT 
                    water_payment_item.payment_item_id AS payment_item_id, 
                    water_payment_item.payment_id AS payment_id, 
                    water_payment_item.amount_paid AS amount_paid, 
                      water_payment_item.invoice_type_id AS invoice_type_id,
                      water_payments.bank_id AS bank_id,
                      water_payment_item.mpesa_id AS mpesa_id,
                      water_payments.transaction_code AS transaction_code,
                      water_payments.transaction_date AS transaction_date,
                      water_payments.payment_date AS payment_date,
                      'Water Income' AS payment_type,
                      'water_income' AS checked_type,
                      water_payments.year AS year,
                      water_payments.month AS month,
                      water_payments.lease_id AS lease_id
                  FROM water_payments,water_payment_item
                  WHERE 
                    water_payments.payment_id = water_payment_item.payment_id 
                    AND water_payments.cancel = 0
                  ) AS data WHERE data.payment_item_id > 0 $add GROUP BY data.mpesa_id";

        $query = $this->db->query($select);
        return $query;
    }

    public function update_mpesa_recon_status($mpesa_id,$amount_paid,$payment_id=0)
    {
       // get amount paid 
        $amount_rs = $this->get_all_payments($mpesa_id);
        $amount_reconcilled = 0;
        if($amount_rs->num_rows() > 0)
        {

          foreach ($amount_rs->result() as $key => $value) {
            // code...
            $amount_reconcilled = $value->amount_paid;
          }
        }

        if(empty($amount_reconcilled))
          $amount_reconcilled = 0;

        if($amount_paid == $amount_reconcilled)
          $recon_status = 1;
        else if($amount_paid > $amount_reconcilled)
          $recon_status = 0;
        else if($amount_paid < $amount_reconcilled)
          $recon_status = 0;


        $array_update['recon_amount'] = $amount_reconcilled;
        $array_update['recon_status'] = $recon_status;
        $array_update['local_id'] = $payment_id;

        $this->db->where('mpesa_id',$mpesa_id);
        if($this->db->update('mpesa_transactions',$array_update))
          return TRUE;
        else
          return FALSE;
    }

}
?>
