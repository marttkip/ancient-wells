<?php

class Dashboard_model extends CI_Model
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	public function count_items_group($table, $where, $select = NULL ,$cpm_group = NULL,$limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		if($select != NULL)
		{
			$this->db->select($select);
		}
		if($cpm_group != NULL)
		{
			$this->db->group_by($cpm_group);
		}

		$this->db->where($where);
		if($select != NULL)
		{
			$query = $this->db->get($table);

			$row = $query->result();

			//var_dump($row); die();


			if(isset($row[0]))
			{
				$number= $row[0]->number;
			}
			else
			{
				$number = 0;
			}


			if(empty($number))
			{
				$number = 0;
			}



			return $number;
		}
		else
		{
			$this->db->from($table);
			return $this->db->count_all_results();
		}
	}


	public function get_content($table, $where,$select,$group_by=NULL,$limit=NULL)
	{
		$this->db->from($table);
		$this->db->select($select);
		$this->db->where($where);
		if($group_by != NULL)
		{
			$this->db->group_by($group_by);
		}
		$query = $this->db->get('');

		return $query;
	}

	public function getAssignedBilling($year,$month)
	{
		$branch_session = $this->session->userdata('branch_id');
		$where ='patients.patient_delete = 0 AND  YEAR(patients.patient_date) = '.$year.'  AND MONTH(patients.patient_date) = "'.$month.'" AND visit_invoice.visit_id = visit.visit_id AND visit_invoice.visit_invoice_delete = 0 AND visit.patient_id = patients.patient_id AND visit.branch_id ='.$branch_session;
		$table = 'patients,visit_invoice,visit';
		$select = 'SUM(visit_invoice.invoice_bill) AS number,patients.about_us';
		$cpm_group = 'patients.about_us';


		$this->db->select($select);
		$this->db->where($where);
		$this->db->group_by($cpm_group);
		$query = $this->db->get($table);

		return $query;


	}
}
?>
