<!-- start: page -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="row">
 <?php
$received_where = 'property_id > 0 ';
$received_table = 'property';

$total_properties = $this->dashboard_model->count_items($received_table, $received_where);



$received_where = 'rental_unit_status = 1';
$received_table = 'rental_unit';

$total_rental_units = $this->dashboard_model->count_items($received_table, $received_where);


$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1';
$received_table = 'rental_unit,tenant_unit,leases';

$occupied_rental_units = $this->dashboard_model->count_items($received_table, $received_where);



$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND tenant_unit.tenant_unit_id NOT IN (SELECT leases.tenant_unit_id FROM leases WHERE leases.lease_status = 1)';
$received_table = 'rental_unit,tenant_unit';

$unoccupied_rental_units = $this->dashboard_model->count_items($received_table, $received_where);


$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = home_owner_unit.rental_unit_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.home_owner_unit_status = 1';
$received_table = 'rental_unit,home_owner_unit,home_owners';

$occupied_owners_rental_units = $this->dashboard_model->count_items($received_table, $received_where);

$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id NOT IN (SELECT rental_unit_id FROM home_owner_unit WHERE home_owner_unit.home_owner_unit_status = 1)';
$received_table = 'rental_unit,home_owner_unit';

$unoccupied_owners_rental_units = $this->dashboard_model->count_items($received_table, $received_where);


?>
    <div class="col-lg-4 ">
        <section class="panel">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="widget-summary">
                        
                        <div class="widget-summary-col">
                            <div class="summary center-align">
                                <h4 class="title"><strong>PROPERTIES</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $total_properties;?></strong><br>
                                    <span class="text-primary">(<?php echo $total_rental_units;?> Rental units)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="widget-summary">
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><strong>TENANT OCCUPANCY</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $occupied_rental_units;?></strong>
                                    <span class="text-primary">(<?php echo $unoccupied_rental_units?> Unccupied)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="widget-summary">
                        
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><strong>HOME OWNER OCCUPANCY</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $occupied_owners_rental_units;?></strong>
                                    <span class="text-primary">(<?php echo 0;?> Unoccupied)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php
$received_where = 'tenant_status = 1 ';
$received_table = 'tenants';

$total_tenants = $this->dashboard_model->count_items($received_table, $received_where);

$received_where = 'home_owner_status = 1 ';
$received_table = 'home_owners';

$total_home_owners = $this->dashboard_model->count_items($received_table, $received_where);

$total_contacts = $total_tenants + $total_home_owners;



$received_where = 'tenant_status = 1 AND (tenants.tenant_phone_number = "" OR tenants.tenant_email = "" )';
$received_table = 'tenants';

$incomplete_tenants = $this->dashboard_model->count_items($received_table, $received_where);


$received_where = 'home_owner_status = 1 AND (home_owners.home_owner_phone_number = "" OR home_owners.home_owner_email = "" )';
$received_table = 'home_owners';

$incomplete_owners = $this->dashboard_model->count_items($received_table, $received_where);
?>
    
    <div class="col-lg-4 ">
        <section class="panel">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="widget-summary">
                        
                        <div class="widget-summary-col">
                            <div class="summary center-align">
                                <h4 class="title"><strong>CONTACTS</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $total_contacts;?></strong><br>
                                    <span class="text-primary">(<?php echo $total_tenants;?> Tenants VS <?php echo $total_home_owners;?> Home Owners)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="widget-summary">
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><strong>TENANTS</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $total_tenants;?></strong>
                                    <span class="text-primary">(<?php echo $incomplete_tenants?> Incomplete)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="widget-summary">
                        
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><strong>HOME OWNERS</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $total_home_owners;?></strong>
                                    <span class="text-primary">(<?php echo $incomplete_owners;?> Incomplete)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
    $received_where = 'email_sent = 0 ';
    $received_table = 'email';

    $total_emails = $this->dashboard_model->count_items($received_table, $received_where);

    $received_where = 'sms_sent = 0 ';
    $received_table = 'sms';

    $total_sms = $this->dashboard_model->count_items($received_table, $received_where);

    $total_notifications = $total_sms + $total_emails;

    $received_where = 'email_sent = 1 ';
    $received_table = 'email';

    $unsent_emails = $this->dashboard_model->count_items($received_table, $received_where);

    $received_where = 'sms_sent = 1';
    $received_table = 'sms';

    $unsent_sms = $this->dashboard_model->count_items($received_table, $received_where);


    ?>
    <div class="col-lg-4 ">
        <section class="panel">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="widget-summary">
                        
                        <div class="widget-summary-col">
                            <div class="summary center-align">
                                <h4 class="title"><strong>NOTIFICATIONS</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $total_notifications;?></strong><br>
                                    <span class="text-primary">(<?php echo $total_emails?> Emails VS <?php echo $total_sms?> SMS)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="widget-summary">
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><strong>EMAILS</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $total_emails?></strong>
                                    <span class="text-primary">(<?php echo 0;?> Unsent)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="widget-summary">
                        
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title"><strong>SMS</strong> </h4>
                                <div class="info">
                                    <strong class="amount"><?php echo $total_sms;?></strong>
                                    <span class="text-primary">(<?php echo 0;?>  Unsent)</span>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
</div>

<?php


$year = $this->reports_model->generate_financial_year();

if(!empty($this->session->userdata('financial_year_search')))
{
    $current_year = $this->session->userdata('financial_year_search');
    $financial_year =  $current_year;

    $next_financial_year = $current_year + 1;
    $previous_financial_year = $current_year - 1;
}
else
{
    $financial_year =  date('Y');

    $next_financial_year = date('Y') + 1;
    $previous_financial_year = date('Y') - 1;
}

$months = '';
$count = $invoice_count= 0;
$total_months =count($year);
// var_dump($total_months); die();
$totals_array = array();

    $categories = '';
for($r=0;$r<$total_months;$r++)
{
    $month_id = $year[$r];
    $totals_array[$month_id]  =0;

    if($month_id === 1 OR $month_id === 2 OR $month_id === 3)
    {
        $financial_year = $next_financial_year;
    }

    $month_name  = $this->reports_model->get_month_name($month_id);

    $total_amount = $this->reports_model->get_creditor_service_amounts(NULL,$month_id,$financial_year);


    if(empty($total_amount))
    {
        $total_amount =0;
    }


    $total_tenant_inflow_amt = $this->reports_model->get_amount_collected_invoice_type(NULL,$month_id,$financial_year);

    $total_owner_inflow_amt = $this->reports_model->get_amount_owners_collected_invoice_type(NULL,$month_id,$financial_year);

    $total_inflow_amt = $total_tenant_inflow_amt + $total_owner_inflow_amt;


    $categories .='["'.$financial_year.' '.$month_name.'",'.$total_inflow_amt.', '.$total_amount.'],';
}

?>
<div class="row">
    <div class="col-lg-8">
        <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Financial Year', 'Income', 'Expenses'],
              <?php echo $categories;?>
            ]);

            var options = {
              title: 'INCOME VS EXPENSES',
              curveType: 'function',
              legend: { position: 'bottom' }
            };

            var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

            chart.draw(data, options);
          }
        </script>
        <div id="curve_chart" style="height: 300px"></div>
    </div>
    <?php

    $properties = '';

    $dashboard_where = 'property_status = 1 ';
    $dashboard_table = 'property';
    $dashboard_select = '*';
    $item_select  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select);
    $collection ='';
    if($item_select->num_rows() > 0)
    {
        foreach ($item_select->result() as $key ) {
            # code...
            $property_id = $key->property_id;
            $property_name = $key->property_name;

            $cpm_where = 'cancel = 0 AND month ="06" AND year ="'.date('Y').'" AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND property.property_id ='.$property_id.' ';
            $cpm_table = 'payments,leases,rental_unit,property';
            $cpm_select = 'SUM(amount_paid) AS number';
            $cpm_group = NULL;
            $total_cpms =  $this->dashboard_model->count_items_group($cpm_table, $cpm_where,$cpm_select,$cpm_group);

             $cpm_where = 'cancel = 0 AND month ="06" AND year ="'.date('Y').'" AND home_owners_payments.rental_unit_id  = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND property.property_id ='.$property_id.' ';
            $cpm_table = 'home_owners_payments,leases,rental_unit,property';
            $cpm_select = 'SUM(amount_paid) AS number';
            $cpm_group = NULL;
            $total_owners_cpms =  $this->dashboard_model->count_items_group($cpm_table, $cpm_where,$cpm_select,$cpm_group);
            $total_payments = $total_owners_cpms + $total_cpms;
            $collection .='["'.$property_name.'", '.$total_payments.'],';


        }

    }

    //var_dump($collection); die();

    ?>
    <div class="col-lg-4 ">
         <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {

            var data = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              <?php echo $collection;?>
            ]);

            var options = {
              title: 'PROPERTY INCOME MAY 2017',
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
          }
        </script>
        <div id="piechart" style="height: 300px;"></div>
    </div>
</div>
<?php

    $properties = '';

    $dashboard_where = 'property_status = 1 ';
    $dashboard_table = 'property';
    $dashboard_select = '*';
    $item_select  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select);
    $collection ='';
    if($item_select->num_rows() > 0)
    {
        foreach ($item_select->result() as $key ) {
            # code...
            $property_id = $key->property_id;
            $property_name = $key->property_name;

            $cpm_where = 'cancel = 0 AND month ="06" AND year ="'.date('Y').'" AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND property.property_id ='.$property_id.' ';
            $cpm_table = 'payments,leases,rental_unit,property';
            $cpm_select = 'SUM(amount_paid) AS number';
            $cpm_group = NULL;
            $total_cpms =  $this->dashboard_model->count_items_group($cpm_table, $cpm_where,$cpm_select,$cpm_group);



            $collection .='["'.$property_name.'", '.$total_cpms.'],';


        }

    }

    //var_dump($collection); die();

    ?>




<!-- start: page -->
<div class="row">
    <div class="col-md-6 col-lg-12 col-xl-6">
        <section class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <script type="text/javascript">
                          google.charts.load('current', {'packages':['bar']});
                          google.charts.setOnLoadCallback(drawChart);

                          function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                              ['Year', 'Sales', 'Expenses', 'Profit'],
                              ['2014', 1000, 400, 200],
                              ['2015', 1170, 460, 250],
                              ['2016', 660, 1120, 300],
                              ['2017', 1030, 540, 350]
                            ]);

                            var options = {
                              chart: {
                                title: 'Company Performance',
                                subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                              }
                            };

                            var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                            chart.draw(data, google.charts.Bar.convertOptions(options));
                          }
                        </script>
                          <div id="columnchart_material" style="height: 400px;"></div>

                    </div>
                    
                </div>
            </div>
        </section>
    </div>
    
</div>      
<!-- end: page -->
 <?php //echo $this->load->view('administration/line_graph');?>
 <?php //echo $this->load->view('administration/bar_graph');?>
                            

<!--<script type="text/javascript" src="<?php echo base_url().'assets/themes/bluish/js/reports.js';?>"></script>-->
<?php
$contacts = $this->site_model->get_contacts();
 $logo = $contacts['logo'];
?>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i>Profile Details</h5>
          <div class="widget-icons pull-right">

          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="col-lg-3">
                        <div class="form-group">
                                 <div class="col-lg-12">
                                        <img src="<?php echo base_url();?>assets/logo/<?php echo $logo;?>">
                                 </div>
                        </div>
                </div>
                <div class="col-lg-3">
                    <h5><strong>Profile Details</strong></h5>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><strong> Personnel Name:</strong> </label>
                        
                        <div class="col-lg-8">
                            <?php echo $this->session->userdata('first_name');?>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> <strong>Username:</strong> </label>
                        
                        <div class="col-lg-8">
                            <?php echo $this->session->userdata('username');?>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><strong>Branch Name: </strong></label>
                        
                        <div class="col-lg-8">
                            <?php echo $this->session->userdata('branch_name');?>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label"><strong> Branch Code:</strong> </label>
                        
                        <div class="col-lg-8">
                            <?php echo $this->session->userdata('branch_code');?>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <h5><strong>Change Password</strong></h5>
                    <form enctype="multipart/form-data" action="<?php echo base_url();?>change-password"  id = "change_password" method="post">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="slideshow_name"> <strong>Current Password </strong></label>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" name="current_password" placeholder="Current Password" >
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="slideshow_name"><strong>New Password</strong></label>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" name="new_password" placeholder="New Password" >
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="slideshow_name"><strong>Confirm New Password</strong></label>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" name="confirm_new_password" placeholder="Confirm new password" >
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                        <div class="form-group center-align">
                                            <button type="submit" class="btn btn-sm btn-success" name="submit" >Change Password</button>
                                        </div>
                                    
                                </div>
                                    
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
    