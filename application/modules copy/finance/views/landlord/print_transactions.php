<?php

 $result = '';
 $total_income = 0;
  if($query_income->num_rows() > 0)
  {
    $count = 0;
    foreach ($query_income->result() as $key => $value) {
      // code...
      $transaction_number = $value->transaction_number;
      $created = $value->payment_date;
      $property_name = $value->property_name;
      $transacted_amount = $value->transacted_amount;
      $invoice_type_name = $value->invoice_type_name;
      $income_id = $value->income_id;

      $created = date('jS M Y',strtotime($created));
     

      $balance = $transacted_amount;
      $total_income += $transacted_amount;
      $count++;
      $result .='
                <tr>
                  <td width="10%">'.$count.'</td>
                  <td width="20%">'.$created.'</td>
                  <td width="50%">'.$property_name.' - '.$invoice_type_name.'</td>
                  <td width="20%">'.number_format($transacted_amount,2).'</td>
              ';
    }
    $result .='
		                <tr>
		                  <th colspan="3">TOTAL INCOME</th>
		                  <th>'.number_format($total_income,2).' </th>

		                </tr>
		                ';
  }


  $result_expense = '';
  $total_expense = 0;
  if($query_expenses->num_rows() > 0)
  {
  	$count = 0;
    foreach ($query_expenses->result() as $key => $value) {
      // code...
     

		$finance_purchase_id = $value->finance_purchase_id;
		$finance_purchase_amount = $value->finance_purchase_amount;
		$transaction_number = $value->transaction_number;
		$transaction_date = $value->transaction_date;
		$finance_purchase_description = $value->finance_purchase_description;
      	$transaction_date = date('jS M Y',strtotime($transaction_date));
      	$total_expense += $finance_purchase_amount;
      $count++;
      $result_expense .='
		                <tr>
		                  <td width="10%">'.$count.'</td>
		                  <td width="20%">'.$transaction_date.'</td>
		                  <td width="50%"> '.$finance_purchase_description.'</td>
		                  <td width="20%">'.number_format($finance_purchase_amount,2).' </td>

		                </tr>
		                ';
    }

    $result_expense .='
		                <tr>
		                  <th colspan="3">TOTAL EXPENSES</th>
		                  <th>'.number_format($total_expense,2).' </th>

		                </tr>
		                ';


  }

  $result_payments = '';
  $total_payments = 0;
  if($query_payments->num_rows() > 0)
  {
    $count = 0;
    foreach ($query_payments->result() as $key => $value) {
      // code...
   		$transaction_date = $value->payment_date;
   		$amount_paid = $value->amount_paid;
   		$property_beneficiary_name = $value->property_beneficiary_name;
   		$receipt_number = $value->receipt_number;

   		$transaction_date = date('jS M Y',strtotime($transaction_date));
      $total_payments += $amount_paid;
      // $balance = $landlord_transaction_amount;
      $count++;
      $result_payments .='
                <tr>
                  <td width="10%">'.$count.'</td>
                  <td width="20%">'.$transaction_date.'</td>
                  <td width="50%">'.$receipt_number.' - '.$property_beneficiary_name.'</td>
                  <td  width="20%">'.number_format($amount_paid,2).'</td>';
    }
     $result_payments .='
		                <tr>
		                  <th colspan="3">TOTAL PAYMENTS</th>
		                  <th>'.number_format($total_payments,2).' </th>

		                </tr>
		                ';


  }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Petty cash</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
			@media print {
			    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
			}
			#header-table
			{
				background-color:orange;
				color:white;
			}
			#header-table2
			{
				background-color:#1a4567;
				color:white;
			}

			#header-table3
			{
				background-color:orange;
				color:white;
			}

			@media print {
			    #header-table {
			        /*background-color: #1a4567 !important;*/
			        background-color:orange;
					color:white;
			        -webkit-print-color-adjust: exact; 
			    }
			}

			@media print {
			    #header-table {
			        color: white !important;
			    }
			}

			@media print {
			    th#header-table {
			        background-color: orange !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}

			@media print {
			    th#header-table2 {
			        background-color: #1a4567 !important;
			         color: white !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}
			@media print {
			    th#header-table3{
			        background-color: orange !important;
			         color: white !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			thead {display: table-header-group;}
			#item-dev{display: table-header-group;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<!-- <div class="row"> -->
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        <!-- </div> -->
    	<!-- <div class="row"> -->
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        <!-- </div> -->
        <?php
        // $search_title = $this->session->userdata('accounts_search_title');
        ?>
      <!-- <div class="row " > -->
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong><?php echo $title?></strong>
            </div>
        <!-- </div> -->

    	<!-- <div class="row"> -->
        	<div class="col-md-12">
	        	<h4><strong>INCOME</strong></h4>
	        	<table class="table table-hover table-bordered table-condensed ">
				 	<thead>
						<tr>
		        		  <th width="10%">#</th>
						  		<th width="20%">Date</th>
		       			  <th width="50%">Property - Invoice Type</th>
						  		<th width="20%">Amount Generated</th>
						</tr>
					 </thead>
				  	<tbody>
		        		<?php echo $result;?>
				  	</tbody>
				</table>
				<h4><strong>EXPENSES</strong></h4>

				<table class="table table-hover table-bordered table-condensed ">
				 	<thead>
						<tr>
		        		<th width="10%">#</th>
								<th width="20%">Date</th>
								<th width="50%">Description</th>
								<th width="20%">Amount</th>
						</tr>
					 </thead>
				  	<tbody>
		        		<?php echo $result_expense;?>
				  	</tbody>
				</table>
				<h4><strong>PAYMENTS</strong></h4>

				<table class="table table-hover table-bordered table-condensed ">
				 	<thead>
						<tr>
		        		<th width="10%">#</th>
								<th width="20%">Date</th>
								<th width="50%">Ref Number (Paid To)</th>
								<th width="20%">Amount</th>
						</tr>
					 </thead>
				  	<tbody>
		        		<?php echo $result_payments;?>
				  	</tbody>
				</table>

				<hr>
				<table class="table table-hover table-bordered table-condensed ">
				 	
				  	<tbody>
		        		<tr>
			        		<th colspan="3">GRAND TOTAL</th>
									<th width="20%"><?php echo number_format($total_income - $total_expense - $total_payments,2)?></th>
							</tr>
				  	</tbody>
				</table>
            </div>
        <!-- </div> -->

    	
    </body>

</html>
