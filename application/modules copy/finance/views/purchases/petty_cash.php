<?php //echo $this->load->view('search/search_petty_cash', '', TRUE);
    $property_owner_id =0;
    if($transaction_id > 0)
    {
      $details = $this->transfer_model->get_transaction_detail($transaction_id);

      if($details->num_rows() > 0)
      {
        foreach ($details->result() as $key => $value) {
          # code...
          $property_owner_id = $value->property_owner_id;
        }
      }
      $properties = $this->property_owners_model->get_all_front_end_property_owners($property_owner_id);
      // var_dump($properties);die();
    }
    else
    {
      $properties = $this->property_owners_model->get_all_front_end_property_owners();
    }
    
    $rs8 = $properties->result();
    $property_list = '';
    foreach ($rs8 as $property_rs) :
        $property_owner_id = $property_rs->property_owner_id;
        $property_owner_name = $property_rs->property_owner_name;

        $property_list .="<option value='".$property_owner_id."'>".$property_owner_name."</option>";

    endforeach;

    // if($property_owner_id > 0)
    // {
    //   $property_owners = $this->property_model->get_active_property_owners_with_beneficiaries($property_owner_id);
    // }
    // else
    // {
      $property_owners = $this->property_model->get_active_property_owners_with_beneficiaries();
    // }


$rs8 = $property_owners->result();

// var_dump($property_owners);die();
$property_beneficiaries_list = '';
$owner_id = 0;
foreach ($property_owners->result() as $property_rs =>$value) :


    $property_owner_id = $value->property_owner_id;
    $property_beneficiary_name = $value->property_beneficiary_name;
    $property_beneficiary_id = $value->property_beneficiary_id;
    $property_owner_name = $value->property_owner_name;

    if($property_owner_id != $owner_id)
    {
      $property_beneficiaries_list .= '<optgroup label="'.strtoupper($property_owner_name).'">';
    }

    // else
    // {

      $property_beneficiaries_list .= '<option value="'.$property_beneficiary_id.'#'.$property_owner_id.'">'.strtoupper($property_beneficiary_name).'</option>';
    // }

    


    if($property_owner_id != $owner_id AND $owner_id != 0)
    {
       $property_beneficiaries_list .= '</optgroup>';
    }
    $owner_id = $property_owner_id;


endforeach;

$document_receipt = $this->purchases_model->create_purchases_payment();





?>
<div class="row">
    <div class="col-md-12">
      <section class="panel panel-info">
          <header class="panel-heading">
              <h3 class="panel-title">Search Petty Cash</h3>
               <div class="panel-tools pull-right" style="margin-top: -25px;">
                <?php

                if($transaction_id > 0)
                {
                  ?>
                   <a href="<?php echo site_url().'transactions'?>"  class="btn btn-warning btn-sm" >Back to Transactions</a>
                  <?php
                }
                ?>
              </div>
          </header>
          <div class="panel-body">
               <div class="pull-right">
                 <!-- <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#record_petty_cash"><i class="fa fa-plus"></i> Record</button>
                 <a href="<?php echo base_url().'accounts/petty_cash/print_petty_cash/';?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo base_url().'administration/sync_app_petty_cash';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a> -->
               </div>
               <div class="row">
                 <div class="col-md-12">
             <?php echo form_open("finance/purchases/search_petty_cash", array("class" => "form-horizontal"));?>


                  <div class="col-md-3">
                    <div class="form-group"  >
                           <label class="col-lg-4 control-label">Property Owner</label>

                           <div class="col-lg-8">
                              <select  name='property_owner_id' class='form-control ' >
                                 <option value=''>None - Please Select a owner</option>
                                 <?php echo $property_list;?>
                               </select>
                           </div>
                       </div>
                  </div>
                  <div class="col-md-3">
                       <div class="form-group">
                           <label class="col-md-4 control-label">Date From: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </span>
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Transaction date" value="" id="datepicker" >
                               </div>
                           </div>
                       </div>




                 </div>
                  <input type="hidden" class="form-control" name="redirect_url" placeholder="Account" value="<?php echo $this->uri->uri_string()?>" required/>
                 <!-- <input type="hidden" name="transaction_id" value="<?php echo $transaction_id?>"> -->
                 <div class="col-md-3">

                         <div class="form-group">
                             <label class="col-md-4 control-label">Date To: </label>

                             <div class="col-md-8">
                                 <div class="input-group">
                                     <span class="input-group-addon">
                                         <i class="fa fa-calendar"></i>
                                     </span>
                                     <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Transaction date" value="" id="datepicker1" >
                                 </div>
                             </div>
                         </div>


                 </div>
                 <div class="col-md-2">
                   <div class="form-group">
                     <div class="text-center">
                         <button type="submit" class="btn btn-sm btn-info">Search record</button>
                     </div>
                   </div>
                 </div>

               <?php echo form_close();?>
               <div class="col-md-1">
                 <a href="<?php echo site_url().'print-petty-cash/'.$transaction_id?>" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print</a>
                 <!-- <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Export to excel Statement</button> -->

               </div>
             </div>


           </div>

            </div>
        </section>
    </div>
    <?php
    if($transaction_id > 0)
    {

    }
    else
    {


      ?>
      <div class="col-md-12">
        <section class="panel panel-info">
            <header class="panel-heading">
                <h3 class="panel-title">Petty Cash Account </h3>
                 <div class="panel-tools pull-right" style="margin-top: -25px;">
                  <?php

                  if($transaction_id > 0)
                  {
                    ?>
                     <a href="<?php echo site_url().'transactions'?>"  class="btn btn-warning btn-sm" >Back to Transactions</a>
                    <?php
                  }
                  ?>
                </div>
            </header>
            <div class="panel-body">
                 <div class="pull-right">
                   <!-- <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#record_petty_cash"><i class="fa fa-plus"></i> Record</button>
                   <a href="<?php echo base_url().'accounts/petty_cash/print_petty_cash/';?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-print"></i> Print</a>
                   <a href="<?php echo base_url().'administration/sync_app_petty_cash';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a> -->
                 </div>


               <?php echo form_open("finance/purchases/record_petty_cash", array("class" => "form-horizontal"));?>
                 <div class="row">
                   <div class="col-md-12">
                   <div class="col-md-6">
                     <input type="hidden" class="form-control" name="account_from_id" placeholder="Account" value="<?php echo $account_from_id?>" required/>
                     <input type="hidden" class="form-control" name="redirect_url" placeholder="Account" value="<?php echo $this->uri->uri_string()?>" required/>
                         <div class="form-group">
                             <label class="col-md-4 control-label">Transaction date: </label>

                             <div class="col-md-8">
                                 <div class="input-group">
                                     <span class="input-group-addon">
                                         <i class="fa fa-calendar"></i>
                                     </span>
                                     <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transaction_date" placeholder="Transaction date" value="<?php echo date('Y-m-d');?>" id="datepicker2" required>
                                 </div>
                             </div>
                         </div>
                         <input type="hidden" class="form-control" name="type" value="1" required/>
                         <div class="form-group">
                             <label class="col-md-4 control-label">Transaction No *</label>

                             <div class="col-md-8">
                                 <input type="text" class="form-control" name="transaction_number" value="<?php echo $document_receipt?>" placeholder="Transaction Number" required/>
                             </div>
                         </div>

                         <input type="hidden" name="transaction_id" value="<?php echo $transaction_id?>">
                         <div class="form-group" style="display: none;">
                             <label class="col-md-4 control-label">Department *</label>

                             <div class="col-md-8">
                                 <select class="form-control select2" name="department_id" id="department_id" >
                                     <option value="0">-- Select a department --</option>
                                     <?php
                                     if($departments->num_rows() > 0)
                                     {
                                         foreach($departments->result() as $res)
                                         {
                                             $department_id = $res->department_id;
                                             $department_name = $res->department_name;
                                             ?>
                                             <option value="<?php echo $department_id;?>"><?php echo $department_name;?></option>
                                             <?php
                                         }
                                     }
                                     ?>
                                 </select>
                             </div>
                         </div>

                         <div class="form-group" >
                             <label class="col-md-4 control-label">Expense Account *</label>

                             <div class="col-md-8">
                                 <select class="form-control select2" name="account_to_id" id="account_to_id" required>
                                   <option value="">--- select an expense account - ---</option>
                                   <?php
                                   if($expense_accounts->num_rows() > 0)
                                   {
                                     foreach ($expense_accounts->result() as $key => $value) {
                                       // code...
                                       $account_id = $value->account_id;
                                       $account_name = $value->account_name;
                                       echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
                                     }
                                   }
                                   ?>
                                 </select>
                             </div>
                         </div>


                   </div>
                   <div class="col-md-6">
                     <div class="form-group" style="display: none;">
                         <label class="col-md-4 control-label">Type *</label>

                         <div class="col-md-8">
                             <div class="radio">
                                 <label>
                                     <input  type="radio" checked value="0" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                     Pure Expense
                                 </label>
                                 <label>
                                     <input  type="radio"  value="1" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                     Expense Linked to a creditor
                                 </label>
                             </div>
                         </div>
                     </div>
                     <input type="hidden" name="transaction_type_id" value="0" >
                     <div class="form-group" style="display:none;" id="creditor_div">
                         <label class="col-md-4 control-label">Creditor*</label>

                         <div class="col-md-8">
                             <select class="form-control" name="creditor_id" id="creditor_id">
                               <option value="0">--- select a creditor - ---</option>
                               <?php
                               if($creditors->num_rows() > 0)
                               {
                                 foreach ($creditors->result() as $key => $value) {
                                   // code...
                                   $creditor_id = $value->creditor_id;
                                   $creditor_name = $value->creditor_name;
                                   echo '<option value="'.$creditor_id.'"> '.$creditor_name.'</option>';
                                 }
                               }
                               ?>
                             </select>
                         </div>
                     </div>

                     <div class="form-group">
                         <label class="col-lg-4 control-label">Transaction Allocation</label>

                         <div class="col-lg-8">

                          <select class="form-control selectpicker" name="transaction_id">
                            <option value="0">---- SELECT A TRANSACTION ---- </option>
                            <?php
                              if($transactions_list->num_rows() > 0)
                              {
                                foreach ($transactions_list->result() as $key => $value) {
                                  # code...
                                  $transaction_month = $value->transaction_month;
                                  $transaction_year = $value->transaction_year;
                                  $transaction_idd = $value->transaction_id;
                                  $property_owner_name = $value->property_owner_name;
                                  $date = '01-'.$transaction_month.'-'.$transaction_year;
                                  ?>

                                    <option value="<?php echo $transaction_idd?>"><?php echo $property_owner_name.' : '.date('M Y',strtotime($date))?></option>
                                  <?php
                                }
                              }
                            ?>
                          </select>

                            
                         </div>
                     </div>



                     <div class="form-group">
                         <label class="col-lg-4 control-label">Beneficairy</label>

                         <div class="col-lg-8">

                          <select class="form-control selectpicker" name="property_beneficiary_id">
                            <option value="0">---- SELECT A BENEFICIARY ---- </option>
                            <?php echo $property_beneficiaries_list;?>
                          </select>

                            
                         </div>
                     </div>

                     

                     <div class="form-group">
                         <label class="col-md-4 control-label">Amount *</label>

                         <div class="col-md-8">
                             <input type="text" class="form-control" name="transacted_amount" placeholder="Amount" required/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-md-4 control-label">Description *</label>

                         <div class="col-md-8">
                             <textarea class="form-control cleditor" name="description" required></textarea>
                         </div>
                     </div>
                     


                   </div>
                   </div>



                 </div>
                 <div class="row" style="margin-top:5px;">
                       <div class="col-md-12">
                           <div class="text-center">
                               <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to add this record ? ')">Save record</button>
                           </div>
                       </div>
                   </div>
                 <?php echo form_close();?>


              </div>

        </section>
      </div>
    <?php
    }
    ?>
</div>
<div class="row">
    <div class="col-md-12">


			<?php
      $search = $this->session->userdata('search_petty_cash');
      // var_dump($search);die();
			if(!empty($search))
			{
				?>
                <a href="<?php echo base_url().'finance/purchases/close_petty_cash_search';?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
                <?php
			}
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');

			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}

			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}

			$result =  '';

			// echo $result;
      $result = '';
      $count = 0;
      $balance = 0;
// get account opening balance
  $opening_balance_query = $this->purchases_model->get_account_opening_balance($account_from_name);
  // var_dump($opening_balance_query);die();
  $cr_amount = 0;
  $dr_amount = 0;

  if($transaction_id > 0)
  {

  }
  else
  {

  }
  if($opening_balance_query->num_rows() > 0)
  {
    $row = $opening_balance_query->row();
    $cr_amount = $row->cr_amount;
    $dr_amount = $row->dr_amount;
  
    if($transaction_id > 0)
    {
        $cr_amount = 0;
        $dr_amount = 0;
      $result .='
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Balance B/F</td>
                  <td >'.number_format(0,2).' </td>
                  <td >'.number_format(0,2).' </td>

                </tr>
                ';
    }
    else
    {
          $balance += $dr_amount;
        $balance -=  $cr_amount;

      $result .='
                <tr>
                 <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Balance B/F</td>
                  <td >'.number_format($dr_amount,2).' </td>
                  <td >'.number_format($cr_amount,2).' </td>
                  <td >'.number_format($balance,2).' </td>

                </tr>
                ';

    }
  }
  else {

    if($transaction_id > 0)
    {
      $result .='
              <tr>
                <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                <td>Balance B/F</td>
                <td>0.00</td>
                <td>0.00</td>
                <td>0.00 </td>

              </tr>
              ';
    }
    else
    {
      $result .='
              <tr>
                <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                <td>Balance B/F</td>
                <td>0.00</td>
                <td>0.00</td>
                <td>0.00 </td>

              </tr>
              ';
    }
  }



  if($query_purchases->num_rows() > 0)
  {
    foreach ($query_purchases->result() as $key => $value) {
      // code...
      $transactionClassification = $value->transactionClassification;

      $document_number = '';
      $transaction_number = '';
      $finance_purchase_description = '';
      $finance_purchase_amount = 0 ;


      $finance_purchase_payment_id = $value->transactionId;
      $referenceId = $value->payingFor;
     $document_number =$transaction_number = $value->referenceCode;
     $finance_purchase_description = $value->transactionName;
     $property_name = $value->property_name;
      $accountName = $value->accountName;
     $deleted = $value->deleted;
      if($transactionClassification == 'Purchase Payment')
      {
        $referenceId = $value->payingFor;

        // get purchase details
        $detail = $this->purchases_model->get_purchases_details($referenceId);
        $row = $detail->row();
        $document_number = $row->document_number;
        $transaction_number = $row->transaction_number;
        $finance_purchase_description = $row->finance_purchase_description;

        $account_name2 = $this->site_model->display_page_title();
        $account_name2 = strtolower($account_name2);
        $name = $this->site_model->create_web_name($account_name2);

        $checked = '  <td><a href="'.base_url().'print-vourcher/'.$finance_purchase_payment_id.'" target="_blank" class="btn btn-xs btn-warning"><i class="fa fa-print"></i> </a>
                       
                    </td>
                    <td>  <a onclick="edit_petty_cash('.$finance_purchase_payment_id.','.$location_id.')"   class="btn btn-xs btn-success fa fa-pencil"></a></td>

                  <td><a href="'.base_url().'delete-petty-cash/'.$finance_purchase_payment_id.'/'.$name.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this record ? \')" ><i class="fa fa-trash"></i></a></td>';

      }else
      {
        $checked = '';
      }
      $cr_amount = $value->cr_amount;
      $dr_amount = $value->dr_amount;


      $transaction_date = $value->transactionDate;
      $transaction_date = date('jS M Y',strtotime($transaction_date));
      $creditor_name = $value->creditor_name;
      $creditor_id = 0;//$value->creditor_id;
      $account_name = '';//$value->account_name;


    


      if($deleted == 0)
      {
        $delete_status = '<span class="label label-xs label-default"> Active</span>';
      }
      else if($deleted == 1)
      {
        $delete_status = '<span class="label label-xs label-warning">Pending Delete Approval</span>';
      }
      else if($deleted == 2)
      {
        $delete_status = '<span class="label label-xs label-danger">Deleted</span>';
      }

      $count++;
      // var_dump($transaction_id);die();
      if($transaction_id > 0)
      {
        $balance +=  $cr_amount;

        $result .='
                  <tr>
                    <td>'.$count.'</td>
                    <td>'.$transaction_date.'</td>
                    <td>'.$transaction_number.'</td>
                    <td>'.$accountName.'</td>
                    <td>'.$finance_purchase_description.'</td>
                    <td>'.number_format($cr_amount,2).'</td>
                    <td>'.number_format($balance,2).'</td>

                  </tr>
                  ';

      }
      else
      {

          $balance += $dr_amount;
        $balance -=  $cr_amount;
        $result .='
                <tr>
                  <td>'.$count.'</td>
                  <td>'.$transaction_date.'</td>
                  <td>'.$transaction_number.'</td>
                  <td>'.$finance_purchase_description.'</td>
                  <td>'.$property_name.'</td>
                  <td>'.number_format($dr_amount,2).' </td>
                  <td>'.number_format($cr_amount,2).'</td>
                  <td>'.number_format($balance,2).'</td>
                  <td>'.$delete_status.'</td>
                  '.$checked.'

                </tr>
                ';
      }
      
    }


  }


  if($transaction_id > 0)
  {
    $result .='
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td>Balance</td>
              <td colspan="3" class="center-align"><strong>KES '.number_format($balance,2).' </strong></td>

            </tr>
            ';
  }
  else
  {
    $result .='
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>Balance</td>
              <td colspan="3" class="center-align"><strong>KES '.number_format($balance,2).' </strong></td>

            </tr>
            ';
  }


?>
<section class="panel panel-info">
    <header class="panel-heading">
        <h3 class="panel-title">Petty Cash Transaction </h3>
         <div class="panel-tools pull-right" style="margin-top: -25px;">
            <?php

            if($transaction_id > 0)
            {
              ?>
               <a href="<?php echo site_url().'transactions'?>"  class="btn btn-warning btn-sm" >Back to Transactions</a>
              <?php
            }
            ?>
          </div>
    </header>
    <div class="panel-body">
      <?php
      $error = $this->session->userdata('error_message');
      $success = $this->session->userdata('success_message');

      if(!empty($error))
      {
        var_dump($error);die();
        echo '<div class="alert alert-warning">'.$error.'</div>';
        $this->session->unset_userdata('error_message');
      }

      if(!empty($success))
      {
        echo '<div class="alert alert-success">'.$success.'</div>';
        $this->session->unset_userdata('success_message');
      }
      ?>
      <div class="center-align"><?php echo $search_title;?></div>
      <table class="table table-condensed table-bordered ">
			 	
          <?php
          if($transaction_id > 0)
          {
            ?>
            <thead>
              <tr>
                <th>#</th>
                <th>Date</th>
                <th>Ref Number</th>
                <th>Account From</th>
                <th>Description</th>
                <th>Credit</th>
                <th>Balance</th>
                
              </tr>
            </thead>

            <?php

          }
          else
          {
            ?>
            <thead>
              <tr>
                <th>#</th>
                <th>Date</th>
                <th>Ref Number</th>
                <th>Description</th>
                <th>Property</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Bal</th>
                <th colspan="2">Action</th>
              </tr>
            </thead>

            <?php
          }

          ?>
        
			  	<tbody>
            <?php echo $result;?>
			  	</tbody>
			</table>

    </div>

</section>
</div>
</div>

 <script type="text/javascript">

        function get_transaction_type_list(type)
        {
            // var type = getRadioCheckedValue(radio_name);
            // $("#charge_to_id").customselect()="";
            // alert(radio_name);
            var myTarget1 = document.getElementById("creditor_div");

            if(type == 1)
            {


                myTarget1.style.display = 'block';
                $('#creditor_id').addClass('select2');
            }
            else {
              myTarget1.style.display = 'none';
            }
            var url = "<?php echo site_url();?>accounting/petty_cash/get_list_type_petty_cash/1";
            // alert(url);
            //get department services
            $.get( url, function( data )
            {
                $( "#charge_to_id" ).html( data );
                // $(".custom-select").customselect();
            });

        }

        function getRadioCheckedValue(radio_name)
        {
           var oRadio = document.forms[0].elements[radio_name];

           for(var i = 0; i < oRadio.length; i++)
           {
              if(oRadio[i].checked)
              {
                 return oRadio[i].value;
              }
           }

           return '';
        }

        function display_payment_model(modal_id)
      	{
      		$('#modal-defaults'+modal_id).modal('show');
      		$('#datepicker1'+modal_id).datepicker({
    	      autoclose: true,
    	      format: 'yyyy-mm-dd',
    	    })
      	}


        function check_payment_type(payment_type_id){

          var myTarget1 = document.getElementById("cheque_div");

          var myTarget2 = document.getElementById("mpesa_div");

          var myTarget3 = document.getElementById("insuarance_div");

          if(payment_type_id == 1)
          {
            // this is a check

            myTarget1.style.display = 'block';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 2)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 3)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'block';
          }
          else if(payment_type_id == 4)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 5)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'block';
            myTarget3.style.display = 'none';
          }
          else
          {
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'block';
          }

        }



        function edit_petty_cash(account_payment_id,location_id)
        {

            document.getElementById("sidebar-right").style.display = "block"; 
            // document.getElementById("existing-sidebar-div").style.display = "none"; 

            var config_url = $('#config_url').val();
            var data_url = config_url+"finance/purchases/edit_petty_cash/"+account_payment_id+"/"+location_id;
            //window.alert(data_url);
            $.ajax({
            type:'POST',
            url: data_url,
            data:{account_payment_id: account_payment_id},
            dataType: 'text',
            success:function(data){

                document.getElementById("current-sidebar-div").style.display = "block"; 
                $("#current-sidebar-div").html(data);
                $('.datepicker').datepicker({
                    format: 'yyyy-mm-dd'
                });


            },
            error: function(xhr, status, error) {
            //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            alert(error);
            }

            });
        }

        function close_side_bar()
        {
            // $('html').removeClass('sidebar-right-opened');
            document.getElementById("sidebar-right").style.display = "none"; 
            document.getElementById("current-sidebar-div").style.display = "none"; 
            document.getElementById("existing-sidebar-div").style.display = "none"; 
            tinymce.remove();
        }

        $(document).on("submit","form#edit-direct-payment",function(e)
        {
            // alert('dasdajksdhakjh');
            e.preventDefault();
            // myApp.showIndicator();
            
            var form_data = new FormData(this);

            // alert(form_data);

            var config_url = $('#config_url').val();    

             var url = config_url+"finance/purchases/edit_petty_cash_data";
            $.ajax({
            type:'POST',
            url: url,
            data:form_data,
            dataType: 'text',
           processData: false,
           contentType: false,
            success:function(data)
            {
              // alert(data);
              var data = jQuery.parseJSON(data);

                if(data.message == "success")
                {

                  var location_id = data.location_id;
                  
                  // var redirect_url = $('#redirect_url').val();

                 
                  if(location_id == 1)
                  {
                    window.location.href = config_url+"accounting/bob-allocation-petty-cash";
                  }
                  else
                  {
                    window.location.href = config_url+"accounting/tony-allocation-petty-cash";
                  }
                  
                
                }
                else
                {
                    alert('Please ensure you have added included all the items');
                }

            },
            error: function(xhr, status, error) {
            alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

            }
            });
             
            
        });

    </script>
