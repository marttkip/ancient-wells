<?php

$result = '';
$balance = 0;
$count = 0;
$total_dr_amount = 0;
$total_cr_amount = 0;
$total_invoice = 0;
$total_payments = 0;
$total_debits = 0;
$total_credits = 0;
if($query->num_rows() > 0)
{
  foreach ($query->result() as $key => $value) {
    // code...

    $transaction_date = $value->createdAt;
    $remarks = $value->transactionDescription;
    $transaction_category = $value->transactionCategory;
    $today = date('d M Y',strtotime($transaction_date));

    $dr_amount= $value->dr_amount;
    $cr_amount = $value->cr_amount;

    $balance += $dr_amount;
    $balance -= $cr_amount;
    $total_dr_amount += $dr_amount;
    $total_cr_amount += $cr_amount;
    if($transaction_category == 'Outgoing Invoice')
    {
      $total_invoice += $dr_amount;
    }


    if($transaction_category == 'Tenant Payments')
    {
      $total_payments += $cr_amount;
    }


    if($transaction_category == 'Credit Note')
    {
      $total_credits += $cr_amount;
    }

    if($transaction_category == 'Debit Note')
    {
      $total_debits += $dr_amount;
    }
    $count++;
    $result .=
							'
							<li class="list-benefit">
								<a href="#" class="item-link item-content">
									<div class="item-inner">
										<div class="item-title-row">
											<div class="item-title">'.$today.' </div>
											<div class="item-after">
												<table>
														<tr>
														<td style="padding-right:5px">'.number_format($dr_amount,2).'</td>
														<td style="padding-right:5px">'.number_format($cr_amount,2).'</td>
														<td style="padding-right:5px">'.number_format($balance,2).'</td>
														</tr>
												</table>
												</div>
										</div>
										 <div class="item-text">
												 <span>('.$remarks.')</span>
											</div>
									</div>
								</a>
							</li>

							';
  }
  //
  // $result .=
  //           '
  //             <tr>
  //               <th colspan="4" style="text-align:right">Totals</th>
  //               <th>'.number_format($total_dr_amount,2).'</th>
  //               <th>'.number_format($total_cr_amount, 2).'</th>
  //               <th>'.number_format($balance, 2).'</th>
  //             </tr>
  //           ';
}

    echo $result;

?>
