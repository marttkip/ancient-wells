
<?php
// $received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND invoice.month = "'.date('m').'"
// AND invoice.year = "'.date('Y').'" AND  lease_invoice.invoice_deleted = 0 AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND rental_unit.property_id = property.property_id AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00")';


$received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND invoice.month = "'.date('m').'"
AND invoice.year = "'.date('Y').'" AND  lease_invoice.invoice_deleted = 0 AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND rental_unit.property_id = property.property_id AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00")';
$received_select = 'sum(invoice.invoice_amount) AS number';
$received_table = 'invoice,lease_invoice,leases,rental_unit,property';
$total_month_invoices = 0;//$this->dashboard_model->count_items_group($received_table, $received_where,$received_select);




$received_where = 'payments.payment_id = payment_item.payment_id AND payment_item.payment_month = "'.date('m').'"
AND payment_item.payment_year = "'.date('Y').'" AND  payments.cancel = 0 ';
$received_select = 'sum(payment_item.amount_paid) AS number';
$received_table = 'payment_item,payments';
$total_month_payments = 0;//$this->dashboard_model->count_items_group($received_table, $received_where,$received_select);



if($total_month_invoices == 0)
{
// 	$percentage4 = ($total_month_invoices-$total_month_payments)/$total_month_invoices *100;
// $percentage4 = 100 - $percentage4;
}




$pending_revenue = $total_month_invoices-$total_month_payments;


$transaction_date = date('Y-m-d');

$landlord_expenses = 0;//$this->reporting_model->get_expense_records($transaction_date,1);
$inhouse_expenses = 0;//$this->reporting_model->get_expense_records($transaction_date,0);


?>
<div class="content-block" >
	<h3><?php echo $date_item;?> Summary</h3>
</div>
<div class="content-block" style="margin-top: 20px !important;">
	<!-- <h4>Revenue</h4> -->
	<div class="row gap">
	    <div class="col-50" style="padding-bottom: 10px !important;">
	    	<a href="property-invoices.html" onclick="get_property_invoices()">
	    		<div class="item-media ">
                  <div class="list-date" style="height: 100px;">
                        <span class="list-dayname" style="height: 50%;">KES. <?php echo number_format($total_month_invoices,0)?></span>
                        <span class="list-daynumber" style="height: 50%;color: #fff !important;line-height: 39px !important;">INVOICED AMOUNT</span>
                    </div>
                </div>
            </a>
	    	
	    </div>

	    <div class="col-50" style="padding-bottom: 10px !important;">
	    	<a href="collection-summary.html">
	    		<div class="item-media ">
                  <div class="list-date" style="height: 100px;">
                        <span class="list-dayname" style="height: 50%;">KES. <?php echo number_format($total_month_payments,0)?></span>
                        <span class="list-daynumber" style="height: 50%; color: #fff !important;line-height: 39px !important;">TOTAL COLLECTION</span>
                    </div>
                </div>
	    	</a>
	    </div>

	    <div class="col-100" style="padding-bottom: 5px !important;">
	    		<div class="item-media ">
                  <div class="list-date" style="height: 100px;">
                        <span class="list-dayname" style="height: 50%;">KES.  <?php echo number_format($pending_revenue,0)?></span>
                        <span class="list-daynumber" style="height: 50%; color: #fff !important;line-height: 39px !important;">PENDING REVENUE</span>
                    </div>
                </div>
	    	
	    </div>
	
	 </div>
	 <h4>Expenses</h4>
	<div class="row gap">
	    <div class="col-50" style="padding-bottom: 10px !important;">
	    	<a href="landlord-expenses.html">
	    		<div class="item-media ">
                  <div class="list-date" style="height: 100px;">
                        <span class="list-dayname" style="height: 50%;">KES. <?php echo number_format($landlord_expenses,0)?></span>
                        <span class="list-daynumber" style="height: 50%;color: #fff !important;line-height: 39px !important;">LANDLORD</span>
                    </div>
                </div>
	    	</a>
	    </div>

	    <div class="col-50" style="padding-bottom: 10px !important;">
	    	<a href="inhouse-expenses.html">
	    		<div class="item-media ">
                  <div class="list-date" style="height: 100px;">
                        <span class="list-dayname" style="height: 50%;">KES.  <?php echo number_format($inhouse_expenses,0)?></span>
                        <span class="list-daynumber" style="height: 50%; color: #fff !important;line-height: 39px !important;">INHOUSE</span>
                    </div>
                </div>
	    	</a>
	    </div>
	     
	
	 </div>
</div>