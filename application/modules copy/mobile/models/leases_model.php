<?php
class Leases_model extends CI_Model
{
	public function unclaimed_payments()
	{
		$this->db->where('mpesa_status', 0);
		$this->db->group_by('serial_number');
		return $this->db->get('mpesa_transactions');
	}
	public function get_all_properties()
	{
		$this->db->where('property_id > 0');
		return $this->db->get('property');
	}
	public function active_leases()
	{
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1 AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND property.property_id = rental_unit.property_id');
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$check_query = $this->db->get('leases,tenant_unit,rental_unit,tenants,property');

		return $check_query;
	}
	public function property_active_leases($table, $where, $per_page, $page, $order, $order_method = 'ASC',$month,$year)
	{
		$this->db->from($table);
		$this->db->select('`rental_unit`.rental_unit_id,rental_unit.rental_unit_name,v_tenant_leases.*');
		$this->db->where($where);
		$this->db->join('v_tenant_leases','v_tenant_leases.rental_unit_id = rental_unit.rental_unit_id AND v_tenant_leases.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice WHERE invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND lease_invoice.invoice_deleted = 0 AND (lease_invoice.invoice_date <= v_tenant_leases.notice_date OR  v_tenant_leases.notice_date IS NULL ) GROUP BY lease_invoice.lease_id)','LEFT');
		// $this->db->join('v_tenant_leases','v_tenant_leases.rental_unit_id = rental_unit.rental_unit_id AND v_tenant_leases.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice WHERE invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND lease_invoice.invoice_deleted = 0 GROUP BY lease_invoice.lease_id)','LEFT');
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');

		return $query;

		// $this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1 AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND property.property_id = rental_unit.property_id AND property.property_id ='.$property_id);
		// $this->db->order_by('rental_unit.rental_unit_name','ASC');
		// $check_query = $this->db->get('leases,tenant_unit,rental_unit,tenants,property');

		// return $check_query;
	}

	/*
	*	Retrieve a single lease
	*	@param int $lease_id
	*
	*/
	public function get_lease_detail_mobile($lease_id)
	{
		//retrieve all leases
		$this->db->from('leases,rental_unit,tenant_unit,tenants,property');
		$this->db->select('*');
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND lease_id = '.$lease_id);
		$query = $this->db->get();

		return $query;
	}

	public function receipt_payment($lease_id,$personnel_id = NULL){
		$amount = $this->input->post('amount_paid');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');



		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}

		// calculate the points to get
		$payment_date = $this->input->post('payment_date');


		// $this->get_points_to_award($lease_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];

		if($type_of_account == 1)
		{
			$receipt_number = $this->create_receipt_number();
		}
		else
		{
			$receipt_number = $this->create_owners_receipt_number();
		}

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount_paid'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $receipt_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		if($type_of_account == 1)
		{
			$data['lease_id'] = $lease_id;

			if($this->db->insert('payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount);

				$service_charge_amount = $this->input->post('service_charge_amount');
				$water_amount = $this->input->post('water_amount');
				$rent_amount = $this->input->post('rent_amount');
				$penalty_fee = $this->input->post('penalty_fee');
				$fixed_charge = $this->input->post('fixed_charge');
				$bought_water = $this->input->post('bought_water');
				$insurance = $this->input->post('insurance');
				$sinking_funds = $this->input->post('sinking_funds');
				$painting_charge = $this->input->post('painting_charge');
				$fixed_charge = $this->input->post('fixed_charge');
				$deposit_charge = $this->input->post('deposit_charge');
				$legal_fees = $this->input->post('legal_fees');

				// $invoice_number = $this->get_invoice_number();

				if(!empty($service_charge_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $service_charge_amount,
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}

				if(!empty($water_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $water_amount,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
					// update the invoice table
					// using invoice_type_id
					$invoice_id = $this->get_last_invoice_id(2);



				}
				if(!empty($rent_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $rent_amount,
										'invoice_type_id' => 1,
										'lease_id' => $lease_id,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);


				}
				if(!empty($penalty_fee))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $penalty_fee,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($fixed_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $fixed_charge,
										'invoice_type_id' => 12,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($deposit_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $deposit_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 13,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}

				if(!empty($bought_water))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $bought_water,
										'lease_id' => $lease_id,
										'invoice_type_id' => 10,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($sinking_funds))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $sinking_funds,
										'invoice_type_id' => 8,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($legal_fees))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $legal_fees,
										'lease_id' => $lease_id,
										'invoice_type_id' => 17,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}


				return $payment_id;
			}
			else{
				return FALSE;
			}
		}
		else if($type_of_account == 0)
		{
			$data['rental_unit_id'] = $lease_id;

			if($this->db->insert('home_owners_payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount);

				$service_charge_amount = $this->input->post('service_charge_amount');
				$water_amount = $this->input->post('water_amount');
				$rent_amount = $this->input->post('rent_amount');
				$penalty_fee = $this->input->post('penalty_fee');

				// $invoice_number = $this->get_invoice_number();

				if(!empty($service_charge_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $service_charge_amount,
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}

				if(!empty($water_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $water_amount,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
					// update the invoice table
					// using invoice_type_id
					$invoice_id = $this->get_last_invoice_id(2);



				}
				if(!empty($rent_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $rent_amount,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);


				}
				if(!empty($penalty_fee))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $penalty_fee,
										'invoice_type_id' => 5,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($bought_water))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $bought_water,
										'invoice_type_id' => 10,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($sinking_funds))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $sinking_funds,
										'invoice_type_id' => 8,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($insurance))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $insurance,
										'invoice_type_id' => 9,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($fixed_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $fixed_charge,
										'invoice_type_id' => 9,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				return $payment_id;
			}
			else{
				return FALSE;
			}
		}
	}
	function create_owners_receipt_number()
	{
		//select product code
		$preffix = "AW-RO-";
		$this->db->from('home_owners_payments');
		$this->db->where("receipt_number LIKE '".$preffix."%'  AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}

		return $number;
	}
	function create_receipt_number()
	{
		//select product code
		$preffix = "AW-RT-";
		$this->db->from('payments');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}

		return $number;
	}


	public function get_statement_leases($rental_unit_id,$lease_date,$invoice_year,$invoice_month)
	{

			$query = $this->db->query('SELECT tenants.tenant_name,tenant_phone_number,tenants.tenant_id,tenant_unit.rental_unit_id,leases.lease_id,property_billing.billing_amount
																	FROM leases
																	LEFT JOIN property_billing ON property_billing.lease_id = leases.lease_id
																	LEFT JOIN tenants ON tenants.tenant_id = leases.tenant_id
																	LEFT JOIN tenant_unit ON tenant_unit.tenant_unit_id = leases.tenant_unit_id
																	-- LEFT JOIN lease_invoice ON lease_invoice.lease_id = leases.lease_id
																	WHERE
																	`tenant_unit`.`rental_unit_id` = '.$rental_unit_id.'
																	AND property_billing.invoice_type_id = 1
																	AND (leases.vacated_on <= "'.$lease_date.'")
																	AND leases.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice WHERE invoice_month = "'.$invoice_month.'" AND invoice_year = "'.$invoice_year.'")
																	');
			return $query;
	}


	public function get_tenant_leases($tenant_id)
	{
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND property.property_id = rental_unit.property_id AND leases.tenant_id ='.$tenant_id);
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$check_query = $this->db->get('leases,tenant_unit,rental_unit,tenants,property');

		return $check_query;
	}

	public function get_lease_invoices($lease_id)
	{
		$this->db->where('(dr_amount > 0 OR dr_amount < 0 )AND lease_id ='.$lease_id);
		$this->db->select('SUM(dr_amount) AS total_dr_amount,v_transactions.*');
		$this->db->group_by('lease_id');
		$check_query = $this->db->get('v_transactions');

		return $check_query;
	}
	public function get_lease_receipts($lease_id)
	{
		$this->db->where('(cr_amount > 0 OR cr_amount <0) and lease_id ='.$lease_id);
		$this->db->select('SUM(cr_amount) AS total_cr_amount,v_transactions.*');
		$this->db->group_by('lease_id');
		$check_query = $this->db->get('v_transactions');

		return $check_query;
	}

	public function get_lease_statement($lease_id)
	{
		$this->db->from('v_transactions_by_date');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id);
		$query = $this->db->get();

		return $query;
	}
	public function get_occipied_units($property_id,$year,$month)
	{

		// $this->db->from('rental_unit,v_tenant_leases');
		// $this->db->select('rental_unit.rental_unit_id');
		// $this->db->where('v_tenant_leases.rental_unit_id = rental_unit.rental_unit_id AND v_tenant_leases.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice WHERE invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND lease_invoice.invoice_deleted = 0 GROUP BY lease_invoice.lease_id) AND rental_unit.property_id ='.$property_id);
		// // $this->db->join('v_tenant_leases','v_tenant_leases.rental_unit_id = rental_unit.rental_unit_id AND v_tenant_leases.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice WHERE invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND lease_invoice.invoice_deleted = 0 GROUP BY lease_invoice.lease_id)','LEFT');
		// // $this->db->order_by($order, $order_method);
		// $this->db->group_by('rental_unit.rental_unit_id');
		// $query = $this->db->get('');


		$this->db->from('rental_unit,lease_invoice,leases');
		$this->db->select('rental_unit.rental_unit_id');
		$this->db->where('lease_invoice.rental_unit_id = rental_unit.rental_unit_id AND leases.lease_id = lease_invoice.lease_id AND invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND lease_invoice.invoice_deleted = 0 AND rental_unit.property_id ='.$property_id);

		$this->db->group_by('rental_unit.rental_unit_id');
		$query = $this->db->get('');

		return $query->num_rows();
	}

	public function get_total_units($property_id)
	{
		$this->db->from('rental_unit');
		$this->db->select('rental_unit.rental_unit_id');
		$this->db->where('rental_unit.property_id ='.$property_id);
		$query = $this->db->get('');

		return $query->num_rows();
	}

	public function get_property_rental_units($property_id)
	{
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1 AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND property.property_id = rental_unit.property_id AND property.property_id ='.$property_id);
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$check_query = $this->db->get('leases,tenant_unit,rental_unit,tenants,property');

		return $check_query;
	}

	public function get_last_property_invoice($property_id)
	{

		$this->db->where('property_invoice.property_invoice_status = 1 AND property_invoice.property_id = '.$property_id);
		$this->db->order_by('CONCAT(year, month )','DESC');
		$this->db->limit(1);
		$check_query = $this->db->get('property_invoice');
		$property_invoice_id = 0;
		if($check_query->num_rows() == 1)
		{
			foreach ($check_query->result() as $key => $value) {
				# code...
				$property_invoice_id = $value->property_invoice_id;
			}
		}
		if(empty($property_invoice_id))
		{
			$property_invoice_id = 0;
		}
		// var_dump($property_invoice_id);die();
		return $property_invoice_id;

	}

	public function get_property_reading($property_id)
	{

		$this->db->where('property_invoice.property_invoice_status = 1 AND property_invoice.property_id = '.$property_id);
		$this->db->order_by('CONCAT(year, month )','DESC');
		$this->db->limit(1);
		$check_query = $this->db->get('property_invoice');
		$property_invoice_id = 0;
		if($check_query->num_rows() == 1)
		{
			foreach ($check_query->result() as $key => $value) {
				# code...
				$property_invoice_id = $value->property_invoice_id;
			}
		}
		if(empty($property_invoice_id))
		{
			$property_invoice_id = 0;
		}
		// var_dump($property_invoice_id);die();
		return $property_invoice_id;

	}


/*
	*	Retrieve a single lease
	*	@param int $lease_id
	*
	*/
	public function get_tenant_lease_details($lease_id)
	{
		//retrieve all leases
		$this->db->from('leases,rental_unit,tenant_unit,tenants,property');
		$this->db->select('*');
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND lease_id = '.$lease_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function update_lease_water_invoice($lease_id,$rental_unit_id)
	{
		$current_reading = $this->input->post('current_reading');
		$previous_reading = $this->input->post('previous_reading');
		$water_charges = $this->input->post('water_charges');
		$meter_number = $this->input->post('meter_number');
		$billing_amount = $this->input->post('billing_amount');

		$units_consumed = $current_reading - $previous_reading;

		$property_invoice_id = $this->input->post('property_invoice_id');

		$array_check_two = array('property_invoice_id'=>$property_invoice_id);
		$this->db->where($array_check_two);
		$query_check_two = $this->db->get('property_invoice');

		$row = $query_check_two->row();
		$property_invoice_date = $row->property_invoice_date;

		$invoice_date =$property_invoice_date;
		$explode = explode('-', $invoice_date);
		$invoice_month = $explode[0];
		$invoice_year = $explode[1];		

		if($lease_id > 0)
		{
			$invoice_type_id = 2;
			$property_invoice_id = $this->input->post('property_invoice_id');
			
			$datestring=''.$invoice_date.' first day of next month';
			$dt=date_create($datestring);
			$next = $dt->format('Y-m-d');
			$next_date = explode('-', $next);
			$next_year = $next_date[0];
			$next_month = $next_date[1];
			$next_date = $next_year.'-'.$next_month.'-'.'01';
			$next_date = strtotime($next_date);
			$next_quarter = ceil(date('m', $next_date) / 3);
			$next_month = ($next_quarter * 3) - 2;
			$next_year = date('Y', $next_date);

			$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

			$total_due = $water_charges * ($current_reading - $previous_reading);
			// check 

			$array_check = array('property_invoice_id'=>$property_invoice_id,'invoice_type'=>$invoice_type_id,'lease_id'=>$lease_id);

			// var_dump($array_check); die();
			$this->db->where($array_check);
			$query_check = $this->db->get('water_invoice');

			if($query_check->num_rows() > 0)
			{
				foreach ($query_check->result() as $key_check)
				{
					# code...
					$invoice_idd = $key_check->invoice_id;
					$this->db->where('invoice_id',$invoice_idd);
					$this->db->delete('water_invoice');
					$this->db->where('invoice_id',$invoice_idd);
					$this->db->delete('water_management');

				}
			}
			$invoice_number = $this->water_management_model->get_invoice_number();
			$insert_array = array(
							'lease_id' => $lease_id,
							'invoice_date' => $invoice_date,
							'invoice_month' => $invoice_month,
							'invoice_year' => $invoice_year,
							'invoice_amount' => $total_due,
							'arrears_bf' => $current_reading,
							'invoice_number' => $invoice_number,
							'invoice_type' => $invoice_type_id,
							'property_invoice_id' => $this->input->post('property_invoice_id'),
							'billing_schedule_quarter'=> $next_quarter
						 );
			
			if($this->db->insert('water_invoice',$insert_array))
			{
			
				$invoice_id = $this->db->insert_id();
				if($invoice_type_id == 2 OR $invoice_type_id == 3)
				{
					$service_charge_insert = array(
											"house_number" => $lease_id,
											"meter_number" => $meter_number,
											"prev_reading" => $previous_reading,
											"current_reading" => $current_reading,
											"units_consumed" => $units_consumed,
											"total_due" => $total_due,
											"billing_amount" => $water_charges,
											"prev_bill" => 0,
											"created" => date("Y-m-d"),
											"created_by" => $this->session->userdata('personnel_id'),
											"branch_code" => $this->session->userdata('branch_code'),
											'invoice_id' => $invoice_id,
											'lease_id' => $lease_id,
											'invoice_type_id' => $invoice_type_id,
											'property_invoice_id' => $this->input->post('property_invoice_id'),
											'property_id' => $this->input->post('property_id'),
										);
					$this->db->insert('water_management', $service_charge_insert);
				}
				return TRUE;
			}
			
			else
			{
				return FALSE;

			}
		}

	}

}
?>
