<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Riders extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
	}
	
	public function all_riders()
	{
		$table = "rider";
		$where = "rider.availability = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			$result['message'] = 'success';
			$result['result'] = $query->result();
		}
		
		else
		{
			$result['message'] = 'error';
		}
		
		echo json_encode($result);
	}
	
	public function send_rider_location($personnel_id, $latitude, $longitude)
	{
		$data = array(
			'latitude' => $latitude, 
			'longitude' => $longitude
		);
		
		$this->db->where('personnel_id', $personnel_id);
		if($this->db->update('personnel', $data))
		{
			$result['message'] = 'success';
		}
		
		else
		{
			$result['message'] = 'error';
		}
		
		echo json_encode($result);
	}
	
	public function set_rider_status($status, $personnel_id)
	{
		$table = "personnel_status";
		$this->db->where('personnel_id', $personnel_id);
		if($this->db->update($table, array('personnel_status_status' => 0)))
		{
			$data = array(
				'personnel_status_date' => date('Y-m-d H:i:s'), 
				'personnel_id' => $personnel_id, 
				'personnel_status_availability' => $status, 
				'personnel_status_status' => 1
			);
			if($this->db->insert($table, $data))
			{
				$result['message'] = 'success';
			}
			
			else
			{
				$result['message'] = 'error';
			}
		}
			
		else
		{
			$result['message'] = 'error';
		}
		
		echo json_encode($result);
	}
	
	public function add_registration_id($personnel_id = NULL, $registration_id = NULL)
	{
		if(($personnel_id != NULL) && ($personnel_id != 'null') && ($registration_id != NULL) && ($registration_id != 'null'))
		{
			//check if exists
			$data = array('personnel_id' => $personnel_id, 'registration_id' => $registration_id);
			$this->db->where($data);
			$query = $this->db->get('personnel');
			
			//save if not exists
			if($query->num_rows() == 0)
			{
				$where = array('personnel_id' => $personnel_id);
				$data2 = array('registration_id' => $registration_id);
				
				$this->db->where($where);
				$this->db->update('personnel', $data);
				$response['message'] = 'success';
			}
			
			else
			{
				$response['message'] = 'success';
			}
		}
			
		else
		{
			$response['message'] = 'success';
		}
		
		echo json_encode($response);
	}
	
	public function send_notification()
	{
		$query = $this->db->get('app_user');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$to = $res->registration_id;
				$message_title = 'IoD (Kenya)';
				$title = $message_title;
				$message = 'We are live';
				$result = $this->send_push_notification($to, $title, $message);
				
				echo $result;
			}
		}
	}
	
	public function get_all_riders()
	{
		$today = date('Y-m-d');
		$table = "personnel";
		$where = "personnel_status = 1 AND personnel_type_id = 3 AND personnel_id NOT IN (SELECT personnel_id FROM ride WHERE ride_status_id >= 1 OR ride_status_id <= 3) AND personnel_id IN (SELECT personnel_id FROM personnel_status WHERE personnel_status_date LIKE '".$today."%' AND personnel_status_availability = 1 AND personnel_status_status = 1)";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			$result['message'] = 'success';
			$result['result'] = $query->result();
		}
		
		else
		{
			$result['message'] = 'error';
		}
		
		echo json_encode($result);
	}
}