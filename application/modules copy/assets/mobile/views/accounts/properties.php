<?php
$routes = '';
if($all_properties->num_rows() > 0)
{
	foreach ($all_properties->result() as $key) {
		# code...
		$property_id = $key->property_id;
		$property_name = $key->property_name;
		$routes .= '
					<li class="list-benefit" onclick="get_property_leases('.$property_id.')">
				      <a class="item-link item-content" >
				       	<div class="item-inner">
				          <div class="item-title-row">
				            <div class="item-title"><span><i class="fa fa-bank"></i></span> '.strtoupper($property_name).'</div>					            
				            <div class="item-after"></div>
				          </div>
				          <div class="item-text">
				          	<span>Occupied Units</span> 2
				          	 <span>Vacant Units:</span> 1 
				          </div>
				        </div>
				      </a>
				    </li>
				   ';
	}
}
echo $routes;
?>