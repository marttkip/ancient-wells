<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Tenant name</th>
						<th>Phone Number</th>
						<th>Tenant Email</th>
						<th>Registration Date</th>
						<th>Tenant National ID</th>
						<th>Status</th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$customer_id = $row->tenant_id;
				$customer_name = $row->tenant_name;
				
				$customer_phone = $row->tenant_phone_number;
				$customer_email = $row->tenant_email;
				$customer_status = $row->tenant_status;
				$customer_created = $row->created;
				$company_status = $row->tenant_status;
				$tenant_national_id= $row->tenant_national_id;
			
				
				//status
				if($customer_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($customer_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button_edit = ' ';
				}
				//create activated status display
				else if($customer_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button_edit ='<a href="'.site_url().'setup/edit_property/'.$customer_id.'" class="btn btn-sm btn-success" title="Profile '.$customer_name.'"><i class="fa fa-pencil"></i> Edit</a>';
				   	}
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$customer_name.'</td>
						<td>'.$customer_phone.'</td>
						<td>'.$customer_email.'</td>
						<td>'.date('jS M Y H:i a',strtotime($row->created)).'</td>
						<td>'.$tenant_national_id.'</td>
						<td>'.$status.'</td>
						<td>'.$button_edit.'</td>
						<td><a href="'.site_url().'tenants-uploads/'.$customer_id.'" class="btn btn-sm btn-info" title="Profile '.$customer_name.'"><i class="fa fa-pencil"></i> Uploads</a></td>
						<td><a href="'.site_url().'tenants-complaints/'.$customer_id.'" class="btn btn-sm btn-warning" title="Profile '.$customer_name.'"><i class="fa fa-alarm"></i> Complaints</a></td>
						
						
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Tenants";
		}
?>






<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                <div class="panel-tools" style="color: #fff;">
                   
                </div>
                <?php echo $title;?>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('customer_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/customer/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>
				<div class="row" style="margin-bottom:20px;">
                                    <div class="col-lg-2 col-lg-offset-10">
                                        <a href="<?php echo site_url();?>setup/add_tenant" class="btn btn-sm btn-info pull-right">Add Tenant</a>
                                     </div>
                                   
                                </div>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

