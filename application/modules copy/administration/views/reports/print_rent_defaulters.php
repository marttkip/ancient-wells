<?php
//if users exist display them
$result ='';
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
	'
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Property</th>
				<th>Flat No.</th>
				<th>Tenant Name</th>
				<th>Phone Number</th>
				<th>Rent Payable</th>
				<th>Months</th>
				<th>Balance</th>

			</tr>
		</thead>
		  <tbody>
		  
	';
	
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		// $arreas_bf = $leases_row->arreas_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		// $arrears_bf = $leases_row->arrears_bf;
		$created = $leases_row->created;
		$total_balance = $leases_row->total_balance;

		// $lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		// $expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	
			
		$lease_start_date = date('jS M Y',strtotime($lease_start_date));

     	$this_month = date('m');
     	$amount_paid = 0;
	 //    $tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
		// $total_arrears = $tenants_response['total_arrears'];
		// $total_invoice_balance = $tenants_response['total_invoice_balance'];
		// $invoice_date = $tenants_response['invoice_date'];

		$rent_payable = $this->accounts_model->get_rent_payable($lease_id);


		$avg = $total_balance/$rent_payable;
		if($avg > 1)
		{
			$class = 'danger';
		}
		else
		{
			$class= 'default';
		}
			// $todays_date = date('Y-m-d');
			// $todays_month = date('m');
			// $todays_year = date('Y');
			// $total_payments = $this->accounts_model->get_total_payments_before($lease_id,$todays_year,$todays_month);
			
			// $total_invoices = $this->accounts_model->get_total_invoices_before($lease_id,$todays_year,$todays_month);
			// // var_dump($total_invoices); die();
			// $current_balance = $total_invoices - $total_payments;

			// $current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
			// $total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;


			
				$count++;
				$result .= 
				'
					<tr class="'.$class.'">
						<td>'.$count.'</td>
						<td>'.$property_name.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>
						<td>'.$tenant_phone_number.'</td>
						<td>'.number_format($rent_payable,2).'</td>
						<td>'.number_format($avg,2).'</td>
						<td>'.number_format($total_balance,2).'</td>
	
						
					</tr> 
				';

			
			
}
$result .= 
'
			  </tbody>
			</table>
';
}

else
{
$result .= "There are no defaulters";
}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $property_name;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	 <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        
		            <div class="col-xs-8 ">
		                <h4><strong>Rent Refaulters for   <?php echo $property_name;?> </strong> as at <?php echo date('jS M Y')?></h4>
		               
		            </div>
		        </div>

		        <div class="row">
			       	<div class="col-xs-12 ">
				       	<?php echo $result;?>
				       		
			       	</div>
			   	</div>




		       </div>
		    </div>
		</body>
    </html>