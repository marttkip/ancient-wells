<?php echo $this->load->view('search/tenants_search','', true); ?>
<?php
// var_dump($query);die();


$property_invoice_rs = $this->water_management_model->get_property_invoice_detail($property_invoice_id);


if($property_invoice_rs->num_rows() > 0)
{
	foreach ($property_invoice_rs->result() as $key => $value) {
		# code...
		$invoice_year = $value->year;
		$invoice_month = $value->month;
		$property_invoice_date = $value->property_invoice_date;

		// var_dump($invoice_year);die();
	}
}
$result = '';
		
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th><a>Lease ID</a></th>
				<th><a>Unit Name</a></th>
				<th><a>Tenant Name</a></th>
				<th><a>Tenant Phone Number</a></th>
				<th><a>Bal BF</a></th>
				<th><a>Previous Reading</a></th>
				<th><a>Current Reading</a></th>
				<th><a>Units Consumed</a></th>
				
				<th><a>Rate</a></th>
				<th><a>Current Due</a></th>
				<th><a>Payments Done</a></th>
				<th><a>Total Arrears</a></th>
				<th colspan="">Actions</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_id = $leases_row->tenant_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$rental_unit_id = $leases_row->rental_unit_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$tenant_email = $leases_row->tenant_email;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$initial_reading = 0;// $leases_row->initial_reading;

		// var_dump($initial_reading);die();
		$rent_calculation = $leases_row->rent_calculation;
		// $property_id = $leases_row->property_id;
		$deposit = $leases_row->deposit;
		$water_charges = 0;//$leases_row->billing_amount;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$points = 0;//$leases_row->points;
		$created = $leases_row->created;

		$todays_date = date('Y-m-d');
		$todays_month = date('m');
		$todays_year = date('Y');

		$total_water_arrears = $this->water_management_model->get_initial_opening_balance($lease_id,$rental_unit_id);

		$total_invoices = $this->water_management_model->get_sum_water_invoices($lease_id);
		$total_payments = $this->water_management_model->get_sum_water_payments($lease_id);
		$total_arrears = $total_invoices - $total_payments;
		$this_month_invoice = $this->water_management_model->get_water_invoices_month($lease_id,$todays_month,$todays_year);

		$bal_b_f = $total_arrears - $this_month_invoice ;


	
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		$invoice_id= $this->water_management_model->get_max_invoice_id($lease_id);
		// var_dump($invoice_id); die();
		$prev_reading = '';
		$current_reading = 0;
		$units_consumed = 0;
		$arrears_bf = 0;
		$property_invoice_status = 0;
	
		if(!empty($invoice_id))
		{

			// var_dump($invoice_id);die();
			$water_readings = $this->water_management_model->get_water_readings($invoice_id);
			if($water_readings->num_rows() > 0)
			{
				foreach ($water_readings->result() as $key) {
					# code...
					$prev_reading = $key->prev_reading;
					// $current_reading = $key->current_reading;
					$arrears_bf = $key->arrears_bf;
					
				
					// $units_consumed = $current_reading - $prev_reading;
				}
			}
		}

		if(empty($arrears_bf))
		{
			$prev_reading = $initial_reading;
		}
		else
		{
			$prev_reading = $arrears_bf;

		}
		$current_response = $this->water_management_model->get_current_active_meter_number($lease_id);
		$current_meter_number = $current_response['property_billing_id'];
		$initial_reading = $current_response['initial_reading'];
		$billing_amount = $current_response['billing_amount'];

		$previous_meter_number = $this->water_management_model->get_previous_active_meter_number($lease_id);

		if($current_meter_number == $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
		{
			$used_meter = $previous_meter_number;
		}
		else if($current_meter_number != $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
		{

			if($property_invoice_status == 1 OR $property_invoice_status == 0)
			{
				$used_meter = $current_meter_number;
			}
			else
			{

				$used_meter = $previous_meter_number;
			}
			
		}

		// var_dump($prev_reading);die();

		

		$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
		$current_reading = 0;

		// if($lease_id == 1)
		// {
		// 	var_dump($water_readings_current->num_rows());die();
		// }
		
		// if($water_readings_current->num_rows() > 0)
		// {
		// 	foreach ($water_readings_current->result() as $key) {
		// 		# code...
		// 		$prev_reading = $key->prev_reading;
		// 		$current_reading = $key->current_reading;
		// 		$arrears_bf = $key->arrears_bf;
		// 		$property_invoice_status = $key->property_invoice_status;
		// 		// var_dump($arrears_bf);die();
		// 		$units_consumed = $current_reading - $prev_reading;
				
		// 	}
		// }

		// else
		// {
		// 	$prev_reading = $initial_reading;
		// }


		if($water_readings_current['status'])
		{
			$current_reading = $water_readings_current['current_reading'];
			$prev_reading = $water_readings_current['prev_reading'];

			if($current_reading == 0)
			{
				$units_consumed = 0;
			}
			else
			{
				$units_consumed = $current_reading - $prev_reading;
			}
		}
		else
		{
			$prev_reading = $initial_reading;
			$current_reading = 0;
			$units_consumed = 0;
		}
		

		// $total_payments = $this->water_management_model->get_sum_water_payments($lease_id);
		


		// $property_invoice_status = $this->water_management_model->get_invoice_status($property_invoice_id);
		//var_dump($arrears_bf);die();

		if($property_invoice_status == 1 OR $property_invoice_status == 0)
		{
			$button = '<td><button  class="btn btn-sm btn-primary" onclick="return confirm(\' You are about to update the reading. Do you want to continue ?\')" >Update</button>
				</td>';
				/*$button = '<td>-</td>';*/

			$current = '';
		}
		else
		{

			$current = 'readonly';
			$button = '<td></td>';
		}
		$todays_date = date('Y-m-d');
		$todays_month = date('m');
		$todays_year = date('Y');


		// this is the added section
		$bal_b_f = $this->water_management_model->get_water_arrears($lease_id,$invoice_year,$invoice_month,$property_invoice_id,$property_invoice_date);
		if(empty($bal_b_f))
		{
			$bal_b_f = 0;
		}



		// $bal_b_f +=$total_water_arrears;

		$current_payment = $this->water_management_model->report_current_payment($lease_id,$invoice_year,$invoice_month,$property_invoice_id,$property_invoice_date);
		if(empty($current_payment))
		{
			$current_payment = 0;
		}
		$current_invoice = $units_consumed * $billing_amount;


		// this is the end of the added section
		



		//check if email for the current_month has been sent for that rental unit
		$all_sms_notifications_sent = $this->accounts_model->check_if_sms_sent($rental_unit_id,$tenant_phone_number,$todays_year,$todays_month,1,4);
		
		// $all_email_notifications_sent = $this->accounts_model->check_if_email_sent($rental_unit_id,$tenant_email,$todays_year,$todays_month,1);
		if(($all_sms_notifications_sent==FALSE))
		{
			$send_notification_button = '<td><a class="btn btn-sm btn-default" href="'.site_url().'send-water-arrears/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$property_invoice_id.'/'.$property_id.'/'.$page.'" onclick="return confirm(\' Are you sure you want to send notification to tenant ? \')"> Send SMS</a></td>';	
		}
		else
		{
			$send_notification_button = '-';
		}
		$count++;

		$string  = 'Do you want to update invoice  ?';
		$result .= 
		'
			'.form_open("update-water-invoice/".$tenant_unit_id."/".$lease_id.'/'.$rental_unit_id, array("class" => "form-horizontal")).'
			<input type="hidden"  class="form-control" value="'.$property_id.'" name="property_id'.$lease_id.'" readonly>
			<input type="hidden"  class="form-control" value="'.$property_invoice_id.'" name="property_invoice_id'.$lease_id.'" readonly>
			<input type="hidden" name="redirect_url" value="'.$this->uri->uri_string().'">
			<input type="hidden" name="meter_number'.$lease_id.'" value="'.$used_meter.'">
			<input type="hidden" name="billing_amount'.$lease_id.'" value="'.$billing_amount.'">
			<tr>
				<td>'.$count.'</td>
				<td>'.$lease_id.'</td>
				<td>'.$rental_unit_name.'</td>
				<td>'.$tenant_name.'</td>
				<td>'.$tenant_phone_number.' </td>
				<td>'.number_format($bal_b_f,2).'</td>
				<input type="hidden" class="form-control" value="'.$billing_amount.'" name="water_charges'.$lease_id.'" >
				<td><input type="text"  class="form-control" value="'.$prev_reading.'" name="previous_reading'.$lease_id.'" readonly></td>
				<td><input type="number"  class="form-control" value="'.$current_reading.'" name="current_reading'.$lease_id.'" '.$current.' required="required"></td>
				<td>'.$units_consumed.'</td>
				<td>'.number_format($billing_amount,2).'</td>		
				<td>'.number_format($bal_b_f + $current_invoice ,2).'</td>
				<td>'.number_format($current_payment ,2).'</td>
				<td>'.number_format($bal_b_f + $current_invoice - $current_payment ,2).'</td>
			    '.$button.'
			    '.$send_notification_button.'
				
			</tr> 
			'.form_close().'
		';
		
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}

// var_dump($property_invoice_id);die();
// $property_invoice_id = $property_invoice_id;
$accounts_search_title = $this->session->userdata('all_water_tenants_search');

$property_invoice_status = $this->water_management_model->get_invoice_status($property_invoice_id);
?>  
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->
<section class="panel">
		<header class="panel-heading">						
			<h2 class="panel-title"><?php echo $title;?></h2>
			<?php
			if($property_invoice_status == 1)
			{
				?>
				<a  href="<?php echo site_url();?>import-water-readings/<?php echo $property_invoice_id;?>/<?php echo $property_id;?>"  style="margin-top:-24px" class="btn btn-sm btn-success pull-right"> <i class="fa fa-arrow-down"></i> Import Readings </a>
				<a href="<?php echo site_url().'close-property-invoice/'.$property_invoice_id.'/'.$property_id.'/2';?>"  style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-danger pull-right" onclick="return confirm(' You are about to mark this invoice as completed note that you will not be able to adjust any invoices for this particular month. Do you want to proceed ? ')"><i class="fa fa-folder-closed" ></i>Close Invoice</a>
				<?php
			}
			else if($property_invoice_status == 2)
			{
				?>
				<a href="<?php echo site_url().'print-reading-details/'.$property_invoice_id.'/'.$property_id;?>" style="margin-top:-24px" class="btn btn-sm btn-info pull-right" target="_blank"><i class="fa fa-folder-open" ></i> Print Report </a>
				<a href="<?php echo site_url().'export-reading-details/'.$property_invoice_id.'/'.$property_id?>"  style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-primary pull-right" target="_blank"><i class="fa fa-folder-open"></i> Export Report </a></td>
				<?php
			}
			else
			{

			}
			?>
			

		</header>
		<div class="panel-body">
        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}
			
			$error = $this->session->userdata('error_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$search =  $this->session->userdata('all_water_accounts_search');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'water_management/close_accounts_search/'.$property_invoice_id.'/'.$property_id.'" class="btn btn-sm btn-warning">Close Search</a>';
			}
					
			?>

        	
			<div class="table-responsive">
            	
				<?php echo $result;?>
		
            </div>
		</div>
        <div class="panel-footer">
        	<?php if(isset($links)){echo $links;}?>
        </div>
	</section>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>