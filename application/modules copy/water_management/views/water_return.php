<?php

$result = '';
$total_invoice_balance =0;
$total_balance_bf = 0;
$total_rent_payable = 0;

$this_year_item = $this->session->userdata('year');
$this_month_item = $this->session->userdata('month');
$return_date = $this->session->userdata('return_date');
$total_invoice_balance_end =0;
$total_payable_end =0;
$total_arreas_end = 0;
$total_rent_end = 0;
$total_deposit_paid = 0;
$total_rent_paid = 0;
$total_legal_paid = 0;
$total_payment = 0;




$total_units_consumed = 0;
$total_invoice = 0;
$total_payment = 0;
$grand_balance = 0;
$grand_arrears = 0;

// var_dump($property_type_id);die();
if($property_type_id == 2)
{	
	$count = 0;
	// starts with the blocks in the property
	if($query->num_rows() > 0)
	{
		
		foreach ($query->result() as $leases_row)
		{
			$block_id = $leases_row->block_id;
			$block_name = $leases_row->block_name;
			$result .= 
					'
					<table class="table table-bordered table-striped table-condensed">
						
						  <tbody>
						  
					';
			$result .= 
						'	<tr>
								<th colspan="13" id="header-table">BLOCK '.$block_name.'</th>
							</tr> 
						';
			// get the floors per block

			$dashboard_where = 'block_id = '.$block_id;
			$dashboard_table = 'floor';
			$dashboard_select = '*';
			$floor_query  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select,null,null,'floor_id','ASC');

			$sub_total_units_consumed = 0;
			$sub_total_invoice = 0;
			$sub_total_payment = 0;
			$sub_total_balance = 0;
			$sub_total_water_arrears = 0;



			if($floor_query->num_rows() > 0)
			{
				foreach ($floor_query->result() as $floor_row)
				{
					$floor_id = $floor_row->floor_id;
					$floor_name = $floor_row->floor_name;

					$result .= 
							'	<tr>
									<th colspan="13" id="header-table2"> '.$floor_name.'</th>
								</tr> 
								<tr>
									<th>#</th>
									<th>PROPERTY</th>
									<th>PERIOD</th>
									<th>NAME</th>
									<th>UNIT</th>
									<th>BAL B/F</th>	
									<th>PWR</th>
									<th>CWR</th>
									<th>UNITS CONSUMED</th>
									<th>AMOUNT</th>
									<th>PAYMENT</th>
									<th>BAL C/F</th>
									<th>REMARKS</th>
								</tr>
							';


					$rental_unit_where = 'block_id = '.$block_id.' AND property.property_id = rental_unit.property_id AND floor_id = '.$floor_id;
					$rental_unit_table = 'rental_unit,property';
					$rental_unit_select = '*';
					$rental_query  = $this->dashboard_model->get_content($rental_unit_table, $rental_unit_where,$rental_unit_select,null,null,'rental_unit.rental_unit_name','ASC','unit_capacity','unit_capacity.unit_capacity_id = rental_unit.unit_capacity_id');
					// var_dump($rental_query);die();
					if($rental_query->num_rows() > 0)
					{
						foreach ($rental_query->result() as $rental_row)
						{
							$rental_unit_id = $rental_row->rental_unit_id;
							$rental_unit_name = $rental_row->rental_unit_name;
							$unit_capacity_name = $rental_row->unit_capacity_name;
							$rental_unit_price = $rental_row->rental_unit_price;
							$property_name = $rental_row->property_name;


							$todays_month = $this->session->userdata('month');
							$todays_year = $this->session->userdata('year');
							$return_date = $this->session->userdata('return_date');


							$period  = date('F Y', strtotime($todays_year.'-'.$todays_month));



							if($todays_month < 10)
							{
								$todays_month = '0'.$todays_month;
							}
							$lease_id = $this->water_management_model->get_water_active_lease($property_id,$rental_unit_id,$todays_month,$todays_year,null,$property_invoice_id);
							// echo $lease_id.' one';
							$total_paid_amount = 0;

										$count++;
							if($lease_id > 0)
							{
								$initial_reading = $this->water_management_model->get_initial_reading_payable($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date);
								$total_water_arrears = $this->water_management_model->get_initial_opening_balance($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date);

								$lease_query = $this->reports_model->get_lease_detail($lease_id);
								foreach ($lease_query->result() as $key => $value) {

									$lease_id = $value->lease_id;
									$tenant_unit_id = $value->tenant_unit_id;
									$rental_unit_id = $value->rental_unit_id;
									$tenant_name = $value->tenant_name;
									$property_name = $value->property_name;
									
									$tenant_email = $value->tenant_email;
									$tenant_phone_number = $value->tenant_phone_number;
									$lease_start_date = $value->lease_start_date;
									$lease_duration = $value->lease_duration;
									$rent_amount = $value->rent_amount;
									$lease_number = $value->lease_number;
									$arreas_bf = $value->arrears_bf;
									$rent_calculation = $value->rent_calculation;
									$deposit = $value->deposit;
									$deposit_ext = $value->deposit_ext;
									$lease_status = $value->lease_status;



									// var_dump($total_water_arrears);die();

									$points = 0;//$value->points;
									$created = $value->created;

										// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
									$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

									$date_checked = explode('-', $lease_start_date);

									$year = $date_checked[0];
									$month = $date_checked[1];

									// var_dump($todays_month);die();

									if($year == $todays_year AND $month == $todays_month)
									{
										$tenant_type = 'N.T';
									}
									else
									{
										$tenant_type = '';
									}
								

								
									$lease_start_date = date('jS M Y',strtotime($lease_start_date));
							     	
							     	$total_invoices = $this->water_management_model->get_sum_water_invoices($lease_id);

									$total_payments = $this->water_management_model->get_sum_water_payments($lease_id);
									$total_arrears = $total_invoices - $total_payments;
									$this_month_invoice = $this->water_management_model->get_water_invoices_month($lease_id,$month,$year);

									$bal_b_f = $total_arrears - $this_month_invoice;


									// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
									// $expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

									$invoice_id= $this->water_management_model->get_max_invoice_id($lease_id);
									// var_dump($invoice_id); die();
									$prev_reading = '';
									$current_reading = 0;
									$units_consumed = 0;
									$arrears_bf = 0;
									$property_invoice_status = 0;
								
									if(!empty($invoice_id))
									{
										$water_readings = $this->water_management_model->get_water_readings($invoice_id);
										if($water_readings->num_rows() > 0)
										{
											foreach ($water_readings->result() as $key) {
												# code...
												$prev_reading = $key->prev_reading;
												// $current_reading = $key->current_reading;
												$arrears_bf = $key->arrears_bf;
												
											
												// $units_consumed = $current_reading - $prev_reading;
											}
										}
									}

									if(empty($arrears_bf))
									{
										$prev_reading = $initial_reading;
									}
									else
									{
										$prev_reading = $arrears_bf;

									}
									

									$current_meter_number = $this->water_management_model->get_current_active_meter_number($lease_id);


									$previous_meter_number = $this->water_management_model->get_previous_active_meter_number($lease_id);

									if($current_meter_number == $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
									{
										$used_meter = $previous_meter_number;
									}
									else if($current_meter_number != $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
									{

										if($property_invoice_status == 1 OR $property_invoice_status == 0)
										{
											$used_meter = $current_reading;
										}
										else
										{

											$used_meter = $previous_meter_number;
										}
										
									}
									

									$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
									$current_reading = 0;

									

									if($water_readings_current['status'])
									{
										$current_reading = $water_readings_current['current_reading'];
										$prev_reading = $water_readings_current['prev_reading'];

										$units_consumed = $current_reading - $prev_reading;

										// var_dump($prev_reading);die();
									}
									else
									{
										$prev_reading = $initial_reading;
										$current_reading = 0;
									}

									$total_water_arrears = $this->water_management_model->get_water_arrears($lease_id,$this_year_item,$this_month_item,$property_invoice_id,$property_invoice_date);
									if(empty($total_water_arrears))
									{
										$total_water_arrears += 0;
									}

									$current_payment = $this->water_management_model->report_current_payment($lease_id,$this_year_item,$this_month_item,$property_invoice_id,$property_invoice_date);
									if(empty($current_payment))
									{
										$current_payment = 0;
									}
									$current_invoice = $units_consumed * 200;
									$total_balance = $total_water_arrears + $current_invoice - $current_payment;
									$result .= 
												'	<tr>
														<td>'.$count.'</td>
														<td>'.$property_name.'</td>
														<td>'.$period.'</td>
														<td>'.$tenant_name.'</td>
														<td>'.$rental_unit_name.'</td>
														<td>'.number_format($total_water_arrears,2).'</td>
														<td>'.$prev_reading.'</td>
														<td>'.$current_reading.'</td>
														<td>'.$units_consumed.'</td>
														<td>'.number_format($current_invoice,2).'</td>
														<td>'.number_format($current_payment,2).'</td>
														<td>'.number_format($total_balance,2).'</td>
														<td></td>
													</tr> 
												';
										// add all the balances
									// subtotal units 
									$sub_total_water_arrears += $total_water_arrears;
									$sub_total_units_consumed += $units_consumed;
									$sub_total_invoice += $current_invoice;
									$sub_total_payment += $current_payment;
									$sub_total_balance += $total_balance;


									$total_units_consumed += $units_consumed;
									$total_invoice += $current_invoice;
									$total_payment += $current_payment;
									$grand_balance += $total_balance;
									$grand_arrears += $total_water_arrears;



										
								}

								

							}
							else{

								$result .= 
										'
											<tr>
												<td>'.$count.'</td>
												<td>'.$property_name.'</td>
												<td>'.$period.'</td>
												<td>Vacant House</td>
												<td>'.$rental_unit_name.'</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td></td>
											</tr> 
										';
							}
						}
					}
					
				

				}
			}


			$result .='<tr> 
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><strong>BLOCK '.$block_name.' SUB TOTALS</strong></td>
								
								<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_water_arrears,2).'</strong></td>
								<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"></td>
								<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"></td>
								<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_units_consumed,2).'</strong></td>								
								<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_invoice,2).'</strong></td>
								<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_payment,2).'</strong></td>
								
								<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_balance,2).'</strong></td>
							
							</tr>';

			$result .= 
						'
						 </tbody>
					</table>
					<div class="pagebreak"> </div>
						';
		}
	}
$result .='
<div class="col-md-6 pull-right">
			<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					
					<th>TOTAL BAL B/F</th>	
					<th>PWR</th>
					<th>CWR</th>
					<th>TOTAL UNITS CONSUMED</th>
					<th>TOTAL AMOUNT</th>
					<th>TOTAL PAYMENT</th>
					<th>TOTAL BAL C/F</th>
				</tr>
			</thead>
			<tbody>

			<tr> 
						
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($grand_arrears,2).'</strong></td>	
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_units_consumed,2).'</strong></td>						
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice,2).'</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payment,2).'</strong></td>
						
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($grand_balance,2).'</strong></td>
					</tr>';
		
		$result .= 
		'
					  </tbody>
					</table>
					</div>
		';
}
else
{
		$result .= 
					'
					<table class="table table-bordered table-striped table-condensed">
						
						  <tbody>
						  
					';

	$count = 0;
	$result .= 
			'	
				<tr>
					<th>#</th>
					<th>PROPERTY</th>
					<th>PERIOD</th>
					<th>NAME</th>
					<th>UNIT</th>
					<th>BAL B/F</th>	
					<th>PWR</th>
					<th>CWR</th>
					<th>UNITS CONSUMED</th>
					<th>AMOUNT</th>
					<th>PAYMENT</th>
					<th>BAL C/F</th>
					<th>REMARKS</th>
				</tr>
			';
	if ($query->num_rows() > 0)
	{
		// var_dump($query->num_rows()); die();

		$sub_total_units_consumed = 0;
		$sub_total_invoice = 0;
		$sub_total_payment = 0;
		$sub_total_balance = 0;
		$sub_total_water_arrears = 0;
		foreach ($query->result() as $leases_row)
		{
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			$rental_unit_price = $leases_row->rental_unit_price;

			$todays_month = $this->session->userdata('month');
			$todays_year = $this->session->userdata('year');
			$return_date = $this->session->userdata('return_date');

			$period  = date('F Y', strtotime($todays_year.'-'.$todays_month));
			if($todays_month < 10)
			{
				$todays_month = '0'.$todays_month;
			}
			$lease_id = $this->water_management_model->get_water_active_lease($property_id,$rental_unit_id,$todays_month,$todays_year,null,$property_invoice_id);
			// echo $lease_id.' one';
			$total_paid_amount = 0;

						$count++;
			if($lease_id > 0)
			{
				$initial_reading = $this->water_management_model->get_initial_reading_payable($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date);
				$total_water_arrears = $this->water_management_model->get_initial_opening_balance($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date);

				$lease_query = $this->reports_model->get_lease_detail($lease_id);
				foreach ($lease_query->result() as $key => $value) {

					$lease_id = $value->lease_id;
					$tenant_unit_id = $value->tenant_unit_id;
					$rental_unit_id = $value->rental_unit_id;
					$tenant_name = $value->tenant_name;
					$property_name = $value->property_name;
					
					$tenant_email = $value->tenant_email;
					$tenant_phone_number = $value->tenant_phone_number;
					$lease_start_date = $value->lease_start_date;
					$lease_duration = $value->lease_duration;
					$rent_amount = $value->rent_amount;
					$lease_number = $value->lease_number;
					$arreas_bf = $value->arrears_bf;
					$rent_calculation = $value->rent_calculation;
					$deposit = $value->deposit;
					$deposit_ext = $value->deposit_ext;
					$lease_status = $value->lease_status;
					// var_dump($arrears_bf);die();

					$points = 0;//$value->points;
					$created = $value->created;

						// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
					$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

					$date_checked = explode('-', $lease_start_date);

					$year = $date_checked[0];
					$month = $date_checked[1];

					// var_dump($todays_month);die();

					if($year == $todays_year AND $month == $todays_month)
					{
						$tenant_type = 'N.T';
					}
					else
					{
						$tenant_type = '';
					}
				

				
					$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			     	
			     	$total_invoices = $this->water_management_model->get_sum_water_invoices($lease_id);

					$total_payments = $this->water_management_model->get_sum_water_payments($lease_id);
					$total_arrears = $total_invoices - $total_payments;
					$this_month_invoice = $this->water_management_model->get_water_invoices_month($lease_id,$month,$year);

					$bal_b_f = $total_arrears - $this_month_invoice;


					// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
					// $expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

					$invoice_id= $this->water_management_model->get_max_invoice_id($lease_id);
					// var_dump($invoice_id); die();
					$prev_reading = '';
					$current_reading = 0;
					$units_consumed = 0;
					$arrears_bf = 0;
					$property_invoice_status = 0;
				
					if(!empty($invoice_id))
					{
						$water_readings = $this->water_management_model->get_water_readings($invoice_id);
						if($water_readings->num_rows() > 0)
						{
							foreach ($water_readings->result() as $key) {
								# code...
								$prev_reading = $key->prev_reading;
								// $current_reading = $key->current_reading;
								$arrears_bf = $key->arrears_bf;
								
							
								// $units_consumed = $current_reading - $prev_reading;
							}
						}
					}

					if(empty($arrears_bf))
					{
						$prev_reading = $initial_reading;
					}
					else
					{
						$prev_reading = $arrears_bf;

					}
					
					$current_meter_number = $this->water_management_model->get_current_active_meter_number($lease_id);


					$previous_meter_number = $this->water_management_model->get_previous_active_meter_number($lease_id);

					if($current_meter_number == $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
					{
						$used_meter = $previous_meter_number;
					}
					else if($current_meter_number != $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
					{

						if($property_invoice_status == 1 OR $property_invoice_status == 0)
						{
							$used_meter = $current_reading;
						}
						else
						{

							$used_meter = $previous_meter_number;
						}
						
					}
				
					
					$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
					$current_reading = 0;

					

					if($water_readings_current['status'])
					{
						$current_reading = $water_readings_current['current_reading'];
						$prev_reading = $water_readings_current['prev_reading'];

						$units_consumed = $current_reading - $prev_reading;

						// var_dump($prev_reading);die();
						if($current_reading == 0)
						{
							$units_consumed = 0;
						}
						else
						{
							$units_consumed = $current_reading - $prev_reading;
						}
					}
					else
					{
						$prev_reading = $initial_reading;
						$current_reading = 0;
						$units_consumed = 0;
					}


					$total_water_arrears = $this->water_management_model->get_water_arrears($lease_id,$this_year_item,$this_month_item,$property_invoice_id,$property_invoice_date);
					if(empty($total_water_arrears))
					{
						$total_water_arrears += 0;
					}
					// $total_water_arrears = $this->water_management_model->get_water_arrears($lease_id,$this_year_item,$this_month_item,$property_invoice_id,$property_invoice_date);
					// if(empty($total_water_arrears))
					// {
					// 	$total_water_arrears = 0;
					// }

					$current_payment = $this->water_management_model->report_current_payment($lease_id,$this_year_item,$this_month_item,$property_invoice_id,$property_invoice_date);
					if(empty($current_payment))
					{
						$current_payment = 0;
					}
					$current_invoice = $units_consumed * 200;
					$total_balance = $total_water_arrears + $current_invoice - $current_payment;
					$result .= 
								'	<tr>
										<td>'.$count.'</td>
										<td>'.$property_name.'</td>
										<td>'.$period.'</td>
										<td>'.$tenant_name.'</td>
										<td>'.$rental_unit_name.'</td>
										<td>'.number_format($total_water_arrears,2).'</td>
										<td>'.$prev_reading.'</td>
										<td>'.$current_reading.'</td>
										<td>'.$units_consumed.'</td>
										<td>'.number_format($current_invoice,2).'</td>
										<td>'.number_format($current_payment,2).'</td>
										<td>'.number_format($total_balance,2).'</td>
										<td></td>
									</tr> 
								';
						// add all the balances
					// subtotal units 
					$sub_total_water_arrears += $total_water_arrears;
					$sub_total_units_consumed += $units_consumed;
					$sub_total_invoice += $current_invoice;
					$sub_total_payment += $current_payment;
					$sub_total_balance += $total_balance;


					$total_units_consumed += $units_consumed;
					$total_invoice += $current_invoice;
					$total_payment += $current_payment;
					$grand_balance += $total_balance;
					$grand_arrears += $total_water_arrears;



						
				}

				

			}
			else{

				$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$property_name.'</td>
								<td>'.$period.'</td>
								<td>Vacant House</td>
								<td>'.$rental_unit_name.'</td>
								<td>0.00</td>
								<td>0.00</td>
								<td>0.00</td>
								<td>0.00</td>
								<td>0.00</td>
								<td>0.00</td>
								<td>0.00</td>
								<td></td>
							</tr> 
						';
			}
				
			
		
			
			
			
			
		}

		$result .='<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td><strong>Totals</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($grand_arrears,2).'</strong></td>	
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_units_consumed,2).'</strong></td>						
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice,2).'</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payment,2).'</strong></td>
						
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($grand_balance,2).'</strong></td>';

		
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}

	else
	{
		$result .= "There are no items for this month";
	}
}


// if($this_month_item < 10)
// {
// 	$this_month_item = '0'.$this_month_item;
// }
$leases = $this->reports_model->get_property_month_water_bill($property_id,$this_month_item,$this_year_item);
$total_leases = 0;
$x = 0;
$total_deposit_refund = 0;
if($leases->num_rows() > 0)
{
	$closed_leases = '';

	foreach ($leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$amount_paid = $key->amount_paid;
		$lease_id = $key->lease_id;
		$invoice_type_name = $key->invoice_type_name;

		// $deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_leases .='  <tr>
								<td>'.$x.'</td>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>'.$invoice_type_name.'</td>
								<td>KES. '.number_format($amount_paid,2).'</td>
							</tr>';
		
		$total_leases = $total_leases + $amount_paid;
	}
	$closed_leases .='<tr>
						<td colspan=3>Total<td>
						<td>KES. '.number_format($total_leases,2).'</td>
				</tr>';
	// $closed_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_leases ,2).' </strong> </li>';
}else
{
	$closed_leases ='<tr><td colspan=3>No closed leases</td></tr>';
}


$property_id = $this->session->userdata('property_id');

$management_fee = 0.06*($total_payment);
?>  
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
			@media print {
			    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
			}
			#header-table
			{
				background-color:orange;
				color:white;
			}
			#header-table2
			{
				background-color:#1a4567;
				color:white;
			}

			@media print {
			    #header-table {
			        /*background-color: #1a4567 !important;*/
			        background-color:orange;
					color:white;
			        -webkit-print-color-adjust: exact; 
			    }
			}

			@media print {
			    #header-table {
			        color: white !important;
			    }
			}

			@media print {
			    th#header-table {
			        background-color: orange !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}

			@media print {
			    th#header-table2 {
			        background-color: #1a4567 !important;
			         color: white !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}
		</style>
    </head>
     <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <div class="receipt_bottom_border">
                    <table class="table table-condensed">
                        <tr>
                            <th><h2><?php echo $contacts['company_name'];?> </h2></th>
                            <th class="align-right">
                                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            	<tr>
                    <th class="align-center"><h3><?php echo $title;?> </h3></th>
                </tr>
                <?php echo $result;?>
            </div>
        </div>
        <div class="row" >
       		<div class="col-xs-12" style="border-top:1px #000 solid;">
       			<h4>Key Areas to Note:</h4>
       			<div class="row">
					<div class="col-md-6">
					</div>
   					<div class="col-md-6">
   						<?php 
   						// var_dump($todays_month);die();
   						$outstanding_balance = 0;//$this->reports_model->get_outstanding_balance($property_id,$todays_month,$todays_year);
   						?>
						<h4>Return Summary</h4>
						<table class="table table-condensed table-bordered ">
							<thead>


							<tbody>
										<tr>
												<td>Total Payments</td>
												<td><?php echo number_format($total_payment,2);?></td>
										</tr>
										<tr>
												<td>Add Other Collection</td>
												<td><?php echo number_format(0,2);?></td>
										</tr>
										<tr>
												<th>Total Gross Payable</th>
												<th><?php echo number_format($total_payment,2);?></th>
										</tr>
										<tr>
												<td>Less Management Fee (6%)</td>
												<td>( <?php echo number_format($management_fee,2);?> )</td>
										</tr>
										<tr>
												<td><b>NET AMOUNT PAYABLE</b></td>
												<td><b><?php echo number_format(($total_payment) - $management_fee,2);?>
													</b>
												</td>
										</tr>
							</tbody>
						</table>
					</div>
				</div>

       		</div>
       	</div>
        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body> 
    </html>