<?php

class Water_management_model extends CI_Model 
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	/*
	*	Retrieve all personnel
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_water_records($table, $where, $per_page, $page, $order = 'property_invoice.property_invoice_date', $order_method = 'DESC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	/*
	*	Import Template
	*
	*/
	function import_template()
	{
		$this->load->library('Excel');
		
		$title = 'Water Management Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'HSE Number';
		$report[$row_count][1] = 'Meter Number';
		$report[$row_count][2] = 'Prev Reading';
		$report[$row_count][3] = 'Curr Reading';
		$report[$row_count][4] = 'Units Consumed';
		$report[$row_count][5] = 'Prev B/f';
		$report[$row_count][6] = 'Invoice Date';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function import_csv_charges($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_csv_charges_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_charges_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);
		$property_invoice_id = $this->input->post('property_invoice_id');
		// var_dump($total_columns); die();
		//get invoice
		//$this->db->where(array('property_invoice_id' => $property_invoice_id, 'invoice_type_id' => $invoice_type_id));
		$this->db->where(array('property_invoice_id' => $property_invoice_id));
		$invoices = $this->db->get('property_invoice');
		
		if($invoices->num_rows() > 0)
		{
			$row = $invoices->row();
			$property_invoice_id = $row->property_invoice_id;
			$property_invoice_amount = $row->property_invoice_amount;
			$property_invoice_units = $row->property_invoice_units;
			$property_invoice_number = $row->property_invoice_number;
			$property_id = $row->property_id;
			$invoice_type_id = $row->invoice_type_id;
			$property_id = $row->property_id;
			
			//if products exist in array
			if(($total_rows > 0) && ($total_columns == 7))
			{
				$count = 0;
				$comment = '';
				$items['modified_by'] = $this->session->userdata('personnel_id');
				$document_number = $this->create_document_number();

				$this->db->where('property_id = '.$property_id.' AND invoice_type_id = '.$invoice_type_id.' AND billing_schedule_id = 4');
				$billing_query = $this->db->get('property_billing');
				$rate_per_use = 0;
				if($billing_query->num_rows() > 0)
				{
					foreach ($billing_query->result() as $key_billing) {
						# code...
						$rate_per_use = $key_billing->billing_amount;
						$charge_to = $key_billing->charge_to;
					}
				}
				// $rate_per_use = 1;


				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					$hse_name = $array[$r][0];
					$meter_number = $array[$r][1];
					$prev_reading = $array[$r][2];
					$curr_reading = $array[$r][3];
					$units_consumed = $array[$r][4];
					$prev_bal = $array[$r][5];
					$invoice_date = $array[$r][6];

					if($invoice_type_id == 2)
					{
						if(empty($curr_reading) || empty($prev_reading) )
						{
                                $total_due = 0;
						}
						else
						{
							if(!empty($prev_bal))
							{

								$total_due = ($curr_reading - $prev_reading) * $rate_per_use +$prev_bal;
							}
							else
							{

								$total_due = ($curr_reading - $prev_reading) * $rate_per_use;
							}


						}
						
					}
					else if($invoice_type_id == 10)
					{
						if(empty($units_consumed))
						{
							$total_due = $prev_bal;
						}
						else
						{
							// $rate_per_use = 0.5;
							$total_due = $units_consumed * $rate_per_use;
						}
						
					}
					else if($invoice_type_id == 3)
					{
						$rate_per_use = 40;
						$total_due = $units_consumed;
					}
					else if($invoice_type_id == 4)
					{
						$total_due = $prev_bal;
					}
					else if($invoice_type_id == 7)
					{
						$total_due = $prev_bal;
					}
					else if($invoice_type_id == 8)
					{
						$total_due = $prev_bal;
					}
					else if($invoice_type_id == 9)
					{
						$total_due = $units_consumed;
					}
					else if($invoice_type_id == 5)
					{
						$total_due = $units_consumed;
					}
					else if($invoice_type_id == 14)
					{
						$total_due = ($units_consumed * $rate_per_use) + 450 * $prev_bal  ;
					}
					else if($invoice_type_id == 15)
					{
						$total_due = $units_consumed;
					}
					$current_date  =  date('Y-m-d');
					$datestring=''.$current_date.' first day of last month';
					$dt=date_create($datestring);
					$previous = $dt->format('Y-m');
					$previous_date = explode('-', $previous);
					$previous_year = $previous_date[0];
					$previous_month = $previous_date[1];
					

					// $total_due_borehole = ($curr_reading - $prev_reading) * 26;
					// $total_due = $total_due + $prev_bal;
					
					// var_dump($total_due); die();
					
					$count++;
					$check_date = explode('-', $invoice_date);
					
					if(count($check_date) > 1)
					{

						$newDate = date("Y-m-d", strtotime($invoice_date));
						//echo $newDate; die();
						$invoice_explode = explode('-', $newDate);
						
						$todaym2 = $invoice_explode[1];
						$year = $invoice_explode[0];

						if($charge_to == 1)
						{
							//check if there are records for the particular month/year
							$this->db->where(array('invoice_month' => $todaym2, 'invoice_year' => $year));
							$query_invoice = $this->db->get('water_invoice');
							
							if($query_invoice->num_rows() > 0)
							{
								$invoice_row = $query_invoice->row();
								$invoice_id = $invoice_row->invoice_id;
								$this->db->where('invoice_id', $invoice_id);
								$mtr_query = $this->db->get('water_management');
								$row2 = $mtr_query->row();
								$document_number = 'MRT001';
							}
							$document_number = $property_invoice_number;
							// get lease id using the unit name 
							if(!empty($hse_name))
							{
								$lease_id = $this->get_lease_id($hse_name);
								// var_dump($total_due); die();
								if($lease_id > 0)
								{
									
									$datestring=''.$invoice_date.' first day of next month';
									$dt=date_create($datestring);
									$next = $dt->format('Y-m-d');
									$next_date = explode('-', $next);
									$next_year = $next_date[0];
									$next_month = $next_date[1];
									$next_date = $next_year.'-'.$next_month.'-'.'01';
									$next_date = strtotime($next_date);
									$next_quarter = ceil(date('m', $next_date) / 3);
									$next_month = ($next_quarter * 3) - 2;
									$next_year = date('Y', $next_date);

									$next_quarter = 'AC'.$next_quarter.'-'.$next_year;



									$invoice_number = $this->get_invoice_number();
									$insert_array = array(
													'lease_id' => $lease_id,
													'invoice_date' => $invoice_date,
													'invoice_month' => $todaym2,
													'invoice_year' => $year,
													'invoice_amount' => $total_due,
													'arrears_bf' => $total_due,
													'invoice_number' => $invoice_number,
													'invoice_type' => $invoice_type_id,
													'property_invoice_id' => $property_invoice_id,
													'billing_schedule_quarter'=> $next_quarter
												 );
									
									if($this->db->insert('water_invoice',$insert_array))
									{
										$comment .= '<br/>Details successfully added to the database';
										$class = 'success';

										$invoice_id = $this->db->insert_id();
										// service charge entry

										if($invoice_type_id == 2 OR $invoice_type_id == 3)
										{
											$service_charge_insert = array(
																	"house_number" => $hse_name,
																	"prev_reading" => $prev_reading,
																	"current_reading" => $curr_reading,
																	"units_consumed" => $units_consumed,
																	"total_due" => $total_due,
																	"prev_bill" => $prev_bal,
																	"created" => date("Y-m-d"),
																	"created_by" => $this->session->userdata('personnel_id'),
																	"branch_code" => $this->session->userdata('branch_code'),
																	'property_id' => $property_id,
																	'property_invoice_id' => $property_invoice_id,
																	'document_number' => $document_number,
																	'invoice_id' => $invoice_id
																);
											$this->db->insert('water_management', $service_charge_insert);
										}
										
									}
									
									else
									{
										$comment .= '<br/>Not saved internal error';
										$class = 'danger';
									}
									
			
									$return['response'] = TRUE;
									$return['check'] = TRUE;
			
								}
			
								
							}
							else
							{
			
								$return['response'] = FALSE;
								$return['check'] = FALSE;
							}
						}
						else if($charge_to == 0)
						{

							// var_dump($todaym2); die();
							$this->db->where(array('invoice_month' => $todaym2, 'invoice_year' => $year));
							$query_invoice = $this->db->get('home_owners_invoice');
							
							if($query_invoice->num_rows() > 0)
							{
								$invoice_row = $query_invoice->row();
								$invoice_id = $invoice_row->invoice_id;
								$this->db->where('invoice_id', $invoice_id);
								$mtr_query = $this->db->get('water_management');
								$row2 = $mtr_query->row();
								$document_number = 'MRT001';
							}
							
							// get lease id using the unit name 
							if(!empty($hse_name))
							{
								// $lease_id = $this->get_lease_id($hse_name);
								$rental_unit_id = $this->get_rental_unit_id($hse_name);
								// var_dump($total_due); die();
								// var_dump($total_due);die(); 
								if($rental_unit_id > 0)
								{
									
									$datestring=''.$invoice_date.' first day of next month';
									$dt=date_create($datestring);
									$next = $dt->format('Y-m-d');
									$next_date = explode('-', $next);
									$next_year = $next_date[0];
									$next_month = $next_date[1];
									$next_date = $next_year.'-'.$next_month.'-'.'01';
									$next_date = strtotime($next_date);
									$next_quarter = ceil(date('m', $next_date) / 3);
									$next_month = ($next_quarter * 3) - 2;
									$next_year = date('Y', $next_date);

									$next_quarter = 'AC'.$next_quarter.'-'.$next_year;



									$invoice_number = $this->get_invoice_number();
									$insert_array = array(
													'rental_unit_id' => $rental_unit_id,
													'invoice_date' => $invoice_date,
													'invoice_month' => $todaym2,
													'invoice_year' => $year,
													'invoice_amount' => $total_due,
													'arrears_bf' => $total_due,
													'invoice_number' => $invoice_number,
													'invoice_type' => $invoice_type_id,
													'property_invoice_id' => $property_invoice_id,
													'billing_schedule_quarter'=> $next_quarter
												 );
									
									if($this->db->insert('home_owners_invoice',$insert_array))
									{
										$comment .= '<br/>Details successfully added to the database';
										$class = 'success';

										$invoice_id = $this->db->insert_id();
										// service charge entry

										if($invoice_type_id == 2 OR $invoice_type_id == 3 OR $invoice_type_id == 14)
										{
											$service_charge_insert = array(
																	"house_number" => $hse_name,
																	"prev_reading" => $prev_reading,
																	"current_reading" => $curr_reading,
																	"units_consumed" => $units_consumed,
																	"total_due" => $total_due,
																	"prev_bill" => $prev_bal,
																	"created" => date("Y-m-d"),
																	"created_by" => $this->session->userdata('personnel_id'),
																	"branch_code" => $this->session->userdata('branch_code'),
																	'property_id' => $property_id,
																	'property_invoice_id' => $property_invoice_id,
																	'invoice_id' => $invoice_id
																);
											$this->db->insert('water_management', $service_charge_insert);
										}
										
									}
									
									else
									{
										$comment .= '<br/>Not saved internal error';
										$class = 'danger';
									}
									
			
									$return['response'] = TRUE;
									$return['check'] = TRUE;
			
								}
			
								
							}
							else
							{
			
								$return['response'] = FALSE;
								$return['check'] = FALSE;
							}
						}

					}
					else
					{
	
						$return['response'] = FALSE;
						$return['check'] = FALSE;
					}
				}	
					
			}
			else
			{
				$return['response'] = FALSE;
				$return['check'] = FALSE;
			}
		}
		
		else
		{
			$return['response'] = FALSE;
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	public function get_invoice_number()
	{
		//select product code
		$preffix = $this->session->userdata('branch_code');
		$this->db->from('water_invoice');
		$this->db->where("invoice_number LIKE '".$preffix."%'");
		$this->db->select('MAX(invoice_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function get_lease_id($hse_name)
	{
		$this->db->where('rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND rental_unit.rental_unit_name = "'.$hse_name.'"');
		$this->db->select('lease_id');
		$query = $this->db->get('leases,rental_unit,tenant_unit');
		if($query->num_rows() > 0)
		{
			$item = $query->row();
			$lease_id = $item->lease_id;
			return $lease_id;
		}
		else
		{
			return FALSE;
		}
	}
	// public function create_document_number()
	// {
	// 	$preffix = $this->session->userdata('branch_code');
	// 	$this->db->from('water_management');
	// 	$this->db->where("document_number LIKE '".$preffix."%'");
	// 	$this->db->select('MAX(document_number) AS number');
	// 	$query = $this->db->get();//echo $query->num_rows();
		
	// 	if($query->num_rows() > 0)
	// 	{
	// 		$result = $query->result();
	// 		$number =  $result[0]->number;
	// 		$real_number = str_replace($preffix, "", $number);
	// 		$real_number++;//go to the next number
	// 		$number = $preffix.sprintf('%03d', $real_number);
	// 	}
	// 	else{//start generating receipt numbers
	// 		$number = $preffix.sprintf('%03d', 1);
	// 	}
		
	// 	return $number;
	// }
	public function get_document_details($document_number)
	{
		// $this->db->where('property_invoice.property_invoice_id = water_management.property_invoice_id AND document_number = "'.$document_number.'"');
		// $this->db->from('water_management, property_invoice');
		$this->db->where('document_number = "'.$document_number.'"');
		$this->db->from('water_management');
		$this->db->select('*');
		$query = $this->db->get();

		return $query;
	}
	public function get_rental_units()
	{
		$this->db->order_by('rental_unit_name');
		$this->db->from('rental_unit');
		$this->db->select('*');
		$query = $this->db->get();

		return $query;
	}

	public function sms($phone,$message)
	{
        // This will override any configuration parameters set on the config file
		// max of 160 characters
		// to get a unique name make payment of 8700 to Africastalking/SMSLeopard
		// unique name should have a maximum of 11 characters
		$phone_number = '+'.$phone;


		// get items 

		$configuration = $this->admin_model->get_configuration();

		$mandrill = '';
		$configuration_id = 0;
		
		if($configuration->num_rows() > 0)
		{
			$res = $configuration->row();
			$configuration_id = $res->configuration_id;
			$mandrill = $res->mandrill;
			$sms_key = $res->sms_key;
			$sms_user = $res->sms_user;
	        $sms_suffix = $res->sms_suffix;
	        $sms_from = $res->sms_from;
		}
	    else
	    {
	        $configuration_id = '';
	        $mandrill = '';
	        $sms_key = '';
	        $sms_user = '';
	        $sms_suffix = '';

	    }

	    $actual_message = $message.' '.$sms_suffix;
	    // var_dump($actual_message); die();
		// get the current branch code
        $params = array('username' => $sms_user, 'apiKey' => $sms_key);  

        $this->load->library('AfricasTalkingGateway', $params);
		// var_dump($params)or die();
        // Send the message
		try 
		{
        	$results = $this->africastalkinggateway->sendMessage($phone_number, $actual_message, $sms_from);
			
			var_dump($results);die();
			foreach($results as $result) {
				// status is either "Success" or "error message"
				// echo " Number: " .$result->number;
				// echo " Status: " .$result->status;
				// echo " MessageId: " .$result->messageId;
				// echo " Cost: "   .$result->cost."\n";
			}
			return $result->status;

		}
		
		catch(AfricasTalkingGatewayException $e)
		{
			// echo "Encountered an error while sending: ".$e->getMessage();
			return FALSE;
		}
    }
	
	public function get_invoice_types()
	{
		$this->db->order_by('invoice_type_name');
		return $this->db->get('invoice_type');
	}
	
	public function add_property_invoice()
	{
		$data = array(
			'property_id'=>$this->input->post('property_id'),
			'property_invoice_number'=>$this->input->post('property_invoice_number'),
			'invoice_type_id'=>$this->input->post('invoice_type_id'),
			'property_invoice_amount'=>$this->input->post('property_invoice_amount'),
			'property_invoice_units'=>$this->input->post('property_invoice_units'),
			'property_invoice_date'=>$this->input->post('property_invoice_date'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'created'=>date('Y-m-d H:i:s')
		);
		
		if($this->db->insert('property_invoice', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function edit_property_invoice($property_invoice_id)
	{
		$data = array(
			'property_id'=>$this->input->post('property_id'),
			'invoice_type_id'=>$this->input->post('invoice_type_id'),
			'invoice_amount'=>$this->input->post('invoice_amount'),
			'invoice_units'=>$this->input->post('invoice_units'),
			'invoice_date'=>$this->input->post('invoice_date'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'created'=>date('Y-m-d H:i:s')
		);
		
		$this->db->where('property_invoice_id', $property_invoice_id);
		if($this->db->update('property_invoice', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_property_invoices()
	{
		$this->db->select('property.property_name,property_invoice.*');
		$this->db->where('property_invoice.property_id = property.property_id');
		$this->db->order_by('property_invoice.property_invoice_date,property.property_name', 'DESC');
		return $this->db->get('property_invoice, property');
	}
	public function delete_roperty_invoice($property_invoice_id)
	{
		$this->db->where('property_invoice_id', $property_invoice_id);
		if($this->db->delete('property_invoice'))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_service_charge_debt($lease_id,$month,$year,$charge_type)
	{
		$where = 'lease_id = '.$lease_id.' AND invoice_type_id = '.$charge_type.'  AND payment_item_status = 1 AND payment_month = "'.$month.'" AND payment_year = "'.$year.'"';
		// var_dump($where);die();
		$this->db->from('water_payment_item');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	function get_months()
	{

		return $this->db->get('month');
	}

	public function generate_property_reading_report()
	{

		$data = array(
			'property_id'=>$this->input->post('property_id'),
			'property_invoice_number'=>$this->create_document_number(),
			'year'=>$this->input->post('year'),
			'month'=>$this->input->post('month'),
			'property_invoice_date'=>$this->input->post('invoice_date'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'created'=>date('Y-m-d H:i:s')
		);
		
		if($this->db->insert('property_invoice', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}

	function create_document_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('property_invoice');
		$this->db->where("property_invoice_id > 0");
		$this->db->select('MAX(property_invoice_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;

			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}

		return $number;
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_water_tenants($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('tenants.*,rental_unit.rental_unit_id AS unit_id,rental_unit.*,property.*,leases.*,tenant_unit.*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		// $this->db->join('property_billing', 'leases.lease_id = property_billing.lease_id AND property_billing.invoice_type_id = 2','left');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}


	public function update_water_invoice($lease_id,$rental_unit_id)
	{
		$current_reading = $this->input->post('current_reading'.$lease_id);
		$previous_reading = $this->input->post('previous_reading'.$lease_id);
		$water_charges = $this->input->post('water_charges'.$lease_id);
		$meter_number = $this->input->post('meter_number'.$lease_id);
		$billing_amount = $this->input->post('billing_amount'.$lease_id);

		$units_consumed = $current_reading - $previous_reading;

		$property_invoice_id = $this->input->post('property_invoice_id'.$lease_id);

		$array_check_two = array('property_invoice_id'=>$property_invoice_id);
		$this->db->where($array_check_two);
		$query_check_two = $this->db->get('property_invoice');

		$row = $query_check_two->row();
		$property_invoice_date = $row->property_invoice_date;

		$invoice_date =$property_invoice_date;
		$explode = explode('-', $invoice_date);
		$invoice_month = $explode[0];
		$invoice_year = $explode[1];		

		if($lease_id > 0)
		{
			$invoice_type_id = 2;
			$property_invoice_id = $this->input->post('property_invoice_id'.$lease_id);
			
			$datestring=''.$invoice_date.' first day of next month';
			$dt=date_create($datestring);
			$next = $dt->format('Y-m-d');
			$next_date = explode('-', $next);
			$next_year = $next_date[0];
			$next_month = $next_date[1];
			$next_date = $next_year.'-'.$next_month.'-'.'01';
			$next_date = strtotime($next_date);
			$next_quarter = ceil(date('m', $next_date) / 3);
			$next_month = ($next_quarter * 3) - 2;
			$next_year = date('Y', $next_date);

			$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

			$total_due = $water_charges * ($current_reading - $previous_reading);
			// check 

			$array_check = array('property_invoice_id'=>$property_invoice_id,'invoice_type'=>$invoice_type_id,'lease_id'=>$lease_id);

			// var_dump($array_check); die();
			$this->db->where($array_check);
			$query_check = $this->db->get('water_invoice');

			if($query_check->num_rows() > 0)
			{
				foreach ($query_check->result() as $key_check)
				{
					# code...
					$invoice_idd = $key_check->invoice_id;
					$this->db->where('invoice_id',$invoice_idd);
					$this->db->delete('water_invoice');
					$this->db->where('invoice_id',$invoice_idd);
					$this->db->delete('water_management');

				}
			}
			$invoice_number = $this->get_invoice_number();
			$insert_array = array(
							'lease_id' => $lease_id,
							'invoice_date' => $invoice_date,
							'invoice_month' => $invoice_month,
							'invoice_year' => $invoice_year,
							'invoice_amount' => $total_due,
							'arrears_bf' => $current_reading,
							'invoice_number' => $invoice_number,
							'invoice_type' => $invoice_type_id,
							'property_invoice_id' => $this->input->post('property_invoice_id'.$lease_id),
							'billing_schedule_quarter'=> $next_quarter
						 );
			
			if($this->db->insert('water_invoice',$insert_array))
			{
			
				$invoice_id = $this->db->insert_id();
				if($invoice_type_id == 2 OR $invoice_type_id == 3)
				{
					$service_charge_insert = array(
											"house_number" => $lease_id,
											"meter_number" => $meter_number,
											"prev_reading" => $previous_reading,
											"current_reading" => $current_reading,
											"units_consumed" => $units_consumed,
											"total_due" => $total_due,
											"billing_amount" => $water_charges,
											"prev_bill" => 0,
											"created" => date("Y-m-d"),
											"created_by" => $this->session->userdata('personnel_id'),
											"branch_code" => $this->session->userdata('branch_code'),
											'invoice_id' => $invoice_id,
											'lease_id' => $lease_id,
											'invoice_type_id' => $invoice_type_id,
											'property_invoice_id' => $this->input->post('property_invoice_id'.$lease_id),
											'property_id' => $this->input->post('property_id'.$lease_id),
										);
					$this->db->insert('water_management', $service_charge_insert);
				}
				return TRUE;
			}
			
			else
			{
				return FALSE;

			}
		}

	}

	public function get_invoice_status($property_invoice_id)
	{
		// var_dump($property_invoice_id);
		$this->db->where('property_invoice_id',$property_invoice_id);
		$query = $this->db->get('property_invoice');

		$rs = $query->row();
		return $rs->property_invoice_status;
	}
	public function export_readings($property_invoice_id,$property_id)
	{

		$this->load->library('excel');
		
		//get all transactions
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id and leases.lease_id = water_management.lease_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id.' AND property_invoice.property_id = property.property_id AND property_invoice.property_invoice_id = water_management.property_invoice_id AND property_invoice.property_invoice_id = '.$property_invoice_id;
		$table = 'tenants,tenant_unit,rental_unit,leases,property,property_invoice,water_management';	
		$this->db->select('tenants.*,rental_unit.rental_unit_id AS unit_id,rental_unit.*,property.*,leases.*,tenant_unit.*,property_invoice.year as property_year,property_invoice.month as property_month,property_invoice.property_invoice_date');
	
		$this->db->where($where);
		$this->db->order_by('rental_unit.rental_unit_name,rental_unit.rental_unit_id,property.property_name','ASC');
		$this->db->join('property_billing', 'leases.lease_id = property_billing.lease_id AND property_billing.invoice_type_id = 2','left');
		// $this->db->order_by('rental_unit.rental_unit_name,rental_unit.rental_unit_id','ASC');
		$transactions_query = $this->db->get($table);

		
		$title = 'Water Report';
		
		if($transactions_query->num_rows() > 0)
		{
			$count = 0;
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Property';
			$report[$row_count][2] = 'Period';
			$report[$row_count][3] = 'Name';
			$report[$row_count][4] = 'Unit';
			$report[$row_count][5] = 'Bal B/F';
			$report[$row_count][6] = 'PWR';
			$report[$row_count][7] = 'CWR';
			$report[$row_count][8] = 'Units';
			$report[$row_count][9] = 'Amount';
			$report[$row_count][10] = 'Payment';
			$report[$row_count][11] = 'Bal C/F';
			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($transactions_query->result() as $leases_rows => $key)
			{
				
				$lease_id = $key->lease_id;
				$tenant_id = $key->tenant_id;
				$tenant_unit_id = $key->tenant_unit_id;
				$rental_unit_id = $key->rental_unit_id;
				$rental_unit_name = $key->rental_unit_name;
				$tenant_name = $key->tenant_name;
				$tenant_email = $key->tenant_email;
				$tenant_phone_number = $key->tenant_phone_number;
				$lease_start_date = $key->lease_start_date;
				$lease_duration = $key->lease_duration;
				$rent_amount = $key->rent_amount;
				$lease_number = $key->lease_number;
				$property_name = $key->property_name;
				$initial_reading = 0;//$key->initial_reading;
				$property_invoice_date = $key->property_invoice_date;
				$year = $key->property_year;
				$month = $key->property_month;
				// $initial_reading = $key->initial_reading;
					
				// $total_item_amount = 200 * $units_consumed;
				// $service_charge = $this->config->item('service_charge');
				// $total = $total_item_amount;

				// $disc = date('jS M Y', strtotime('+10 days', strtotime($parent_invoice_date))); 
				$period = date('F Y', strtotime($year.'-'.$month));

// var_dump($lease_id); die();
				$total_invoices = $this->get_sum_water_invoices($lease_id);

				$total_payments = $this->get_sum_water_payments($lease_id);
				$total_arrears = $total_invoices - $total_payments;
				$this_month_invoice = $this->get_water_invoices_month($lease_id,$month,$year);

				$bal_b_f = $total_arrears - $this_month_invoice;


				// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
				// $expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

				$invoice_id= $this->get_max_invoice_id($lease_id);
				// var_dump($invoice_id); die();
				$prev_reading = '';
				$current_reading = 0;
				$units_consumed = 0;
				$arrears_bf = 0;
				$property_invoice_status = 0;
			
				if(!empty($invoice_id))
				{
					$water_readings = $this->get_water_readings($invoice_id);
					if($water_readings->num_rows() > 0)
					{
						foreach ($water_readings->result() as $key) {
							# code...
							$prev_reading = $key->prev_reading;
							// $current_reading = $key->current_reading;
							$arrears_bf = $key->arrears_bf;
							
						
							// $units_consumed = $current_reading - $prev_reading;
						}
					}
				}

				if(empty($arrears_bf))
				{
					$prev_reading = $initial_reading;
				}
				else
				{
					$prev_reading = $arrears_bf;

				}
				
				
			
				
				// $water_readings_current = $this->get_current_reading($lease_id,$property_invoice_id);
				// $current_reading = 0;
				
				// if($water_readings_current->num_rows() > 0)
				// {
				// 	foreach ($water_readings_current->result() as $key) {
				// 		# code...
				// 		$prev_reading = $key->prev_reading;
				// 		$current_reading = $key->current_reading;
				// 		$arrears_bf = $key->arrears_bf;
				// 		$property_invoice_status = $key->property_invoice_status;
				// 		// var_dump($arrears_bf);die();
				// 		$units_consumed = $current_reading - $prev_reading;
						
				// 	}
				// }
				// if($current_reading > 0)
				// {
				// 	$current_reading = $current_reading;
				// }
				// else
				// {
				// 	$current_reading = 0;
				// }
				$current_response = $this->water_management_model->get_current_active_meter_number($lease_id);
				$current_meter_number = $current_response['property_billing_id'];
				$initial_reading = $current_response['initial_reading'];
				$billing_amount = $current_response['billing_amount'];
				$previous_meter_number = $this->water_management_model->get_previous_active_meter_number($lease_id);

				if($current_meter_number == $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
				{
					$used_meter = $previous_meter_number;
				}
				else if($current_meter_number != $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
				{

					if($property_invoice_status == 1 OR $property_invoice_status == 0)
					{
						$used_meter = $current_meter_number;
					}
					else
					{

						$used_meter = $previous_meter_number;
					}
					
				}


				$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
				$current_reading = 0;

				

				if($water_readings_current['status'])
				{
					$current_reading = $water_readings_current['current_reading'];
					$prev_reading = $water_readings_current['prev_reading'];

					if($current_reading == 0)
					{
						$units_consumed = 0;
					}
					else
					{
						$units_consumed = $current_reading - $prev_reading;
					}


					// var_dump($prev_reading);die();
				}
				else
				{
					$prev_reading = $initial_reading;
					$current_reading = 0;
					$units_consumed = 0;
				}

				$total_water_arrears = $this->get_water_arrears($lease_id,$year,$month,$property_invoice_id,$property_invoice_date);
				if(empty($total_water_arrears))
				{
					$total_water_arrears = 0;
				}

				$current_payment = $this->report_current_payment($lease_id,$year,$month,$property_invoice_id,$property_invoice_date);
				if(empty($current_payment))
				{
					$current_payment = 0;
				}
				$current_invoice = $units_consumed * 200;

				$row_count++;
				$count++;
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $property_name;
				$report[$row_count][2] = $period;
				$report[$row_count][3] = $tenant_name;
				$report[$row_count][4] = $rental_unit_name;
				$report[$row_count][5] = number_format($total_water_arrears,0);
				$report[$row_count][6] = $prev_reading;
				$report[$row_count][7] = $current_reading;
				$report[$row_count][8] = $units_consumed;
				$report[$row_count][9] = $current_invoice;
				$report[$row_count][10] = $current_payment;
				$report[$row_count][11] = $total_water_arrears + $current_invoice - $current_payment;
				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);

	}
	
	/*
	*	Import Template
	*
	*/
	function import_water_template()
	{
		$this->load->library('Excel');
		
		$title = 'Water Management Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'HSE Number';
		$report[$row_count][1] = 'Meter Number';
		$report[$row_count][2] = 'Prev Reading';
		$report[$row_count][3] = 'Curr Reading';
		$report[$row_count][4] = 'Units Consumed';
		$report[$row_count][5] = 'Prev B/f';

		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}



	public function import_csv_charges_water($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_csv_charges_water($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_charges_water($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);
		$property_invoice_id = $this->input->post('property_invoice_id');
		// var_dump($total_columns); die();
		//get invoice
		//$this->db->where(array('property_invoice_id' => $property_invoice_id, 'invoice_type_id' => $invoice_type_id));
		$this->db->where(array('property_invoice_id' => $property_invoice_id));
		$invoices = $this->db->get('property_invoice');
		
		if($invoices->num_rows() > 0)
		{
			$row = $invoices->row();
			$property_invoice_id = $row->property_invoice_id;
			$property_invoice_amount = $row->property_invoice_amount;
			$property_invoice_date = $row->property_invoice_date;
			$property_invoice_number = $row->property_invoice_number;
			$property_id = $row->property_id;
			$invoice_type_id = 2;
			$property_id = $row->property_id;
			
			//if products exist in array
			if(($total_rows > 0) && ($total_columns == 6))
			{
				$count = 0;
				$comment = '';
				$items['modified_by'] = $this->session->userdata('personnel_id');
				$document_number = $this->create_document_number();

				
				// $rate_per_use = 1;


				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					$hse_name = $array[$r][0];
					$meter_number = $array[$r][1];
					$prev_reading = $array[$r][2];
					$curr_reading = $array[$r][3];
					$units_consumed = $array[$r][4];
					$prev_bal = $array[$r][5];

					
					
					$current_date = $invoice_date  =  $property_invoice_date;
					$datestring=''.$current_date.' first day of last month';
					$dt=date_create($datestring);
					$previous = $dt->format('Y-m');
					$previous_date = explode('-', $previous);
					$previous_year = $previous_date[0];
					$previous_month = $previous_date[1];
					

					// $total_due_borehole = ($curr_reading - $prev_reading) * 26;
					// $total_due = $total_due + $prev_bal;
					
					// var_dump($total_due); die();
					
					$count++;
					$check_date = explode('-', $invoice_date);
					
					

					$newDate = date("Y-m-d", strtotime($invoice_date));
					//echo $newDate; die();
					$invoice_explode = explode('-', $newDate);
					
					$todaym2 = $invoice_explode[1];
					$year = $invoice_explode[0];

												
					if(!empty($hse_name))
					{
						$lease_id = $this->get_lease_id($hse_name);
					
						if($lease_id > 0)
						{

							$this->db->where('lease_id = '.$lease_id.' AND invoice_type_id = 2 AND billing_schedule_id = 4');
							$this->db->limit(1);
							$billing_query = $this->db->get('property_billing');
							$rate_per_use = 0;
							if($billing_query->num_rows() > 0)
							{
								foreach ($billing_query->result() as $key_billing)
								{
									# code...
									$rate_per_use = $key_billing->billing_amount;
								}
							}
								
							$current_reading = $curr_reading;
							$previous_reading = $prev_reading;
							$water_charges = $rate_per_use;

							$units_consumed = $current_reading - $previous_reading;

							$invoice_date = $property_invoice_date;
							$explode = explode('-', $invoice_date);
							$invoice_month = $explode[0];
							$invoice_year = $explode[1];	


							$invoice_type_id = 2;
							
							
							$datestring=''.$invoice_date.' first day of next month';
							$dt=date_create($datestring);
							$next = $dt->format('Y-m-d');
							$next_date = explode('-', $next);
							$next_year = $next_date[0];
							$next_month = $next_date[1];
							$next_date = $next_year.'-'.$next_month.'-'.'01';
							$next_date = strtotime($next_date);
							$next_quarter = ceil(date('m', $next_date) / 3);
							$next_month = ($next_quarter * 3) - 2;
							$next_year = date('Y', $next_date);

							$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

							$total_due = $water_charges * ($current_reading - $previous_reading);
							// check 

							$array_check = array('property_invoice_id'=>$property_invoice_id,'invoice_type'=>$invoice_type_id,'lease_id'=>$lease_id);

							// var_dump($array_check); die();
							$this->db->where($array_check);
							$query_check = $this->db->get('water_invoice');

							if($query_check->num_rows() > 0)
							{
								foreach ($query_check->result() as $key_check)
								{
									# code...
									$invoice_idd = $key_check->invoice_id;
									$this->db->where('invoice_id',$invoice_idd);
									$this->db->delete('water_invoice');
									$this->db->where('invoice_id',$invoice_idd);
									$this->db->delete('water_management');

								}
							}
							$invoice_number = $this->get_invoice_number();
							$insert_array = array(
											'lease_id' => $lease_id,
											'invoice_date' => $invoice_date,
											'invoice_month' => $invoice_month,
											'invoice_year' => $invoice_year,
											'invoice_amount' => $total_due,
											'arrears_bf' => $current_reading,
											'invoice_number' => $invoice_number,
											'invoice_type' => $invoice_type_id,
											'property_invoice_id' => $property_invoice_id,
											'billing_schedule_quarter'=> $next_quarter
										 );
							// var_dump($insert_array); die();
							if($this->db->insert('water_invoice',$insert_array))
							{
							
								$invoice_id = $this->db->insert_id();
								if($invoice_type_id == 2 OR $invoice_type_id == 3)
								{
								$service_charge_insert = array(
														"house_number" => $lease_id,
														"prev_reading" => $previous_reading,
														"current_reading" => $current_reading,
														"units_consumed" => $units_consumed,
														"total_due" => $total_due,
														"prev_bill" => 0,
														"created" => date("Y-m-d"),
														"created_by" => $this->session->userdata('personnel_id'),
														"branch_code" => $this->session->userdata('branch_code'),
														'invoice_id' => $invoice_id,
														'lease_id' => $lease_id,
														'invoice_type_id' => $invoice_type_id,
														'property_invoice_id' => $property_invoice_id,
														'property_id' => $property_id,
													);
								$this->db->insert('water_management', $service_charge_insert);
								}
							
							}
							$return['response'] = TRUE;
							$return['check'] = TRUE;
	
						}
	
						
					}
					else
					{
	
						$return['response'] = FALSE;
						$return['check'] = FALSE;
					}
					
						

					
				}	
					
			}
			else
			{
				$return['response'] = FALSE;
				$return['check'] = FALSE;
			}
		}
		
		else
		{
			$return['response'] = FALSE;
			$return['check'] = FALSE;
		}
		
		return $return;
	}


	public function get_sum_water_payments($lease_id)
	{
		$where = 'leases.lease_id = water_payments.lease_id AND water_payments.lease_id = '.$lease_id.' AND leases.lease_status = 1';

		$this->db->from('water_payments,leases');
		$this->db->select('SUM(amount_paid) AS total_payments');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_payments;
		}
		return $total_paid;
	}
	public function get_this_months_water_payment($lease_id,$month)
	{

		$this->db->from('water_payments');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.' AND payment_status = 1 AND cancel = 0 AND month = "'.$month.'"');
		$query = $this->db->get();
		return $query;

	}

	public function get_water_invoices_month($lease_id,$month,$year)
	{
		$where = 'lease_id = '.$lease_id.' AND invoice_month = "'.$year.'" AND invoice_year= "'.$month.'" AND invoice_type.invoice_type_id = water_invoice.invoice_type';

		$this->db->from('water_invoice,invoice_type');
		$this->db->select('invoice_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->invoice_amount;
		}
		return $total_paid;
	}
	public function get_water_arrears_old($lease_id,$month,$year,$property_invoice_id,$property_invoice_date = null)
	{

		$where = 'lease_id = '.$lease_id.' AND (water_invoice.invoice_month < '.$month.' OR (water_invoice.invoice_month  = '.$month.' AND water_invoice.invoice_year <= "'.$year.'"))  AND invoice_type.invoice_type_id = water_invoice.invoice_type AND property_invoice.property_invoice_id = water_invoice.property_invoice_id AND property_invoice.property_invoice_status = 2 AND water_invoice.invoice_date < "'.$property_invoice_date.'"';

		$this->db->from('water_invoice,invoice_type,property_invoice');
		$this->db->select('SUM(invoice_amount) AS invoice_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_amount = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_amount = $row->invoice_amount;
		}



		$where = 'lease_id = '.$lease_id.' AND (year <  '.$month.' OR (year  = '.$month.' AND month < "'.$year.'")) AND cancel = 0 AND payment_date <= "'.$property_invoice_date.'"';

		$this->db->from('water_payments');
		$this->db->select('SUM(amount_paid) AS invoice_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->invoice_amount;
		}
		

		return $invoice_amount - $total_paid;
	}

	public function get_water_arrears($lease_id,$month,$year,$property_invoice_id,$property_invoice_date = null)
	{

		$where = 'lease_id = '.$lease_id.' AND invoice_type_id = 2';

		$this->db->from('property_billing');
		$this->db->select('SUM(arrears_bf) AS invoice_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$opening_balance = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$opening_balance = $row->invoice_amount;
		}

		// if($lease_id == 1)
		// {
		// 	var_dump($opening_balance);die();
		// }

		$where = 'lease_id = '.$lease_id.' AND (year <  '.$month.' OR (year  = '.$month.' AND month < "'.$year.'")) AND cancel = 0 AND payment_date <= "'.$property_invoice_date.'"';

		$this->db->from('water_payments');
		$this->db->select('SUM(amount_paid) AS invoice_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->invoice_amount;
		}

		$where = 'lease_id = '.$lease_id.' AND (water_invoice.invoice_month < '.$month.' OR (water_invoice.invoice_month  = '.$month.' AND water_invoice.invoice_year <= "'.$year.'"))  AND invoice_type.invoice_type_id = water_invoice.invoice_type AND property_invoice.property_invoice_id = water_invoice.property_invoice_id AND property_invoice.property_invoice_status = 2 AND water_invoice.invoice_date < "'.$property_invoice_date.'"';

		$this->db->from('water_invoice,invoice_type,property_invoice');
		$this->db->select('SUM(invoice_amount) AS invoice_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_amount = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_amount = $row->invoice_amount;
		}


		$where = 'lease_id = '.$lease_id.' AND (year <  '.$month.' OR (year  = '.$month.' AND month < "'.$year.'")) AND cancel = 0 AND pardon_date <= "'.$property_invoice_date.'"';

		$this->db->from('water_pardon_payments');
		$this->db->select('SUM(pardon_amount) AS total_pardon_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$total_pardon = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_pardon = $row->total_pardon_amount;
		}

		return ($invoice_amount + $opening_balance) - ($total_paid + $total_pardon);
	}

	public function report_current_payment($lease_id,$month,$year,$property_invoice_id,$property_invoice_date = null)
	{
		$where = 'lease_id = '.$lease_id.' AND (year  = '.$month.' AND month = "'.$year.'") AND cancel = 0 ';

		$this->db->from('water_payments');
		$this->db->select('SUM(amount_paid) AS invoice_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->invoice_amount;
		}

		return $total_paid;
	}

	
	public function get_rental_unit_id($rental_unit_name)
	{
		$this->db->from('rental_unit');
		$this->db->select('*');
		$this->db->where('rental_unit_name = "'.$rental_unit_name.'"');

		$query = $this->db->get();

		$result = $query->result();

		$rental_unit_id = 0;

		if($query->num_rows() > 0 )
		{
			$result = $query->result();
			$rental_unit_id = $result[0]->rental_unit_id;
		}

		return $rental_unit_id;

	}

	// public function get_invoice_number()
	// {
	// 	//select product code
	// 	$preffix = $this->session->userdata('branch_code');
	// 	$this->db->from('water_invoice');
	// 	$this->db->where("invoice_number LIKE '".$preffix."%'");
	// 	$this->db->select('MAX(invoice_number) AS number');
	// 	$query = $this->db->get();//echo $query->num_rows();
		
	// 	if($query->num_rows() > 0)
	// 	{
	// 		$result = $query->result();
	// 		$number =  $result[0]->number;
	// 		$real_number = str_replace($preffix, "", $number);
	// 		$real_number++;//go to the next number
	// 		$number = $preffix.sprintf('%03d', $real_number);
	// 	}
	// 	else{//start generating receipt numbers
	// 		$number = $preffix.sprintf('%03d', 1);
	// 	}
		
	// 	return $number;
	// }

	public function get_max_invoice_id($lease_id,$invoice_month = NULL,$invoice_year=NULL)
	{
		if($invoice_month != NULL || $invoice_year != NULL)
		{
			$add = 'AND invoice_month = "'.$invoice_month.'" AND invoice_year = '.$invoice_year.'';
		}
		else
		{
			$add = '';
		}
		$where = 'lease_id = '.$lease_id.' '.$add;

		$this->db->from('water_invoice');
		$this->db->select('MAX(invoice_id) AS invoice_id');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_id = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_id = $row->invoice_id;
		}
		return $invoice_id;
	}

	public function get_water_readings($invoice_id)
	{

		$this->db->select("*");
		$this->db->where('water_management.invoice_id = '.$invoice_id.' AND water_invoice.invoice_id = water_management.invoice_id AND property_invoice.property_invoice_id = water_management.property_invoice_id');
		$query = $this->db->get('water_management,water_invoice,property_invoice');
		return $query;

	}


	public function get_current_reading($lease_id,$property_invoice_id,$meter_number)
	{
		
		$this->db->select("*");
		$this->db->where('water_management.lease_id = '.$lease_id.' AND water_management.property_invoice_id = '.$property_invoice_id.'  AND water_invoice.invoice_id = water_management.invoice_id AND property_invoice.property_invoice_id = water_management.property_invoice_id');
		$query = $this->db->get('water_management,water_invoice,property_invoice');

		$prev_reading = 0;
		$current_reading = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$current_reading = $value->current_reading;
				$prev_reading = $value->prev_reading;
				$record_id = $value->record_id;

				


			}
			$status = TRUE;
		}
		else
		{
			$this->db->select("*");
			$this->db->where('water_management.lease_id = '.$lease_id.' AND water_management.property_invoice_id < '.$property_invoice_id.'  AND water_invoice.invoice_id = water_management.invoice_id AND property_invoice.property_invoice_id = water_management.property_invoice_id');
			$this->db->order_by('water_management.record_id','DESC');
			$this->db->limit(1);
			$query = $this->db->get('water_management,water_invoice,property_invoice');


			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
				# code...
				// $current_reading = $value->current_reading;
				$prev_reading = $value->current_reading;
				// $record_id = $value->record_id;

				}
				$status = TRUE;

			}
			else
			{
				$status = FALSE;
			}
		}

		$response['prev_reading'] = $prev_reading;
		$response['current_reading'] = $current_reading;
		$response['status'] = $status;
		return $response;


	}

	public function get_current_reading_old($lease_id,$property_invoice_id,$meter_number)
	{

		$this->db->select("*");
		$this->db->where('water_management.lease_id = '.$lease_id.' AND water_management.property_invoice_id = '.$property_invoice_id.'  AND water_invoice.invoice_id = water_management.invoice_id AND property_invoice.property_invoice_id = water_management.property_invoice_id');
		$query = $this->db->get('water_management,water_invoice,property_invoice');
		return $query;


	}

	public function get_current_active_meter_number($lease_id)
	{
		$this->db->select("*");
		$this->db->where('property_billing.lease_id = '.$lease_id.' AND property_billing.invoice_type_id = 2 AND property_billing_deleted = 0');
		$query = $this->db->get('property_billing');
		$property_billing_id = 0;
		$initial_reading = 0;
		$billing_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$property_billing_id = $value->property_billing_id;
				$initial_reading = $value->initial_reading;
				$billing_amount = $value->billing_amount;
			}
		}

		$response['initial_reading'] = $initial_reading;
		$response['billing_amount'] = $billing_amount;
		$response['property_billing_id'] = $property_billing_id;
		return $response;
	}


	public function get_previous_active_meter_number($lease_id)
	{

		$this->db->select("water_management.meter_number");
		$this->db->where('water_management.lease_id = '.$lease_id.' AND water_invoice.invoice_id = water_management.invoice_id');
		$this->db->limit(1);
		$this->db->order_by('water_management.record_id','DESC');
		$query = $this->db->get('water_management,water_invoice');

		$meter_number = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$meter_number = $value->meter_number;
			}
		}
		return $meter_number;


	}

	public function get_property_invoice_detail($property_invoice_id)
	{


		$where = 'property_invoice_id = '.$property_invoice_id.'';

		$this->db->from('property_invoice');
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();
		
		return $query;
	}


	public function get_sum_water_invoices($lease_id)
	{
		$where = 'leases.lease_id = water_invoice.lease_id AND water_invoice.lease_id = '.$lease_id.' AND leases.lease_status = 1';

		$this->db->from('water_invoice,leases');
		$this->db->select('SUM(invoice_amount) AS total_invoices');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_invoices;
		}
		return $total_paid;
	}

	public function get_account_opening_balance($lease_id,$type= null)
	{
		// var_dump($lease_id); die();
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND property_billing.invoice_type_id  <> 2';
			}
			else
			{
				$add_where = ' AND property_billing.invoice_type_id = 2';
			}
		}
		else
		{
			$add_where ='';
		}
		$this->db->from('property_billing,leases');
		$this->db->select('property_billing.arrears_bf AS total_invoice,MONTH(leases.lease_start_date) AS invoice_month,YEAR(leases.lease_start_date) AS invoice_year,property_billing.start_date AS invoice_date');
		$this->db->where('leases.lease_id = '.$lease_id.' AND property_billing.property_billing_deleted = 0 AND leases.lease_id = property_billing.lease_id '.$add_where);
		$query = $this->db->get();
		return $query;
	}


	public function get_tenants_billings($lease_id,$tenant_unit_id=null)
	{
		$bills = $this->get_all_invoice_month($lease_id);
		$opening_arrears = $this->get_account_opening_balance($lease_id,2);
		$payments = $this->get_all_payments_lease($lease_id);
		$pardons = $this->get_all_pardons_lease($lease_id);

		
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;
		$payment_amount = 0;
		// var_dump($opening_arrears); die();
		if($opening_arrears->num_rows() > 0)
		{
			foreach ($opening_arrears->result() as $opening_arrears_key) {
				# code...
				$invoice_date = $opening_arrears_key->invoice_date;
				$invoice_year = $opening_arrears_key->invoice_year;
				$invoice_month = $opening_arrears_key->invoice_month;
				$total_invoice = $opening_arrears_key->total_invoice;

				$total_arrears += $total_invoice;
				$total_invoice_balance += $total_invoice;
						
				// if($invoice_year >= $current_year)
				// {
					$result .= 
					'
						<tr>
							<td>'.date('d M Y',strtotime($invoice_date)).' </td>
							<td>Balance B/F </td>
							<td>'.number_format($total_invoice, 2).'</td>
							<td></td>
							<td>'.number_format($total_arrears, 2).'</td>
							<td></td>
						</tr> 
					';
				// }
				
			}
		}


		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->total_invoice;
				$invoice_id = $key_bills->invoice_id;
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;

						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;

						if(($pardon_date <= $invoice_date) && ($pardon_date > $last_date) && ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>Pardon</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr> 
								';
							// }
							
							$total_pardon_amount += $pardon_amount;

						}
					}
				}
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;
						
					// if($invoice_year >= $current_year)
					// {
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($total_arrears, 2).'</td>
								<td><a href="'.site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'" target="_blank" class="btn btn-sm btn-warning">Invoice</a>
									<a href="'.site_url().'delete-water-invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$invoice_id.'/'.$tenant_unit_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this invoice ? \')"> Delete</a></td>							</tr> 
						';
					// }
				}
						
				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}

					if($pardons->num_rows() > 0)
					{
						foreach ($pardons->result() as $pardons_key) {
							# code...
							$pardon_date = $pardons_key->pardon_date;
							$pardon_explode = explode('-', $pardon_date);
							$pardon_year = $pardon_explode[0];
							$pardon_month = $pardon_explode[1];
							$pardon_amount = $pardons_key->pardon_amount;

							if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
							{
								$total_arrears -= $pardon_amount;
								// if($pardon_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($pardon_date)).' </td>
											<td>Pardon</td>
											<td></td>
											<td>'.number_format($pardon_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr> 
									';
								// }
								
								$total_pardon_amount += $pardon_amount;

							}
						}
					}
				}
						$last_date = $invoice_date;
			}
		}	
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr> 
							';
						// }
						
						$total_payment_amount += $payment_amount;

					}
				}
			}
			if($pardons->num_rows() > 0)
			{
				foreach ($pardons->result() as $pardons_key) {
					# code...
					$pardon_date = $pardons_key->pardon_date;
					$pardon_explode = explode('-', $pardon_date);
					$pardon_year = $pardon_explode[0];
					$pardon_month = $pardon_explode[1];
					$pardon_amount = $pardons_key->pardon_amount;

					if(($payment_amount > 0))
					{
						$total_arrears -= $pardon_amount;
						// if($pardon_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($pardon_date)).' </td>
									<td>Pardon</td>
									<td></td>
									<td>'.number_format($pardon_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr> 
							';
						// }
						
						$total_pardon_amount += $pardon_amount;

					}
				}
			}

		}
						
		//display loan
		$result .= 
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($total_arrears, 2).'</th>
				<td></td>
			</tr> 
		';

		$invoice_date = $this->get_max_invoice_date($lease_id);

		$account_invoice_amount = $this->get_invoice_tenants_brought_forward($lease_id,$invoice_date);
		$account_paid_amount = $this->get_paid_tenants_brought_forward($lease_id,$invoice_date,null);
		$account_todays_payment =  $this->get_today_tenants_paid_current_forward($lease_id,$invoice_date);
		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount;
		

		if($lease_id == 30)
		{
			// var_dump($account_invoice_amount); die();

		}
		


		$response['total_arrears'] = $total_arrears;
		$response['invoice_date'] = $invoice_date;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['total_pardon_amount'] = $total_pardon_amount;

		// var_dump($response); die();

		return $response;
	}

	public function get_all_invoice_month($lease_id,$type= null)
	{
		// var_dump($lease_id); die();
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND invoice_type <> 2';
			}
			else
			{
				$add_where = ' AND invoice_type = 2';
			}
		}
		else
		{
			$add_where ='';
		}
		$this->db->from('water_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice,invoice_month,invoice_year,invoice_date,invoice_id');
		$this->db->where('lease_id = '.$lease_id.' AND invoice_status = 1 AND invoice_date <> "0000-00-00" '.$add_where);
		$this->db->order_by('invoice_date','ASC');
		$this->db->group_by('invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_invoice_current_month($lease_id,$last_month,$last_year)
	{
		// var_dump($lease_id); die();

		//  get the dates correctly

		$invoice_date = $last_year.'-'.$last_month.'-'.date('d');

		// 'lease_id = '.$lease_id.' AND invoice_date <> "0000-00-00" AND invoice_month < "'.$last_month.'" AND invoice_year <= "'.$last_year.'" '
		$where = 'lease_id = '.$lease_id.' AND invoice_date <> "0000-00-00" AND invoice_date <= "'.$invoice_date.'" ';

		$this->db->from('water_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice,invoice_month,invoice_year,invoice_date');
		$this->db->where($where);

		$this->db->order_by('invoice_month','ASC');
		$this->db->group_by('invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_payments_lease($lease_id)
	{
		$this->db->from('water_payments');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.'  AND payment_status = 1 AND cancel = 0');
		$this->db->order_by('payment_date','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_pardons_lease($lease_id)
	{
		$this->db->from('water_pardon_payments');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.'  AND pardon_status = 1 AND pardon_delete = 0');
		$this->db->order_by('pardon_date','ASC');
		$query = $this->db->get();
		return $query;
	}

	// teannts
	public function get_invoice_tenants_brought_forward($lease_id, $invoice_date=NULL,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		if(!empty($invoice_date))
		{

			$date_add = ' AND invoice_date < "'.$invoice_date.'"';	
					
		}
		else
		{
			$date_add = '';
		}

		$where = 'invoice_status = 1 AND lease_id = '.$lease_id.' '.$date_add.' '.$add;
		// var_dump($where); die();

		
		$this->db->from('water_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_paid_tenants_brought_forward($lease_id,$invoice_date=NULL,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND water_payments.payment_id = water_payment_item.payment_id AND water_payment_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'water_payments,water_payment_item';
			$amount_cal = 'SUM(payment_item.amount_paid) AS total_paid';
		}
		else
		{
			$add = ' AND water_payments.payment_status = 1 AND cancel = 0 ';
			$table_add = 'water_payments';
			$amount_cal = 'SUM(water_payments.amount_paid) AS total_paid';
		}

		if(!empty($invoice_date))
		{
			$date_add = '  AND (water_payments.payment_date < "'.$invoice_date.'")';
		}
		else
		{
			$date_add = '';
		}
		

		$where = 'water_payments.lease_id = '.$lease_id.'   '.$date_add.' '.$add;

		

		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_today_tenants_paid_current_forward($lease_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND water_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'water_payments.lease_id = '.$lease_id.' AND water_payments.payment_id = water_payment_item.payment_id AND water_payments.payment_status = 1 AND cancel = 0 AND (water_payments.payment_date = "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('water_payment_item,water_payments');
		$this->db->select('SUM(water_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_latest_invoice_amount($lease_id,$year,$month)
	{

		$this->db->from('water_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where('lease_id = '.$lease_id.' AND invoice_month = "'.$month.'" AND invoice_year = "'.$year.'"');
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;

	}

	public function get_max_invoice_date($lease_id,$invoice_month = NULL,$invoice_year=NULL)
	{
		if($invoice_month != NULL || $invoice_year != NULL)
		{
			$add = 'AND invoice_month = "'.$invoice_month.'" AND invoice_year = '.$invoice_year.'';
		}
		else
		{
			$add = '';
		}
		$where = 'lease_id = '.$lease_id.' '.$add;

		$this->db->from('water_invoice');
		$this->db->select('MAX(invoice_date) AS invoice_date');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_date = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_date = $row->invoice_date;
		}
		return $invoice_date;
	}


	public function get_cummulated_balance($lease_id,$invoice_type_id)
	{
		$current_date = date('Y-m-d');

		$date = explode('-', $current_date);
		$month = $date[1];
		$year = $date[0];

		$this->db->from('water_invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$month.'" and invoice_year = '.$year.' AND lease_id = '.$lease_id.' AND invoice_type = '.$invoice_type_id);
		$this->db->order_by('invoice_month','DESC');
		$this->db->limit(1);
		$invoice_query2 = $this->db->get();

		$invoice_amount = 0;
		// var_dump($invoice_query2->num_rows());
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				$invoice_amount = $invoice_amount + $prev_key->invoice_amount;
			}
		}
		return $invoice_amount;
	}
	

	public function get_lease_payments($lease_id)
	{
		$this->db->where('leases.lease_id = water_payments.lease_id AND water_payments.lease_id = '.$lease_id.'  AND water_payments.cancel = 0');
		
		return $this->db->get('water_payments,leases');
	}
	public function get_lease_pardons($lease_id)
	{
		$this->db->where('leases.lease_id = water_pardon_payments.lease_id AND water_pardon_payments.lease_id = '.$lease_id.' AND leases.lease_status = 1 AND water_pardon_payments.pardon_delete = 0');
		
		return $this->db->get('water_pardon_payments,leases');
	}

	public function receipt_water_payment($lease_id,$personnel_id = NULL)
	{
		$amount = $this->input->post('amount_paid');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		
		
		
		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}

		// calculate the points to get 
		$payment_date = $this->input->post('payment_date');


		// $this->get_points_to_award($lease_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];

		$receipt_number = $this->create_receipt_number();
		
		
		$data = array(
			'payment_method_id'=>$payment_method,
			'amount_paid'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $receipt_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		if($type_of_account == 1)
		{
			$data['lease_id'] = $lease_id;

			if($this->db->insert('water_payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount);

				$service_charge_amount = $this->input->post('service_charge_amount');
				$water_amount = $this->input->post('water_amount');
				$rent_amount = $this->input->post('rent_amount');
				$penalty_fee = $this->input->post('penalty_fee');
				$fixed_charge = $this->input->post('fixed_charge');
				$bought_water = $this->input->post('bought_water');
				$insurance = $this->input->post('insurance');
				$sinking_funds = $this->input->post('sinking_funds');
				$painting_charge = $this->input->post('painting_charge');
				$fixed_charge = $this->input->post('fixed_charge');
				$deposit_charge = $this->input->post('deposit_charge');
				$legal_fees = $this->input->post('legal_fees');

				$invoice_number = $this->get_invoice_number(); 

				if(!empty($service_charge_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $service_charge_amount,
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}

				if(!empty($water_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $water_amount,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
					// update the invoice table 
					// using invoice_type_id
					// $invoice_id = $this->get_last_invoice_id(2);

					

				}
				if(!empty($rent_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $rent_amount,
										'invoice_type_id' => 1,
										'lease_id' => $lease_id,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);

					
				}
				if(!empty($penalty_fee))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $penalty_fee,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}
				if(!empty($fixed_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $fixed_charge,
										'invoice_type_id' => 12,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}
				if(!empty($deposit_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $deposit_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 13,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}

				if(!empty($bought_water))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $bought_water,
										'lease_id' => $lease_id,
										'invoice_type_id' => 10,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}
				if(!empty($sinking_funds))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $sinking_funds,
										'invoice_type_id' => 8,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}
				if(!empty($legal_fees))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $legal_fees,
										'lease_id' => $lease_id,
										'invoice_type_id' => 17,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('water_payment_item',$service);
				}


				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		
	}

	function create_receipt_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('water_payments');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}


	public function get_months_last_debt($lease_id,$month)
	{
		$where = 'lease_id = '.$lease_id.' AND payment_status = 1 AND month = "'.$month.'"';
		// var_dump($where);die();
		$this->db->from('water_payments');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	public function get_months_payments($lease_id,$month,$year)
	{
		$where = 'lease_id = '.$lease_id.' AND payment_status = 1 AND month = "'.$month.'" AND  year = "'.$year.'"';
	
		$this->db->from('water_payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	public function get_months_last_arrears($month,$year,$lease_id)
	{
		$where = 'invoice_month = "'.$month.'" and invoice_year = '.$year.' AND lease_id = '.$lease_id.'';
		// var_dump($where); die();
		$this->db->from('water_invoice');
		$this->db->select('*');
		$this->db->where($where);
		$invoice_query2 = $this->db->get();
		$arrears_bf = 0;
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				# code...
				$arrears_bf = $prev_key->arrears_bf;
			}
		}
		return $arrears_bf;
	}
	public function get_months_invoices($lease_id,$month)
	{
		$where = 'lease_id = '.$lease_id.' AND invoice_date <> "0000-00-00" AND invoice_month = "'.$month.'"';
		$this->db->from('water_invoice');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('invoice_date', 'ASC');
		$query = $this->db->get();
		return $query;
	}

	public function get_payments_invoice_types($payment_id)
	{
		$this->db->select("invoice_type.invoice_type_id,invoice_type.invoice_type_name");
		$this->db->where('invoice_type.invoice_type_id IN(SELECT invoice_type_id FROM water_payment_item WHERE payment_id = '.$payment_id.')');
		$query = $this->db->get('invoice_type');
		return $query;
	}

	public function get_payments_detail($payment_id,$invoice_type_id)
	{
		$this->db->select("amount_paid");
		$this->db->where('payment_id = '.$payment_id.'  AND invoice_type_id = '.$invoice_type_id.'');
		$query = $this->db->get('water_payment_item');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$amount_paid = $key->amount_paid;
			}
		}
		else
		{
			$amount_paid = 0;
		}
		return $amount_paid;

	}

	public function get_invoiced_amount($invoice_type_id)
	{
		$this->db->select("invoice_amount");
		$this->db->where('invoice_status = 1 AND invoice_type = '.$invoice_type_id.'');
		$this->db->order_by("invoice_id",'DESC');
		$this->db->limit(1);
		$query = $this->db->get('water_invoice');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_amount = $key->invoice_amount;
			}
		}
		else
		{
			$invoice_amount = 0;
		}
		return $invoice_amount;
	}
	public function get_payments_amount($invoice_type_id)
	{
		$this->db->select("SUM(amount_paid) AS amount_paid");
		$this->db->where('invoice_type_id = '.$invoice_type_id.'');
		$query = $this->db->get('water_payment_item');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$amount_paid = $key->amount_paid;
			}
		}
		else
		{
			$amount_paid = 0;
		}
		return $amount_paid;
	}

	public function get_total_invoices_before_payment($lease_id, $payment_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'lease_id = '.$lease_id.' AND invoice_date < "'.$payment_date.'"'.$add;
		// var_dump($where); die();

		
		$this->db->from('water_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_total_payments_before_payment_lease($lease_id, $payment_date,$invoice_type_id)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND water_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'cancel = 0 AND water_payments.lease_id = '.$lease_id.' AND water_payments.payment_date < "'.$payment_date.'"'.$add;
		// var_dump($where); die();

		
		$this->db->from('water_payments');
		$this->db->select('SUM(water_payments.amount_paid) AS total_payments');
		$this->db->where($where);
		$query = $this->db->get();
		$total_payments = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_payments = $row->total_payments;
		}
		return $total_payments;
	}

	public function get_payment_details($payment_id)
	{
		$this->db->where('leases.lease_id = water_payments.lease_id AND cancel = 0 AND water_payments.payment_id = '.$payment_id);
		
		return $this->db->get('water_payments, leases');
	}

	// public function get_lease_payments($lease_id)
	// {
	// 	$this->db->where('leases.lease_id = water_payments.lease_id AND water_payments.lease_id = '.$lease_id.' AND leases.lease_status = 1 AND water_payments.cancel = 0 AND water_payments.year <='.date('Y'));
		
	// 	return $this->db->get('water_payments,leases');
	// }

	public function receipt_pardon($lease_id)
	{
		$table = "water_pardon_payments";
		$prefix = "HA-PP-";
		$column_name = "document_number";

		$pardon_date = $this->input->post('pardon_date');
		$pardon_date_exp = explode('-', $pardon_date);
		$pardon_year = $pardon_date_exp[0];
		$pardon_month = $pardon_date_exp[1];


		$document_number = $this->create_document_numbers($table,$prefix,$column_name);
		$data = array(
			'pardon_date'=>$this->input->post('pardon_date'),
			'created_by'=>$this->session->userdata("personnel_id"),
			'document_number'=>$document_number,
			'year'=>$pardon_year,
			'month'=>$pardon_month,
			'pardon_reason'=>$this->input->post('pardon_reason'),
			'pardon_amount'=>$this->input->post('pardon_amount')
		);

		$data['lease_id'] = $lease_id;
		// var_dump($data); die();
		if($this->db->insert('water_pardon_payments', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function create_document_numbers($table,$preffix,$column_name)
	{
		//select product code
		$this->db->from(''.$table.'');
		$this->db->where("".$column_name." LIKE '".$preffix."%'");
		$this->db->select('MAX('.$column_name.') AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function cancel_payment($payment_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d H:i:s"),
			"cancel" => 1
		);
		
		$this->db->where('payment_id', $payment_id);
		if($this->db->update('water_payments', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}

	public function get_invoice_tenants_current_month_forward($lease_id, $invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date=null,$end_date=null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		if(!empty($start_date) AND !empty($end_date))
		{
			$invoice_date_checked = ' AND invoice_date >= "'.$start_date.'" AND invoice_date <= "'.$end_date.'" ';
		}
		else
		{
			$invoice_date_checked = ' AND invoice_month = "'.$invoice_month.'" AND invoice_year = "'.$invoice_year.'"';
		}
		// $ex
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'invoice_status = 1 AND lease_id = '.$lease_id.' '.$invoice_date_checked.' '.$add;
		// var_dump($where); die();

		
		$this->db->from('water_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_today_tenants_paid_current_month_forward($lease_id,$invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date = null,$end_date=null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND water_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		if(!empty($start_date) AND !empty($end_date))
		{
			$invoice_date_checked = ' AND water_payments.payment_date >= "'.$start_date.'" AND water_payments.payment_date <= "'.$end_date.'" ';
		}
		else
		{
			$invoice_date_checked = ' AND water_payments.month = "'.$invoice_month.'" AND water_payments.year = "'.$invoice_year.'"';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'water_payments.lease_id = '.$lease_id.' AND cancel = 0 '.$invoice_date_checked.' '.$add;

		//var_dump($where); die();

		$this->db->from('water_payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_lease_deposit_months($lease_id,$todays_month,$todays_year,$invoice_type_id)
	{
		$where = 'water_payments.lease_id = '.$lease_id.' AND  water_payments.payment_id = water_payment_item.payment_id AND water_payments.cancel = 0 AND water_payment_item.invoice_type_id = '.$invoice_type_id.' AND water_payments.year = '.$todays_year.' AND water_payments.month = "'.$todays_month.'" ';
		$this->db->from('water_payment_item,water_payments');
		$this->db->select('SUM(water_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;

	}

	public function get_water_active_lease($property_id,$rental_unit_id,$todays_month,$todays_year,$return_date=null,$property_invoice_id)
	{
		// if(!empty($return_date))
		// {
		// 	$addition = ' AND invoice_date >= "'.$todays_year.'-'.$todays_month.'-'.$return_date.'" ';
		// }
		// else
		// {
			$addition = '';
		// }
		$where = 'tenant_unit.rental_unit_id ='.$rental_unit_id.'
					AND leases.lease_id = water_management.lease_id
		 			AND property_invoice.property_invoice_id = water_management.property_invoice_id 
		 			AND property_invoice.property_invoice_id = '.$property_invoice_id.'
		 			AND leases.tenant_unit_id = tenant_unit.tenant_unit_id 
		 			AND tenants.tenant_id = tenant_unit.tenant_id 
		 			AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id
		 			AND water_invoice.lease_id = leases.lease_id  '.$addition;

		$this->db->from('water_invoice,tenant_unit,leases,property_invoice,water_management,tenants,rental_unit');


		// $where = 'tenants.tenant_id > 0 
		// 			AND tenants.tenant_id = tenant_unit.tenant_id 
		// 			AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  
		// 			AND tenant_unit.tenant_unit_id = leases.tenant_unit_id 
		// 			AND leases.lease_status = 1 
		// 			AND property.property_id = rental_unit.property_id 
		// 			AND property.property_id = '.$property_id.' 
		// 			AND property_invoice.property_id = property.property_id 
		// 			AND leases.lease_id = water_management.lease_id
		// 			AND property_invoice.property_invoice_id = water_management.property_invoice_id 
		// 			AND property_invoice.property_invoice_id = '.$property_invoice_id;
		// $table = 'tenants,tenant_unit,rental_unit,leases,property,property_invoice,water_management';


		$this->db->select('*');
		$this->db->where($where);
		$this->db->group_by('leases.lease_id');
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$query = $this->db->get('');
		$lease_id = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$lease_id = $value->lease_id;
		}

		return $lease_id;
	}

	public function get_initial_reading_payable($lease_id,$rental_unit_id)
	{


		$where = 'property_billing.invoice_type_id = 2 AND property_billing.property_billing_status = 1 AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id ='.$lease_id;


		$this->db->from('property_billing');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->limit(1);
		$this->db->group_by('property_billing.lease_id');
		$query = $this->db->get('');

		$initial_reading = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$initial_reading = $value->initial_reading;
		}

		return $initial_reading;


	}


	public function get_initial_opening_balance($lease_id,$rental_unit_id)
	{


		$where = 'property_billing.invoice_type_id = 2 AND property_billing.property_billing_status = 1 AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id ='.$lease_id;


		$this->db->from('property_billing');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->limit(1);
		$this->db->group_by('property_billing.lease_id');
		$query = $this->db->get('');

		$arrears_bf = 0;
		foreach ($query->result() as $key => $value) {
			# code...
			$arrears_bf = $value->arrears_bf;
		}

		return $arrears_bf;


	}

	function check_transaction_code($transaction_code)
	{
		$this->db->where('transaction_code = "'.$transaction_code.'" AND payment_status <= 2 AND cancel = 0');
		$query = $this->db->get('water_payments');

		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}
