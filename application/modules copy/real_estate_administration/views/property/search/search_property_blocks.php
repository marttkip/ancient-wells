<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
      <h2 class="panel-title pull-right"></h2>
      <h2 class="panel-title">Search</h2>
    </header>             

  <!-- Widget content -->
        <div class="panel-body">
  <?php
    echo form_open("search-property-blocks", array("class" => "form-horizontal"));
    ?>
    <div class="row">
        <div class="col-md-5">            
            <div class="form-group">
                <label class="col-lg-4 control-label">Property: </label>
                                        
              <div class="col-lg-8">                
                  <select class="form-control select2" name="property_id">
                        <option value="">-- Select property --</option>
                        <?php
                        if($properties->num_rows() > 0)
                        {
                            $property = $properties->result();
                            
                            foreach($property as $res)
                            {
                                $db_property = $res->property_id;
                                $property_name = $res->property_name;
                                echo '<option value="'.$db_property.'">'.$property_name.'</option>';                                
                            }
                        }
                    ?>
                    </select>
              </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label class="col-lg-4 control-label">Block Name: </label>
                                        
              <div class="col-lg-8">
                <input type="text" class="form-control" name="block_name" placeholder="Block Name" value="">
              </div>
            </div>
        </div>
        <div class="col-md-2">            
             <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <div class="center-align">
                      <button type="submit" class="btn btn-info">Search</button>
                  </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <?php
    echo form_close();
    ?>
  </div>
</section>