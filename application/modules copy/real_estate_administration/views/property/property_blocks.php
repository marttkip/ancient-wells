<?php echo $this->load->view('search/search_property_blocks','', true); ?>
<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a>Block Name</a></th>
						<th><a>Property Name</a></th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			
			foreach ($query->result() as $row)
			{
				$block_id = $row->block_id;
				$block_name = $row->block_name;
				$property_name = $row->property_name;
				
				//status
				
				
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$block_name.'</td>
						<td>'.$property_name.'</td>
						<td><a href="'.site_url().'edit-property-block/'.$block_id.'" class="btn btn-sm btn-success" title="Edit '.$block_name.'"><i class="fa fa-pencil"></i> Edit Block</a></td>
						<td><a href="'.site_url().'block-floors/'.$block_id.'" class="btn btn-sm btn-warning" title="Floors '.$block_name.'"><i class="fa fa-list"></i> Block Floors</a></td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no rental units added";
		}
?>

						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?> 
								<div class="pull-right" >
									<a style="margin-top:-5px" id="open_new_rental_unit" class="btn btn-sm btn-info" onclick="get_new_rental_unit()"><i class="fa fa-plus"></i> Add property block</a>
									<a style="display:none; margin-top:-5px" id="close_new_rental_unit" class="btn btn-sm btn-warning" onclick="close_new_rental_unit()"><i class="fa fa-plus"></i> Close property block</a>
								</div>
								</h2>
							</header>
							<div class="panel-body">
                            	<?php
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								$search =  $this->session->userdata('all_block_search');
								if(!empty($search))
								{
									echo '<a href="'.site_url().'close_search_property_blocks" class="btn btn-sm btn-warning">Close Search</a>';
								}
								?>
                            	
                                   <div style="display:none;" class="col-md-12" style="margin-bottom:20px;" id="new_rental_unit" >
                                	<section class="panel">
										<header class="panel-heading">
											<div class="panel-actions">
											</div>
											<h2 class="panel-title">Add a property block</h2>
										</header>
										<div class="panel-body">
											<div class="row" style="margin-bottom:20px;">
                                    			<div class="col-lg-12 col-sm-12 col-md-12">
                                    				<div class="row">
                                    				<?php echo form_open("add-property-block", array("class" => "form-horizontal", "role" => "form"));?>
	                                    				<div class="col-md-12">
	                                    					<div class="row">
		                                    					<div class="col-md-5">
			                                    					<div class="form-group">
															            <label class="col-lg-5 control-label">Block Name: </label>
															            
															            <div class="col-lg-7">
															            	<input type="text" class="form-control" name="block_name" placeholder="Block Name" value="">
															            </div>
															        </div>
															    </div>
															    <div class="col-md-5">
															    	<div class="form-group">
															            <label class="col-lg-5 control-label">Property Name: </label>
															            
															            <div class="col-lg-5">
															            	<select id='property_id' name='property_id' class='form-control custom-select '>
														                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
														                      <option value=''>None - Please Select a property</option>
														                      <?php echo $property_list;?>
														                    </select>
															            </div>
															        </div>
															    </div>
															</div>
														    <div class="row" style="margin-top:10px;">
																<div class="col-md-12">
															        <div class="form-actions center-align">
															            <button class="submit btn btn-primary" type="submit">
															                Add property block
															            </button>
															        </div>
															    </div>
															</div>
	                                    				</div>
	                                    				<?php echo form_close();?>
	                                    				<!-- end of form -->
	                                    			</div>

                                    				
                                    			</div>
                                    			
                                    		</div>
										</div>
									</section>
                                </div>
								
                                <div class="row" style="margin-bottom:20px;">
                                   
                                    <div class="col-lg-12">
                                    	<div class="table-responsive">
											<?php echo $result;?>
                                		</div>
                                    </div>
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>
<script type="text/javascript">
	$(function() {
	    $("#property_id").customselect();
	});
	$(document).ready(function(){
		$(function() {
			$("#property_id").customselect();
		});
	});
	function get_new_rental_unit(){

		var myTarget2 = document.getElementById("new_rental_unit");
		var button = document.getElementById("open_new_rental_unit");
		var button2 = document.getElementById("close_new_rental_unit");

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_new_rental_unit(){

		var myTarget2 = document.getElementById("new_rental_unit");
		var button = document.getElementById("open_new_rental_unit");
		var button2 = document.getElementById("close_new_rental_unit");

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
</script>