<?php
//personnel data
$paybill_code = set_value('paybill_code');
$invoice_type_id = set_value('invoice_type_id');
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?>
                    	<a href="<?php echo site_url();?>property-manager/property-paybills" class="btn btn-sm btn-info pull-right" style="margin-top:-5px;">Back to Paybills</a>

                    </h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
			
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
						<div class="row">
							
                             <div class="col-md-6">
						    <div class="form-group">
                                    <label class="col-md-5 control-label">Paybill Code</label>
                                    
                                    <div class="col-md-7">
						            	<input type="text" class="form-control" name="paybill_code" placeholder="Code" value="<?php echo $paybill_code;?>">
						            </div>
                                </div>
                               </div>

                               
		

                            <div class="col-md-6">
						    <div class="form-group">
                                    <label class="col-md-5 control-label">Paybill Service</label>
                                    
                                    <div class="col-md-7">
                                        <select class="form-control" name="invoice_type_id">
                                            <option value="">-- Select Paybill Invoice Type --</option>
                                            <?php
                                            if($invoice_services->num_rows() > 0)
                                            {
                                                $invoice_service = $invoice_services->result();
                                                
                                                foreach($invoice_service as $res)
                                                {
                                                    $db_invoice_service_id = $res->invoice_type_id;
                                                    $invoice_service_name = $res->invoice_type_name;
                                                    
                                                    if($db_invoice_service_id == $invoice_service_id)
                                                    {
                                                        echo '<option value="'.$db_invoice_service_id.'" selected>'.$invoice_service_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_invoice_service_id.'">'.$invoice_service_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                               </div>
                               </div>
						<div class="row" style="margin-top:10px;">
							<div class="col-md-12">
						        <div class="form-actions center-align">
						            <button class="submit btn btn-sm btn-primary" type="submit">
						                Add Paybill
						            </button>
						        </div>
						    </div>
						</div>
                    <?php echo form_close();?>
                </div>

            </section>