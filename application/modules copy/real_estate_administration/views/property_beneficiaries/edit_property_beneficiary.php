<?php
//personnel data
$row = $query->row();

$property_beneficiary_id = $row->property_beneficiary_id;
$property_beneficiary_id = $row->property_beneficiary_id;
$property_beneficiary_name = $row->property_beneficiary_name;
$property_beneficiary_email = $row->property_beneficiary_email;
$property_beneficiary_username = $row->property_beneficiary_username;
$created = $row->created;
$property_beneficiary_phone = $row->property_beneficiary_phone;
$property_beneficiary_status = $row->property_beneficiary_status;
$property_owner_id = $row->property_owner_id;


$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$property_beneficiary_name = set_value('property_beneficiary_name');
	$property_beneficiary_email = set_value('property_beneficiary_email');
	$property_beneficiary_phone = set_value('property_beneficiary_phone');
	$property_beneficiary_status = set_value('property_beneficiary_status');
	$property_beneficiary_username = set_value('property_beneficiary_username');

}
?>          
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<div class="row" style="margin-bottom:20px;">
            <div class="col-lg-12">
                <a href="<?php echo site_url();?>property-manager/beneficiaries/<?php echo $property_owner_id?>" class="btn btn-info pull-right">Back to property beneficiarys</a>
            </div>
        </div>
            
        <!-- Adding Errors -->
        <?php
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				
				$this->session->unset_userdata('error_message');
			}

			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{
				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}
        
			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{
				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}
        ?>
        
        <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
			<div class="row">
				<div class="col-md-6">
			      
			        <div class="form-group">
			            <label class="col-lg-5 control-label">Property beneficiary Name: </label>
			            
			            <div class="col-lg-7">
			            	<input type="text" class="form-control" name="property_beneficiary_name" placeholder="Names" value="<?php echo $property_beneficiary_name;?>">
			            </div>
			        </div>
			        
			        <div class="form-group">
			            <label class="col-lg-5 control-label">Property beneficiary Email: </label>
			            
			            <div class="col-lg-7">
			            	<input type="email" class="form-control" name="property_beneficiary_email" placeholder="Email" value="<?php echo $property_beneficiary_email;?>">
			            </div>
			        </div>
			        
			       
			        
			        
				</div>
			    
			    <div class="col-md-6">
			         <div class="form-group">
			            <label class="col-lg-5 control-label">Property beneficiary Phone: </label>
			            
			            <div class="col-lg-7">
			            	<input type="text" class="form-control" name="property_beneficiary_phone" placeholder="Phone number" value="<?php echo $property_beneficiary_phone;?>">
			            </div>
			        </div>
			        
			        <div class="form-group">
			            <label class="col-lg-5 control-label">Property beneficiary Username: </label>
			            
			            <div class="col-lg-7">
			            	<input type="text" class="form-control" name="property_beneficiary_username" placeholder="Username" value="<?php echo $property_beneficiary_username;?>">
			            </div>
			        </div>
			        
			        
			    </div>
			</div>
			<div class="row" style="margin-top:10px;">
				<div class="col-md-12">
			        <div class="form-actions center-align">
			            <button class="submit btn btn-primary" type="submit">
			                Edit Property beneficiary
			            </button>
			        </div>
			    </div>
			</div>
        <?php echo form_close();?>
    </div>
</section>