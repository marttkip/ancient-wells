<?php
// var_dump($query);
foreach ($query->result() as $key) {
	# code...
	$property_id_one = $key->property_id;
	$rental_unit_id = $key->rental_unit_id;
	$rental_unit_name = $key->rental_unit_name;
	$block_id = $key->block_id;
	$floor_id = $key->floor_id;
	$unit_capacity_id = $key->unit_capacity_id;
	$rental_unit_price = $key->rental_unit_price;
	$property_type_id = $key->property_type_id;
}


$properties = $this->property_model->get_active_property();
$rs8 = $properties->result();
$property_list = '';
foreach ($rs8 as $property_rs) :
	$property_id = $property_rs->property_id;
	$property_name = $property_rs->property_name;
	$property_location = $property_rs->property_location;
	
	if($property_id == $property_id_one)
	{
		  $property_list .="<option value='".$property_id."' selected>".$property_name." Location: ".$property_location."</option>";
	}
	else
	{
		  $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";
	}
  

endforeach;

// var_dump($property_id);die();

$properties = '';

if($property_type_id == 2)
{

	$block_where = 'block_id > 0  AND property_id ='.$property_id_one;
	$block_table = 'block';
	$block_select = '*';
	$block_item_select  = $this->dashboard_model->get_content($block_table, $block_where,$block_select);
	$blocks ='';
	if($block_item_select->num_rows() > 0)
	{
	    foreach ($block_item_select->result() as $key ) 
	    {
	        # code...
	        $db_block_id = $key->block_id;
	        $block_name = $key->block_name;
	        if($block_id == $db_block_id)
			{
	        	$blocks .= '<option value="'.$db_block_id.'" selected>'.$block_name.'</option>';
	        }
	        else
	        {
	        	$blocks .= '<option value="'.$db_block_id.'">'.$block_name.'</option>';
	        }
	    }
	}



	$floor_where = 'floor_id > 0 AND block.block_id = floor.block_id';
	$floor_table = 'floor,block';
	$floor_select = '*';
	$floor_item_select  = $this->dashboard_model->get_content($floor_table, $floor_where,$floor_select);
	$floors ='';
	if($floor_item_select->num_rows() > 0)
	{
	    foreach ($floor_item_select->result() as $key ) 
	    {
	        # code...
	        $db_floor_id = $key->floor_id;
	        $floor_name = $key->floor_name;
	        $block_name = $key->block_name;
	        if($floor_id == $db_floor_id)
			{
	        	$floors .= '<option value="'.$db_floor_id.'" selected>Block '.$block_name.' '.$floor_name.'</option>';
	        }
	        else
	        {
	        	$floors .= '<option value="'.$db_floor_id.'">Block '.$block_name.' - '.$floor_name.'</option>';
	        }
	    }
	}




	$unit_capacity_where = 'unit_capacity_id > 0 ';
	$unit_capacity_table = 'unit_capacity';
	$unit_capacity_select = '*';
	$unit_capacity_item_select  = $this->dashboard_model->get_content($unit_capacity_table, $unit_capacity_where,$unit_capacity_select);
	$unit_capacities ='';
	if($unit_capacity_item_select->num_rows() > 0)
	{
	    foreach ($unit_capacity_item_select->result() as $key ) 
	    {
	        # code...
	        $db_unit_capacity_id = $key->unit_capacity_id;
	        $unit_capacity_name = $key->unit_capacity_name;

	        if($unit_capacity_id == $db_unit_capacity_id)
			{
	        	$unit_capacities .= '<option value="'.$db_unit_capacity_id.'" selected>'.$unit_capacity_name.'</option>';
	        }
	        else
	        {
	        	$unit_capacities .= '<option value="'.$db_unit_capacity_id.'">'.$unit_capacity_name.'</option>';
	        }
	    }
	}
}


?>
<section class="panel">
	<header class="panel-heading">
		<div class="panel-actions">
		</div>
		<h2 class="panel-title">Add a rental unit</h2>
	</header>
	<div class="panel-body">
		<div class="row" style="margin-bottom:20px;">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="row">
				<?php echo form_open("edit-rental-unit/".$rental_unit_id, array("class" => "form-horizontal", "role" => "form"));?>
    				<div class="col-md-12">
    					<div class="row">
        					<div class="col-md-5">
            					<div class="form-group">
						            <label class="col-lg-5 control-label">Unit Name: </label>
						            
						            <div class="col-lg-7">
						            	<input type="text" class="form-control" name="rental_unit_name" placeholder="Rental Unit Name" value="<?php echo $rental_unit_name?>">
						            </div>
						        </div>
						    </div>
						    <div class="col-md-5">
						    	<div class="form-group">
						            <label class="col-lg-5 control-label">Property Name: </label>
						            
						            <div class="col-lg-5">
						            	<select id='property_id' name='property_id' class='form-control custom-select '>
					                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
					                      <option value=''>None - Please Select a property</option>
					                      <?php echo $property_list;?>
					                    </select>
						            </div>
						        </div>
						    </div>
						</div>
						<?php
						if($property_type_id == 2)
						{
							?>
							<br>
							<div class="row">
	        					<div class="col-md-5">
	            					<div class="form-group">
							            <label class="col-lg-5 control-label">Block Number: </label>
							            
							            <div class="col-lg-7">
							            	<select id='block_id' name='block_id' class='form-control custom-select '>
					                      		<option value=''>None - Please Select a block</option>
					                      		<?php echo $blocks;?>
					                    	</select>
							            </div>
							        </div>
							    </div>
							    <div class="col-md-5">
							    	<div class="form-group">
							            <label class="col-lg-5 control-label">Floor: </label>
							            
							            <div class="col-lg-5">
							            	<select id='floor_id' name='floor_id' class='form-control custom-select '>
					                      		<option value=''>None - Please Select a floor</option>
					                      		<?php echo $floors;?>
					                    	</select>
							            </div>
							        </div>
							    </div>
							</div>


							<?php


						}

						?>
						<br>
						<div class="row">
	        					
							    <div class="col-md-5">
							    	<div class="form-group">
							            <label class="col-lg-5 control-label">Unit capacity: </label>
							            
							            <div class="col-lg-5">
							            	<select id='unit_capacity_id' name='unit_capacity_id' class='form-control custom-select '>
						                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
						                      <option value=''>None - Please Select capacity</option>
						                      <?php echo $unit_capacities;?>
						                    </select>
							            </div>
							        </div>
							    </div>
							    <div class="col-md-5">
	            					<div class="form-group">
							            <label class="col-lg-5 control-label">Unit Price: </label>
							            
							            <div class="col-lg-7">
							            	<input type="text" class="form-control" name="rental_unit_price" placeholder="Rental Unit Price" value="<?php echo $rental_unit_price?>">
							            </div>
							        </div>
							    </div>
							</div>
					    <div class="row" style="margin-top:10px;">
							<div class="col-md-12">
						        <div class="form-actions center-align">
						            <button class="submit btn btn-primary" type="submit">
						                Edit rental unit
						            </button>
						        </div>
						    </div>
						</div>
    				</div>
    				<?php echo form_close();?>
    				<!-- end of form -->
    			</div>

				
			</div>
			
		</div>
	</div>
</section>