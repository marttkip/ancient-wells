<?php

class Paybill_model extends CI_Model 
{	

	public function get_all_paybills($table, $where, $per_page, $page)
	{
		//retrieve all property
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('paybill.paybill_code');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;

	}
	public function get_all_paybill()
	{
		//retrieve all property
		$this->db->from('paybill');
		$this->db->select('*');
		$this->db->where('deleted_on = 0');
		$this->db->order_by('paybill.paybill_code');
		$query = $this->db->get('');
		
		return $query;

	}
	public function get_all_invoice_service()
	{
		//retrieve all property
		$this->db->from('invoice_type');
		$this->db->select('*');
		$this->db->where('invoice_type_status = 1');
		$this->db->order_by('invoice_type.invoice_type_name');
		$query = $this->db->get('');
		
		return $query;
	}
	public function get_invoice_service($invoice_service_id)
	{
		$this->db->where('invoice_type_id = '.$invoice_service_id);
		$this->db->from('invoice_type');
		$this->db->select('invoice_type_name');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_service_name = $key->invoice_type_name;
				
				
			}

			return $invoice_service_name;
		}
	}
	
	/*
	*	Delete an existing property
	*	@param int $property_id
	*
	*/
	public function delete_paybill($paybill_id)
	{
		$data = array(
				'deleted_on' => 1
			);
		$this->db->where('paybill_id', $paybill_id);
		
		if($this->db->update('paybill', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_property($property_id)
	{
		$this->db->select('*');
		$this->db->where('property_id', $property_id);
		$query = $this->db->get('property');
		return $query;
	}
	/*
	*	Activate a deactivated property
	*	@param int $property_id
	*
	*/
	public function activate_property($property_id)
	{
		$data = array(
				'property_status' => 1
			);
		$this->db->where('property_id', $property_id);
		
		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated property
	*	@param int $property_id
	*
	*/
	public function deactivate_property($property_id)
	{
		$data = array(
				'property_status' => 0
			);
		$this->db->where('property_id', $property_id);
		
		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_active_property()
	{
  		$table = "property";
		$where = "property_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_property_name($property_id)
	{
		
		$table = "property";
		$where = "property_id = ".$property_id;
		
		$this->db->where($where);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$property_name =$key->property_name;
			}
			return $property_name;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_property_billings($table, $where, $per_page, $page)
	{
		//retrieve all property
		$this->db->from($table);
		$this->db->select('invoice_type.invoice_type_name,billing_schedule.billing_schedule_name, property.property_name,property_billing.*');
		$this->db->where($where);
		$this->db->order_by('property_billing.property_billing_id');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	/*
	*	Retrieve all community_group
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_active_list($table, $where, $order, $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');
		
		return $query;
	}

	public function add_billing($property_id)
	{
		// check if there is data already added
		$charge_to = $this->input->post('charge_to');
		$invoice_type_id = $this->input->post('invoice_type_id');
		$billing_schedule_id = $this->input->post('billing_schedule_id');
		$amount = $this->input->post('amount');
		$where_array = array(
								'charge_to'=>$charge_to,
								'invoice_type_id' => $invoice_type_id,
								'billing_schedule_id' => $billing_schedule_id,
								'property_id' => $property_id,
							);
		$this->db->where($where_array);
		$query = $this->db->get('property_billing');
		
		if($query->num_rows() > 0)
		{
			$this->db->where($where_array);
			$where_array['billing_amount'] = $amount;
			if($this->db->update('property_billing',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//  do an insert
			
			$where_array['billing_amount'] = $amount;
			if($this->db->insert('property_billing',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		

	}
	public function get_property_invoicing($table, $where,$per_page ,$page, $order='invoice_structure.invoice_structure_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');
		
		return $query;
	}
	public function add_property_invoicing($property_id)
	{
		// check if there is data already added
		$charge_to = $this->input->post('charge_to');
		$invoice_structure_id = $this->input->post('invoice_structure_id');
		$where_array = array(
								'charge_to'=>$charge_to,
								'invoice_structure_id' => $invoice_structure_id,
								'property_id' => $property_id,
							);
		$this->db->where($where_array);
		$query = $this->db->get('property_invoice_structure');
		
		if($query->num_rows() > 0)
		{
			$this->db->where($where_array);
			if($this->db->update('property_invoice_structure',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//  do an insert
			
			if($this->db->insert('property_invoice_structure',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		

	}
	
	public function get_months()
	{
		return $this->db->get('month');
	}

	public function get_active_properties()
	{
		$this->db->select('*');
		$this->db->where('property_id > 0');
		$query = $this->db->get('property');
		return $query;
	}

	public function get_all_property_types()
	{
		$this->db->from('property_type');
		$this->db->select('*');
		$this->db->where('property_type_id > 0');
		$query = $this->db->get();
		
		return $query;
	}

	public function get_all_property_blocks($table, $where, $per_page, $page)
	{
		
		//retrieve all rental_unit
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('block.block_name');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_property_block()
	{
		$table = "block";
		$where = "property_id = ".$this->input->post("property_id")." AND block_name = '".$this->input->post("block_name")."'";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			$data2 = array(
				'block_name'=>$this->input->post("block_name"),
				'property_id'=>$this->input->post("property_id")
			);
			
			$table = "block";
			if($this->db->insert($table, $data2))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}

	public function add_property_block_floor($block_id)
	{
		$table = "floor";
		$where = "block_id = ".$block_id." AND floor_name = '".$this->input->post("floor_name")."'";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			$data2 = array(
				'floor_name'=>$this->input->post("floor_name"),
				'block_id'=>$block_id
			);
			
			$table = "floor";
			if($this->db->insert($table, $data2))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
}
