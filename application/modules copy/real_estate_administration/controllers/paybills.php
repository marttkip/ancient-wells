<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Paybills extends admin {
	var $property_path;
	var $property_location;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('real_estate_administration/property_owners_model');
		$this->load->model('real_estate_administration/paybill_model');
		$this->load->model('admin/admin_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('real_estate_administration/tenants_model');	
		$this->load->model('administration/reports_model');
		$this->load->model('accounts/accounts_model');	
		$this->load->library('image_lib');
		
		
		//path to image directory
		$this->property_path = realpath(APPPATH . '../assets/property');
		$this->property_location = base_url().'assets/property/';
	}
    
	/*
	*
	*	Default action is to show all the registered property
	*
	*/
	public function index() 
	{
		$where = 'paybill_id > 0 and deleted_on = 0';
		$table = 'paybill';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'property-manager/property-paybill';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->paybill_model->get_all_paybills($table, $where, $config["per_page"], $page);

		
		
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			
			$v_data['title'] = 'All Paybills';
			
			$data['content'] = $this->load->view('paybill/all_paybills', $v_data, true);
		
		$data['title'] = 'All Paybills';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	
	function add_paybill()
	{
		
		$paybill_error = $this->session->userdata('paybill_error_message');
		
		$this->form_validation->set_rules('paybill_code', 'Paybill Code', 'required|xss_clean|trim|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice', 'required|xss_clean|trim|xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($paybill_error))
			{
				$data2 = array(
					'paybill_code'=>$this->input->post("paybill_code"),
					'paybill_invoice'=>$this->input->post("invoice_type_id"),
					'paybill_status'=> 1
				);
				
				//  add paybill 
				$table = "paybill";
				$this->db->insert($table, $data2);

				$paybill_id = $this->db->insert_id();

				// $this->create_paybill_units($paybill_id,$this->input->post("total_units"));

				$this->session->unset_userdata('paybill_error_message');
				$this->session->set_userdata('success_message', 'paybill has been added');
				
				redirect('property-manager/property-paybills');
			}
		}
		$v_data['invoice_services'] = $this->paybill_model->get_all_invoice_service();
		$v_data['title'] = 'Add Paybill';
		$data['title'] = 'Add Paybill';
		$data['content'] = $this->load->view("paybill/add_paybill", $v_data, TRUE);
		
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	function delete_paybill($paybill_id)
	{
		
		if($this->paybill_model->delete_paybill($paybill_id))
		{
			$this->session->set_userdata('success_message', 'paybill has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'paybill could not be deleted');
		}
		redirect('property-manager/property-paybills');
	}
	function allocate_paybill($property_id)
	{
		
		$paybill_error = $this->session->userdata('paybill_error_message');
		
		$this->form_validation->set_rules('paybill_id', 'Paybill Code', 'required|xss_clean|trim|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice', 'required|xss_clean|trim|xss_clean');

		if ($this->form_validation->run())
		{	
			if(empty($paybill_error))
			{
				$data2 = array(
					'paybill_id'=>$this->input->post("paybill_id"),
					'paybill_invoice_id'=>$this->input->post("invoice_type_id"),
					'property_id' => $property_id,
					'property_paybill_status'=> 1
				);
				
				//  add paybill 
				$table = "property_paybill";
				$this->db->insert($table, $data2);

				$paybill_id = $this->db->insert_id();

				// $this->create_paybill_units($paybill_id,$this->input->post("total_units"));

				$this->session->unset_userdata('paybill_error_message');
				$this->session->set_userdata('success_message', 'paybill has been added');
				
				redirect('property-manager/properties');
			}
		}
		$v_data['invoice_services'] = $this->paybill_model->get_all_invoice_service();
		$v_data['paybills'] = $this->paybill_model->get_all_paybill();
		$v_data['title'] = 'Add Property Paybill';
		$data['title'] = 'Add Property Paybill';
		$data['content'] = $this->load->view("paybill/add_property_paybills", $v_data, TRUE);
		
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	
}