<?php 
	
	$contacts = $this->site_model->get_contacts();
	$branches = $this->auth_model->get_branches();

	
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}


	$personnel_password = "";
	$personnel_username = "";
?>
<section class="body-sign" style="height: 60vh !important;">
    <div class="center-sign">
		<a href="<?php echo site_url().'login';?>" class="logo pull-left">
			<img src="<?php echo base_url().'assets/logo/'.$logo;?>" height="35" alt="<?php echo $company_name;?>" class="img-responsive" />
		</a>

		<div class="panel panel-sign">
			<div class="panel-title-sign mt-xl text-right">
				<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-personnel mr-xs"></i> <?php echo $company_name;?> confirmation</h2>
			</div>
			<div class="panel-body">
				<?php
					$login_error = $this->session->userdata('login_error');
					$this->session->unset_userdata('login_error');
					
					if(!empty($login_error))
					{
						echo '<div class="alert alert-danger">'.$login_error.'</div>';
					}

					if($page == 0)
					{
						?>
						<form action="" method="post" id="confirm-authenitcation">
	                	<?php

					}
					else if($page == 1)
					{
						?>
							<form action="" method="post" id="confirm-authenitcation-authorise">
                		<?php
					}
					else if($page == 2)
					{
						?>
							<form action="" method="post" id="confirm-authenitcation-payment">
                		<?php
					}
					else if($page == 3)
					{
						?>
							<form action="" method="post" id="confirm-authenitcation-decline">
                		<?php
					}
					
						//case of an input error
						if(!empty($personnel_username_error))
						{
							?>
                            <div class="form-group mb-lg has-error">
                                <label>Username</label>
                                <div class="input-group input-group-icon">
                                    <input name="personnel_username" type="text" class="form-control input-lg" value="<?php echo $personnel_username;?>" required/>
                                    <label for="personnel_username" class="error"><?php echo $personnel_username_error;?></label>
                                    <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
							<?php
						}
						
						else
						{
							?>
                            <div class="form-group mb-lg">
                                <label>Username</label>
                                <div class="input-group input-group-icon">
                                    <input name="personnel_username" type="text" class="form-control input-lg" value="<?php echo $personnel_username;?>" autocomplete="off" required/>
                                    <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
							<?php
						}
					?>
                    
                    <?php
						//case of an input error
						if(!empty($personnel_password_error))
						{
							?>
                            <div class="form-group mb-lg has-error">
                                <div class="clearfix">
                                    <label class="pull-left">Password</label>
                                    <a href="#" class="pull-right"> Lost Password?</a>
                                </div>
                                <div class="input-group input-group-icon">
                                    <input name="personnel_password" type="password" class="form-control input-lg" value="<?php echo $personnel_password;?>" autocomplete="off" />
                                    <label for="personnel_username" class="error"><?php echo $personnel_username_error;?></label>
                                    <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
							<?php
						}
						
						else
						{
							?>
							<div class="form-group mb-lg">
                                <div class="clearfix">
                                    <label class="pull-left">Password</label>
                                    <!-- <a href="#" class="pull-right"> Lost Password?</a> -->
                                </div>
                                <div class="input-group input-group-icon">
                                    <input name="personnel_password" type="password" class="form-control input-lg" value="<?php echo $personnel_password;?>" required/>
                                    <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
							<?php
						}

						
					?>
					<!-- <input type="text" name="url" id="url" value="<?php echo $this->uri->uri_string();?>"> -->

					<div class="row">
						
						<div class="col-sm-4 text-right">
							<!-- <button type="submit" class="btn btn-warning hidden-xs">CONFIRM NA</button> -->
							<button type="submit" class="btn btn-primary hidden-xs">CONFIRM USER</button>
							<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">CONFIRM</button>
						</div>
					</div>

				</form>
			</div>
		</div>

	</div>
</section>
<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        	
				        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        	
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>