<div class="row">
	
	<?php echo $this->load->view('search/summary_search','', true);?>

	<section class="panel panel-featured panel-featured-info">
		<header class="panel-heading">
			<h2 class="panel-title"><?php echo $title;?></h2>
		</header>             

		<!-- Widget content -->
	    <div class="panel-body">
	    	<?php
	    	$search =  $this->session->userdata('summaries_search');
			if($search)
			{
				echo '<a href="'.site_url().'financials/company_financial/close_landlord_summaries" class="btn btn-sm btn-warning">Close Search</a>
					<a href="'.site_url().'financials/company_financial/print_landlord_summaries" target="_blank" class="btn btn-sm btn-success pull-right"> Print Items</a>';
			}


			$search =  $this->session->userdata('summaries_search');
			if($search)
			{
		    	$property_rs = $this->reports_model->get_active_property_owner_properties();
		    	$start_date = $this->session->userdata('transaction_date_from');
		    	$end_date = $this->session->userdata('transaction_date_to');

		    	$property_items = '';
		    	if($property_rs->num_rows() > 0)
		    	{
		    		foreach ($property_rs->result() as $key => $value) {
		    			# code...
		    			$property_id = $value->property_id;
		    			$property_name = $value->property_name;
		    			$manager_percent = $value->manager_percent;

		    			$amount = $this->reports_model->get_property_owner_return($property_id,$start_date,$end_date,$manager_percent);

		    			$property_items .= ' <tr>
					                            <td>'.strtoupper($property_name).'</td>
					                            <td>'.number_format($amount,2).'</td>
					                        </tr>';
		    		}
		    	}

		    	
		    	$transfers_rs = $this->reports_model->get_landload_payments();

				$result = '';
				$total_transfers = 0;
				// var_dump($query); die();
				if($transfers_rs->num_rows() > 0)
				{
				 $x=0;
				    foreach ($transfers_rs->result() as $key => $value) {
				        # code...
				        $account_from_id = $value->account_from_id;
				        $account_to_type = $value->account_to_type;
				        $account_to_id = $value->account_to_id;
				        $receipt_number = $value->receipt_number;
				        $account_payment_id = $value->account_payment_id;
				        $account_payment_description = $value->account_payment_description;
				         $payment_date = $value->payment_date;
				         $created = $value->created;
				        $amount_paid = $value->amount_paid;
				        $payment_to = $value->payment_to;

				        $account_from_name = $this->transfer_model->get_account_name($account_from_id);
				        if($account_to_type == 1)
				        {
				            $payment_type = 'Transfer';
				            $account_to_name = $this->transfer_model->get_account_name($account_to_id);
				        }
				        else if($account_to_type == 3)
				        {
				            // doctor payments
				            $payment_type = "Landlord Payment";
				            $account_to_name = $this->transfer_model->get_owner_name($payment_to);
				        }
				        else if($account_to_type == 2)
				        {
				            // creditor
				            $payment_type = "Creditor Payment";
				            $account_to_name = $this->transfer_model->get_creditor_name($account_to_id);
				        }
				        else if($account_to_type == 4)
				        {
				            // expense account
				            $payment_type = "Direct Expense Payment";
				            $account_to_name = $this->transfer_model->get_account_name($account_to_id);
				        }


				     

				        $x++;
				        $total_transfers += $amount_paid;
				        $result .= '<tr>
				                        <td>'.$x.'</td>
				                        <td>'.date('jS M Y',strtotime($payment_date)).'</td>
				                        <td>'.strtoupper($receipt_number).'</td>
				                        <td>'.$account_payment_description.' - '.$account_from_name.'</td>
				                        <td>'.number_format($amount_paid,2).'</td>
				                    </tr>';

				    }


				    $result .= '<tr>
				                        <th colspan="4">Total</th>
				                        
				                        <th>'.number_format($total_transfers,2).'</th>
				                    </tr>';
				}



				// expenses 

				$expenses_rs = $this->reports_model->get_landload_expenses();

				$expense_result = '';
				$total_expense_amount = 0;
				// var_dump($query); die();
				if($expenses_rs->num_rows() > 0)
				{
				 	$y=0;
				    foreach ($expenses_rs->result() as $key => $value_expense) {

				    	$finance_purchase_amount = $value_expense->finance_purchase_amount;
				    	$finance_purchase_description = $value_expense->finance_purchase_description;
				    	$account_from_id = $value_expense->account_from_id;
				    	$transaction_date = $value_expense->transaction_date;
				    	$transaction_number = $value_expense->transaction_number;
				    	$account_from_name = $this->transfer_model->get_account_name($account_from_id);
				    	$y++;
				    	$total_expense_amount += $finance_purchase_amount;
				    	$expense_result .= '<tr>
					                        <td>'.$y.'</td>
					                        <td>'.date('jS M Y',strtotime($transaction_date)).'</td>
					                        <td>'.strtoupper($transaction_number).'</td>
					                        <td>'.$finance_purchase_description.' - '.$account_from_name.'</td>
					                        <td>'.number_format($finance_purchase_amount,2).'</td>
					                    </tr>';
				    }

				    $expense_result .= '<tr>
				                        <th colspan="4">Total</th>
				                        
				                        <th>'.number_format($total_expense_amount,2).'</th>
				                    </tr>';
				}

			}
			else
			{
				$property_items = '';
				$result = '';
				$expense_result = '';
			}
	    	?>
	    	<div class="row">
	    		<div class="col-md-3">
	    			<h3>Income Breakdown</h3>
	                <table class="table table-striped table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>Property</th>
	                            <th>Period Income (Ksh.)</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php echo $property_items;?>
	                    </tbody>
	                </table>
	    			
	    		</div>
	    		<div class="col-md-5">

	    			<h3>Direct Payments</h3>
	                <table class="table table-striped table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>#</th>
	                        	<th>Date</th>
	                        	<th>Transaction Code</th>
	                        	<th>Description</th>
	                            <th>Amount  (Ksh.)</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                       <?php echo $result;?>
	                       
	                    </tbody>
	                </table>
	    			
	    		</div>
	    		<div class="col-md-4">

	    			<h3>Expenses (Petty Cash)</h3>
	                <table class="table table-striped table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>#</th>
	                        	<th>Date</th>
	                        	<th>Transaction Code</th>
	                        	<th>Description</th>
	                            <th>Amount  (Ksh.)</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                       
	                       <?php echo $expense_result;?>
	                    </tbody>
	                </table>
	    			
	    		</div>
	    		
	    	</div>

	    </div>
	</section>

</div>
