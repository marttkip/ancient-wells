<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> </title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			
			.col-md-6 {
			    width: 50%;
			 }
		
			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:12px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			/*th, td {
			    border: 1px solid #000 !important;
			    padding: 0.5em 1em !important;
			}
			thead tr:first-child th:first-child {
			    border-radius: 10px 0 0 0 !important;
			}
			thead tr:first-child th:last-child {
			    border-radius: 0 10px 0 0 !important;
			}
			tbody tr:last-child td:first-child {
			    border-radius: 0 0 0 10px !important;
			}
			tbody tr:last-child td:last-child {
			    border-radius: 0 0 10px 10px !important;
			}
			tbody tr:first-child td:first-child {
			    border-radius: 10px 10px 0 0 !important;
			    border-bottom: 0px solid #000 !important;
			}*/
			.padd
			{
				margin:10px;
			}
			
		</style>
    </head>
    <body class="receipt_spacing">
	        <div class="padd">
	        	<div class="col-print-12">
			    	<div class="col-print-6">
			    		<h4><?php echo $contacts['company_name'];?> </h4>
			        </div>
			        <div class="col-print-6">
			        	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
			        </div>



			    </div>

			    <div class="col-print-12 center-align receipt_bottom_border">
	        
		            <h4>

		            	<?php
		            		$search =  $this->session->userdata('summaries_search');
							if($search)
							{
								echo 'Landlord Summary '.$this->session->userdata('summary_search_title');
							}
							else
							{
								echo  'Landlord Summary Statement';
							}
		            	?>
		           	</h4>
		               
		          
		        </div>
		        <div class="col-print-12">
		        <?php

		        $search =  $this->session->userdata('summaries_search');
				if($search)
				{
			    	$property_rs = $this->reports_model->get_active_property_owner_properties();
			    	$start_date = $this->session->userdata('transaction_date_from');
			    	$end_date = $this->session->userdata('transaction_date_to');

			    	$property_items = '';
			    	if($property_rs->num_rows() > 0)
			    	{
			    		foreach ($property_rs->result() as $key => $value) {
			    			# code...
			    			$property_id = $value->property_id;
			    			$property_name = $value->property_name;
			    			$manager_percent = $value->manager_percent;

			    			$amount = $this->reports_model->get_property_owner_return($property_id,$start_date,$end_date,$manager_percent);

			    			$property_items .= ' <tr>
						                            <td>'.strtoupper($property_name).'</td>
						                            <td>'.number_format($amount,2).'</td>
						                        </tr>';
			    		}
			    	}

			    	
			    	$transfers_rs = $this->reports_model->get_landload_payments();

					$result = '';
					$total_transfers = 0;
					// var_dump($query); die();
					if($transfers_rs->num_rows() > 0)
					{
					 $x=0;
					    foreach ($transfers_rs->result() as $key => $value) {
					        # code...
					        $account_from_id = $value->account_from_id;
					        $account_to_type = $value->account_to_type;
					        $account_to_id = $value->account_to_id;
					        $receipt_number = $value->receipt_number;
					        $account_payment_id = $value->account_payment_id;
					        $account_payment_description = $value->account_payment_description;
					         $payment_date = $value->payment_date;
					         $created = $value->created;
					        $amount_paid = $value->amount_paid;
					        $payment_to = $value->payment_to;

					        $account_from_name = $this->transfer_model->get_account_name($account_from_id);
					        if($account_to_type == 1)
					        {
					            $payment_type = 'Transfer';
					            $account_to_name = $this->transfer_model->get_account_name($account_to_id);
					        }
					        else if($account_to_type == 3)
					        {
					            // doctor payments
					            $payment_type = "Landlord Payment";
					            $account_to_name = $this->transfer_model->get_owner_name($payment_to);
					        }
					        else if($account_to_type == 2)
					        {
					            // creditor
					            $payment_type = "Creditor Payment";
					            $account_to_name = $this->transfer_model->get_creditor_name($account_to_id);
					        }
					        else if($account_to_type == 4)
					        {
					            // expense account
					            $payment_type = "Direct Expense Payment";
					            $account_to_name = $this->transfer_model->get_account_name($account_to_id);
					        }


					     

					        $x++;
					        $total_transfers += $amount_paid;
					        $result .= '<tr>
					                        <td>'.$x.'</td>
					                        <td>'.date('jS M Y',strtotime($payment_date)).'</td>
					                        <td>'.strtoupper($receipt_number).'</td>
					                        <td>'.$account_payment_description.' - '.$account_from_name.'</td>
					                        <td>'.number_format($amount_paid,2).'</td>
					                    </tr>';

					    }


					    $result .= '<tr>
					                        <th colspan="4">Total</th>
					                        
					                        <th>'.number_format($total_transfers,2).'</th>
					                    </tr>';
					}



					// expenses 

					$expenses_rs = $this->reports_model->get_landload_expenses();

					$expense_result = '';
					$total_expense_amount = 0;
					// var_dump($query); die();
					if($expenses_rs->num_rows() > 0)
					{
					 	$y=0;
					    foreach ($expenses_rs->result() as $key => $value_expense) {

					    	$finance_purchase_amount = $value_expense->finance_purchase_amount;
					    	$finance_purchase_description = $value_expense->finance_purchase_description;
					    	$account_from_id = $value_expense->account_from_id;
					    	$transaction_date = $value_expense->transaction_date;
					    	$transaction_number = $value_expense->transaction_number;
					    	$account_from_name = $this->transfer_model->get_account_name($account_from_id);
					    	$y++;
					    	$total_expense_amount += $finance_purchase_amount;
					    	$expense_result .= '<tr>
						                        <td>'.$y.'</td>
						                        <td>'.date('jS M Y',strtotime($transaction_date)).'</td>
						                        <td>'.strtoupper($transaction_number).'</td>
						                        <td>'.$finance_purchase_description.' - '.$account_from_name.'</td>
						                        <td>'.number_format($finance_purchase_amount,2).'</td>
						                    </tr>';
					    }

					    $expense_result .= '<tr>
					                        <th colspan="4">Total</th>
					                        
					                        <th>'.number_format($total_expense_amount,2).'</th>
					                    </tr>';
					}

				}
				else
				{
					$property_items = '';
					$result = '';
					$expense_result = '';
				}
	    	?>

		        <div class="col-md-12">
	    			<h4>Income Breakdown</h4>
	                <table class="table table-striped table-bordered table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>Property</th>
	                            <th>Period Income (Ksh.)</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php echo $property_items;?>
	                    </tbody>
	                </table>
	    			
	    		</div>
	    		<div class="col-md-12">

	    			<h4>Direct Payments</h4>
	                <table class="table table-striped table-bordered table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>#</th>
	                        	<th>Date</th>
	                        	<th>Transaction Code</th>
	                        	<th>Description</th>
	                            <th>Amount  (Ksh.)</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                       <?php echo $result;?>
	                       
	                    </tbody>
	                </table>
	    			
	    		</div>
	    		<div class="col-md-12">

	    			<h4>Expenses (Petty Cash)</h4>
	                <table class="table table-striped table-bordered table-hover table-condensed">
	                	<thead>
	                    	<tr>
	                        	<th>#</th>
	                        	<th>Date</th>
	                        	<th>Transaction Code</th>
	                        	<th>Description</th>
	                            <th>Amount  (Ksh.)</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                       
	                       <?php echo $expense_result;?>
	                    </tbody>
	                </table>
	    			
	    		</div>
		        </div>

		        
			    	
		        
		    </div>
		      <!-- Patient Details -->
	    	

		</body>
    </html>