<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/admin/controllers/admin.php";
error_reporting(0);
class Company_financial extends admin
{

    var $documents_path;


	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('financials_model');
        $this->load->model('company_financial_model');
         $this->load->model('finance/transfer_model');
    	$this->load->model('admin/users_model');
    	$this->load->model('real_estate_administration/property_model');



		$this->load->model('admin/file_model');

		//path to image directory
		$this->documents_path = realpath(APPPATH . '../assets/documents/vehicles');


		$this->load->library('image_lib');

		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}


  /*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index()
	{

		$data['title'] = 'Company Financials';
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('financials/financials/landing', $v_data, true);

		// $this->load->view('admin/templates/general_page', $data);
    // $data['content'] = $this->load->view('finance/transfer/write_cheques', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
	}

	public function profit_and_loss()
	{
		$data['title'] = 'Profit and Loss';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/financials/profit_and_loss', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}

	public function balance_sheet()
	{
		$data['title'] = 'Balance Sheet';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/financials/balance_sheet', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}
	public function aged_receivables()
	{
		$data['title'] = 'Aged Receivables';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/receivables/aged_receivables', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}
	public function sales_taxes()
	{
		$data['title'] = 'Sales Taxes';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/taxes/sales_taxes', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function customers_income()
	{
		$data['title'] = 'Income By Customer';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/income/customer_income', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function vendor_expenses()
	{
		$data['title'] = 'Vendor Expenses';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/vendors/vendor_expenses', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function aged_payables()
	{
		$data['title'] = 'Vendor Expenses';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/vendors/aged_payables', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}
	public function general_ledger()
	{
		$data['title'] = 'General Ledger';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/other/general_ledger', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function account_transactions()
	{
		$data['title'] = 'General Ledger';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/other/account_transactions', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

  public function mpesa()
  {
      $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      $credentials = base64_encode('wE2XiEqWDIwOH94xqtABTJGRgVGQuP04:xzNBaRpi61SdFUcL');
      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
      curl_setopt($curl, CURLOPT_HEADER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $curl_response = curl_exec($curl);

      echo json_decode($curl_response);

  }



  public function creditor_statement($creditor_id)
  {

  	$where = 'creditor_account.transaction_type_id = transaction_type.transaction_type_id AND creditor_account.creditor_account_delete = 0 AND creditor_account.creditor_id = '.$creditor_id;
	$table = 'creditor_account, transaction_type';

	if(!empty($date_from) && !empty($date_to))
	{
		$where .= ' AND (creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= \''.$date_to.'\')';
		$search_title = 'Statement from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
	}

	else if(!empty($date_from))
	{
		$where .= ' AND creditor_account.creditor_account_date = \''.$date_from.'\'';
		$search_title = 'Statement of '.date('jS M Y', strtotime($date_from)).' ';
	}

	else if(!empty($date_to))
	{
		$where .= ' AND creditor_account.creditor_account_date = \''.$date_to.'\'';
		$search_title = 'Statement of '.date('jS M Y', strtotime($date_to)).' ';
		$date_from = $date_to;
	}

	else
	{
		// $where .= ' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%m\') = \''.date('m').'\' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%Y\') = \''.date('Y').'\'';
		$where .= '';
		$search_title = 'Statement for the month of '.date('M Y').' ';
		$date_from = date('Y-m-d');
	}
	// echo $where;die();
	$v_data['balance_brought_forward'] = 0;// $this->creditors_model->calculate_balance_brought_forward($date_from,$creditor_id);
	$creditor = $this->company_financial_model->get_creditor($creditor_id);
	$row = $creditor->row();
	$creditor_name = $row->creditor_name;
	$opening_balance = $row->opening_balance;
	$debit_id = $row->debit_id;
	$start_date = $row->start_date;
	// var_dump($opening_balance); die();
	$v_data['module'] = 1;
	$v_data['creditor_name'] = $creditor_name;
	$v_data['creditor_id'] = $creditor_id;
	$v_data['opening_balance'] = $opening_balance;
	$v_data['debit_id'] = $debit_id;


  	// $v_data['query'] = $this->company_financial_model->get_creditor_account($where, $table);
	$v_data['title'] = $creditor_name.' '.$search_title;
	$data['title'] = $creditor_name.' Statement';
	$data['content'] = $this->load->view('vendors/creditor_statement', $v_data, TRUE);
	$this->load->view('admin/templates/general_page', $data);
  }
  public function print_creditor_statement($creditor_id)
  {
    $where = 'creditor_account.transaction_type_id = transaction_type.transaction_type_id AND creditor_account.creditor_account_delete = 0 AND creditor_account.creditor_id = '.$creditor_id;
  	$table = 'creditor_account, transaction_type';

  	if(!empty($date_from) && !empty($date_to))
  	{
  		$where .= ' AND (creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= \''.$date_to.'\')';
  		$search_title = 'Statement from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
  	}

  	else if(!empty($date_from))
  	{
  		$where .= ' AND creditor_account.creditor_account_date = \''.$date_from.'\'';
  		$search_title = 'Statement of '.date('jS M Y', strtotime($date_from)).' ';
  	}

  	else if(!empty($date_to))
  	{
  		$where .= ' AND creditor_account.creditor_account_date = \''.$date_to.'\'';
  		$search_title = 'Statement of '.date('jS M Y', strtotime($date_to)).' ';
  		$date_from = $date_to;
  	}

  	else
  	{
  		// $where .= ' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%m\') = \''.date('m').'\' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%Y\') = \''.date('Y').'\'';
  		$where .= '';
  		$search_title = 'Statement for the month of '.date('M Y').' ';
  		$date_from = date('Y-m-d');
  	}
  	// echo $where;die();
  	$v_data['balance_brought_forward'] = 0;// $this->creditors_model->calculate_balance_brought_forward($date_from,$creditor_id);
    // var_dump($where);
    $creditor = $this->company_financial_model->get_creditor($creditor_id);
    $row = $creditor->row();
    $creditor_name = $row->creditor_name;
    $start_date = $row->start_date;
    $opening_balance = $row->opening_balance;
    $debit_id = $row->debit_id;

    $v_data['contacts'] = $this->site_model->get_contacts();
    $v_data['creditor_id'] = $creditor_id;
    $v_data['start_date'] = $start_date;
    $v_data['opening_balance'] = $opening_balance;
    $v_data['debit_id'] = $debit_id;


    	// $v_data['query'] = $this->company_financial_model->get_creditor_account($where, $table);
  	$v_data['title'] = $creditor_name.' '.$search_title;
  	$data['title'] = $creditor_name.' Statement';
    $this->load->view('vendors/print_creditor_account', $v_data);
  	// $this->load->view('admin/templates/general_page', $data);
  }

  public function account_balances()
	{
		$order = 'account.account_type_id';
		$order_method ='ASC';
		$where = 'account_id > 0 AND account_type.account_type_id = account.account_type_id';
		$table = 'account,account_type';

		$search = $this->session->userdata('search_petty_cash1');
		$where .= $search;
		
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/charts-of-accounts';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->company_financial_model->get_all_cash_accounts($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('accounts/all_accounts', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function deactivate_account($account_id)
	{
		if($this->company_financial_model->deactivate_account($account_id))
		{
			$this->session->set_userdata('success_message', 'Account deactivated successfully');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Account deactivation failed');
		}
		
		redirect('accounting/charts-of-accounts');
	}
	public function activate_account($account_id)
	{
		if($this->company_financial_model->activate_account($account_id))
		{
			$this->session->set_userdata('success_message', 'Account activated successfully');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Account activation failed');
		}
		
		redirect('accounting/charts-of-accounts');
	}
	public function edit_account($account_id)
	{
		//form validation
		$this->form_validation->set_rules('account_name', 'Name','required|xss_clean');
		$this->form_validation->set_rules('account_balance', 'Opening Balance','required|xss_clean');
		$this->form_validation->set_rules('account_type_id', 'Account type','required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date','required|xss_clean');

		
		if ($this->form_validation->run())
		{
			//update order
			if($this->company_financial_model->update_account($account_id))
			{
				$this->session->set_userdata('success_message', 'Account updated successfully');
				redirect('accounting/charts-of-accounts');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update account. Please try again');
			}
		}
		
		//open the add new order
		$data['title'] = $v_data['title']= 'Edit Account';
		$v_data['types'] = $this->company_financial_model->get_type();
		$v_data['parent_accounts'] = $this->company_financial_model->get_parent_accounts();
		
		//select the order from the database
		$query = $this->company_financial_model->get_account($account_id);
		$v_data['query'] = $query;
		$data['content'] = $this->load->view('accounts/edit_account', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_account()
	{
		//form validation
		$this->form_validation->set_rules('account_name', 'Name','required|xss_clean');
		$this->form_validation->set_rules('account_balance', 'Opening Balance','required|xss_clean');
		$this->form_validation->set_rules('account_type_id', 'Account_type','required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date','required|xss_clean');
		
		if ($this->form_validation->run())
		{
			//update order
			if($this->company_financial_model->add_account())
			{
				$this->session->set_userdata('success_message', 'Account updated successfully');
				redirect('accounting/charts-of-accounts');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update account. Please try again');
			}
		}
		
		//open the add new order
		$v_data['types'] = $this->company_financial_model->get_type();
		$v_data['parent_accounts'] = $this->company_financial_model->get_parent_accounts();
		$data['title'] = $v_data['title']= 'Add Account';
		$data['content'] = $this->load->view('accounts/add_account', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_accounts()
	{
		$account_name = $this->input->post('account_name');
		
		if(!empty($account_name))
		{
			$this->session->set_userdata('search_petty_cash1', ' AND account.account_name LIKE \'%'.$account_name.'%\'');
		}
		
		redirect('accounting/charts-of-accounts');
	}
	
	public function close_search_petty_cash()
	{
		$this->session->unset_userdata('search_petty_cash1');
		
		redirect('accounting/charts-of-accounts');
	}


	public function landlord_statements()
	{

		//open the add new order

		$search =  $this->session->userdata('summaries_search');
		if($search)
		{
			$data['title'] = $v_data['title']= 'Landlord Summary '.$this->session->userdata('summary_search_title');
		}
		else
		{
			$data['title'] = $v_data['title']= 'Landlord Summary Statement';
		}
		$v_data['types'] = $this->company_financial_model->get_type();
		$v_data['parent_accounts'] = $this->company_financial_model->get_parent_accounts();

		$properties = $this->property_model->get_active_property_owners();
		$months = $this->property_model->get_months();
		$rs8 = $properties->result();
		
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_owner_id;
			$property_name = $property_rs->property_owner_name;

		    $property_list .="<option value='".$property_id."'>".$property_name." </option>";

		endforeach;
		
		$month_list = '';
		if($months->num_rows() > 0)
		{
			foreach ($months->result() as $month_rs) :
				$month_id = $month_rs->month_id;
				$month_name = $month_rs->month_name;
				$month_list .="<option value='".$month_id."_".$month_name."'>".$month_name."</option>";
			endforeach;
		}
		$v_data['months'] = $month_list;
		$v_data['property_list'] = $property_list;
		
		//select the order from the database
		$data['content'] = $this->load->view('landlords/landlord_summaries', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_landlord_summaries()
	{
		$property_owner_id = $this->input->post('property_owner_id');
		$transaction_date_from = $this->input->post('transaction_date_from');
		$transaction_date_to = $this->input->post('transaction_date_to');
		// var_dump($_POST);die();
		$search_title = '';

		

		if(!empty($property_owner_id))
		{
			$property_owner_id_old = ' AND property_owner_id = "'.$property_owner_id.'"';
		}

		if(!empty($transaction_date_from) && !empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) >= \''.$transaction_date_from.'\' AND DATE(completion_time) <= \''.$transaction_date_to.'\'';
			$search_title .= 'transactions from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else if(!empty($transaction_date_from))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_from.'\'';
			$search_title .= 'transactions of '.date('jS M Y', strtotime($transaction_date_from)).' ';
		}

		else if(!empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_to.'\'';
			$search_title .= 'transactions of '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else
		{
			$transaction_date = '';
		}



		$this->session->set_userdata('summaries_search', true);
		$this->session->set_userdata('property_owner_id', $property_owner_id);
		$this->session->set_userdata('summary_search_title', $search_title);
		$this->session->set_userdata('transaction_date_from', $transaction_date_from);
		$this->session->set_userdata('transaction_date_to', $transaction_date_to);
		
		

		redirect('company-financials/landlord-summaries');
	}

	public function close_landlord_summaries()
	{

		$this->session->unset_userdata('summaries_search', false);
		$this->session->unset_userdata('property_owner_id');
		$this->session->unset_userdata('transaction_date_from');
		$this->session->unset_userdata('summary_search_title');
		$this->session->unset_userdata('transaction_date_to');
		
		redirect('company-financials/landlord-summaries');
	}
	public function print_landlord_summaries()
	{
		// $data = array('visit_id'=>$visit_id,'payment_id'=>$payment_id);
		$data['contacts'] = $this->site_model->get_contacts();
		
		// $patient = $this->reception_model->patient_names2(NULL, $visit_id);
		// $data['patient'] = $patient;
		$this->load->view('print_landlord_summary', $data);
	}
}
?>
