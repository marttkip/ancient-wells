<?php
$contacts = $this->site_model->get_contacts();
if(count($contacts) > 0)
{
	$email = $contacts['email'];
	$email2 = $contacts['email'];
	$facebook = $contacts['facebook'];
	$twitter = $contacts['twitter'];
	$linkedin = $contacts['linkedin'];
	$logo = $contacts['logo'];
	$building = $contacts['building'];
	$floor = $contacts['floor'];
	$location = $contacts['location'];
	$company_name = $contacts['company_name'];
	$phone = $contacts['phone'];
	
	if(!empty($facebook))
	{
		//$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
	}
	
}
else
{
	$email = '';
	$facebook = '';
	$twitter = '';
	$linkedin = '';
	$logo = '';
	$company_name = '';
	$google = '';
}
?>
<!DOCTYPE html>
<html>
    <head>
		<?php echo $this->load->view('site/includes/header', '', TRUE); ?>
    </head>
    
    <body>
        <!-- START PAGE HEADER -->
		<?php echo $this->load->view('site/includes/navigation', '', TRUE); ?>
        <!-- END PAGE HEADER -->
        <?php echo $content;?>
        
		<?php echo $this->load->view('site/includes/footer', '', TRUE); ?>
        
        <!-- JS LIBS-->
        <!-- you can add remote instance of jquery -->
        <!--<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>-->
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/video.js/dist/video.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/videojs-youtube/dist/Youtube.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/parallax.js/parallax.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/libs/scrollreveal/dist/scrollreveal.min.js" type="text/javascript"></script>
        
        <!-- PROMINENT JS CODE -->
        <script src="<?php echo base_url()."assets/themes/theme/";?>assets/js/init.js" type="text/javascript"></script>
        
    </body>
</html>
