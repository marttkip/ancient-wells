<?php
$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$property_id = $leases_row->property_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

		//create deactivated status display
		if($tenant_status == 0)
		{
			$status_tenant = '<span class="label label-default">Deactivated</span>';
		}
		//create activated status display
		else if($tenant_status == 1)
		{
			$status_tenant = '<span class="label label-success">Active</span>';
		}
	}

$total_arrears = $this->accounts_model-> get_tenants_statements($lease_id,1);


?>


<?php

$tenant_response = $this->accounts_model->get_rent_and_service_charge($lease_id,$tenant_unit_id);

$grand_rent_bill = $this->accounts_model->get_cummulated_balance($lease_id,1);
$grand_water_bill = $this->accounts_model->get_cummulated_balance($lease_id,2);
$grand_electricity_bill = $this->accounts_model->get_cummulated_balance($lease_id,3);
$grand_service_charge_bill = $this->accounts_model->get_cummulated_balance($lease_id,4);
$grand_penalty_bill = $this->accounts_model->get_cummulated_balance($lease_id,5);

//  interface  for making payments
// use the property id and also the type of the person
// var_dump($property_id); die();
$property_invoices = $this->accounts_model->get_property_invoice_types($lease_id,1,1);
$inputs = '';
if($property_invoices->num_rows() > 0)
{
	// var_dump($property_invoices->result()); die();
	foreach ($property_invoices->result() as $key_types) {
		# code...
		$property_invoice_type_id = $key_types->invoice_type_id;

		if($property_invoice_type_id == 1)
		{
			$inputs .='	
						<div class="form-group">
							<label class="col-md-4 control-label">Rent Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="rent_amount" placeholder="'.$grand_rent_bill.'" value="'.$grand_rent_bill.'" autocomplete="off" >
							</div>
						</div>	
					   ';
		}
		else if($property_invoice_type_id == 2)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">W/C Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="water_amount" placeholder="'.$grand_water_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 3)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 4)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">S/C Amount: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="service_charge_amount" value="'.$grand_service_charge_bill.'" placeholder="'.$grand_service_charge_bill.'" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 5)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Penalty: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="penalty_fee" placeholder="'.$grand_penalty_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 6)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 7)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Painting Charge: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="painting_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 8)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Sinking Funds: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="sinking_funds" placeholder="" autocomplete="off">
							</div>
						</div>
					';
		}
		else if($property_invoice_type_id == 9)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Insurance: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="insurance" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 12)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Fixed Charge: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="fixed_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 10)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Bought Water: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="bought_water" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 13)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Deposit: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="deposit_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 17)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Legal Fees: </label>
						  
							<div class="col-md-7">
								<input type="number" class="form-control" name="legal_fees" placeholder="" autocomplete="off">
							</div>
						</div>';
		}

		else if($property_invoice_type_id == 11)
		{
			$inputs .='';
		}
	}
}


$list = '';


$item_list = $this->accounts_model->get_all_invoice_items($lease_id);
if($item_list->num_rows() > 0)
{
	foreach ($item_list->result() as $key => $value) {
		// code...
		$invoice_type_name = $value->invoice_type_name;
		$total_amount = $value->total_amount;

		$list .= '<tr><td><span>'.$invoice_type_name.' :</span></td><td style="text-align:right">'.number_format($total_amount,2).' </td></tr>';
	}
}


?>

<div class="row">
	<div class="col-md-12">
		<?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
			  echo '<div class="alert alert-danger">'.$error.'</div>';
			  $this->session->unset_userdata('error_message');
			}
			
			if(!empty($success))
			{
			  echo '<div class="alert alert-success">'.$success.'</div>';
			  $this->session->unset_userdata('success_message');
			}
		 ?>
	</div>
	<div class="col-md-12">
		<div class="col-md-2">
				
			<section class="panel panel-featured panel-featured-info">
				<header class="panel-heading">
					
					<h2 class="panel-title">Account Details</h2>
				</header>
				<div class="panel-body" style="padding: 0px;">
					<div class="col-md-12">
						<a href="<?php echo site_url();?>tenants-accounts" class="btn btn-sm btn-warning col-md-12"  ><i class="fa fa-arrow-left"></i> Back to leases</a> 
					</div>
					<table class="table table-hover table-bordered col-md-12">

						<thead>
							<tr>
								<th colspan="2">Tenant Details</th>
							</tr>
							<tr>
								<th>Title</th>
								<th>Detail</th>
							</tr>
						</thead>
					  	<tbody>
					  		<tr><td><span>Tenant Name :</span></td><td><?php echo $tenant_name;?></td></tr>
					  		<tr><td><span>Tenant Phone :</span></td><td><?php echo $tenant_phone_number;?></td></tr>
					  		<tr><td><span>Tenant National Id :</span></td><td><?php echo $tenant_national_id;?></td></tr>
					  		<tr><td><span>Property Name :</span></td><td><?php echo $property_name.' '.$rental_unit_name;?></td></tr>
					  	
					  		<tr><td><span>Account Status :</span></td><td><?php echo $status_tenant;?></td></tr>
					  		<tr>
								<th colspan="2">Lease Details</th>
							</tr>
					  		<tr><td><span>Lease Number :</span></td><td><?php echo $lease_number;?></td></tr>
					  		<tr><td><span>Lease Start date :</span></td><td><?php echo $lease_start_date;?></td></tr>
					  		<tr><td><span>Lease Expiry date :</span></td><td><?php echo $expiry_date;?></td></tr>
					  		
					  	</tbody>
					</table>
					 <div class="col-md-12">
						<div class="form-actions center-align">
				            <a href="<?php echo site_url();?>lease-detail/<?php echo $lease_id?>" class="btn btn-xs btn-info col-md-12">
				                View Lease <?php echo $total_arrears?>
				            </a>
				        </div>
					</div>
					
			        
				</div>
			</section>
		</div>
		<div class="col-md-5">
			<section class="panel panel-featured panel-featured-info">      
				<div class="panel-body">

					<div class="tabs">
						<ul class="nav nav-tabs nav-justified">
							<li class="active">
								<a class="text-center" data-toggle="tab" href="#general"><i class="fa fa-user"></i> Payments</a>
							</li>
							<li>
								<a class="text-center" data-toggle="tab" href="#account"><i class="fa fa-lock"></i> Pardons</a>
							</li>
					
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="general">
								<?php echo form_open("accounts/make_payments/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
									<input type="hidden" name="type_of_account" value="1">
									<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
									<div class="form-group" id="payment_method">
										<label class="col-md-4 control-label">Payment Method: </label>
										  
										<div class="col-md-7">
											<select class="form-control" name="payment_method" onchange="check_payment_type(this.value)" required>
												<option value="0">Select a payment method</option>
                                            	<?php
												  $method_rs = $this->accounts_model->get_payment_methods();
													
													foreach($method_rs->result() as $res)
													{
													  $payment_method_id = $res->payment_method_id;
													  $payment_method = $res->payment_method;
													  
														echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
													  
													}
												  
											  ?>
											</select>
										  </div>
									</div>
									<div id="mpesa_div" class="form-group" style="display:none;" >
										<label class="col-md-4 control-label"> Mpesa TX Code: </label>

										<div class="col-md-7">
											<input type="text" class="form-control" name="mpesa_code" placeholder="">
										</div>
									</div>
								  
								  
									<div id="cheque_div" class="form-group" style="display:none;" >
										<label class="col-md-4 control-label"> Bank Name: </label>
									  
										<div class="col-md-7">
											<input type="text" class="form-control" name="bank_name" placeholder="Barclays">
										</div>
									</div>
									

									<div class="form-group">
										<label class="col-md-4 control-label">Total Amount: </label>
									  
										<div class="col-md-7">
											<input type="number" class="form-control" name="amount_paid" placeholder=""  autocomplete="off" required>
										</div>
									</div>

									<!-- where you put all the item that are to be paid for in this property -->
									<?php echo $inputs;?>
									<!-- end of the items to be paid for in this property -->
									<div class="form-group">
										<label class="col-md-4 control-label">Paid in By: </label>
									  
										<div class="col-md-7">
											<input type="text" class="form-control" name="paid_by" placeholder="<?php echo $tenant_name;?>" value="<?php echo $tenant_name;?>" autocomplete="off" >
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label">Payment Date: </label>
									  
										<div class="col-md-7">
											 <div class="input-group">
				                                <span class="input-group-addon">
				                                    <i class="fa fa-calendar"></i>
				                                </span>
				                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" required>
				                            </div>
										</div>
									</div>
									
									<div class="center-align">
										<button class="btn btn-info btn-sm" type="submit">Add Payment Information</button>
									</div>
									<?php echo form_close();?>
							</div>
							<div class="tab-pane" id="account">
							<?php echo form_open("accounts/create_pardon/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
									<input type="hidden" name="type_of_account" value="1">
									<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
									<div class="form-group">
										<label class="col-md-4 control-label">Pardon Amount: </label>
									  
										<div class="col-md-7">
											<input type="number" class="form-control" name="pardon_amount" placeholder="" autocomplete="off" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">Pardon Reason: </label>
									  
										<div class="col-md-7">
											<textarea class="form-control" name="pardon_reason" autocomplete="off"></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label">Pardon Date: </label>
									  
										<div class="col-md-7">
											 <div class="input-group">
				                                <span class="input-group-addon">
				                                    <i class="fa fa-calendar"></i>
				                                </span>
				                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="pardon_date" placeholder="Pardon Date" required>
				                            </div>
										</div>
									</div>
									
									<div class="center-align">
										<button class="btn btn-info btn-sm" type="submit">Add Pardon Information</button>
									</div>
									<?php echo form_close();?>
							
							</div>
						
						</div>
					</div>

					
				</div>
			</section>

			
		</div>
		<div class="col-md-5">
			<section class="panel panel-featured panel-featured-info">
				<header class="panel-heading">
					
					<h2 class="panel-title"> Account Pardons</h2>
					

				</header>
				<div class="panel-body" style="padding: 0px;overflow-y: scroll;height: 35vh;">
               
					<table class="table table-hover table-bordered col-md-12 table-linked">
						<thead>
							<tr>
								<th>#</th>
								<th>Document number</th>
								<th>Pardoned Date</th>
								<th>Pardoned Amount </th>
								<th>Pardoned By</th>
								<th>Reason</th>
								<th colspan="1">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if($lease_pardons->num_rows() > 0)
							{
								$y = 0;
								foreach ($lease_pardons->result() as $key_pardons) {
									# code...
									$document_number = $key_pardons->document_number;
									$pardon_amount = $key_pardons->pardon_amount;
									$pardon_id = $key_pardons->pardon_id;
									$created_by = $key_pardons->created_by;
									$payment_date = $key_pardons->payment_date;
									$pardon_date = $key_pardons->pardon_date;
									$pardon_reason = $key_pardons->pardon_reason;
									$payment_explode = explode('-', $payment_date);

									$pardon_date = date('jS M Y',strtotime($pardon_date));
									$y++;
									$message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($pardon_amount, 0).' has been received for Hse No. '.$rental_unit_name);
									
									?>
									<tr>
										<td><?php echo $y?></td>
										<td><?php echo $document_number?></td>
										<td><?php echo $pardon_date;?></td>
										<td><?php echo number_format($pardon_amount,2);?></td>
										<td><?php echo $created_by;?></td>
										<td><?php echo $pardon_reason;?></td>
										
                                        <td>
                                      		<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $pardon_id;?>"><i class="fa fa-times"></i></button>
                                        	
                                        	<div class="modal fade" id="refund_payment<?php echo $pardon_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											    <div class="modal-dialog" role="document">
											        <div class="modal-content">
											            <div class="modal-header">
											            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											            	<h4 class="modal-title" id="myModalLabel">Cancel Pardons</h4>
											            </div>
											            <div class="modal-body">
											            	<?php echo form_open("accounts/cancel_pardon/".$pardon_id, array("class" => "form-horizontal"));?>

											            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
											                <div class="form-group">
											                    <label class="col-md-4 control-label">Action: </label>
											                    
											                    <div class="col-md-8">
											                        <select class="form-control" name="cancel_action_id">
											                        	<option value="">-- Select action --</option>
											                            <?php
											                                if($cancel_actions->num_rows() > 0)
											                                {
											                                    foreach($cancel_actions->result() as $res)
											                                    {
											                                        $cancel_action_id = $res->cancel_action_id;
											                                        $cancel_action_name = $res->cancel_action_name;
											                                        
											                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
											                                    }
											                                }
											                            ?>
											                        </select>
											                    </div>
											                </div>
											                
											                <div class="form-group">
											                    <label class="col-md-4 control-label">Description: </label>
											                    
											                    <div class="col-md-8">
											                        <textarea class="form-control" name="cancel_description"></textarea>
											                    </div>
											                </div>
											                
											                <div class="row">
											                	<div class="col-md-8 col-md-offset-4">
											                    	<div class="center-align">
											                        	<button type="submit" class="btn btn-primary">Save action</button>
											                        </div>
											                    </div>
											                </div>
											                <?php echo form_close();?>
											            </div>
											            <div class="modal-footer">
											                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											            </div>
											        </div>
											    </div>
											</div>
                                        </td>
									</tr>
									<?php

								}
							}
							?>
							
						</tbody>
					</table>
				</div>
			</section>
		</div>
		

	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<section class="panel panel-featured panel-featured-primary">
				<header class="panel-heading">
					<h2 class="panel-title"><?php echo $title;?> </h2>
				</header>
				
				<div class="panel-body" style="padding:0px;height: 43vh; overflow-y:scroll;">
					<table class="table  table-condensed table-bordered table-linked">
						<thead>
							<tr>
								<th>#</th>
								<th>Payment Date</th>
								<th>Receipt Number</th>
								<th>Amount Paid</th>
								<th>Paid By</th>
								<th>Receipted Date</th>
								<th>Reference No.</th>
								<th>Confirmation Status</th>
								<th colspan="4">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$total_payment =0;
							if($lease_payments->num_rows() > 0)
							{
								$y = 0;
								foreach ($lease_payments->result() as $key) {
									# code...
									$receipt_number = $key->receipt_number;
									$amount_paid = $key->amount_paid;
									$payment_id = $key->payment_id;
									$paid_by = $key->paid_by;
									$payment_date = $key->payment_date;
									$payment_created = $key->payment_created;
									$payment_created_by = $key->payment_created_by;
									$confirmation_status = $key->confirmation_status;
									$transaction_code = $key->transaction_code;
									$transaction_date = $key->transaction_date;

									if($confirmation_status == 0)
									{
										$confirmation_status_text = "Confirmed";
										$button = '<a class="btn btn-xs btn-danger" href="'.site_url().'accounts/deactivate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Deactivate this payment Kes.'.$amount_paid.'?\');" title="Deactivate For Company Return'.$amount_paid.'"> <i class="fa fa-thumbs-down"></i> </a>';


									}
									else
									{
										$confirmation_status_text = "Not Confirmed";
										$button = '<a class="btn btn-xs btn-info" href="'.site_url().'accounts/activate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Activate this payment Kes.'.$amount_paid.'?\');" title="Activate For Company Return'.$amount_paid.'"> <i class="fa fa-thumbs-up"></i></a>';


									}
									$payment_explode = explode('-', $payment_date);

									if(empty($transaction_date))
									{
										$transaction_date = date('jS M Y',strtotime($payment_date));
										$marker = 'warning';
									}
									else
									{
										$transaction_date = date('jS M Y',strtotime($transaction_date));
										$marker = 'success';
									}
									$payment_created = date('jS M Y',strtotime($payment_created));
									$y++;
									$message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($amount_paid, 0).' has been received for Hse No. '.$rental_unit_name);
									$total_payment += $amount_paid;
									?>
									<tr>
										<td><?php echo $y?></td>
										<td class="<?php echo $marker;?>"><?php echo $transaction_date;?></td>
										<td><?php echo $receipt_number?></td>
										<td><?php echo number_format($amount_paid,2);?></td>
										<td><?php echo $paid_by;?></td>
										<td><?php echo $payment_created;?></td>
										<td><?php echo $transaction_code;?></td>
										<td><?php echo $confirmation_status_text;?></td>
										<td><?php echo $button; ?></td>

									
										<td><a href="<?php echo site_url().'cash-office/print-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-print"></i></a></td>
		                                <td><a href="<?php echo site_url().'send-tenants-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-xs btn-success"> SMS</a></td>
		                                <td>
		                              		<button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $payment_id;?>"><i class="fa fa-times"></i></button>
		                                	
		                                	<div class="modal fade" id="refund_payment<?php echo $payment_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											    <div class="modal-dialog" role="document">
											        <div class="modal-content">
											            <div class="modal-header">
											            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
											            </div>
											            <div class="modal-body">
											            	<?php echo form_open("accounts/cancel_payment/".$payment_id, array("class" => "form-horizontal"));?>

											            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
											                <div class="form-group">
											                    <label class="col-md-4 control-label">Action: </label>
											                    
											                    <div class="col-md-8">
											                        <select class="form-control" name="cancel_action_id">
											                        	<option value="">-- Select action --</option>
											                            <?php
											                                if($cancel_actions->num_rows() > 0)
											                                {
											                                    foreach($cancel_actions->result() as $res)
											                                    {
											                                        $cancel_action_id = $res->cancel_action_id;
											                                        $cancel_action_name = $res->cancel_action_name;
											                                        
											                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
											                                    }
											                                }
											                            ?>
											                        </select>
											                    </div>
											                </div>
											                
											                <div class="form-group">
											                    <label class="col-md-4 control-label">Description: </label>
											                    
											                    <div class="col-md-8">
											                        <textarea class="form-control" name="cancel_description"></textarea>
											                    </div>
											                </div>
											                
											                <div class="row">
											                	<div class="col-md-8 col-md-offset-4">
											                    	<div class="center-align">
											                        	<button type="submit" class="btn btn-primary">Save action</button>
											                        </div>
											                    </div>
											                </div>
											                <?php echo form_close();?>
											            </div>
											            <div class="modal-footer">
											                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											            </div>
											        </div>
											    </div>
											</div>
		                                </td>
									</tr>
									<?php

								}
							}
							?>
							
						</tbody>
						<tfoot>
							<tr>
								<th colspan="3">Total Payments</th>
								<th><?php echo number_format($total_payment,2)?></th>
								<th colspan="8"></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</section>
		
		</div>
		<div class="col-md-6">
			<section class="panel panel-featured panel-featured-info">
				<header class="panel-heading">
					
					<h2 class="panel-title">Tenant Statement <?php echo date('Y');?>
						

					</h2>
					<a href="<?php echo site_url();?>print-tenant-statement/<?php echo $lease_id;?>" target="_blank" class="btn btn-sm btn-warning pull-right"  style="margin-top:-25px;"> <i class="fa fa-pencil"></i> Print Statement</a>

				</header>
				<div class="panel-body" style="padding: 0px;overflow-y: scroll;height: 43vh;">
                	
					<?php
					$account_rs = $this->accounts_model->get_tenants_statements($lease_id);
					?>

					<table class="table table-hover table-bordered col-md-12 table-linked">
						<thead>
							<tr>
								<th>Date</th>
								<th>Type</th>
								<th>Description</th>
								<th style="text-align: right;">Bills</th>
								<th style="text-align: right;">Deductions</th>
								<th style="text-align: right;">Arrears</th>
								<th></th>
							</tr>
						</thead>
					  	<tbody>

					  		<?php //echo $tenant_response['result'];?>
					  		<?php
					  		$result_statements = '';
					  		$total_debit = 0;
					  		$total_credit = 0;
					  		$grand_arrears = 0;
					  		$run_balance = 0;
					  		if($account_rs->num_rows() > 0)
					  		{
					  			foreach ($account_rs->result() as $key => $value) {
					  				// code...
					  				$transactionDate = $value->createdAt;
					  				$transactionCategory = $value->transactionCategory;
					  				$transactionDescription = $value->transactionDescription;
					  				$confirmation_status = $value->confirmation_status;
					  				$dr_amount = $value->dr_amount;
					  				$cr_amount = $value->cr_amount;
					  				$lease_id = $value->lease_id;
					  				$cr_amount = $value->cr_amount;
					  				$invoice_month = $value->transaction_month;
					  				$invoice_year = $value->transaction_year;
					  				$tenant_unit_id = $value->tenant_unit_id;
					  				$invoice_id = $value->transactionId;
					  					$tenant_unit_id = $value->tenant_unit_id;
									$invoice_id = $value->transactionId;

					  				$total_debit += $dr_amount;
					  				$total_credit += $cr_amount;

					  				$run_balance += $dr_amount;
					  				$run_balance -= $cr_amount;
					  				if($confirmation_status == 0)
					  					$color = '';
					  				else
					  					$color = 'warning';


					  		
									
					  				if($transactionCategory =="Invoice")
									$links = '<td><a onclick="open_invoice_edit('.$lease_id.','.$invoice_month.','.$invoice_year.','.$tenant_unit_id.')"  class="btn btn-xs btn-success" > <i class="fa fa-pencil"></i></a>
													<a href="'.site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'" target="_blank" class="btn btn-xs btn-warning"><i class="fa fa-print"></i></a>
													<a href="'.site_url().'delete-invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$invoice_id.'/'.$tenant_unit_id.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this invoice ? \')"> <i class="fa fa-trash"></i></a></td>
											</td>';
									else
										$links = '<td></td>';

					  				$result_statements .='<tr>
					  										<td>'.date('jS M Y',strtotime($transactionDate)).'</td>
					  										<td class="'.$color.'">'.$transactionCategory.'</td>
					  										<td class="'.$color.'">'.$transactionDescription.'</td>
					  										<td class="'.$color.'" style="text-align: right;">'.number_format($dr_amount,2).'</td>
					  										<td class="'.$color.'" style="text-align: right;">'.number_format($cr_amount,2).'</td>
					  										<td class="'.$color.'" style="text-align: right;">'.number_format($run_balance,2).'</td>
					  										'.$links.'
					  									  </tr>';
					  			}
					  		}
					  		?>
					  		<?php echo $result_statements;?>
					  	</tbody>
					  	<tfoot>
					  		<th></th>
					  		<th></th>
					  		<th>TOTAL</th>
					  		<th style="text-align: right;"><?php echo number_format($total_debit,2)?></th>
					  		<th style="text-align: right;"><?php echo number_format($total_credit,2)?></th>
					  		<th style="text-align: right;"><?php echo number_format($run_balance,2)?></th>
					  	</tfoot>
					</table>
				</div>
			</section>
		</div>
	</div>
</div>

  <!-- END OF ROW -->
<script type="text/javascript">
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }
  function open_invoice_edit(lease_id,invoice_month,invoice_year,tenant_unit_id)
  {
  		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_invoice_month_details/"+lease_id+"/"+invoice_month+"/"+invoice_year+"/"+tenant_unit_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
		
				document.getElementById("current-sidebar-div").style.display = "block"; 
				$("#current-sidebar-div").html(data);
				
			
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
  }

  function update_invoice_items(lease_id,invoice_month,invoice_year,tenant_unit_id,invoice_id)
  {

  		var res = confirm('Are you sure you want to continue with the update ?');

  		if(res)
  		{
  			var config_url = $('#config_url').val();
	  		var invoice_amount = $('#invoice_amount'+invoice_id).val();
			var data_url = config_url+"accounts/update_invoice_item/"+invoice_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{invoice_amount: invoice_amount,invoice_id: invoice_id},
			dataType: 'text',
			success:function(data){
			
					
					 get_invoice_month_details(lease_id,invoice_month,invoice_year,tenant_unit_id)
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

  		}
  		


  }


  function delete_invoice_items(lease_id,invoice_month,invoice_year,tenant_unit_id,invoice_id)
  {

  		var res = confirm('Are you sure you want to continue with the delete action ?');

  		if(res)
  		{
  			var config_url = $('#config_url').val();

			var data_url = config_url+"accounts/delete_invoice_item/"+invoice_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{invoice_amount: 0,invoice_id: invoice_id},
			dataType: 'text',
			success:function(data){
			
					
					 get_invoice_month_details(lease_id,invoice_month,invoice_year,tenant_unit_id)
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

  		}
  		


  }


  function get_invoice_month_details(lease_id,invoice_month,invoice_year,tenant_unit_id)
  {		
  		document.getElementById("current-sidebar-div").style.display = "none"; 
  		var config_url = $('#config_url').val();
		var data_url = config_url+"accounts/get_invoice_month_details/"+lease_id+"/"+invoice_month+"/"+invoice_year+"/"+tenant_unit_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
		
				document.getElementById("current-sidebar-div").style.display = "block"; 
				$("#current-sidebar-div").html(data);
				
			
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
  }

  	$(document).on("submit","form#confirm-invoice-details",function(e)
	{
		e.preventDefault();
		
		
		var config_url = $('#config_url').val();	
		var lease_id = $('#lease_id').val();	
		var tenant_unit_id = $('#tenant_unit_id').val();	

		window.location.href = config_url+'accounts/payments/'+lease_id+"/"+tenant_unit_id;
		
	});
</script>