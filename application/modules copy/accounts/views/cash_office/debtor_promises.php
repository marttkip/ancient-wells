<?php
$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
	}
?>
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
    	<h2 class="panel-title pull-right"></h2>
    	<h2 class="panel-title">Record Debtors Promises</h2>

    </header>             

  <!-- Widget content -->
        <div class="panel-body">
	<?php
    echo form_open("record-debtors-promises/".$lease_id, array("class" => "form-horizontal"));
    ?>
    <div class="row">
     <div class="col-md-4">
            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
            <div class="form-group">
                <div class="form-group">
					<label class="col-md-4 control-label">Follow Up Date: </label>
				  
					<div class="col-md-7">
						 <div class="input-group">
	                        <span class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </span>
	                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="follow_up_date" placeholder="Follow up Date" required>
	                    </div>
					</div>
				</div>
            </div>
            
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-lg-4 control-label">Information Given: </label>
                
                <div class="col-lg-8">
                	<div class="col-lg-12">
                	<input type="text" class="form-control" name="information_given" placeholder="Summary" >
                </div>
                </div>
            </div>
        </div>

        
 
    </div>
    <br>
    <div class="row">
        <div class="form-group">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="center-align">
                        <button type="submit" class="btn btn-info">Record</button>
                    </div>
                </div>
            </div>
    </div>
    
    
    <?php
    echo form_close();
    ?>

  </div>
  <?php

$result = '';
$query = $this->accounts_model->get_all_tenant_promises($lease_id);
	
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th><a>Follow Up Date</a></th>
				<th><a>Promise Information</a></th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	$count = 0;
	
	foreach ($query->result() as $leases_row)
	{
		$count++;

		$lease_id = $leases_row->lease_id;
		$tenant_promise_date = $leases_row->tenant_promise_date;
		$tenant_promise_information = $leases_row->tenant_promise_information;
		


			$tenant_promise_date = date('jS M Y',strtotime($tenant_promise_date));
	     	
	     	$this_month = date('m');
	     	
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$tenant_promise_date.'</td>
						<td>'.$tenant_promise_information.'</td>
						
					</tr> 
				';
		
					
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no promises recorded";
}
?>

<div class="table-responsive">
            	
				<?php echo $result;?>
		
            </div>

</section>