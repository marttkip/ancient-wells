<?php

$payment_item_rs = $this->accounts_model->get_invoice_month_details($lease_id,$invoice_month,$invoice_year);
// var_dump($patient_id);die();
echo form_open("finance/creditors/confirm_payment/".$lease_id, array("class" => "form-horizontal","id" => "confirm-invoice-details"));
$result = "

			<table  class='table table-striped table-bordered table-condensed'>
			<tr>
				<th></th>
				<th></th>
				<th>Type</th>
				<th>Invoice Amount</th>
				<th>Amount Paid</th>
				<th colspan='2'></th>

			</tr>
		";

$x = 0;
$total_payment = 0;
if($payment_item_rs->num_rows() > 0)
{

	foreach ($payment_item_rs->result() as $key => $value) {
		# code...
		$invoice_id = $value->invoice_id;
		$invoice_type_name = $value->invoice_type_name;
		$invoice_amount = $value->invoice_amount;
		$invoice_date = $value->invoice_date;


		$checkbox_data = array(
			                    'name'        => 'visit_payment_items[]',
			                    'id'          => 'checkbox'.$invoice_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $invoice_id
			                  );
		$checked = "<td>
						<a class='btn btn-sm btn-success' onclick='update_invoice_items(".$lease_id.", ".$invoice_month.",".$invoice_year.",".$tenant_unit_id.",".$invoice_id.")'><i class='fa fa-pencil'></i></a>
					</td>
					<td>
						<a class='btn btn-sm btn-danger' onclick='delete_invoice_items(".$lease_id.", ".$invoice_month.",".$invoice_year.",".$tenant_unit_id.",".$invoice_id.")'><i class='fa fa-trash'></i></a>
					</td>";
		$x++;
		$result .= '<tr>
						<td >'.form_checkbox($checkbox_data).'<label for="checkbox'.$invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td>'.$x.'</td>
						<td>'.$invoice_type_name.'</td>
						<td><input type="number" class="form-control" name="invoice_amount'.$invoice_id.'" id="invoice_amount'.$invoice_id.'" value="'.$invoice_amount.'"></td>
						'.$checked.'
					</tr>';
		$total_payment += $invoice_amount;
	}

	$result .= '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>Total</td>
						<td>'.number_format($total_payment,2).'</td>
					</tr>';
}
  $result .='</tbody>
                      </table>';

?>
<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
		<div class="col-md-12">
			<div class="padd">
			
					<input type="hidden" name="type_payment" value="1">
				
		             <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id;?>">
		             <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
					
		            <div class="col-md-12">
		            	<?php echo $result;?>
		            </div>
		            <div class="col-md-12">
		            	<div class="col-md-6">
		            		
		            	</div>
		            	<div class="col-md-6">
		            		<div class="form-group">
				                <label class="col-md-4 control-label">Total Amount: </label>
				                
				                <div class="col-md-8">
				                    <div class="input-group">
				                        <span class="input-group-addon">
				                            <i class="fa fa-calendar"></i>
				                        </span>
				                        <input type="text" class="form-control" name="total_invoice_amount" placeholder="Total Amount" value="<?php echo $total_payment;?>" readonly>
				                    </div>
				                </div>
				            </div>
		            		<div class="form-group">
				                <label class="col-md-4 control-label">Invoice Date: </label>
				                
				                <div class="col-md-8">
				                    <div class="input-group">
				                        <span class="input-group-addon">
				                            <i class="fa fa-calendar"></i>
				                        </span>
				                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="invoice_date" placeholder="Invoice Date" value="<?php echo $invoice_date;?>" readonly>
				                    </div>
				                </div>
				            </div>
		            		<div class="form-group">
				                <div class="center-align" style="margin-top:10px;">
									<button type="submit" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to update this invoice ? ')"> UPDATE INVOICE</button>
								</div>
					                
				            </div>
		            	</div>
		            </div>
					
				
			</div>
		</div>
	</div>
</section>
</div>

<div class="col-md-12">                             
    <div class="center-align">
        <a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
    </div>  
</div>

	<?php echo form_close();?>