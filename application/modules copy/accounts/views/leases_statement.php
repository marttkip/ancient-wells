<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
  $account_id = $leases_row->account_id;
	$created = $leases_row->created;
	$tenant_code = $leases_row->tenant_number;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = $lease_start_date_norm = date('jS M Y',strtotime($lease_start_date));
	// var_dump($lease_duration);die();

	if(empty($lease_duration))
	{
		$lease_duration = 36;
		$expiry_date  = '-';//date('jS M Y', strtotime(''.$lease_start_date_norm.'+ '.$lease_duration.' month'));
	}
	else {
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date_norm.'+ '.$lease_duration.' month'));
	}
	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");


}

$search_title = strtoupper($tenant_name).' STATEMENT';
?>

<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>

           <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>

					<script src="<?php echo base_url()."assets/"?>table-resources/table2excel/jquery.table2excel.min.js"></script>
				<style type="text/css">
						body {
								font-family: "tohoma";
								font-size: 12px;
								line-height: 0;
								color: #333333;
								background-color: #ffffff;
						}
						.receipt_spacing {
								letter-spacing: 0px;
								font-size: 12px;
						}
						.center-align{margin:0 auto; text-align:center;}

						.receipt_bottom_border{border-bottom: #888888 medium solid;}
						.row .col-md-12 table {
							border:solid #000 !important;
							border-width:1px 0 0 1px !important;
							/* font-size:10px; */
						}
						.row .col-md-12 th, .row .col-md-12 td {
							border:1px solid #000 !important;
							border-width:0 1px 1px 0 !important;
						}
						.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
						{
							 padding: 1px;
								 border:1px solid #000 !important;
						}
						/* .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td
						{

						} */

				.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
				.title-img{float:left; padding-left:30px;}
				img.logo{margin:0 auto;}
			</style>

				<script language="javascript">
		    function doc_keyUp(e) {
		        // this would test for whichever key is 40 and the ctrl key at the same time
		        if (e.keyCode === 69) {
		            alert("Your Excel Document is being prepared and should start downloading in a few. Don't hit E again until it does.");
		            // call your function to do the thing
		            downloadExcel();
		        }
		    }
		// register the handler 
		    document.addEventListener('keyup', doc_keyUp, false);
		    function downloadExcel() {
		    	// alert("sasa")
		        $("#testTable").table2excel({
		            exclude: ".noExl",
		            name: '<?php echo $search_title?>',
		            filename: "<?php echo $search_title?>.xls",
		            fileext: ".xls",
		            exclude_img: true,
		            exclude_links: true,
		            exclude_inputs: true
		        });
		    }
		</script>
    </head>
     <body class="receipt_spacing">
     	     <div class="col-md-12">
				 <div class="col-md-12">
					 <div class="center-align">
							 <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive "/>
						 </div>
				 </div>
				 <br>

				 <div class="col-md-12 " >
					<div class="col-md-12 center-align">
						 <h3><strong>TENANTS STATEMENT</strong></h3>
					 </div>

					</div>

				 <div class="col-md-12 ">
					 <div class="col-xs-6 invoice-col">
				     <address>
				       <b>TENANT NAME :</b> <?php echo strtoupper($tenant_name);?><br>
				       <b>TENANT CODE :</b> <?php echo $tenant_code;?><br>
				       <b>INCEPTION DATE :</b> <?php echo $lease_start_date;?> <br>
				     </address>
				   </div>


					 <div class="col-xs-6 ">
				     <address>
							 <b>LANDLORD NAME :</b> <?php echo strtoupper($property_name);?><br>
							<b>UNIT NO :</b> <?php echo strtoupper($rental_unit_name);?><br>
							<b>EXPIRY DATE :</b> <?php echo $expiry_date;?> <br>
				     </address>
				   </div>

				   <!-- /.col -->
				 </div>
			</div>



        <!-- Table row -->
        <div class="col-md-12">
          <div class="col-md-12 table-responsive">
           <?php
					$account_rs = $this->accounts_model->get_tenants_statements($lease_id);
					?>

					<table class="table table-hover table-bordered col-md-12 table-linked" id="testTable">
						<thead>
							<tr>
								<th style="border:1px solid grey;">Date</th>
								<th style="border:1px solid grey;">Type</th>
								<th style="border:1px solid grey;">Description</th>
								<th style="text-align: right;border:1px solid grey;">Bills</th>
								<th style="text-align: right;border:1px solid grey;">Deductions</th>
								<th style="text-align: right;border:1px solid grey;">Arrears</th>
							</tr>
						</thead>
					  	<tbody>

					  		<?php //echo $tenant_response['result'];?>
					  		<?php
					  		$result_statements = '';
					  		$total_debit = 0;
					  		$total_credit = 0;
					  		$grand_arrears = 0;
					  		$run_balance = 0;
					  		$total_pardons = 0;
					  		$total_payments = 0;
					  		$total_invoice = 0;
					  		if($account_rs->num_rows() > 0)
					  		{
					  			foreach ($account_rs->result() as $key => $value) {
					  				// code...
					  				$transactionDate = $value->createdAt;
					  				$transactionCategory = $value->transactionCategory;
					  				$transactionDescription = $value->transactionDescription;
					  				$confirmation_status = $value->confirmation_status;
					  				$dr_amount = $value->dr_amount;
					  				$cr_amount = $value->cr_amount;
					  				$lease_id = $value->lease_id;
					  				$cr_amount = $value->cr_amount;
					  				$invoice_month = $value->transaction_month;
					  				$invoice_year = $value->transaction_year;
					  				$tenant_unit_id = $value->tenant_unit_id;
					  				$invoice_id = $value->transactionId;
					  					$tenant_unit_id = $value->tenant_unit_id;
									$invoice_id = $value->transactionId;

					  				$total_debit += $dr_amount;
					  				$total_credit += $cr_amount;

					  				$run_balance += $dr_amount;
					  				$run_balance -= $cr_amount;
					  				if($confirmation_status == 0)
					  					$color = '';
					  				else
					  					$color = 'warning';


					  		
									if($transactionCategory =="Invoice")
										$total_invoice += $dr_amount;
									else if($transactionCategory =="Payments")
										$total_payments += $cr_amount;
									else if($transactionCategory =="Invoice")
										$total_pardons += $cr_amount;
					  			

					  				$result_statements .='<tr>
					  										<td style="border:1px solid grey">'.date('jS M Y',strtotime($transactionDate)).'</td>
					  										<td style="border:1px solid grey" class="'.$color.'">'.$transactionCategory.'</td>
					  										<td style="border:1px solid grey" class="'.$color.'">'.$transactionDescription.'</td>
					  										<td style="text-align: right;border:1px solid grey" class="'.$color.'" >'.number_format($dr_amount,2).'</td>
					  										<td style="text-align: right;border:1px solid grey" class="'.$color.'" >'.number_format($cr_amount,2).'</td>
					  										<td style="text-align: right;border:1px solid grey" class="'.$color.'" >'.number_format($run_balance,2).'</td>
					  										'.$links.'
					  									  </tr>';
					  			}
					  		}
					  		?>
					  		<?php echo $result_statements;?>
					  	</tbody>
					  	<tfoot>
					  		<th style="border:1px solid grey"></th>
					  		<th style="border:1px solid grey"></th>
					  		<th style="border:1px solid grey">TOTAL</th>
					  		<th style="text-align: right; border:1px solid grey;"><?php echo number_format($total_debit,2)?></th>
					  		<th style="text-align: right; border:1px solid grey;"><?php echo number_format($total_credit,2)?></th>
					  		<th style="text-align: right; border:1px solid grey;"><?php echo number_format($run_balance,2)?></th>
					  	</tfoot>
					</table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

				<div class="col-md-12">
				  <!-- accepted payments column -->
				  <div class="col-xs-7">
				 </div>
				  <!-- /.col -->
				  <div class="col-xs-5">
				    <h4 >Payment Summary</h4>

				    <div class="table-responsive">
				      <table class="table no-border">
				        <tr>
				          <td>Total Amount Payable:</td>
				          <td style="text-align: right"><?php echo number_format($total_debit,2);?></td>
				        </tr>
				        
				        <tr>
				          <td>Total Pardons:</td>
				          <td style="text-align: right"> (<?php echo number_format($total_pardons,2);?>)</td>
				        </tr>
				        <tr>
				          <td>Total Amount Paid:</td>
				          <td style="text-align: right"><strong> (<?php echo number_format($total_payments,2);?>)</strong> </td>
				        </tr>

				        <tr>
				          <td>Total Amount Due:</td>
				          <td style="text-align: right"><strong> <?php echo number_format(($run_balance),2);?></strong> </td>
				        </tr>
				      </table>
				    </div>
				  </div>
				  <!-- /.col -->
				</div>
				<!-- /.row -->

				<div class="col-md-12">
					<!-- <p class="lead">Payment Methods:</p> -->
						<p style="margin-bottom:80px;"><b>Prepared By :</b> ..............................  <b>Checked By :</b> ..................................  <b>Approved By :</b> ............................................ </p>

						 <p><b>Signature :</b> ....................................  <b>Signature :</b> .......................................  <b>Signature :</b> ............................................ </p>
					</div>

					
				</div>


     </body>
</html>
