<?php echo $this->load->view('liabilities_search','', true); ?>
<?php

$result = '';
		
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th><a>Unit</a></th>
				<th><a>L-ID</a></th>
				<th><a>Name</a></th>
				<th><a>Entry</a></th>
				<th><a>Exit</a></th>
				<th><a>Period</a></th>				
				<th><a>Rent Arrears BF</a></th>
                <th><a>Invoiced Rent</a></th>
				<th><a>Rent Payments</a></th>
				<th><a>Rent Liability</a></th>
                <th><a>Final Reading</a></th>
				<th><a>Intial Reading</a></th>
				<th><a>Cons 200 Per Unit</a></th>
				<th><a>Water Arrears BF</a></th>
				<th><a>Water Invoiced</a></th>
				<th><a>Water Paid</a></th>
				<th><a>Water Liability</a></th>
				<th><a>Deposit</a></th>
				<th><a>Expenses Inc. Paint</a></th>
				<th><a>Liability</a></th>
				<th><a>Water Profit</a></th>
				<th><a>Balance</a></th>
				
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$rental_unit_id = $leases_row->unit_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$tenant_email = $leases_row->tenant_email;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_end_date = $leases_row->closing_end_date;
		//var_dump($lease_id);die();

		//$period = $this->liabilities_model->get_period($lease_id);
		$diff = abs(strtotime($lease_end_date) - strtotime($lease_start_date));

        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

        if ($years == 0)
        {
        	if($months == 0)
        	{
        	 $period = 'No Period';
        	}
        	else if($month == 1)
        	{
        	 $period = $months.' Month';
        	}
        	else
        	{
        	 $period = $months.' Months';
        	}

        }
        else  if ($years == 1)
        {
        	if($months == 0)
        	{
        	 $period = $years.' Year';
        	}
        	else if($month == 1)
        	{
        	 $period = $years.' Year '.$months.' Month';
        	}
        	else
        	{
        	 $period = $years.' Year '.$months.' Months';
        	}
        }
        else
        {
        	if($months == 0)
        	{
        	 $period = $years.' Years';
        	}
        	else if($month == 1)
        	{
        	 $period = $years.' Years '.$months.' Month';
        	}
        	else
        	{
        	 $period = $years.' Years '.$months.' Months';
        	}
        }
        

		$rent_arrears_bf = $this->liabilities_model->get_rent_arrears_bf($lease_id);
		$invoice_rent = $this->liabilities_model->get_invoiced_rent($lease_id);
		$total_rent = $this->liabilities_model->get_total_rent($lease_id);
		$rent_liability = $rent_arrears_bf + $invoice_rent - $total_rent;


		$initial_reading = $this->liabilities_model->get_intial_water_reading($lease_id);
		$final_reading = $this->liabilities_model->get_final_water_reading($lease_id);
		if($final_reading < $initial_reading)
		{
			$final_reading = $initial_reading;

			$Consumption = $final_reading - $initial_reading;
		}
		else
		{
			$Consumption = $final_reading - $initial_reading;
		}
		

		$water_arrears_bf = $this->liabilities_model->get_water_arrears_bf($lease_id);
		$water_invoice = $this->liabilities_model->get_water_invoice($lease_id);
		$water_payments = $this->liabilities_model->get_water_payments($lease_id);
		$water_liabilty = $water_arrears_bf + $water_invoice - $water_payments;
		$rent_deposits = $this->liabilities_model->get_deposits($lease_id);
		$expenses = $this->liabilities_model->get_expenses($lease_id);
		$paint_charges = $this->liabilities_model->get_actual_paint_charges($lease_id);
		
	    if($rent_deposits == $expenses)
		{
			$expenses = $rent_liability + $water_liabilty + $paint_charges;
			$refunds = $rent_liability + $water_liabilty + $paint_charges - $rent_deposits;

		}
		else
		{
		     $refunds =  $rent_liability + $water_liabilty + $expenses - $rent_deposits;
        }
        $water_profit = ($water_payments/200)*50;
        $total_bal = $refunds - $water_profit;


		

		
		

		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$points = 0;//$leases_row->points;
		$created = $leases_row->created;

// var_dump($rental_unit_id); die();
	
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		
		

		//create deactivated status display
		// if($lease_status == 0)
		// {
		// 	$status = '<span class="label label-default"> Deactivated</span>';

		// 	$button = '';
		// 	$delete_button = '';
		// }
		// //create activated status display
		// else if($lease_status == 1)
		// {
		// 	$status = '<span class="label label-success">Active</span>';
		// 	$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
		// 	$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		// }

		// 	$lease_start_date  = date('jS M Y', strtotime($lease_start_date));
		//     $lease_end_date  = date('jS M Y', strtotime($lease_end_date));
	     	
	 //     	$this_month = date('m');
	 //     	$amount_paid = 0;
		//      	$payments = $this->accounts_model->get_this_months_payment($lease_id,$this_month);
		//      	$current_items = '<td> -</td>
		//      			  <td>-</td>'; 
		//      	$total_paid_amount = 0;
		//      	if($payments->num_rows() > 0)
		//      	{
		//      		$counter = 0;
		     		
		//      		$receipt_counter = '';
		//      		foreach ($payments->result() as $value) {
		//      			# code...
		//      			$receipt_number = $value->receipt_number;
		//      			$amount_paid = $value->amount_paid;

		//      			if($counter > 0)
		//      			{
		//      				$addition = '#';
		//      			}
		//      			else
		//      			{
		//      				$addition = ' ';
		     				
		//      			}
		//      			$receipt_counter .= $receipt_number.$addition;

		//      			$total_paid_amount = $total_paid_amount + $amount_paid;
		//      			$counter++;
		//      		}
		//      		$current_items = '<td>'.$receipt_number.'</td>
		//      					<td>'.number_format($amount_paid,0).'</td>';
		//      	}
		//      	else
		//      	{
		//      		$current_items = '<td> -</td>
		//      					<td>-</td>';
		//      	}
				
		//      	$todays_date = date('Y-m-d');

				// $todays_date = date('Y-m-d');
				// $todays_month = date('m');
				// $todays_year = date('Y');

				// $tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
				// $total_arrears = $tenants_response['total_arrears'];
				// $total_invoice_balance = $tenants_response['total_invoice_balance'];
				// $invoice_date = $tenants_response['invoice_date'];

				// $current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
				// // $total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;

				// $count++;
				// //check if email for the current_month has been sent for that rental unit
				// $all_sms_notifications_sent = $this->accounts_model->check_if_sms_sent($rental_unit_id,$tenant_phone_number,$todays_year,$todays_month,1);
				// $all_email_notifications_sent = $this->accounts_model->check_if_email_sent($rental_unit_id,$tenant_email,$todays_year,$todays_month,1);
				// if(($all_sms_notifications_sent==FALSE) &&($all_email_notifications_sent==FALSE))
				// {
				// 	$send_notification_button = '<td><a class="btn btn-sm btn-default" href="'.site_url().'send-arrears/'.$lease_id.'/'.$page.'"> Send Message</a></td>';	
				// }
				// else
				// {
				// 	$send_notification_button = '-';
				// }
				$count ++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$lease_id.'</td>
						<td>'.$tenant_name.'</td>
						<td>'.$lease_start_date.'</td>
						<td>'.$lease_end_date.'</td>
						<td>'.$period.'</td>
						<td>'.number_format($rent_arrears_bf,2).'</td>
						<td>'.number_format($invoice_rent,2).'</td>
						<td>'.number_format($total_rent,2).'</td>
						<td>'.number_format($rent_liability,2).'</td>
						<td>'.$final_reading.'</td>
						<td>'.$initial_reading.'</td>
						<td>'.$Consumption.'</td>
						<td>'.number_format($water_arrears_bf,2).'</td>
						<td>'.number_format($water_invoice,2).'</td>
						<td>'.number_format($water_payments,2).'</td>
						<td>'.number_format($water_liabilty,2).'</td>
						<td>'.number_format($rent_deposits,2).'</td>
						<td>'.number_format($expenses,2).'</td>
						<td>'.number_format($refunds,2).'</td>
						<td>'.number_format($water_profit,2).'</td>
						<td>'.number_format($total_bal,2).'</td>
					
						
						
						
					</tr> 
				';
		$v_data['lease_id'] = $lease_id;
		$v_data['amount_paid'] = $amount_paid;
		$v_data['current_balance'] = $current_balance;
		$v_data['balance_bf'] = $current_balance;
		
	}
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}


$accounts_search_title = $this->session->userdata('liabilities_search_title');
?>  
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->
<section class="panel">
		<header class="panel-heading">						
			<h2 class="panel-title"><?php echo $title;?></h2>
			

			<div class="widget-tools pull-right">
				<a href="<?php echo site_url();?>export-liabilities" style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-success pull-right" target="_blank" > <i class="fa fa-outbox"></i> Export List</a>
			</div>
			


		</header>
		<div class="panel-body">
        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}
			
			$error = $this->session->userdata('error_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}

			$search =  $this->session->userdata('all_liability_search');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'accounts/liabilities/close_liability_search" class="btn btn-sm btn-warning">Close Search</a>';
			}
			
					
			?>

        	<div class="row" style="margin-bottom:20px;">
                <div class="col-lg-2 col-lg-offset-8">
                    
                </div>
                <div class="col-lg-12">
                </div>
            </div>
			<div class="table-responsive">
            	
				<?php echo $result;?>
		
            </div>
		</div>
        <div class="panel-footer">
        	<?php if(isset($links)){echo $links;}?>
        </div>
	</section>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>