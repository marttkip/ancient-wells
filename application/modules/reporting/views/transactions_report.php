<?php




$visit_date = date('jS M Y',strtotime($transaction_date));

$months_date = date('M Y',strtotime($transaction_date));






$properties_rs = $this->reporting_model->get_all_properties();

$outstanding = 0;
$month = date('m');
$year = date('Y');
$checked = 0;
$total_paid_amount = 0;
$total_balance_bf = 0;
$total_current_bill = 0;
if($properties_rs->num_rows() > 0)
{
  foreach ($properties_rs->result() as $key => $value) {
    # code...
    $property_id = $value->property_id;
    $property_name = $value->property_name;



    $reviews = $this->reporting_model->get_outstanding_brought_forward($property_id,$month,$year);


    $balance_bf =  $reviews['balance_bf'];
    $current_bill = $reviews['current_bill'];
    $months_payments = $reviews['months_payments'];
    $total_outstanding = $reviews['total_outstanding'];

    $total_paid_amount += $months_payments;
    $total_balance_bf += $balance_bf;
    $total_current_bill += $current_bill;
    $outstanding += $total_outstanding;

    $checked .= "<tr>
                    <td>".strtoupper(strtolower($property_name))."</td>
                    <td> ".number_format($balance_bf,2)."</td>
                    <td> ".number_format($current_bill,2)."</td>
                    <td> ".number_format($months_payments,2)."</td>
                    <td> ".number_format($total_outstanding,2)."</td>
                  </tr>";
  }
  $checked .= "<tr>
                    <td></td>
                    <td> ".number_format($total_balance_bf,2)."</td>
                    <td> ".number_format($total_current_bill,2)."</td>
                    <td> ".number_format($total_paid_amount,2)." </td>
                    <td> ".number_format($outstanding,2)."</td>
                  </tr>";
}


$where = 'payments.lease_id = leases.lease_id AND payment_method.payment_method_id = payments.payment_method_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND payments.payment_status = 1 AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0 AND payments.payment_date = "'.$transaction_date.'"';
$table = 'payments, leases,rental_unit,property,tenant_unit,tenants,payment_method';

$total_payments = $this->reporting_model->get_total_cash_collection($where, $table);
$normal_payments = $this->reporting_model->get_normal_payments($where, $table);
$payment_methods = $this->reporting_model->get_payment_methods($where, $table);

$total_cash_breakdown = 0;
$transactions='';
if($payment_methods->num_rows() > 0)
{
    foreach($payment_methods->result() as $res)
    {
        $method_name = $res->payment_method;
        $payment_method_id = $res->payment_method_id;
        $total = 0;

        if($normal_payments->num_rows() > 0)
        {
            foreach($normal_payments->result() as $res2)
            {
                $payment_method_id2 = $res2->payment_method_id;

                if($payment_method_id == $payment_method_id2)
                {
                    $total += $res2->amount_paid;
                }
            }
        }

        $total_cash_breakdown += $total;

        $transactions .=
        '
        <tr>
            <th style="text-align:left">'.strtoupper($method_name).'</th>
            <td style="text-align:right">'.number_format($total, 2).'</td>
        </tr>
        ';
    }

    $transactions .=
    '
    <tr>
        <th style="text-align:left">TOTAL</th>
        <td style="text-align:right;border-top:#000 2px solid;">'.number_format($total_cash_breakdown, 2).'</td>
    </tr>
    ';

}


$landlord_expenses = $this->reporting_model->get_expense_records($transaction_date,1);
$inhouse_expenses = $this->reporting_model->get_expense_records($transaction_date,0);

// $received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND invoice.month = "'.date('m').'"
// AND invoice.year = "'.date('Y').'" AND  lease_invoice.invoice_deleted = 0 AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND rental_unit.property_id = property.property_id AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00")';
// $received_select = 'sum(invoice.invoice_amount) AS number';
// $received_table = 'invoice,lease_invoice,leases,rental_unit,property';
// $total_month_invoices = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);



// $received_where = 'payments.payment_id = payment_item.payment_id AND payment_item.payment_month = "'.date('m').'"
// AND payment_item.payment_year = "'.date('Y').'" AND  payments.cancel = 0 ';
// $received_select = 'sum(payment_item.amount_paid) AS number';
// $received_table = 'payment_item,payments';
// $total_month_payments = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);


$total_bill =  $total_balance_bf + $total_month_invoices;

$percentage4 = ($total_paid_amount/$total_bill) * 100;



// $percentage4 = ($total_month_invoices-$total_month_payments)/$total_month_invoices *100;
// $percentage4 = 100 - $percentage4;




$received_where = 'DATE(created) = "'.date('Y-m-d').'"';
$received_select = 'sum(amount) AS number';
$received_table = 'mpesa_transactions';
$total_received = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);


$received_where = 'DATE(mpesa_transactions.created) = "'.date('Y-m-d').'" AND mpesa_transactions.mpesa_id = payment_item.mpesa_id AND payment_item.payment_id = payments.payment_id AND payments.cancel = 0 ';
$received_select = 'sum(payment_item.amount_paid) AS number';
$received_table = 'mpesa_transactions,payment_item,payments';


$total_reconcilled = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);



$received_where = 'mpesa_status = 0 AND lease_id is null AND amount <> recon_amount';
$received_select = 'sum(amount - recon_amount) AS number';
$received_table = 'v_mpesa_transactions';
$total_unreconcilled = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);

$response_items = '<p>Good evening to you,<br>
  		Herein is a report of today '.$visit_date.'
  		</p>


      <h4 style="text-decoration:underline"><strong>'.strtoupper($months_date).' REVENUE SUMMARY</strong></h4>
      <table  class="table table-hover table-bordered ">
      <thead>
        <tr>
          <th width="70%"></th>
          <th width="30%"></th>
        </tr>
      </thead>
      </tbody>
        <tr>
            <td style="text-align:left">BALANCE B/F</td>
            <td style="text-align:right">'.number_format($total_balance_bf, 2).'</td>
        </tr>
        
        <tr>
            <td style="text-align:left">MONTH\'s INVOICED AMOUNT</td>
            <td style="text-align:right">'.number_format($total_current_bill, 2).'</td>
        </tr>
        <tr>
            <td style="text-align:left">MONTH\'s PAYMENT MADE</td>
            <td style="text-align:right">'.number_format($total_paid_amount, 2).'</td>
        </tr>        
        <tr>
            <td style="text-align:left"> CURRENT BALANCE</td>
            <td style="text-align:right">'.number_format($outstanding, 2).'</td>
        </tr>
        <tr>
            <td style="text-align:left">PERCENTAGE</td>
            <td style="text-align:right">'.number_format($percentage4, 2).' %</td>
        </tr>

      </tbody>
      </table>

  		<h4 style="text-decoration:underline"><strong>COLLECTIONS SUMMARY</strong></h4>
  		<table  class="table table-hover table-bordered ">
  				<thead>
  					<tr>
  						<th width="70%"></th>
  						<th width="30%"></th>
  					</tr>
  				</thead>
  				</tbody>
  		      '.$transactions.'
  		  	</tbody>

  		</table>
      <h4 style="text-decoration:underline"><strong>PAYBILL RECONCILLIATION SUMMARY</strong></h4>

      <table  class="table table-hover table-bordered ">
          <thead>
            <tr>
              <th width="70%"></th>
              <th width="30%"></th>
            </tr>
          </thead>
          </tbody>
            <tr>
                <td style="text-align:left">TRANSACTIONS TOTAL</td>
                <td style="text-align:right">'.number_format($total_received, 2).'</td>
            </tr>
            <tr>
                <td style="text-align:left">RECONCILLED AMOUNT</td>
                <td style="text-align:right">'.number_format($total_reconcilled, 2).'</td>
            </tr>
          </tbody>

      </table>
      <h4 style="text-decoration:underline"><strong>EXPENSES</strong></h4>
      <table  class="table table-hover table-bordered ">
          <thead>
            <tr>
              <th width="70%"></th>
              <th width="30%"></th>
            </tr>
          </thead>
          </tbody>
            <tr>
                <td style="text-align:left">LANDLORD EXPENSES</td>
                <td style="text-align:right">'.number_format($landlord_expenses, 2).'</td>
            </tr>
            <tr>
                <td style="text-align:left">INHOUSE EXPENSES</td>
                <td style="text-align:right">'.number_format($inhouse_expenses, 2).'</td>
            </tr>
            <tr>
                <td style="text-align:left"><strong>TOTAL EXPENSES</strong></td>
                <td style="text-align:right;border-top:#000 2px solid;">'.number_format($inhouse_expenses+$landlord_expenses, 2).'</td>
            </tr>
          </tbody>

      </table>

       <h4 style="text-decoration:underline"><strong>PROPERTY ARREARS</strong></h4>
      <table  class="table table-hover table-bordered ">
          <thead>
            <tr>
              <th width="40%">Property</th>
              <th width="10%">Balance B/F </th>
              <th width="20%">Month\'s Invoices</th>
              <th width="10%">Month\'s Payments</th>
              <th width="20%">Balance</th>
            </tr>
          </thead>
          </tbody>
             '.$checked.'
          </tbody>

      </table>

      
		';

?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $date_tomorrow;?> | Transactions Report</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
      .receipt_spacing{letter-spacing:0px; font-size: 12px;}
      .center-align{margin:0 auto; text-align:center;}
      
      .receipt_bottom_border{border-bottom: #888888 medium solid;}
      .row .col-md-12 table {
        /*border:solid #000 !important;*/
        /*border-width:1px 0 0 1px !important;*/
        font-size:12px;
        margin-top:10px;
      }
      .col-md-6 {
          width: 50%;
       }
      .row .col-md-12 th, .row .col-md-12 td {
        /*border:solid #000 !important;*/
        /*border-width:0 1px 1px 0 !important;*/
      }
      .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
      {
         /*padding: 2px;*/
         padding: 2px;
      }
      h3
      {
        font-size: 30px;
      }
      
      .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
      .title-img{float:left; padding-left:30px;}
      img.logo{ margin:0 auto;}
    </style>
    </head>
    <body class="receipt_spacing">
      <?php echo $response_items;?>

    </body>
</html>