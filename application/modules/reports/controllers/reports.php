<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
	
		
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function print_montly_report($property_id, $start_date, $end_date)
	{

		$data['contacts'] = $this->site_model->get_contacts();
		
		$data['property_id'] = $property_id;
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$contacts = $data['contacts'];

		$this->load->view('reports/prints/return_report', $data);
	}
	
}
?>