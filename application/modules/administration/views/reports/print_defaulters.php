<?php
	$result = '';
				
		$last_invoices_rs = $this->accounts_model->last_property_invoices();

				$lastest_invoices_array = array();
				if($last_invoices_rs->num_rows() > 0)
				{
					foreach ($last_invoices_rs->result() as $key_item) {
						// code...
						$lastest_invoices_array[$key_item->property_id] = $key_item;
					}
				}


				$water_meters_rs = $this->water_management_model->get_current_active_meters();
				$water_meter_array = array();
				if($water_meters_rs->num_rows() > 0)
				{
					foreach ($water_meters_rs->result() as $key_water) {
						// code...
						$water_meter_array[$key_water->lease_id] = $key_water;
					}
				}


		$query = $this->accounts_model->get_tenants_statements(null,3);
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed table-linked" id="testTable">
				<thead>
					<tr>
						<th style="border:1px solid grey">#</th>
						<th style="border:1px solid grey">Property</th>
						<th style="border:1px solid grey">Flat No.</th>
						<th style="border:1px solid grey">Tenant Name</th>
						<th style="border:1px solid grey">Phone Number</th>
						<th style="border:1px solid grey">Months</th>
						<th style="border:1px solid grey">Rent Balance</th>
						<th style="border:1px solid grey">Water Balance</th>
						<th style="border:1px solid grey">Action (E for Engage or W for Wait)</th>

					</tr>
				</thead>
				  <tbody>
				  
			';
			
			$total_rent_balance = 0;
			$total_water_balance = 0;
			
			foreach ($query->result() as $leases_row)
			{
				$lease_id = $leases_row->lease_id;
				$property_name = $leases_row->property_name;
				$rental_unit_name = $leases_row->rental_unit_name;
				$tenant_name = $leases_row->tenant_name;
				$tenant_phone_number = $leases_row->tenant_phone_number;
				$balance = $leases_row->balance;
				$water_balance = $leases_row->water_balance;

				$avg = $leases_row->avg;
				$water_minimum_value = $leases_row->water_minimum_value;
				$property_id = $leases_row->property_id;

 	

 	
		  

	     		$property_invoice_id = 0;
					if(array_key_exists($property_id, $lastest_invoices_array))
						$property_invoice_id = $lastest_invoices_array[$property_id]->property_invoice_id;


					$current_meter_number = 0;
					$initial_reading = 0;
					$billing_amount = 0;
					if(array_key_exists($lease_id, $water_meter_array)){
						$current_meter_number = $water_meter_array[$lease_id]->property_billing_id;
						$initial_reading = $water_meter_array[$lease_id]->initial_reading;
						$billing_amount = $water_meter_array[$lease_id]->billing_amount;
					}

					
					$used_meter = 0;

					$units_consumed = 0;
					if($property_invoice_id > 0)
					{
						$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
						$current_reading = 0;

						if($water_readings_current['status'])
						{
							$current_reading = $water_readings_current['current_reading'];
							$prev_reading = $water_readings_current['prev_reading'];

							if($current_reading == 0)
							{
								$units_consumed = 0;
							}
							else
							{
								$units_consumed = $current_reading - $prev_reading;
							}
						}
						else
						{
							$prev_reading = $initial_reading;
							$current_reading = 0;
							$units_consumed = 0;
						}
	
					}

						





					 




				if($avg > 1)
				{
					$class = 'background-color:pink;color:black';
				}
				else
				{
					$class= '';
				}

		 	  	if($avg >= 2)
				   		$status = 'AUC E & W';
				   	else
				   		if(($avg > 1 AND $avg <=2) AND $units_consumed == 0)
				   			$status = 'E';
				   		else if(($avg >= 2) AND $units_consumed > 0)
				   			$status = 'AUC E';
				   		else if(($avg == 1) AND $units_consumed == 0)
				   			$status = 'C.E';
				   		else if(($avg > 0 AND $avg < 2) AND $units_consumed >= 0)
							$status = 'W';
				   		else if(($avg <= 0) AND $water_balance > $water_minimum_value)
							$status = 'C.W';
			   
			   $total_water_balance += $water_balance;
			   $total_rent_balance += $balance;

					
				$count++;
				$result .= 
				'
					<tr>
						<td style="border:1px solid black !important;'.$class.'" >'.$count.'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.$property_name.'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.$rental_unit_name.'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.$tenant_name.'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.$tenant_phone_number.'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.number_format($avg,2).'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.number_format($balance,2).'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.number_format($water_balance,2).'</td>
						<td style="border:1px solid black !important;'.$class.'" >'.$status.'</td>
						
	
						
					</tr> 
				';

					
					
		}

		$result .= '
					  </tbody>
					  <tfoot>
					  	<th style="border:1px solid grey;">Total</th>
					  	<th style="border:1px solid grey;"></th>
					  	<th style="border:1px solid grey;"></th>
					  	<th style="border:1px solid grey;"></th>
					  	<th style="border:1px solid grey;"></th>
					  	<th style="border:1px solid grey;"></th>
					  	<th style="border:1px solid grey;">'.number_format($total_rent_balance,2).'</th>
					  	<th style="border:1px solid grey;">'.number_format($total_water_balance,2).'</th>
					  	<th style="border:1px solid grey;"></th>
					  </tfoot>
			';
		$result .= 
		'
					
					</table>
		';
	}

	else
	{
		$result .= "There are no defaulters";
	}
	unset($lastest_invoices_array);
			unset($water_meter_array);
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $property_name;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
            <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>

		<script src="<?php echo base_url()."assets/"?>table-resources/table2excel/jquery.table2excel.min.js"></script>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
		<script language="javascript">
		    function doc_keyUp(e) {
		        // this would test for whichever key is 40 and the ctrl key at the same time
		        if (e.keyCode === 69) {
		            alert("Your Excel Document is being prepared and should start downloading in a few. Don't hit E again until it does.");
		            // call your function to do the thing
		            downloadExcel();
		        }
		    }
		// register the handler 
		    document.addEventListener('keyup', doc_keyUp, false);
		    function downloadExcel() {
		    	// alert("sasa")
		        $("#testTable").table2excel({
		            exclude: ".noExl",
		            name: '<?php echo 'Defaulters List'?>',
		            filename: "<?php echo 'Defaulters List'?>.xls",
		            fileext: ".xls",
		            exclude_img: true,
		            exclude_links: true,
		            exclude_inputs: true
		        });
		    }
		</script>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	 <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        
		            <div class="col-xs-8 ">
		                <h4><strong>Rent Refaulters for   <?php echo $property_name;?> </strong> as at <?php echo date('jS M Y')?></h4>
		               
		            </div>
		        </div>

		        <div class="row">
			       	<div class="col-xs-12 ">
				       	<?php echo $result;?>
				       		
			       	</div>
			   	</div>




		       </div>
		    </div>
		</body>
    </html>