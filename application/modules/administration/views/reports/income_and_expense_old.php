<?php
$property_id = set_value('property_id');
$creditor_account_date = set_value('creditor_account_date');
$year = $this->reports_model->generate_financial_year();
$months = '';
$count = $invoice_count= 0;
$total_months =count($year);
$totals_array = array();
$balance_array = array();
for($r=0;$r<$total_months;$r++)
{
	$month_id = $year[$r];
	$totals_array[$month_id]  =0;
	$month_name  = $this->reports_model->get_month_name($month_id);
	$months .='<th>'.$month_name.'</th>'; 
}


// bank balances 

$bank_balance = 100000000;



// end of bank balance

//display al creditors 

$result ='';
if($all_creditor_services->num_rows() > 0)
{
	/*$result .='
			<table class="table table-condensed table-striped table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Service</th>
						'.$months.'
					</tr>
				</thead>
				<tbody>
				';*/
	foreach($all_creditor_services->result() as $creditors_services)
	{
		$creditor_service_id = $creditors_services->creditor_service_id;
		$creditor_service_name = $creditors_services->creditor_service_name;
		$count++;
		$result .='
		<tr>
			<td>'.$count.'</td>
			<td>'.$creditor_service_name.'</td>
		';
		$creditor_result = '';
		for($r=0;$r<$total_months;$r++)
		{
			$month_id = $original_month_id = $year[$r];
			if(strlen($month_id)!=2)
			{
				$month_id = '0'.$month_id;
			}
			$total_amount = $service_total = 0;
			if($all_creditor_accounts->num_rows()>0)
			{
				foreach($all_creditor_accounts->result() as $creditor_accounts)
				{
					$credit_account_date = $creditor_accounts->creditor_account_date;
					$creditor_account_service_id = $creditor_accounts->creditor_service_id;
					
					$date_explode = explode('-',$credit_account_date);
					$month = $date_explode[1];
					
					if(($month==$month_id)&&($creditor_account_service_id==$creditor_service_id))
					{
						//sum of the service per month
						$credit_account_amount = $creditor_accounts->creditor_account_amount;
						$total_amount += $credit_account_amount;
					}
				}
			}
			$totals_array[$original_month_id] += $total_amount;
			$result .='
				<td>'.number_format($total_amount).'</td>
			';

		}
		
		$result .= '</tr>
					';
	}
	
	$result.='<tr>
			<th colspan="2">Totals</th>';
	for($r=0;$r<$total_months;$r++)
	{
		
		$month_id = $year[$r];
		$month_amount = $totals_array[$month_id];
		$result .='
					<th>'.number_format($month_amount).'</th>'; 
	}
	$result.='</tr>';
	 $result .='
    </tbody>
</table>
';
}




//inflows 
$income_result ='';
$total_incomes = array();
for($r=0;$r<$total_months;$r++)
{
	$month_id = $original_month_id = $year[$r];
	$total_incomes[$month_id] = 0;
}
if($all_invoice_types->num_rows() > 0)
{
	$income_result .='
			<table class="table table-condensed table-striped table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Invoice Type</th>
						'.$months.'
					</tr>
				</thead>
				<tbody>
				';



	foreach($all_invoice_types->result() as $invoice_types)
	{
		$invoice_type_id = $invoice_types->invoice_type_id;
		$invoice_type_name = $invoice_types->invoice_type_name;
		$invoice_count++;
		$income_result .='
		<tr>
			<td>'.$invoice_count.'</td>
			<td>'.$invoice_type_name.'</td>
		';
		$creditor_result = '';
		for($r=0;$r<$total_months;$r++)
		{
			$month_id = $year[$r];
			if(strlen($month_id)!= 2)
			{
				$month_id = '0'.$month_id;
			}
			$total_amount = 0;
			$total_home_owner_amount= $total_inflow_amt = $total_tenant_owner_amount = 0;
			if($all_home_owner_accounts->num_rows()>0)
			{
				foreach($all_home_owner_accounts->result() as $home_owner_accounts)
				{
					$payment_item_created = $home_owner_accounts->payment_item_created;
					$home_owner_invoice_type_id = $home_owner_accounts->invoice_type_id;
					
					$payment_item_created_explode = explode('-',$payment_item_created);
					$month = $payment_item_created_explode[1];
					
					if(($month==$month_id)&&($home_owner_invoice_type_id==$invoice_type_id))
					{
						//sum of the service per month
						$home_owner_account_amount = $home_owner_accounts->amount_paid;
						$total_inflow_amt += $home_owner_account_amount;
						//$total_inflow_amt += $total_home_owner_amount;
					}
				}
			}
			$total_incomes[$original_month_id] += $total_inflow_amt;
			//tenants
			if($all_tenants_account->num_rows()>0)
			{
				foreach($all_tenants_account->result() as $tenants_account)
				{
					$tenant_payment_item_created = $tenants_account->payment_item_created;
					$tenant_owner_invoice_type_id = $tenants_account->invoice_type_id;
					
					$tenant_payment_item_created_explode = explode('-',$tenant_payment_item_created);
					$month = $tenant_payment_item_created_explode[1];
					
					if(($month==$month_id)&&($tenant_owner_invoice_type_id==$invoice_type_id))
					{
						//sum of the service per month
						$tenant_owner_account_amount = $tenants_account->amount_paid;
						$total_inflow_amt += $tenant_owner_account_amount;
						//$total_inflow_amt += $total_tenant_owner_amount;
					}
				}
			}
			$total_incomes[$original_month_id] += $total_inflow_amt;
			$income_result .='
				<td>'.number_format($total_inflow_amt).'</td>
			';

		}
		$income_result .= '</tr>';
	}
	$income_result .='<tr>
					<th colspan="2">Totals</th>';
	for($r=0;$r<$total_months;$r++)
	{
		
		$month_id = $year[$r];
		$month_payment_amount = $total_incomes[$month_id];
		$income_result .='
					<th>'.number_format($month_payment_amount).'</th>'; 
	}
	$income_result.='</tr>';
	/*
	 $income_result .='   	
    </tbody>
</table>
';*/
}
?>
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">Search Period</h2>
    </header>
    <div class="panel-body">
    	<?php echo form_open("administration/reports/search_income_expense_statement", array("class" => "form-horizontal"));?>
    	<div class="row">
        	<div class="col-md-6">
            	<div class="form-group">
                    <label class="col-lg-5 control-label"> Start Period* </label>
                    
                    <div class="col-lg-7">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="creditor_account_date" placeholder="Period Start Date" value="<?php echo $creditor_account_date;?>">
                        </div>
                    </div>
                </div>
            </div>
        	<div class="col-md-6">
            	<div class="form-group">
                    <label class="col-md-4 control-label">Property *</label>
                    
                    <div class="col-md-8">
                        <select class="form-control" name="property_id">
                            <option value="">-- Select property --</option>
                            <?php
                            if($properties->num_rows() > 0)
                            {
                                $property = $properties->result();
                                
                                foreach($property as $res)
                                {
                                    $db_property_id = $res->property_id;
                                    $property_name = $res->property_name;
                                    
                                    if($db_property_id == $property_id)
                                    {
                                        echo '<option value="'.$db_property_id.'" selected>'.$property_name.'</option>';
                                    }
                                    
                                    else
                                    {
                                        echo '<option value="'.$db_property_id.'">'.$property_name.'</option>';
                                    }
                                }
                            }
                        ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:10px;">
            <div class="col-md-12">
                <div class="form-actions center-align">
                    <button class="submit btn btn-primary" type="submit">
                        Search Statement
                    </button>
                </div>
            </div>
        </div>
        <?php echo form_close();?>
    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>

    <!-- Widget content -->
    <div class="panel-body">
    	<tr>
       		<td colspan="14">INFLOWS</td>
        </tr>
    	<?php echo $income_result;?>
        <tr>
       		<td colspan="14">OUTFLOWS</td>
        </tr>
    	<?php echo $result;?>
    </div>
</section>