<!-- search -->
<?php echo $this->load->view('search/owner_defaulters', '', TRUE);?>
<!-- end search -->
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?> <a href="<?php echo site_url();?>export-defaulters-owners" class="btn btn-sm btn-success pull-right" style='margin-top:-5px;'> Export Defaulters List</a>
            	<?php
            	 	$search =  $this->session->userdata('all_defaulters_owners_search');
					if(!empty($search))
					{
						echo '<a href="'.site_url().'print-owners-report" style="margin-top:-5px; margin-right:4px;" target="_blank" class="btn btn-sm btn-primary pull-right">Print Owners Report</a>';
					}

            	 ?>

            	 </h2>

            </header>             

          <!-- Widget content -->
           <div class="panel-body">
	          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
	         <?php

				$result = '';
				$search =  $this->session->userdata('all_defaulters_owners_search');
				if(!empty($search))
				{
					echo '<a href="'.site_url().'administration/reports/close_defaulters_owners_search/'.$module.'" class="btn btn-sm btn-warning">Close Search</a>';
				}
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Property</th>
								<th>Flat No.</th>
								<th>Owner Name</th>
								<th>Arreas B/F</th>
								<th>Receipt/Date</th>
								<th>Amount paid</th>
								<th>Arreas C/F</th>
								<th>Phone Number</th>
								<th colspan="7">Actions</th>
							</tr>
						</thead>
						  <tbody>
						  
					';
					
					
					foreach ($query->result() as $row)
					{
						$rental_unit_id = $row->rental_unit_id;
						$home_owner_id = $row->home_owner_id;
						$rental_unit_name = $row->rental_unit_name;
						$rental_unit_price = $row->rental_unit_price;
						$home_owner_name = $row->home_owner_name;
						$home_owner_phone_number = $row->home_owner_phone_number;
						$home_owner_email = $row->home_owner_email;
						$home_owner_national_id = $row->home_owner_national_id;
						$lease_start_date = $row->lease_start_date;
						$property_name = $row->property_name;

						$created = $row->created;
						$rental_unit_status = $row->rental_unit_status;
						
						
						$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			     	
				     	$this_month = date('m');
				     	$amount_paid = 0;
					     	$payments = $this->accounts_model->get_this_months_payment_owners($rental_unit_id,$this_month);
					     	$current_items = '<td> -</td>
					     			  <td>-</td>'; 
					     	$total_paid_amount = 0;

					     	if($payments->num_rows() > 0)
					     	{
					     		$counter = 0;
					     		$receipt_counter = '';
					     		foreach ($payments->result() as $value) {
					     			# code...
					     			$receipt_number = $value->receipt_number;
					     			$amount_paid = $value->amount_paid;

					     			if($counter > 0)
					     			{
					     				$addition = '#';
					     				// $receipt_counter .= $receipt_number.$addition;
					     			}
					     			else
					     			{
					     				$addition = ' ';
					     				
					     			}
					     			$receipt_counter .= $receipt_number.$addition;
					     			$total_paid_amount = $total_paid_amount + $amount_paid;
					     			$counter++;
					     		}
					     		$current_items = '<td>'.$receipt_number.'</td>
					     					<td>'.number_format($amount_paid,0).'</td>';
					     	}
					     	else
					     	{
					     		$current_items = '<td> -</td>
					     					<td>-</td>';
					     	}

					     	
					     	$owners_response = $this->accounts_model->get_owners_billings($rental_unit_id,$home_owner_id);
							$arrears_amount = $owners_response['total_arrears'];
							$total_invoice_balance = $owners_response['total_invoice_balance'];
							$invoice_date = $owners_response['invoice_date'];
							//  get the amount changed
							//  get the amount changed
							$invoice_amount = $this->accounts_model->get_invoice_brought_forward($rental_unit_id,$invoice_date);
							$paid_amount = $this->accounts_model->get_paid_brought_forward($rental_unit_id,$invoice_date);
							$todays_payment =  $this->accounts_model->get_today_paid_current_forward($rental_unit_id,$invoice_date);
							$total_brought_forward = $invoice_amount - $paid_amount - $todays_payment;
							// end of amount changed
							// var_dump($paid_amount); die();

							// get current invoice
							$current_invoice =  $this->accounts_model->get_invoice_current_forward($rental_unit_id,$invoice_date);
							$current_payment =  $this->accounts_model->get_paid_current_forward($rental_unit_id,$invoice_date);

							
								$count++;
								$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$property_name.'</td>
										<td>'.$rental_unit_name.'</td>
										<td>'.$home_owner_name.'</td>
										<td>'.number_format($total_invoice_balance,2).'</td>
											'.$current_items.'
										<td>'.number_format($arrears_amount,2).'</td>
										<td>'.$home_owner_phone_number.'</td>
										<td><a href="'.site_url().'reports/owner-statement/'.$rental_unit_id.'/'.$home_owner_id.'" target="_blank" class="btn btn-sm btn-default" ><i class="fa fa-upload"></i> View Statement</a></td>
										<td><a href="'.site_url().'reports/send-owner-statement/'.$rental_unit_id.'/'.$home_owner_id.'"  class="btn btn-sm btn-warning" ><i class="fa fa-upload"></i> Send Statement</a></td>
					
										
									</tr> 
								';

							
							
					}
				$result .= 
				'
							  </tbody>
							</table>
				';
			}

			else
			{
				$result .= "There are no defaulters";
			}

			echo $result;
			?>  
			
	        </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>
 <script type="text/javascript">
	$(function() {
	    $("#property_id").customselect();
	    $("#branch_code").customselect();
	});
	$(document).ready(function(){
		$(function() {
			$("#property_id").customselect();
			$("#branch_code").customselect();
		});
	});
</script>