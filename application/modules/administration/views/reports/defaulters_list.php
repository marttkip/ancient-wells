<!-- search -->
<?php echo $this->load->view('search/defaulters_search', '', TRUE);?>
<!-- end search -->
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?> <?php echo $this->session->userdata('defaulter_search_title');?>
            	 <!-- <a href="<?php echo site_url();?>export-defaulters" class="btn btn-sm btn-success pull-right" style='margin-top:-5px;'> Export Defaulters List</a> -->

            	 <?php
            	 	
						echo '<a href="'.site_url().'print-defaulters-report" style="margin-top:-5px; margin-right:4px;" target="_blank" class="btn btn-sm btn-warning pull-right">Print Defaulters</a>';	
						echo '<a href="'.site_url().'administration/reports/export_defaulter" style="margin-top:-5px; margin-right:4px;" target="_blank" class="btn btn-sm btn-warning pull-right">Export Defaulters</a>';

						$search =  $this->session->userdata('defaulters_search');
				if(!empty($search))
				{
					echo '<a href="'.site_url().'administration/reports/close_defaulter" class="btn btn-sm btn-warning">Close Search</a>';
				}

            	 ?>

            	 </h2>
            </header>             

          <!-- Widget content -->
           <div class="panel-body" style="height: 70vh;overflow-y: scroll;padding: 0px !important;">
	    
	         <?php

				$result = '';
				$last_invoices_rs = $this->accounts_model->last_property_invoices();

				$lastest_invoices_array = array();
				if($last_invoices_rs->num_rows() > 0)
				{
					foreach ($last_invoices_rs->result() as $key_item) {
						// code...
						$lastest_invoices_array[$key_item->property_id] = $key_item;
					}
				}


				$water_meters_rs = $this->water_management_model->get_current_active_meters();
				$water_meter_array = array();
				if($water_meters_rs->num_rows() > 0)
				{
					foreach ($water_meters_rs->result() as $key_water) {
						// code...
						$water_meter_array[$key_water->lease_id] = $key_water;
					}
				}


				$query = $this->accounts_model->get_tenants_statements(null,3);
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = 0;
					
					$result .= 
					'
					<table class="table table-bordered table-striped table-condensed table-linked">
						<thead>
							<tr>
								<th>#</th>
								<th>Property</th>
								<th>Flat No.</th>
								<th>Tenant Name</th>
								<th>Phone Number</th>
								<th>Months</th>
								<th>Rent Balance</th>
								<th>Water Balance</th>
								<th>Action (E for Engage or W for Wait)</th>
								<th colspan="2">Actions</th>

							</tr>
						</thead>
						  <tbody>
						  
					';
					
					$total_rent_balance = 0;
					$total_water_balance = 0;
					
					foreach ($query->result() as $leases_row)
					{
						$lease_id = $leases_row->lease_id;
						$property_name = $leases_row->property_name;
						$rental_unit_name = $leases_row->rental_unit_name;
						$tenant_name = $leases_row->tenant_name;
						$tenant_phone_number = $leases_row->tenant_phone_number;
						$balance = $leases_row->balance;
						$water_balance = $leases_row->water_balance;
						$avg = $leases_row->avg;
						$water_minimum_value = $leases_row->water_minimum_value;
						$property_id = $leases_row->property_id;

	     	
				     	$this_month = date('m');
				   

						if($avg > 1)
						{
							$class = 'danger';
						}
						else
						{
							$class= 'default';
						}

						// get last property_billing_id 
						$property_invoice_id = 0;
						if(array_key_exists($property_id, $lastest_invoices_array))
							$property_invoice_id = $lastest_invoices_array[$property_id]->property_invoice_id;


						$current_meter_number = 0;
						$initial_reading = 0;
						$billing_amount = 0;
						if(array_key_exists($lease_id, $water_meter_array)){
							$current_meter_number = $water_meter_array[$lease_id]->property_billing_id;
							$initial_reading = $water_meter_array[$lease_id]->initial_reading;
							$billing_amount = $water_meter_array[$lease_id]->billing_amount;
						}

						
						$used_meter = 0;

						$units_consumed = 0;
						if($property_invoice_id > 0)
						{
							$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
							$current_reading = 0;

							if($water_readings_current['status'])
							{
								$current_reading = $water_readings_current['current_reading'];
								$prev_reading = $water_readings_current['prev_reading'];

								if($current_reading == 0)
								{
									$units_consumed = 0;
								}
								else
								{
									$units_consumed = $current_reading - $prev_reading;
								}
							}
							else
							{
								$prev_reading = $initial_reading;
								$current_reading = 0;
								$units_consumed = 0;
							}
		
						}

						





					   	if($avg >= 2)
					   		$status = 'AUC E & W';
					   	else
					   		if(($avg > 1 AND $avg <=2) AND $units_consumed == 0)
					   			$status = 'E';
					   		else if(($avg >= 2) AND $units_consumed > 0)
					   			$status = 'AUC E';
					   		else if(($avg == 1) AND $units_consumed == 0)
					   			$status = 'C.E';
					   		else if(($avg > 0 AND $avg < 2) AND $units_consumed >= 0)
								$status = 'W';
					   		else if(($avg <= 0) AND $water_balance > $water_minimum_value)
								$status = 'C.W';


				



					   $total_water_balance += $water_balance;
					   $total_rent_balance += $balance;

							
						$count++;
						$result .= 
						'
							<tr>
								<td class="'.$class.'">'.$count.'</td>
								<td class="'.$class.'">'.$property_name.'</td>
								<td class="'.$class.'">'.$rental_unit_name.'</td>
								<td class="'.$class.'">'.$tenant_name.'</td>
								<td class="'.$class.'">'.$tenant_phone_number.'</td>
								<td class="'.$class.'">'.number_format($avg,2).'</td>
								<td class="'.$class.'">'.number_format($balance,2).'</td>
								<td class="'.$class.'">'.number_format($water_balance,2).'</td>
								<td class="'.$class.'">'.$status.'</td>
								<td><a href="'.site_url().'reports/tenant-statement/'.$lease_id.'" target="_blank" class="btn btn-xs btn-default" ><i class="fa fa-upload"></i> </a>
									<a href="'.site_url().'reports/send-statement/'.$lease_id.'"  class="btn btn-xs btn-warning" ><i class="fa fa-upload"></i> </a>
								</td>
			
								
							</tr> 
						';

							
							
				}

				$result .= '
							  </tbody>
							  <tfoot>
							  	<th>Total</th>
							  	<th></th>
							  	<th></th>
							  	<th></th>
							  	<th></th>
							  	<th></th>
							  	<th>'.number_format($total_rent_balance,2).'</th>
							  	<th>'.number_format($total_water_balance,2).'</th>
							  	<th></th>
							  </tfoot>
					';
				$result .= 
				'
							
							</table>
				';
			}


			else
			{
				$result .= "There are no defaulters";
			}

			unset($lastest_invoices_array);
			unset($water_meter_array);

			echo $result;
			?>  
			
	        </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>
 <script type="text/javascript">
	$(function() {
	    $("#property_id").customselect();
	    $("#branch_code").customselect();
	});
	$(document).ready(function(){
		$(function() {
			$("#property_id").customselect();
			$("#branch_code").customselect();
		});
	});
</script>