<?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];

		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}

	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}

	$configuration_rs = $this->site_model->get_company_configuration();

	$configuration_array = array();
	foreach ($configuration_rs->result() as $key) {
		// code...
		$configuration_array[$key->company_configuration_name] = $key;
	}

?>
			<!-- start: header -->
			<header class="header " >
				<div class="col-md-3" >
					<a href="" class="logo">
						<!-- <img src="<?php echo base_url().'assets/logo/'.$logo;?>" height="35" alt="<?php echo $company_name;?>" /> -->
						<h4 style="color: white;"><?php echo strtoupper($company_name);?></h4>
					</a>
				</div>
				<div class="col-md-2">
					<div class="col-md-12 " id="blinker-div"> </div>
				</div>

				<!-- start: search & user box -->
				<div class="col-md-7 ">
					<?php
					$personnel_id = $this->session->userdata('personnel_id');
					if(isset($configuration_array['timesheet']) OR isset($configuration_array['cloakin']))
					{

						$config_status = $configuration_array['timesheet']->company_configuration_status;
						$cloakin = false;
						if(array_key_exists('cloakin', $configuration_array))
						$cloakin = $configuration_array['cloakin']->company_configuration_status;

						if($config_status == 'true' OR $cloakin == 'true')
						{
					
							$query_old = $this->admin_model->get_days_schedule($personnel_id);
							$shift_type = 0;
							if($query_old->num_rows() > 0)
							{

								foreach ($query_old->result() as $key => $value) {
									# code...
									$sign_time_in = $value->sign_time_in;
		                        	$sign_time_out = $value->sign_time_out;
								}
								if(!empty($sign_time_in) AND empty($sign_time_out))
								{
									?>
									
									<a class='btn btn-xs btn-danger' data-toggle='modal' data-target='#close_shift' > <i class="fa fa-plus"></i> Sign out </a>
									<?php

								}else if(!empty($sign_time_in) AND !empty($sign_time_out))
								{
									 $sign_time_in = $value->sign_time_in;
			                        $sign_time_out = $value->sign_time_out;
			                        $visit_date = date('jS M Y',strtotime($value->sign_time_in));
			                        $visit_in = date('h:i a',strtotime($value->sign_time_in));

			                        if(!empty($sign_time_out))
			                        {
			                             $time_out = date('h:i a',strtotime($value->sign_time_out));

			                            $seconds = strtotime($value->sign_time_out) - strtotime($value->sign_time_in);//$row->waiting_time;
			                            $days    = floor($seconds / 86400);
			                            $hours   = floor(($seconds - ($days * 86400)) / 3600);
			                            $minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
			                            
			                            $seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));

			                            
			                            
			                            //$total_time = date('H:i',(strtotime($row->visit_time_out) - strtotime($row->visit_time)));//date('H:i',$row->waiting_time);
			                            $total_time = $days.' days '.$hours.' hours : '.$minutes.' minutes : '.$seconds.' seconds';
			                        }
			                        else
			                        {
			                            $time_out = '';
			                            $total_time = '';
			                        }
									?>
									<i class='alert alert-sm alert-info alert-sm '   > Time Spent : <?php echo $total_time;?> </i>
									<?php
								}
								else
								{
									?>
									<a class='btn btn-success ' data-toggle='modal' data-target='#open_shift'  > <i class="fa fa-plus"></i> Sign In </a>
									<?php
								}

							}
							else
							{
								?>
									<a class='btn btn-xs btn-success ' data-toggle='modal' data-target='#open_shift'  > <i class="fa fa-plus"></i> Sign In </a>
								<?php
							}
						}
					}
						
					//
					?>

					<div class="modal fade bs-example-modal-lg" id="open_shift" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						    <div class="modal-dialog modal-lg" role="document">
						        <div class="modal-content">
						            <div class="modal-header">
						                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						                <h4 class="modal-title" id="myModalLabel">Sign In AT (<?php echo date('Y-m-d H:i A')?>)</h4>
						            </div>
						              <div class="modal-body">
						            <?php echo form_open("open-shift", array("class" => "form-horizontal"));?>
						          
						            	<div class="row">
						                	<div class='col-md-12'>
						                      	Are you sure you want to start sign in ?
												
												 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
												 <input type="hidden" class="form-control" name="personnel_id" placeholder="" autocomplete="off" value="<?php echo $this->session->userdata('personnel_id')?>">
												 <input type="hidden" class="form-control" name="shift_type" placeholder="" autocomplete="off" value="1">
						                   
								            <div class="modal-footer">
								            	<button type="submit" class='btn btn-info btn-sm' type='submit' onclick="return confirm('You are about to start the shift ')">Sign In</button>
								                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								                
								            </div>
						              </div>
						    		</div>
						            <?php echo form_close();?>
						      
						</div>
						</div>
					</div>
				</div>
					<div class="modal fade bs-example-modal-lg" id="close_shift" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					    <div class="modal-dialog modal-lg" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					                <h4 class="modal-title" id="myModalLabel">Sign Out  AT (<?php echo date('Y-m-d H:i A')?>)</h4>
					            </div>
					            <?php echo form_open("close-shift", array("class" => "form-horizontal"));?>
					            <div class="modal-body">
					            	<div class="row">
					                	<div class='col-md-12'>
					                		Are you sure you want to close this shift ?
											 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
											  <input type="hidden" class="form-control" name="personnel_id" placeholder="" autocomplete="off" value="<?php echo $this->session->userdata('personnel_id')?>">
											  <input type="hidden" class="form-control" name="shift_type" placeholder="" autocomplete="off" value="0">
					                    
					                    </div>
					                </div>
					            </div>
					            <div class="modal-footer">
					            	<button type="submit" class='btn btn-info btn-sm' type='submit'  onclick="return confirm('Are you sure you want to sign out ? You will not be able to sign in again.')">Sign Out</button>
					                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					                
					            </div>
					            <?php echo form_close();?>
					        </div>
					    </div>
					</div>


					<?php

					$personnel_id = $this->session->userdata('personnel_id');
					$authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');

					    $branches = $this->site_model->get_all_branches();
					    $branch_list = '';
					    $session_branch_id = $this->session->userdata('branch_id');
					    if($branches->num_rows() > 0)
					    {
					    	foreach ($branches->result() as $key => $value) {
					    		# code...
					    		$branch_id = $value->branch_id;
					    		$branch_name = $value->branch_name;
					    		$branch_code = $value->branch_code;
					    		if($session_branch_id == $branch_id)
					    		{
					    			$branch_list .= '<option value="'.$branch_id.'" selected> '.$branch_name.' </option>';
					    		}
					    		else
					    		{
					    			$branch_list .= '<option value="'.$branch_id.'" > '.$branch_name.' </option>';
					    		}
					    	}
					    }

				    ?>

				    <?php
				    if($authorize_invoice_changes)
				    {


					    ?>

		                    <select name="section_parent_id"  name="section_parent_id" onchange="change_branch(this.value)">
		                        <?php echo $branch_list;?>
		                    </select>
		                <?php
	            	}
	                ?>
						<a  href="<?php echo site_url().$this->uri->uri_string();?>" class="btn btn-xs btn-info" ><i class="fa fa-recycle"></i> Refresh</a>

	                    <input type="hidden" name="current_page" id="current_page" value="<?php echo site_url().$this->uri->uri_string();?>">


					<span class="separator"></span>
					<?php
					$image =  $this->session->userdata('image');
					if(empty($image))
					{
						$avator = base_url().'assets/img/avatar.jpg';
					}
					else
					{
						$avator = base_url().'assets/personnel/'.$image;
					}
					?>

					<div class="pull-right visible-xs" style="color: white;">
						<div class="side-bar-opener" onclick="toggleSidebar()">
							<i class="fa fa-bars" aria-label="Toggle sidebar" style="font-size: xxx-large!important;"></i>
						</div>
					</div>
					<div id="userbox" class="userbox pull-right">
						<!-- <a class="btn btn-xs btn-success" onclick="get_inquery_box()" ><i class="fa fa-folder"></i> Inquiries</a> -->
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?php echo $avator;?>" alt="<?php echo $this->session->userdata('first_name');?>" class="img-circle" data-lock-picture="<?php echo $avator;?>" width="25px"  />
							</figure>
							<div class="profile-info" data-lock-name="<?php echo $this->session->userdata('first_name');?>" data-lock-email="<?php echo $this->session->userdata('email');?>">
								<span class="name">
									<?php
									//salutation
									// if(date('a') == 'am')
									// {
									// 	echo 'Good morning, ';
									// }

									// else if((date('H') >= 12) && (date('H') < 17))
									// {
									// 	echo 'Good afternoon, ';
									// }

									// else
									// {
									// 	echo 'Good evening, ';
									// }
									echo $this->session->userdata('first_name');



									?>
                                </span>
								<span class="role"><?php echo $this->session->userdata('branch_code');?></span>
							</div>

							<i class="fa custom-caret"></i>
						</a>

						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo site_url()."my-profile";?>"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo site_url()."logout-admin";?>"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
						
					</div>
					
				</div>
				<script>
					// function to toggle sidebar
					// .custom-sidebar {
                    // position: relative;
                    // width: 100%;
                    // border-right: none;
                    // overflow-y: visible;
                    // display: none;
                // }
					function toggleSidebar() {
						var sidebar = document.querySelector('.custom-sidebar');
						if(sidebar.style.display == 'block'){
							sidebar.style.display = 'none';
						} else {
							sidebar.style.display = 'block';
						}
					}
				</script>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->
