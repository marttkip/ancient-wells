<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>username</th>
						<th>Status</th>
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$property_beneficiary_id = $row->property_beneficiary_id;
				$property_owner_id = $row->property_owner_id;
				$property_beneficiary_name = $row->property_beneficiary_name;
				$property_beneficiary_email = $row->property_beneficiary_email;
				$property_beneficiary_username = $row->property_beneficiary_username;
				$created = $row->created;
				$property_beneficiary_phone = $row->property_beneficiary_phone;
				$property_beneficiary_status = $row->property_beneficiary_status;
				
				//status
				if($property_beneficiary_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($property_beneficiary_status == 0)
				{
					$status = '<span class="label label-default"> Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'property-manager/activate-property-beneficiary/'.$property_beneficiary_id.'/'.$property_owner_id.'" onclick="return confirm(\'Do you want to activate '.$property_beneficiary_name.'?\');" title="Activate '.$property_beneficiary_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($property_beneficiary_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'property-manager/deactivate-property-beneficiary/'.$property_beneficiary_id.'/'.$property_owner_id.'" onclick="return confirm(\'Do you want to deactivate '.$property_beneficiary_name.'?\');" title="Deactivate '.$property_beneficiary_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
			
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$property_beneficiary_name.'</td>
						<td>'.$property_beneficiary_email.'</td>
						<td>'.$property_beneficiary_phone.'</td>
						<td>'.$property_beneficiary_username.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'property-manager/edit-property-beneficiary/'.$property_beneficiary_id.'/'.$property_owner_id.'" class="btn btn-sm btn-success" title="Edit '.$property_beneficiary_name.'"><i class="fa fa-pencil"></i></a></td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'property-manager/delete-property-beneficiary/'.$property_beneficiary_id.'/'.$property_owner_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$property_beneficiary_name.'?\');" title="Delete '.$property_beneficiary_name.'"><i class="fa fa-trash"></i></a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no property beneficiaries added";
		}
?>

						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?> 
								</h2>
								<div class="pull-right" ><a href="<?php echo site_url();?>property-manager/add-property-beneficiary/<?php echo $property_owner_id?>" style="margin-top:-30px" class="btn btn-sm btn-info "><i class="fa fa-plus"></i> Add Property beneficiary</a><a href="<?php echo site_url();?>property-manager/property-owner" style="margin-top:-30px" class="btn btn-sm btn-warning "><i class="fa fa-arrow-left"></i> Back to Property Owners</a></div>
							</header>
							<div class="panel-body">
                            	<?php
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>human-resource/export-personnel" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    </div>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>