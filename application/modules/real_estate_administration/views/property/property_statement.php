<?php

$result = '';
$total_invoice_balance =0;
$total_balance_bf = 0;
$total_rent_payable = 0;

$this_year_item = $this->session->userdata('year');
$this_month_item = $this->session->userdata('month');
$return_date = $this->session->userdata('return_date');
$total_invoice_balance_end =0;
$total_payable_end =0;
$total_arreas_end = 0;
$total_rent_end = 0;
$total_deposit_paid = 0;
$total_rent_paid = 0;
$total_legal_paid = 0;
$total_service_charge = 0;
$total_payment = 0;


if($property_type_id == 2)
{	
	$count = 0;
	// starts with the blocks in the property
	$result .= 
					'
					<table class="table table-bordered table-striped table-condensed" id="testTable">
						
						 
						  
					';

	if($query->num_rows() > 0)
	{

		
		foreach ($query->result() as $leases_row)
		{
			$block_id = $leases_row->block_id;
			$block_name = $leases_row->block_name;
			
			$result .= 
						'	
						<tr>
							<th colspan="13" class="header-table">BLOCK '.$block_name.'</th>
						</tr> 
						';
			$result .= 
							'	
								<thead  class="header-table">
									
										
									
									<th>#</th>
									<th>HOUSE NUMBER</th>
									<th>HOUSE SIZE</th>
									<th>RENT PAYABLE</th>
									<th>TENANT NAME</th>									
									<th>DESC</th>
									<th>DEPOSIT</th>
									<th>RENT</th>
									<th>L.AGREEMENT</th>
									<th>SERVICE CHARGE</th>
									<th>ARREARS B/F</th>
									<th>TOTAL AMOUNT</th>
									<th>ARREARS C/F</th>
								</thead>
								<tbody>
							';
			// get the floors per block

			$dashboard_where = 'block_id = '.$block_id;
			$dashboard_table = 'floor';
			$dashboard_select = '*';
			$floor_query  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select,null,null,'floor_id','ASC');


			$sub_total_deposit_paid =0;
			$sub_total_rent_paid = 0;
			$sub_total_legal_paid = 0;
			$sub_total_arreas_end = 0;
			$sub_total_payable_end = 0;
			$sub_total_invoice_balance_end = 0;
			$sub_total_service_charge_paid = 0;

			if($floor_query->num_rows() > 0)
			{
				foreach ($floor_query->result() as $floor_row)
				{
					$floor_id = $floor_row->floor_id;
					$floor_name = $floor_row->floor_name;

					


					$rental_unit_where = 'block_id = '.$block_id.' AND floor_id = '.$floor_id;
					$rental_unit_table = 'rental_unit';
					$rental_unit_select = '*';
					$rental_query  = $this->dashboard_model->get_content($rental_unit_table, $rental_unit_where,$rental_unit_select,null,null,'rental_unit.rental_unit_name','ASC','unit_capacity','unit_capacity.unit_capacity_id = rental_unit.unit_capacity_id');



					// if($block_name == "J" AND $floor_id == 37)
					// {
					// 	var_dump($rental_query->result());die();
					// }
					if($rental_query->num_rows() > 0)
					{
						foreach ($rental_query->result() as $rental_row)
						{
							$rental_unit_id = $rental_row->rental_unit_id;
							$rental_unit_name = $rental_row->rental_unit_name;
							$unit_capacity_name = $rental_row->unit_capacity_name;
							$rental_unit_price = $rental_row->rental_unit_price;


							$todays_month = $this->session->userdata('month');
							$todays_year = $this->session->userdata('year');
							$return_date = $this->session->userdata('return_date');


							if($todays_month < 10)
							{
								$todays_month = '0'.$todays_month;
							}
							$lease_id = $this->reports_model->get_active_lease($property_id,$rental_unit_id,$todays_month,$todays_year,null);
							// echo $lease_id.' one';
							$total_paid_amount = 0;

										$count++;
							if($lease_id > 0)
							{
								$rent_payable = $this->reports_model->get_rent_payable($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date);
									
								$lease_query = $this->reports_model->get_lease_detail($lease_id);
								foreach ($lease_query->result() as $key => $value) {

									$lease_id = $value->lease_id;
									$tenant_unit_id = $value->tenant_unit_id;
									$rental_unit_id = $value->rental_unit_id;
									$tenant_name = $value->tenant_name;
									
									$tenant_email = $value->tenant_email;
									$tenant_phone_number = $value->tenant_phone_number;
									$lease_start_date = $value->lease_start_date;
									$lease_duration = $value->lease_duration;
									$rent_amount = $value->rent_amount;
									$lease_number = $value->lease_number;
									$arreas_bf = $value->arrears_bf;
									$rent_calculation = $value->rent_calculation;
									$deposit = $value->deposit;
									$deposit_ext = $value->deposit_ext;
									$lease_status = $value->lease_status;

									$points = 0;//$value->points;
									$created = $value->created;

										// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
									$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

									$date_checked = explode('-', $lease_start_date);

									$year = $date_checked[0];
									$month = $date_checked[1];

									// var_dump($todays_month);die();

									if($year == $todays_year AND $month == $todays_month)
									{
										$tenant_type = 'N.T';
									}
									else
									{
										$tenant_type = '';
									}
								

								
									$lease_start_date = date('jS M Y',strtotime($lease_start_date));
							     	
							     	// $this_month = $this->session->userdata('month');

									$parent_invoice_date = date('Y-m-d', strtotime($todays_year.'-'.$todays_month.'-'.$return_date));
									$previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month')); 

									$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
									$total_pardon_amount = $tenants_response['total_pardon_amount'];

									$rent_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$previous_invoice_date,null);
									$rent_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$previous_invoice_date,null);


									$total_rent_brought_forward = $rent_invoice_amount - $rent_paid_amount -$total_pardon_amount;

									
									
									$rent_current_invoice =  $this->accounts_model->get_invoice_tenants_current_month_forward($lease_id,$todays_month,$todays_year,null,$previous_invoice_date,$parent_invoice_date);
									

									 $rent_current_payment =  $this->accounts_model->get_today_tenants_paid_current_month_forward($lease_id,$todays_month,$todays_year,null,$previous_invoice_date,$parent_invoice_date);
								
									
									
									$total_variance = $rent_current_invoice - $rent_current_payment;
									
									$total_balance = $total_variance + $total_rent_brought_forward;



									$deposit_amount = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,13);

									$rent_paid = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,1);

									$lease_agreement = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,17);

									$service_charge = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,4);
									
									$total_deposit_paid += $deposit_amount;


									$rent_current_payment = $deposit_amount + $rent_paid + $lease_agreement + $service_charge; 





									// if($lease_id = 1)
									// {
									// 	var_dump($total_rent_brought_forward); die();
									// }
									
									$result .= 
												'	<tr>
														<td>'.$count.'</td>
														<td>'.$rental_unit_name.'</td>
														<td>'.$unit_capacity_name.'</td>
														<td>'.number_format($rental_unit_price,2).'</td>
														<td>'.$tenant_name.'</td>
														<td>'.$tenant_type.'</td>
														<td>'.number_format($deposit_amount,2).'</td>
														<td>'.number_format($rent_paid,2).'</td>
														<td>'.number_format($lease_agreement,2).'</td>
														<td>'.number_format($service_charge,2).'</td>
														<td>'.number_format($total_rent_brought_forward,2).'</td>
														<td>'.number_format($rent_current_payment,2).'</td>
														<td>'.number_format($total_balance,0).'</td>

													</tr> 
												';
										// add all the balances

									$total_invoice_balance_end = $total_invoice_balance_end + $total_balance;
									$total_arreas_end = $total_arreas_end + $total_rent_brought_forward;
									$total_payable_end = $total_payable_end + $rent_current_payment;

									$total_rent_paid += $rent_paid;
									$total_legal_paid += $lease_agreement;
									$total_service_charge += $service_charge;
									
									$total_rent_end = $total_rent_end + $rent_payable;
										



									$sub_total_invoice_balance_end = $sub_total_invoice_balance_end + $total_balance;
									$sub_total_arreas_end = $sub_total_arreas_end + $total_rent_brought_forward;
									$sub_total_payable_end = $sub_total_payable_end + $rent_current_payment;

									$sub_total_rent_paid += $rent_paid;
									$sub_total_legal_paid += $lease_agreement;
									$sub_total_service_charge_paid += $service_charge;
									
									$sub_total_rent_end = $total_rent_end + $rent_payable;
									$sub_total_deposit_paid += $deposit_amount;
										
								}

								

							}
							else{

								$result .= 
										'
											<tr>
												<td>'.$count.'</td>
												<td>'.$rental_unit_name.'</td>
												<td>'.$unit_capacity_name.'</td>
												<td>'.number_format($rental_unit_price,2).'</td>
												<td>Vacant House</td>
												<td></td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
												<td>0.00</td>
											</tr> 
										';
							}
						}
					}
					
				

				}
			}


			// $result .='<tr> 
			// 					<td></td>
			// 					<td></td>
			// 					<td></td>
			// 					<td></td>
			// 					<td></td>
			// 					<td><strong>BLOCK '.$block_name.' SUB TOTALS</strong></td>
			// 					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_deposit_paid,2).'</strong></td>
			// 					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_rent_paid,2).'</strong></td>
			// 					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_legal_paid,2).'</strong></td>
			// 					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_service_charge_paid,2).'</strong></td>
			// 					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_arreas_end,2).'</strong></td>
			// 					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_payable_end,2).'</strong></td>
								
			// 					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($sub_total_invoice_balance_end,2).'</strong></td>
							
			// 				</tr>';

			
		}
	}
	$result .='	
				<tr> 
					<td></td>	
					<td></td>	
					<td></td>	
					<td></td>	
					<td></td>	
					<td>Total</td>	
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_deposit_paid,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_rent_paid,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_legal_paid,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_service_charge,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_arreas_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payable_end,2).'</strong></td>
					
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice_balance_end,2).'</strong></td>
				</tr>';
	$result .= 
						'
						 </tbody>
					</table>
					
						';

		
}
else
{

	$count = 0;
	$result .= 
					'
					<table class="table table-bordered table-striped table-condensed">
						<tr >						
							<th id="header-table3">#</th>
							<th id="header-table3">HOUSE NUMBER</th>
							<th id="header-table3">HOUSE SIZE</th>
							<th id="header-table3">RENT PAYABLE</th>
							<th id="header-table3">TENANT NAME</th>									
							<th id="header-table3">DESC</th>
							<th id="header-table3">DEPOSIT</th>
							<th id="header-table3">RENT</th>
							<th id="header-table3">L.AGREEMENT</th>
							<th id="header-table3">SERVICE CHARGE</th>
							<th id="header-table3">ARREARS B/F</th>
							<th id="header-table3">TOTAL AMOUNT</th>
							<th id="header-table3">ARREARS C/F</th>
						</tr>
						
						<tbody>
						  
					';
	$result .= 
			'	
				
			';
	if ($query->num_rows() > 0)
	{
		// var_dump($query->num_rows()); die();

		$sub_total_deposit_paid =0;
			$sub_total_rent_paid = 0;
			$sub_total_legal_paid = 0;
			$sub_total_arreas_end = 0;
			$sub_total_payable_end = 0;
			$sub_total_invoice_balance_end = 0;
			$sub_total_service_charge_paid = 0;
		foreach ($query->result() as $leases_row)
		{
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			$rental_unit_price = $leases_row->rental_unit_price;
			$unit_capacity_name = $leases_row->unit_capacity_name;

			$todays_month = $this->session->userdata('month');
			$todays_year = $this->session->userdata('year');
			$return_date = $this->session->userdata('return_date');


			if($todays_month < 10)
			{
				$todays_month = '0'.$todays_month;
			}
			$lease_id = $this->reports_model->get_active_lease($property_id,$rental_unit_id,$todays_month,$todays_year,null);
			// echo $lease_id.' one';
			$total_paid_amount = 0;

						$count++;
			if($lease_id > 0)
			{
				$rent_payable = $this->reports_model->get_rent_payable($lease_id,$rental_unit_id,$todays_month,$todays_year,$return_date);
					
				$lease_query = $this->reports_model->get_lease_detail($lease_id);
				foreach ($lease_query->result() as $key => $value) {

					$lease_id = $value->lease_id;
					$tenant_unit_id = $value->tenant_unit_id;
					$rental_unit_id = $value->rental_unit_id;
					$tenant_name = $value->tenant_name;
					
					$tenant_email = $value->tenant_email;
					$tenant_phone_number = $value->tenant_phone_number;
					$lease_start_date = $value->lease_start_date;
					$lease_duration = $value->lease_duration;
					$rent_amount = $value->rent_amount;
					$lease_number = $value->lease_number;
					$arreas_bf = $value->arrears_bf;
					$rent_calculation = $value->rent_calculation;
					$deposit = $value->deposit;
					$deposit_ext = $value->deposit_ext;
					$lease_status = $value->lease_status;
					$unit_capacity_name = $value->unit_capacity_name;


					$points = 0;//$value->points;
					$created = $value->created;

					$date_checked = explode('-', $lease_start_date);

					$year = $date_checked[0];
					$month = $date_checked[1];

						// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
					$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
				

				
					$lease_start_date = date('jS M Y',strtotime($lease_start_date));
					// var_dump($lease_start_date);die();
					

					// var_dump($todays_month);die();

					if($year == $todays_year AND $month == $todays_month)
					{
						$tenant_type = 'N.T';
					}
					else
					{
						$tenant_type = '-';
					}
			     	
			     	// $this_month = $this->session->userdata('month');


					$parent_invoice_date = date('Y-m-d', strtotime($todays_year.'-'.$todays_month.'-'.$return_date));
					$previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month')); 

					$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
					$total_pardon_amount = $tenants_response['total_pardon_amount'];

					$rent_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$previous_invoice_date,null);
					$rent_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$previous_invoice_date,null);


					$total_rent_brought_forward = $rent_invoice_amount - $rent_paid_amount -$total_pardon_amount;

					
					
					$rent_current_invoice =  $this->accounts_model->get_invoice_tenants_current_month_forward($lease_id,$todays_month,$todays_year,null,$previous_invoice_date,$parent_invoice_date);
					

					 $rent_current_payment =  $this->accounts_model->get_today_tenants_paid_current_month_forward($lease_id,$todays_month,$todays_year,null,$previous_invoice_date,$parent_invoice_date);
				
					
					
					$total_variance = $rent_current_invoice - $rent_current_payment;
					
					$total_balance = $total_variance + $total_rent_brought_forward;



					$deposit_amount = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,13);

					$rent_paid = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,1);

					$lease_agreement = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,17);

					$service_charge = $this->reports_model->get_lease_deposit_months($lease_id,$todays_month,$todays_year,4);
					
					$total_deposit_paid += $deposit_amount;


					$rent_current_payment = $deposit_amount + $rent_paid + $lease_agreement + $service_charge; 
					
					$result .= 
								'	<tr>
										<td>'.$count.'</td>
										<td>'.$rental_unit_name.'</td>
										<td>'.$unit_capacity_name.'</td>
										<td>'.number_format($rental_unit_price,2).'</td>
										<td>'.$tenant_name.'</td>
										<td>'.$tenant_type.'</td>
										<td>'.number_format($deposit_amount,2).'</td>
										<td>'.number_format($rent_paid,2).'</td>
										<td>'.number_format($lease_agreement,2).'</td>
										<td>'.number_format($service_charge,2).'</td>
										<td>'.number_format($total_rent_brought_forward,2).'</td>
										<td>'.number_format($rent_current_payment,2).'</td>
										<td>'.number_format($total_balance,0).'</td>

									</tr> 
								';
						// add all the balances

					$total_invoice_balance_end = $total_invoice_balance_end + $total_balance;
					$total_arreas_end = $total_arreas_end + $total_rent_brought_forward;
					$total_payable_end = $total_payable_end + $rent_current_payment;

					$total_rent_paid += $rent_paid;
					$total_legal_paid += $lease_agreement;
					$total_service_charge += $service_charge;
					
					$total_rent_end = $total_rent_end + $rent_payable;
						



					$sub_total_invoice_balance_end = $sub_total_invoice_balance_end + $total_balance;
					$sub_total_arreas_end = $sub_total_arreas_end + $total_rent_brought_forward;
					$sub_total_payable_end = $sub_total_payable_end + $rent_current_payment;

					$sub_total_rent_paid += $rent_paid;
					$sub_total_legal_paid += $lease_agreement;
					$sub_total_service_charge_paid += $service_charge;
					
					$sub_total_rent_end = $total_rent_end + $rent_payable;
					$sub_total_deposit_paid += $deposit_amount;
				}

				

			}
			else{

				$result .= 
							'
								<tr>
									<td>'.$count.'</td>
									<td>'.$rental_unit_name.'</td>
									<td>'.$unit_capacity_name.'</td>
									<td>'.number_format($rental_unit_price,2).'</td>
									<td>Vacant House</td>
									<td></td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
								</tr> 
							';
			}

				
			
		
			
			
			
			
		}


		$result .='
			
			<tr> 
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td><strong>Totals</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_deposit_paid,2).'</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_rent_paid,2).'</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_legal_paid,2).'</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_service_charge,2).'</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_arreas_end,2).'</strong></td>
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payable_end,2).'</strong></td>
						
						<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice_balance_end,2).'</strong></td>
					</tr>';
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}

	else
	{
		$result .= "There are no items for this month";
	}
}


$property_id = $this->session->userdata('property_id');


//  GET ALL OUT FLOWS OF THE MONTH 
$outflows = $this->reports_model->get_property_month_outflows($property_id,$this_month_item,$this_year_item);
$total_outflows = 0;
$x = 0;


// var_dump($outflows); die();
if($outflows->num_rows() > 0)
{
	$outflow = '';

	foreach ($outflows->result() as $key) {
		# code...

		$account_name = $key->account_name;
		$account_invoice_description = $key->account_invoice_description;
		$invoice_amount = $key->invoice_amount;
		$x++;
		$outflow .='<tr>
						<td>'.$x.'</td>
						<td>'.$account_name.'</td>
						<td>'.$account_invoice_description.'</td>
						<td>KES. '.number_format($invoice_amount,2).'</td>
					</tr>';
		$total_outflows = $total_outflows + $invoice_amount;
	}	
	// $outflow .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$outflow ='<tr><td colspan=3>No expense added</td></tr>';
}
// var_dump($total_outflows); die();

$percent = $this->reports_model->get_manager_percent($property_id);
$commission = $total_payable_end * ($percent/100);
$x++;
$outflow .='<tr>
						<td>'.$x.'</td>
						<td colspan=2>Agent Commission '.$percent.'% </td>
						<td>KES. '.number_format($commission,2).'</td>
					</tr>';
$total_outflows += $commission;


$outflow .='<tr>
				<td colspan=2>Total<td>
				<td>KES. '.number_format($total_outflows,2).'</td>
		</tr>';



$leases = $this->reports_model->get_property_month_leases($property_id,$this_month_item,$this_year_item);
$total_leases = 0;
$x = 0;
$total_deposit_refund = 0;
if($leases->num_rows() > 0)
{
	$closed_leases = '';

	foreach ($leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		
		$remarks = $key->remarks;
		$expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_leases .='  <tr>
								<td>'.$x.'</td>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>KES. '.number_format($deposit_amount,2).'</td>
								<td>( KES. '.number_format($expense_amount,2).' )</td>
								<td>KES. '.number_format($deposit_amount - $expense_amount,2).'</td>
							</tr>';
		
		$total_leases = $total_leases + $expense_amount;
		$total_deposit_refund = $total_deposit_refund + ($deposit_amount - $expense_amount);
	}
	$closed_leases .='<tr>
						<td colspan=3>Total<td>
						<td>KES. '.number_format($total_leases,2).'</td>
						<td>KES. '.number_format($total_deposit_refund,2).'</td>
				</tr>';
	// $closed_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_leases ,2).' </strong> </li>';
}else
{
	$closed_leases ='<tr><td colspan=3>No closed leases</td></tr>';
}



$new_leases = $this->reports_model->get_property_month_new_leases($property_id,$this_month_item,$this_year_item);
$total_new_lease = 0;
$x = 0;
if($new_leases->num_rows() > 0)
{
	$closed_new_leases = '';

	foreach ($new_leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$remarks = $key->remarks;
		$expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_new_leases .='<tr>
						<td>'.$x.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>
						
					</tr>';
		// if($expense_amount > 0)
		// {

		// }
		$total_new_lease = $total_new_lease + $deposit_amount;
	}
	
	// $closed_new_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_new_lease ,2).' </strong> </li>';
}else
{
	$closed_new_leases ='<tr><td colspan=3>No expense added</td></tr>';
}



if($this_month_item < 10)
{
	$this_month_item = '0'.$this_month_item;
}


$collection_leases = $this->reports_model->get_property_month_rental_bill($property_id,$this_month_item,$this_year_item);
//var_dump($collection_leases);
$total_collection_leases = 0;
$x = 0;
// $total_deposit_refund = 0;
if($collection_leases->num_rows() > 0)
{
	$closed_collection_leases = '';

	foreach ($collection_leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$amount_paid = $key->amount_paid;
		$lease_id = $key->lease_id;
		$invoice_type_name = $key->invoice_type_name;

		// $deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_collection_leases .='  <tr>
								<td>'.$x.'</td>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>'.$invoice_type_name.'</td>
								<td>KES. '.number_format($amount_paid,2).'</td>
							</tr>';
		
		$total_collection_leases = $total_collection_leases + $amount_paid;
	}
	$closed_collection_leases .='<tr>
						<td colspan=3>Total<td>
						<td>KES. '.number_format($total_collection_leases,2).'</td>
				</tr>';
	// $closed_collection_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_collection_leases ,2).' </strong> </li>';
}else
{
	$closed_collection_leases ='<tr><td colspan=3>No closed collection_leases</td></tr>';
}
?>  
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
			@media print {
			    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
			}
			#header-table
			{
				background-color:orange;
				color:white;
			}
			#header-table2
			{
				background-color:#1a4567;
				color:white;
			}

			#header-table3
			{
				background-color:orange;
				color:white;
			}

			@media print {
			    #header-table {
			        /*background-color: #1a4567 !important;*/
			        background-color:orange;
					color:white;
			        -webkit-print-color-adjust: exact; 
			    }
			}

			@media print {
			    #header-table {
			        color: white !important;
			    }
			}

			@media print {
			    th#header-table {
			        background-color: orange !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}

			@media print {
			    th#header-table2 {
			        background-color: #1a4567 !important;
			         color: white !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}
			@media print {
			    th#header-table3{
			        background-color: orange !important;
			         color: white !important;
			        -webkit-print-color-adjust: exact; 
			    }
			}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			thead {display: table-header-group;}
			#item-dev{display: table-header-group;}
		</style>
    </head>
     <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <div class="receipt_bottom_border">
                    <table class="table table-condensed">
                        <tr>
                            <th><h2><?php echo $contacts['company_name'];?> </h2></th>
                            <th class="align-right">
                                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            	<tr>
                    <th class="align-center"><h3><?php echo $title;?> </h3></th>
                </tr>
                <?php echo $result;?>
            </div>
        </div>
        <div class="pagebreak"></div>
        <div class="row" >
       		<div class="col-xs-12" style="border-top:1px #000 solid;">
       			<h4>Key Areas to Note:</h4>
       			<div class="row">
       				<div class="col-print-6">
	       				
						<h5>Other collections for the month</h5>
	       				<table class="table table-hover table-bordered ">
						 	<thead>
								<tr>
								  <th>#</th>						  
								  <th>Rental Unit</th>
								  <th>Tenant</th>							  		
								  <th>Payment for</th>
								  <th>Amount Paid</th>						
								</tr>
							</thead>
							<tbody>							
	       						<?php echo $closed_collection_leases;?>
							</tbody>
						</table>
						<h5>Expenses</h5>
	       				<table class="table table-hover table-bordered ">
						 	<thead>
								<tr>
								  <th>#</th>						  
								  <th>Account</th>
								  <th>Description</th>
								  <th>Amount</th>						
								</tr>
							</thead>
							<tbody>							
	       						<?php echo $outflow;?>
							</tbody>
						</table>
						<?php 
   						// var_dump($todays_month);die();
   						$outstanding_balance = 0;//$this->reports_model->get_outstanding_balance($property_id,$todays_month,$todays_year);
   						?>
						<h4>Return Summary</h4>
						<table class="table table-condensed table-bordered ">
							<thead>


							<tbody>
										<tr>
												<td>Total Gross Payable</td>
												<td><?php echo number_format($total_payable_end,2);?></td>
										</tr>
										<tr>
												<td>Add Other Deposits</td>
												<td><?php echo number_format(0,2);?></td>
										</tr>
										<tr>
												<td>Add Other Incomes</td>
												<td><?php echo number_format($total_collection_leases,2);?></td>
										</tr>
										<tr>
												<td>Less Deposit Refunds</td>
												<td>( <?php echo number_format($total_deposit_refund,2);?> )</td>
										</tr>
										<tr>
												<td>Less Other Payments/Expenses</td>
												<td>( <?php echo number_format($total_outflows,2);?> ) </td>
										</tr>
										<tr>
												<td>OUTSTANDING PAYABLE</td>
												<td><?php echo number_format($outstanding_balance,2);?></td>
										</tr>
										<tr>
												<td><b>NET AMOUNT PAYABLE</b></td>
												<td><b><?php echo number_format(($total_payable_end + $total_collection_leases) - ($total_outflows + $total_deposit_refund ) + $outstanding_balance ,2);?>
													</b>
												</td>
										</tr>
							</tbody>
						</table>
	       			</div>
	       			<div class="col-print-6">
	       				<h5>Closed Leases</h5>
	       				<table class="table table-hover table-bordered ">
						 	<thead>
								<tr>
								  <th>#</th>						  
								  <th>Rental Unit</th>
								  <th>Tenant</th>							  		
								  <th>Deposit Paid</th>
								  <th>Expense</th>	
								  <th>Deposit Refund</th>					
								</tr>
							</thead>
							<tbody>							
	       						<?php echo $closed_leases;?>
							</tbody>
						</table>
	       			</div>
       				
       			</div>
       			
       			<!-- <div class="pagebreak"></div> -->
       			
       			 <!-- <div class="row">
	       			<div class="col-md-6">
	       				
	       			</div>
	       			<div class="col-md-6">
	       				<?php echo '<h4>Amount banked : <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows  - $total_deposit_refund ,2).' </strong> </h4>';?>
	       			</div>
	       		</div>  -->

       			
       			
       		</div>
       	</div>
        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
            <a href="#" onclick="javascript:xport.toCSV('testTable');">XLS</a>
       	</div>
     </body> 
    </html>


    <script type="text/javascript">
	var xport = {
  _fallbacktoCSV: true,
  toXLS: function(tableId, filename) {
    this._filename = (typeof filename == 'undefined') ? tableId : filename;

    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }

    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it

      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};

</script>
