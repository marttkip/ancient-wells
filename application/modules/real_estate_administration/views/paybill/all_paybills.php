<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a>Code</a></th>
						<th><a >Invoice</a></th>
						<th><a >Status</a></th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			
			foreach ($query->result() as $row)
			{
				$paybill_id = $row->paybill_id;
				$paybill_code = $row->paybill_code;
				$paybill_service_id = $row->paybill_invoice;
				$paybill_service = $this->paybill_model->get_invoice_service($paybill_service_id);
				$created = $row->created;
				$paybill_status = $row->paybill_status;
				
				//status
				if($paybill_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($paybill_status == 0)
				{
					$status = '<span class="label label-default"> Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'paybill-manager/properties/activate-paybill/'.$paybill_id.'" onclick="return confirm(\'Do you want to activate '.$paybill_code.'?\');" title="Activate '.$paybill_code.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($paybill_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'paybill-manager/properties/deactivate-paybill/'.$paybill_id.'" onclick="return confirm(\'Do you want to deactivate '.$paybill_code.'?\');" title="Deactivate '.$paybill_code.'"><i class="fa fa-thumbs-down"></i></a>';
				}
			
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$paybill_code.'</td>
						<td>'.$paybill_service.'</td>
						<td>'.$status.'</td>
						
						<td><a href="'.site_url().'property-manager/delete-paybill/'.$paybill_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$paybill_code.'?\');" title="Delete '.$paybill_code.'"><i class="fa fa-trash"></i></a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no paybills added";
		}
?>

						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?> <div class="pull-right" ><a href="<?php echo site_url();?>property-manager/add-paybill" style="margin-top:-5px" class="btn btn-sm btn-info "><i class="fa fa-plus"></i> Add Paybill</a></div></h2>
							</header>
							<div class="panel-body">
                            	<?php
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>human-resource/export-personnel" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    </div>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>