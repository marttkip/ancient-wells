<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/real_estate_administration/controllers/property.php";
error_reporting(0);
// error_reporting(E_ALL);

class Liabilities extends property {
	var $csv_path;
	var $invoice_path;
	var $statement_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('real_estate_administration/tenants_model');
		$this->load->model('real_estate_administration/rental_unit_model');
		$this->load->model('real_estate_administration/leases_model');
		$this->load->model('real_estate_administration/property_owners_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('admin/email_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('administration/reports_model');
		$this->load->model('accounts/liabilities_model');
		// $this->load->model('accounting/salary_advance_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->invoice_path = realpath(APPPATH . '../invoices/');
		$this->statement_path = realpath(APPPATH . '../statements/');
	}
    
	/*
	*
	*	Default action is to show all the tenants
	*
	*/
	public function index() 
	{
			
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 2 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';	

		$all_liability_search = $this->session->userdata('all_liability_search');
		if(!empty($all_liability_search))
		{
			$where .= $all_liability_search;
		}
		


		$segment = 3;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'reports/liabilities';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		
		// $this->accounts_model->update_invoices();
		

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Vacated Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('accounts/liabilities', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function export_liabilities()
	{
		$this->liabilities_model->export_liabilities();
	}


	public function search_liability_accounts()
	{
		$property_id = $this->input->post('property_id');
		// $tenant_name = $this->input->post('tenant_name');
		$rental_unit_name = $this->input->post('rental_unit_name');
		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
			
			
		}
		
		// if(!empty($tenant_name))
		// {
		// 	$search_title .= $tenant_name.' ';
		// 	$tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';

			
		// }
		// else
		// {
		// 	$tenant_name = '';
		// 	$search_title .= '';
		// }

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
			
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();
		
		$this->session->set_userdata('liability_property_search', $property_search);
		$this->session->set_userdata('all_liability_search', $search);
		$this->session->set_userdata('liabilities_search_title', $search_title);
		
		redirect('reports/liabilities');
	}

	public function close_liability_search()
	{

		$this->session->unset_userdata('liability_property_search');
		$this->session->unset_userdata('all_liability_search');
		$this->session->unset_userdata('liabilities_search_title');
		
		redirect('reports/liabilities');
	}
	
}
?>