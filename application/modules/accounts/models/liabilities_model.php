<?php

class Liabilities_model extends CI_Model 
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	
	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_tenants($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('tenants.*,rental_unit.rental_unit_id AS unit_id,rental_unit.*,property.*,leases.*,tenant_unit.*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_period($lease_id)
	{
		$this->db->from('invoice');
		$this->db->select('COUNT(invoice_status) AS total_paid');
		$this->db->where('lease_id = '.$lease_id.' AND invoice_type = 1');
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		return $total_paid;
	}
	public function get_rent_arrears_bf($lease_id)
	{
		$this->db->from('property_billing');
		$this->db->select('arrears_bf as final_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `invoice_type_id` = 1');
		$query = $this->db->get();
		$final_reading = 0;
		foreach ($query->result() as $key) {

			# code...
			$final_reading = $key->final_reading;
			if(is_null($final_reading))
			{
				$final_reading = 0;

			}
			else
			{
				$final_reading = $key->final_reading;

			}
		}
		return $final_reading;
	}
	public function get_invoiced_rent($lease_id)
	{
		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_paid');
		$this->db->where('lease_id = '.$lease_id);
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		return $total_paid;
	}
	public function get_total_rent($lease_id)
	{
		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where('lease_id = '.$lease_id.' AND confirmation_status = 0 AND cancel = 0');
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		return $total_paid;
	}
	public function get_intial_water_reading($lease_id)
	{
		$this->db->from('property_billing');
		$this->db->select('initial_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `invoice_type_id` = 2');
		$query = $this->db->get();
		$initial_reading = 0;
		foreach ($query->result() as $key) {
			# code...
			$initial_reading = $key->initial_reading;
		}
		return $initial_reading;
	}
	public function get_final_water_reading($lease_id)
	{
		$this->db->from('water_invoice');
		$this->db->select('Max(arrears_bf) as final_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `invoice_type` = 2');
		$query = $this->db->get();
		$final_reading = 0;
		foreach ($query->result() as $key) {

			# code...
			$final_reading = $key->final_reading;
			if(is_null($final_reading))
			{
				$final_reading = 0;

			}
			else
			{
				$final_reading = $key->final_reading;

			}
		}
		return $final_reading;
	}
	public function get_water_arrears_bf($lease_id)
	{
		$this->db->from('property_billing');
		$this->db->select('arrears_bf as final_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `invoice_type_id` = 2');
		$query = $this->db->get();
		$final_reading = 0;
		foreach ($query->result() as $key) {

			# code...
			$final_reading = $key->final_reading;
			if(is_null($final_reading))
			{
				$final_reading = 0;

			}
			else
			{
				$final_reading = $key->final_reading;

			}
		}
		return $final_reading;
	}
	public function get_water_invoice($lease_id)
	{
		$this->db->from('water_invoice');
		$this->db->select('SUM(invoice_amount) as final_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `invoice_type` = 2 AND invoice_status = 1');
		$query = $this->db->get();
		$final_reading = 0;
		foreach ($query->result() as $key) {

			# code...
			$final_reading = $key->final_reading;
			if(is_null($final_reading))
			{
				$final_reading = 0;

			}
			else
			{
				$final_reading = $key->final_reading;

			}
		}
		return $final_reading;
	}
	public function get_water_payments($lease_id)
	{
		$this->db->from('water_payments');
		$this->db->select('SUM(amount_paid) as final_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `payment_status` = 1 AND cancel = 0');
		$query = $this->db->get();
		$final_reading = 0;
		foreach ($query->result() as $key) {

			# code...
			$final_reading = $key->final_reading;
			if(is_null($final_reading))
			{
				$final_reading = 0;

			}
			else
			{
				$final_reading = $key->final_reading;

			}
		}
		return $final_reading;
	}
	public function get_deposits($lease_id)
	{
		
		$this->db->from('property_billing');
		$this->db->select('billing_amount as final_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `property_billing_status` = 1 AND invoice_type_id = 13');
		$query = $this->db->get();
		$final_reading = 0;
		foreach ($query->result() as $key) {
          # code...
			$final_reading = $key->final_reading;
			if(is_null($final_reading))
			{
				$final_reading = 0;

			}
			else
			{
				$final_reading = $key->final_reading;

			}
	
		}
		return $final_reading;
	}
	public function get_expenses($lease_id)
	{
		
		$this->db->from('leases');
		$this->db->select('expense_amount as final_reading');
		$this->db->where('lease_id = '.$lease_id. ' AND `lease_status` = 2');
		$query = $this->db->get();
		$final_reading = 0;
		foreach ($query->result() as $key) {
          # code...
			$final_reading = $key->final_reading;
			if(is_null($final_reading))
			{
				$final_reading = 0;

			}
			else
			{
				$final_reading = $key->final_reading;

			}
	
		}
		return $final_reading;
	}
	public function get_actual_paint_charges($lease_id)
	{
		$lease_id = 643;
		$this->db->from('leases');
		$this->db->select('tenant_unit_id');
		$this->db->where('lease_id = '.$lease_id. ' AND `lease_status` = 2');
		$query = $this->db->get();
		$tenant_unit_id = 0;
		foreach ($query->result() as $key) {
          # code...
			$tenant_unit_id = $key->tenant_unit_id;

			$rental_unit_id = $this->get_rental_unit_id($tenant_unit_id);
			
			$paint_charges = $this->get_paint_charges($rental_unit_id);
			
			

			
		}
		return $paint_charges;

	}
	public function get_rental_unit_id($tenant_unit_id)
	{
		
		$this->db->from('tenant_unit');
		$this->db->select('rental_unit_id');
		$this->db->where('tenant_unit_id = '.$tenant_unit_id);
		$query = $this->db->get();
		$rental_unit_id = 0;
		foreach ($query->result() as $key) {
          # code...
			$rental_unit_id = $key->rental_unit_id;

			
		}
		return $rental_unit_id;
	}
	public function get_paint_charges($rental_unit_id)
	{
		
		$this->db->from('rental_unit');
		$this->db->select('unit_capacity_id, property_id');
		$this->db->where('rental_unit_id = '.$rental_unit_id. ' AND `rental_unit_status` = 1');
		$query = $this->db->get();
		$unit_capacity_id = 0;
		$property_id = 0;
		foreach ($query->result() as $key) {
          # code...
			$property_id = $key->property_id;
			$unit_capacity_id = $key->unit_capacity_id;
			if($property_id == 3)
			{
				$paint_charges = 3500;
			}
			else if($property_id == 1)
			{
				$paint_charges = 3500;
			}
			else if($unit_capacity_id == 1 AND $property_id == 8)
			{
				$paint_charges = 4500;
			}
			else if($unit_capacity_id == 2 AND $property_id == 8)
			{
				$paint_charges = 5500;
			}
			else if($unit_capacity_id == 3 AND $property_id == 8)
			{
				$paint_charges = 6500;
			}
			else
			{
				$paint_charges = 3500;
			}

			
		}
		return $paint_charges;
	}

	public function export_liabilities()
	{

		$this->load->library('excel');
		
		//get all transactions
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 2 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';	

		
		// $transaction_search = $this->session->userdata('all_transactions_search');
		// $table_search = $this->session->userdata('all_transactions_tables');
		
		// if(!empty($transaction_search))
		// {
		// 	$where .= $transaction_search;
		
		// 	if(!empty($table_search))
		// 	{
		// 		$table .= $table_search;
		// 	}
			
		// }

		
		$all_liability_search = $this->session->userdata('all_liability_search');
		if(!empty($all_liability_search))
		{
			$where .= $all_liability_search;
		}
		
		$this->db->where($where);
		// $this->db->limit(10);
		$this->db->order_by('rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', 'ASC');
		$this->db->select('*');
		$transactions_query = $this->db->get($table);
		
		$title = 'Liabilities Report';
		
		
		if($transactions_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Unit';
			$report[$row_count][2] = 'L-ID.';
			$report[$row_count][3] = 'Name';
			$report[$row_count][4] = 'Entry';
			$report[$row_count][5] = 'Exit';
			$report[$row_count][6] = 'Period';
			$report[$row_count][7] = 'Rent Arrears BF';
			$report[$row_count][8] = 'Invoiced Rent';
			$report[$row_count][9] = 'Rent Payments';
			$report[$row_count][10] = 'Rent Liability';
			$report[$row_count][11] = 'Final Reading';
			$report[$row_count][12] = 'Initial Reading';
			$report[$row_count][13] = 'Cons 200 per Unit';
			$report[$row_count][14] = 'Water Arrears BF';
			$report[$row_count][15] = 'Water Invoiced';
			$report[$row_count][16] = 'Water Paid';
			$report[$row_count][17] = 'Water Liability';
			$report[$row_count][18] = 'Deposit';
			$report[$row_count][19] = 'Expenses Inc. Paint';
			$report[$row_count][20] = 'Liability';
			$report[$row_count][21] = 'Water Profit';
			$report[$row_count][22] = 'Balance';

			//get & display all services
			
			//display all patient data in the leftmost columns
			foreach($transactions_query->result() as $leases_row)
			{
				
				$lease_id = $leases_row->lease_id;
				$tenant_unit_id = $leases_row->tenant_unit_id;
				$rental_unit_id = $leases_row->unit_id;
				$rental_unit_name = $leases_row->rental_unit_name;
				$tenant_name = $leases_row->tenant_name;
				$tenant_email = $leases_row->tenant_email;
				$tenant_phone_number = $leases_row->tenant_phone_number;
				$lease_start_date = $leases_row->lease_start_date;
				$lease_end_date = $leases_row->closing_end_date;
				//var_dump($lease_id);die();

				//$period = $this->liabilities_model->get_period($lease_id);
				$diff = abs(strtotime($lease_end_date) - strtotime($lease_start_date));

		        $years = floor($diff / (365*60*60*24));
		        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

		        if ($years == 0)
		        {
		        	if($months == 0)
		        	{
		        	 $period = 'No Period';
		        	}
		        	else if($month == 1)
		        	{
		        	 $period = $months.' Month';
		        	}
		        	else
		        	{
		        	 $period = $months.' Months';
		        	}

		        }
		        else  if ($years == 1)
		        {
		        	if($months == 0)
		        	{
		        	 $period = $years.' Year';
		        	}
		        	else if($month == 1)
		        	{
		        	 $period = $years.' Year '.$months.' Month';
		        	}
		        	else
		        	{
		        	 $period = $years.' Year '.$months.' Months';
		        	}
		        }
		        else
		        {
		        	if($months == 0)
		        	{
		        	 $period = $years.' Years';
		        	}
		        	else if($month == 1)
		        	{
		        	 $period = $years.' Years '.$months.' Month';
		        	}
		        	else
		        	{
		        	 $period = $years.' Years '.$months.' Months';
		        	}
		        }
		        

				$rent_arrears_bf = $this->liabilities_model->get_rent_arrears_bf($lease_id);
				$invoice_rent = $this->liabilities_model->get_invoiced_rent($lease_id);
				$total_rent = $this->liabilities_model->get_total_rent($lease_id);
				$rent_liability = $rent_arrears_bf + $invoice_rent - $total_rent;


				$initial_reading = $this->liabilities_model->get_intial_water_reading($lease_id);
				$final_reading = $this->liabilities_model->get_final_water_reading($lease_id);
				if($final_reading < $initial_reading)
				{
					$final_reading = $initial_reading;

					$Consumption = $final_reading - $initial_reading;
				}
				else
				{
					$Consumption = $final_reading - $initial_reading;
				}
				

				$water_arrears_bf = $this->liabilities_model->get_water_arrears_bf($lease_id);
				$water_invoice = $this->liabilities_model->get_water_invoice($lease_id);
				$water_payments = $this->liabilities_model->get_water_payments($lease_id);
				$water_liabilty = $water_arrears_bf + $water_invoice - $water_payments;
				$rent_deposits = $this->liabilities_model->get_deposits($lease_id);
				$expenses = $this->liabilities_model->get_expenses($lease_id);
				$paint_charges = $this->liabilities_model->get_actual_paint_charges($lease_id);
				
			    if($rent_deposits == $expenses)
				{
					$expenses = $rent_liability + $water_liabilty + $paint_charges;
					$refunds = $rent_liability + $water_liabilty + $paint_charges - $rent_deposits;

				}
				else
				{
				     $refunds =  $rent_liability + $water_liabilty + $expenses - $rent_deposits;
		        }
		        $water_profit = ($water_payments/200)*50;
		        $total_bal = $refunds - $water_profit;


				

				
				

				$lease_duration = $leases_row->lease_duration;
				$rent_amount = $leases_row->rent_amount;
				$lease_number = $leases_row->lease_number;
				$arreas_bf = $leases_row->arrears_bf;
				$rent_calculation = $leases_row->rent_calculation;
				$deposit = $leases_row->deposit;
				$deposit_ext = $leases_row->deposit_ext;
				$lease_status = $leases_row->lease_status;
				$points = 0;//$leases_row->points;
				$created = $leases_row->created;





				$count++;
				

				$row_count++;
						
						
						
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $rental_unit_name;
				$report[$row_count][2] = $lease_id;
				$report[$row_count][3] = $tenant_name;
				$report[$row_count][4] = $lease_start_date;
				$report[$row_count][5] = $lease_end_date;

				$report[$row_count][6] = $period;
				$report[$row_count][7] = number_format($rent_arrears_bf,2);
				$report[$row_count][8] = number_format($invoice_rent,2);
				$report[$row_count][9] = number_format($total_rent,2);
				$report[$row_count][10] = number_format($rent_liability,2);

				$report[$row_count][11] = $final_reading;
				$report[$row_count][12] = $initial_reading;
				$report[$row_count][13] = $Consumption;
				$report[$row_count][14] = number_format($water_arrears_bf,2);
				$report[$row_count][15] = number_format($water_invoice,2);
				$report[$row_count][16] = number_format($water_payments,2);
				$report[$row_count][17] = number_format($water_liabilty,2);
				$report[$row_count][18] = number_format($rent_deposits,2);
				$report[$row_count][19] = number_format($expenses,2);
				$report[$row_count][20] = number_format($refunds,2);
				$report[$row_count][21] = number_format($water_profit,2);
				$report[$row_count][22] = number_format($total_bal,2);

				
			}
		}
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);

	}

	
}