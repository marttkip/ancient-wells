<?php

class Landlord_model extends CI_Model
{
  /*
  *	Count all items from a table
  *	@param string $table
  * 	@param string $where
  *
  */
  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }


  public function get_account_payments_transactions($table, $where, $config, $page, $order, $order_method)
  {
    //retrieve all accounts
    $this->db->from($table);
    $this->db->select('landlord_transactions.*,property.property_name,property_owners.property_owner_name,account.account_name');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('account', 'account.account_id = landlord_transactions.bank_id','left');
    $this->db->join('property', 'property.property_id = landlord_transactions.property_id','left');
    $this->db->join('property_owners', 'property_owners.property_owner_id = property.property_owner_id','left');
    $query = $this->db->get('', $config, $page);

    return $query;
  }


  public function get_account_payments_receipt($table, $where, $config, $page, $order, $order_method)
  {
    //retrieve all accounts
    $this->db->from($table);
    $this->db->select('landlord_receipts.*,property.property_name,property_owners.property_owner_name,account.account_name');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('account', 'account.account_id = landlord_receipts.account_to_id','left');
    $this->db->join('property', 'property.property_id = landlord_receipts.property_id','left');
    $this->db->join('property_owners', 'property_owners.property_owner_id = property.property_owner_id','left');
    $query = $this->db->get('', $config, $page);

    return $query;
  }


  public function add_payment_amount()
  {

    $document_number_two = $this->create_transaction_number();
    $transaction_explode = explode('-',$this->input->post('transaction_date'));
    // $year = $transaction_explode[0];
    // $month = $transaction_explode[1];

    $this->db->where('property_id',$this->input->post('property_id'));
    $query = $this->db->get('property');
    $row = $query->row();



    $landlord_id = $row->property_owner_id;
    $account = array(
          'account_to_id'=>$this->input->post('account_to_id'),
          'property_id'=>$this->input->post('property_id'),
          'landlord_transaction_description'=>$this->input->post('description'),
          'remarks'=>$this->input->post('description'),
          'bank_id'=>$this->input->post('bank_id'),
          'transaction_type_id'=>$this->input->post('transaction_type_id'),
          'transaction_number'=>$this->input->post('transaction_number'),
          'transaction_date'=>$this->input->post('transaction_date'),
          'payment_method_id'=>$this->input->post('payment_method'),
          'created_by'=>$this->session->userdata('personnel_id'),
          'document_number'=>$document_number_two,
          'landlordid'=>$landlord_id,
          'month'=>$this->input->post('month'),
          'year'=>$this->input->post('year'),
          'chequeno'=>$this->input->post('transaction_number'),
          'paymenttermid'=>$this->input->post('invoice_type_id'),
          'created'=>date('Y-m-d H:i:s'),
          'last_modified'=>date('Y-m-d H:i:s')
          );
      $transaction_type_id = $this->input->post('transaction_type_id');
      if($transaction_type_id == 4)
      {

        $account['landlord_transaction_amount']  = -$this->input->post('transacted_amount');
      }
      else {
        $account['landlord_transaction_amount']  = $this->input->post('transacted_amount');
      }
    if($this->db->insert('landlord_transactions',$account))
    {
      $landlord_transaction_id = $this->db->insert_id();

      $transaction_type_id = $this->input->post('transaction_type_id');
      if($transaction_type_id == 4)
      {
        $account = array(
              'account_to_id'=>$this->input->post('account_to_id'),
              'property_id'=>$this->input->post('property_to_id'),
              'landlord_transaction_description'=>$this->input->post('description'),
              'remarks'=>$this->input->post('description'),
              'bank_id'=>$this->input->post('bank_id'),
              'transaction_type_id'=>$this->input->post('transaction_type_id'),
              'transaction_number'=>$this->input->post('transaction_number'),
              'transaction_date'=>$this->input->post('transaction_date'),
              'payment_method_id'=>$this->input->post('payment_method'),
              'created_by'=>$this->session->userdata('personnel_id'),
              'document_number'=>$document_number_two,
              'landlordid'=>$landlord_id,
              'month'=>$this->input->post('month'),
              'year'=>$this->input->post('year'),
              'chequeno'=>$this->input->post('transaction_number'),
              'paymenttermid'=>$this->input->post('invoice_type_id'),
              'parent_transaction'=>$landlord_transaction_id,
              'created'=>date('Y-m-d H:i:s'),
              'last_modified'=>date('Y-m-d H:i:s')
              );
          $transaction_type_id = $this->input->post('transaction_type_id');
          $account['landlord_transaction_amount']  = $this->input->post('transacted_amount');
          $this->db->insert('landlord_transactions',$account);
      }
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }


  public function add_receipt_amount()
  {

    $document_number_two = $this->create_receipt_number();
    $transaction_explode = explode('-',$this->input->post('transaction_date'));
    // $year = $transaction_explode[0];
    // $month = $transaction_explode[1];

    $this->db->where('property_id',$this->input->post('property_id'));
    $query = $this->db->get('property');
    $row = $query->row();



    $landlord_id = $row->property_owner_id;
    $account = array(
          'account_to_id'=>$this->input->post('account_to_id'),
          'property_id'=>$this->input->post('property_id'),
          'landlord_receipt_description'=>$this->input->post('description'),
          'remarks'=>$this->input->post('description'),
          'bank_id'=>$this->input->post('bank_id'),
          'transaction_number'=>$this->input->post('transaction_number'),
          'transaction_date'=>$this->input->post('transaction_date'),
          'payment_method_id'=>$this->input->post('payment_method'),
          'created_by'=>$this->session->userdata('personnel_id'),
          'document_number'=>$document_number_two,
          'landlordid'=>$landlord_id,
          'month'=>$this->input->post('month'),
          'year'=>$this->input->post('year'),
          'chequeno'=>$this->input->post('transaction_number'),
          'paymenttermid'=>$this->input->post('invoice_type_id'),
          'created'=>date('Y-m-d H:i:s'),
          'last_modified'=>date('Y-m-d H:i:s')
          );
      $transaction_type_id = $this->input->post('transaction_type_id');
      if($transaction_type_id == 4)
      {

        $account['landlord_receipt_amount']  = -$this->input->post('transacted_amount');
      }
      else {
        $account['landlord_receipt_amount']  = $this->input->post('transacted_amount');
      }
    if($this->db->insert('landlord_receipts',$account))
    {
      $landlord_receipt_id = $this->db->insert_id();

      $transaction_type_id = $this->input->post('transaction_type_id');
      if($transaction_type_id == 4)
      {
        $account = array(
              'account_to_id'=>$this->input->post('account_to_id'),
              'property_id'=>$this->input->post('property_to_id'),
              'landlord_receipt_description'=>$this->input->post('description'),
              'remarks'=>$this->input->post('description'),
              'bank_id'=>$this->input->post('bank_id'),
              'transaction_number'=>$this->input->post('transaction_number'),
              'transaction_date'=>$this->input->post('transaction_date'),
              'payment_method_id'=>$this->input->post('payment_method'),
              'created_by'=>$this->session->userdata('personnel_id'),
              'document_number'=>$document_number_two,
              'landlordid'=>$landlord_id,
              'month'=>$this->input->post('month'),
              'year'=>$this->input->post('year'),
              'chequeno'=>$this->input->post('transaction_number'),
              'paymenttermid'=>$this->input->post('invoice_type_id'),
              'parent_transaction'=>$landlord_receipt_id,
              'created'=>date('Y-m-d H:i:s'),
              'last_modified'=>date('Y-m-d H:i:s')
              );
          $transaction_type_id = $this->input->post('transaction_type_id');
          $account['landlord_receipt_amount']  = $this->input->post('transacted_amount');
          $this->db->insert('landlord_receipts',$account);
      }
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }

  function create_transaction_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('landlord_transactions');
		$this->db->where("landlord_transaction_id > 0");
		$this->db->select('MAX(document_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;

			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}

		return $number;
	}
  function create_receipt_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('landlord_receipts');
		$this->db->where("landlord_receipt_id > 0");
		$this->db->select('MAX(document_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;

			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}

		return $number;
	}

public function get_landlord_transactions($where, $table)
{
    $this->db->select('landlord_transactions.*,property.property_name,property_owners.property_owner_name,account.account_name,payment_method.payment_method,account.account_id');
    $this->db->where($where);
    $this->db->join('account', 'account.account_id = landlord_transactions.account_to_id','left');
    $this->db->join('property', 'property.property_id = landlord_transactions.property_id','left');
    $this->db->join('property_owners', 'property_owners.property_owner_id = property.property_owner_id','left');
    $this->db->join('payment_method', 'payment_method.payment_method_id = landlord_transactions.payment_method_id','left');
    // $this->db->join('account', 'account.account_id = landlord_transactions.bank_id','left');
    $query = $this->db->get($table);

  return $query;
}

public function get_transaction_details($landlord_transaction_id)
{
  $this->db->select('landlord_transactions.*');
  $this->db->where('landlord_transaction_id',$landlord_transaction_id);
  $query = $this->db->get('landlord_transactions');

  return $query;
}




  public function update_payment_amount($landlord_transaction_id)
  {

    // $document_number_two = $this->create_transaction_number();
    $transaction_explode = explode('-',$this->input->post('transaction_date'));
    // $year = $transaction_explode[0];
    // $month = $transaction_explode[1];

    $this->db->where('property_id',$this->input->post('property_id'));
    $query = $this->db->get('property');
    $row = $query->row();

    $landlord_id = $row->property_owner_id;
    $account = array(
          'account_to_id'=>$this->input->post('account_to_id'),
          'property_id'=>$this->input->post('property_id'),
          'landlord_transaction_description'=>$this->input->post('description'),
          'remarks'=>$this->input->post('description'),
          'bank_id'=>$this->input->post('bank_id'),
          'transaction_type_id'=>$this->input->post('transaction_type_id'),
          'transaction_number'=>$this->input->post('transaction_number'),
          'transaction_date'=>$this->input->post('transaction_date'),
          'payment_method_id'=>$this->input->post('payment_method'),
          'modified_by'=>$this->session->userdata('personnel_id'),
          'landlordid'=>$landlord_id,
          'month'=>$this->input->post('month'),
          'year'=>$this->input->post('year'),
          'chequeno'=>$this->input->post('transaction_number'),
          'paymenttermid'=>$this->input->post('invoice_type_id'),
          'last_modified'=>date('Y-m-d H:i:s')
          );
      $transaction_type_id = $this->input->post('transaction_type_id');
      $account['landlord_transaction_amount']  = $this->input->post('transacted_amount');
      
    $this->db->where('landlord_transaction_id',$landlord_transaction_id);
    if($this->db->update('landlord_transactions',$account))
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }

  public function get_account_transactions($table, $where, $config, $page, $order, $order_method)
  {
    //retrieve all accounts
    $this->db->from($table);
    $this->db->select('transactions.*,property.property_name,property_owners.property_owner_name');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    // $this->db->join('account', 'account.account_id = transactions.bank_id','left');
    $this->db->join('property', 'property.property_id = transactions.property_id','left');
    $this->db->join('property_owners', 'property_owners.property_owner_id = transactions.property_owner_id','left');
    $query = $this->db->get('', $config, $page);

    return $query;
  }
  function get_months()
  {

    return $this->db->get('month');
  }
  /*
  * Retrieve all front end property_owners
  *
  */
  public function get_all_front_end_property_owners()
  {
    $this->db->from('property_owners');
    $this->db->select('*');
    $this->db->where('property_owner_id > 0 AND property_owner_status = 1');
    $query = $this->db->get();
    
    return $query;
  }

  public function generate_transaction_report()
  {

    

    $data = array(
      'property_owner_id'=>$this->input->post('company_id'),
      'transaction_year'=>$this->input->post('year'),
      'transaction_month'=>$this->input->post('month'),
      'transaction_date'=>$this->input->post('invoice_date'),
      'created_by'=>$this->session->userdata('personnel_id'),
      'created'=>date('Y-m-d')
    );
    $suffix = $main_suffix = $this->create_document_number();

    if($suffix > 100)
    {
      if($suffix < 10)
      {
        $suffix = '00'.$suffix;
      }
      else
      {
        $suffix = '0'.$suffix;
      }
    }
    else
    {
      $suffix = $suffix;
    }

    $data['suffix'] = $main_suffix;
    $data['prefix'] = 'LT';
    $data['transaction_number'] = $data['prefix'].$data['suffix'];
    if($this->db->insert('transactions', $data))
    {
      return $this->db->insert_id();
    }
    else{
      return FALSE;
    }
  }

  function create_document_number()
  {
    //select product code
    $preffix = "HA-RT-";
    $this->db->from('transactions');
    $this->db->where("transaction_id > 0");
    $this->db->select('MAX(suffix) AS number');
    $query = $this->db->get();//echo $query->num_rows();

    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;

      $number++;//go to the next number
    }
    else{//start generating receipt numbers
      $number = 1;
    }

    return $number;
  }


  public function get_all_income($table, $where, $config, $page, $order, $order_method)
  {
    //retrieve all accounts
    $this->db->from($table);
    $this->db->select('incomes.*,property.property_name,invoice_type.invoice_type_name');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('invoice_type', 'invoice_type.invoice_type_id = incomes.invoice_type_id','left');
    $this->db->join('property', 'property.property_id = incomes.property_id','left');
    // $this->db->join('property_owners', 'property_owners.property_owner_id = property.property_owner_id','left');
    $query = $this->db->get('', $config, $page);

    return $query;
  }

   public function add_income_amount()
  {

    
    

    $transaction_id = $this->input->post('transaction_id');

    $account = array(
          'property_id'=>$this->input->post('property_id'),
          'invoice_type_id'=>$this->input->post('invoice_type_id'),
          'transacted_amount'=>$this->input->post('transacted_amount'),
          'transaction_description'=>$this->input->post('transaction_description'),
          'created_by'=>$this->session->userdata('created_by'),
          'transaction_id'=>$transaction_id,
          'created'=>date('Y-m-d H:i:s')
          );

    $suffix = $main_suffix = $this->create_income_document_number();

    if($suffix > 100)
    {
      if($suffix < 10)
      {
        $suffix = '00'.$suffix;
      }
      else
      {
        $suffix = '0'.$suffix;
      }
    }
    else
    {
      $suffix = $suffix;
    }

    $account['suffix'] = $main_suffix;
    $account['prefix'] = 'IC';
    $account['transaction_number'] = $account['prefix'].$account['suffix'];
    if($this->db->insert('incomes',$account))
    {
      
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }


  function create_income_document_number()
  {
    //select product code
    $preffix = "HA-RT-";
    $this->db->from('incomes');
    $this->db->where("income_id > 0");
    $this->db->select('MAX(suffix) AS number');
    $query = $this->db->get();//echo $query->num_rows();

    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;

      $number++;//go to the next number
    }
    else{//start generating receipt numbers
      $number = 1;
    }

    return $number;
  }
  public function get_total_income($transaction_id)
  {

      $this->db->from('incomes');
      $this->db->where("income_delete = 0 AND transaction_id = ".$transaction_id);
      $this->db->select('SUM(transacted_amount) AS total_amount');
      $query = $this->db->get();//echo $query->num_rows();

      if($query->num_rows() > 0)
      {
        $result = $query->result();
        $total_amount =  $result[0]->total_amount;

      }
      else{
        $total_amount = 0;
      }
      if(empty($total_amount))
      {
        $total_amount = 0;
      }
      return $total_amount;

  }

  public function get_total_expenses($transaction_id)
  {

      $this->db->from('finance_purchase,finance_purchase_payment');
      $this->db->where("finance_purchase.finance_purchase_deleted = 0 AND finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase.transaction_id =".$transaction_id);
      $this->db->select('SUM(finance_purchase.finance_purchase_amount) AS total_amount');
      $query = $this->db->get();//echo $query->num_rows();

      if($query->num_rows() > 0)
      {
        $result = $query->result();
        $total_amount =  $result[0]->total_amount;

      }
      else{
        $total_amount = 0;
      }
      if(empty($total_amount))
      {
        $total_amount = 0;
      }
      return $total_amount;

  }

  public function get_total_transactions($transaction_id)
  {

      $this->db->from('account_payments');
      $this->db->where("account_payment_deleted = 0 AND transaction_id =".$transaction_id);
      $this->db->select('SUM(amount_paid) AS total_amount');
      $query = $this->db->get();//echo $query->num_rows();

      if($query->num_rows() > 0)
      {
        $result = $query->result();
        $total_amount =  $result[0]->total_amount;

      }
      else{
        $total_amount = 0;
      }
      if(empty($total_amount))
      {
        $total_amount = 0;
      }
      return $total_amount;

  }

  public function get_income_transactions($where, $table)
  {
      $this->db->select('incomes.created AS payment_date,property.*,incomes.*,invoice_type.*');
      $this->db->where($where);
      $this->db->join('property', 'property.property_id = incomes.property_id','left');
      $this->db->join('invoice_type', 'invoice_type.invoice_type_id = incomes.invoice_type_id','left');
      $query = $this->db->get($table);

    return $query;
  }

  public function get_expense_transactions($where, $table)
  {
      $this->db->select('*');
      $this->db->where($where);
      // $this->db->join('property', 'property.property_id = incomes.property_id','left');
      $query = $this->db->get($table);

    return $query;
  }

  public function get_payments_transactions($where, $table)
  {
      $this->db->select('*');
      $this->db->where($where);
      $this->db->join('property_beneficiaries', 'property_beneficiaries.property_beneficiary_id = account_payments.property_beneficiary_id','left');
      $query = $this->db->get($table);

    return $query;
  }




}
?>
