<?php echo $this->load->view('generate_list','', true); ?>
<?php
	$result = '';
	$action_point = '';
	//if users exist display them
	// var_dump($property_invoices);die();
	// 
	$result2 = '';
	//if users exist display them
	if ($query_purchases->num_rows() > 0)
	{
		$count = $page;
		
		$result2 .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Transaction Date</th>
					<th>Company</th>
					<th>Transaction Number</th>
					<th>Period</th>
					<th>Income</th>
					<th>Expenses</th>
					<th>Payments</th>
					<th>Balance</th>
					<th>Status</th>
					<th colspan="5">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($query_purchases->result() as $row)
		{
			$transaction_id = $row->transaction_id;
			$property_owner_name = $row->property_owner_name;
			$transaction_number = $row->transaction_number;
			$transaction_date = date('jS M Y',strtotime($row->transaction_date));
			$transaction_status = $row->transaction_status;
		
			$transaction_month = $row->transaction_month;
			$transaction_year = $row->transaction_year;
			$created = date('F Y',strtotime($transaction_year.'-'.$transaction_month));

			if($transaction_status == 1)
			{
				$buttons = '<td><a href="'.site_url().'income/'.$transaction_id.'"  class="btn btn-sm btn-success" ><i class="fa fa-folder-open"></i> Income List</a></td>
						
							<td><a href="'.site_url().'expenses/'.$transaction_id.'"  class="btn btn-sm btn-warning" ><i class="fa fa-folder-open"></i> Expenses </a></td>
							<td><a href="'.site_url().'landlord-payments/'.$transaction_id.'"  class="btn btn-sm btn-info" ><i class="fa fa-folder-open"></i> Landlord Payments </a></td>
							<td><a href="'.site_url().'close-transactions/'.$transaction_id.'/2"  class="btn btn-sm btn-warning" onclick="return confirm(\' You are about to mark this invoice as completed note that you will not be able to adjust any invoices for this particular transaction_month. Do you want to proceed ? \')"><i class="fa fa-folder-closed" ></i>Close Transactions</a></td>
							<td><a href="'.site_url().'close-property-invoice/'.$transaction_id.'/3"  class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this invoice\')"><i class="fa fa-trash"></i></a></td>';
				$status = 'success';
				$status_name = 'Open';
			}
			else if($transaction_status == 2)
			{
				$buttons = '<td><a href="'.site_url().'open-transactions/'.$transaction_id.'/1"  class="btn btn-sm btn-success" onclick="return confirm(\' You are about to mark this invoice as not completed. Do you want to proceed ? \')"><i class="fa fa-folder-open" ></i> Open Transactions </a></td>
					<td><a href="'.site_url().'print-transactions/'.$transaction_id.'"  class="btn btn-sm btn-info" target="_blank"><i class="fa fa-folder-open" ></i> Print Report </a></td>
					<td><a href="'.site_url().'export-reading-details/'.$transaction_id.'"  class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-folder-open"></i> Export Report </a></td>';
				$status = 'warning';
				$status_name = 'Closed';
				// <td><a href="'.site_url().'print-reading-details/'.$transaction_id.'"  class="btn btn-sm btn-info" target="_blank"><i class="fa fa-folder-open" ></i> Print Report </a></td>
			}

			else
			{
				$buttons = '<td><a href="'.site_url().'view-reading-details/'.$transaction_id.'"  class="btn btn-sm btn-success" ><i class="fa fa-folder-open"></i> View</a></td>';
				$status = '';
				$status_name = 'success';
			}

			$income = $this->landlord_model->get_total_income($transaction_id);
			$expenses = $this->landlord_model->get_total_expenses($transaction_id);
			$landlord_payments = $this->landlord_model->get_total_transactions($transaction_id);
			$balance = $income - $expenses - $landlord_payments;
			$count++;
			$result2 .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td class="'.$status.'">'.$transaction_date.'</td>
					<td >'.$property_owner_name.'</td>
					<td >'.$transaction_number.'</td>
					<td >'.$created.'</td>
					
					<td >'.number_format($income,2).'</td>
					<td >'.number_format($expenses,2).'</td>
					<td >'.number_format($landlord_payments,2).'</td>
					<td >'.number_format($balance).'</td>
					<td >'.$status_name.'</td>
					'.$buttons.'
	
				</tr> 
			';
		}
		
		$result2 .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result2 .= "There are no invoices";
	}
?>

<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title">Transactions</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
            	<?php
					$success = $this->session->userdata('success_message');
				
					if(!empty($success))
					{
						echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
						$this->session->unset_userdata('success_message');
					}
				
					$error = $this->session->userdata('error_message');
				
					if(!empty($error))
					{
						echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
						$this->session->unset_userdata('error_message');
					}

					$search =  $this->session->userdata('all_water_property');
					if(!empty($search))
					{
						echo '<a href="'.site_url().'water_management/close_water_search" class="btn btn-sm btn-warning">Close Search</a>';
					}
				?>
				
				<?php
					if(isset($import_response))
					{
						if(!empty($import_response))
						{
							echo $import_response;
						}
					}
					
					if(isset($import_response_error))
					{
						if(!empty($import_response_error))
						{
							echo '<div class="center-align alert alert-danger">'.$import_response_error.'</div>';
						}
					}
				?>
               <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <?php echo $result2;?>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
				    <?php if(isset($links)){echo $links;}?>
				</div>
                                                
            </div>
        </div>
    </div>
</section>