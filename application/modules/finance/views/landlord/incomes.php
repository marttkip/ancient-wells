<?php //echo $this->load->view('search/search_petty_cash', '', TRUE);
	$property_owner_id =0;
    if($transaction_id > 0)
    {
      $details = $this->transfer_model->get_transaction_detail($transaction_id);

      if($details->num_rows() > 0)
      {
        foreach ($details->result() as $key => $value) {
          # code...
          $property_owner_id = $value->property_owner_id;
        }
      }
      $properties = $this->property_model->get_active_property($property_owner_id);
      // var_dump($properties);die();
    }
    else
    {
      $properties = $this->property_model->get_active_property();
    }



    
    $rs8 = $properties->result();
    $property_list = '';
    foreach ($rs8 as $property_rs) :
        $property_id = $property_rs->property_id;
        $property_name = $property_rs->property_name;
        $property_location = $property_rs->property_location;

        $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

    endforeach;


    $invoice_type_order = 'invoice_type.invoice_type_id';
    $invoice_type_table = 'invoice_type';
    $invoice_type_where = 'invoice_type.invoice_type_status = 1';

    $invoice_type_query = $this->tenants_model->get_tenant_list($invoice_type_table, $invoice_type_where, $invoice_type_order);
    $rs8 = $invoice_type_query->result();
    $invoice_type_list = '';
    foreach ($rs8 as $invoice_rs) :
      $invoice_type_id = $invoice_rs->invoice_type_id;
      $invoice_type_name = $invoice_rs->invoice_type_name;


        $invoice_type_list .="<option value='".$invoice_type_id."'>".$invoice_type_name."</option>";

    endforeach;

    $v_data['invoice_type_list'] = $invoice_type_list;

?>
<!--end reports -->
<div class="row">
    
    <div class="col-md-12">
       <section class="panel panel-success">
          <header class="panel-heading">
            <h3 class="panel-title"><?php echo $title;?></h3>
             <div class="panel-tools pull-right" style="margin-top: -25px;">
                <?php

                if($transaction_id > 0)
                {
                  ?>
                   <a href="<?php echo site_url().'transactions'?>"  class="btn btn-info btn-sm" >Back to Transactions</a>
                  <?php
                }
                ?>
              </div>
          </header>
          <div class="panel-body">

             <?php echo form_open("finance/landlord/record_income", array("class" => "form-horizontal"));?>
               <div class="row">
                 <div class="col-md-12">
                 <div class="col-md-3">
                       <div class="form-group">
                           <label class="col-lg-4 control-label">Property</label>

                           <div class="col-lg-8">
                              <select  name='property_id' class='form-control' required="required">
                                 <option value=''>------- SELECT PROPERTY -------</option>
                                 <?php echo $property_list;?>
                               </select>
                           </div>
                       </div>
                 </div>
                 <div class="col-md-3">
                       <div class="form-group">
                           <label class="col-lg-4 control-label">Invoice Type</label>

                           <div class="col-lg-8">
                              <select  name='invoice_type_id' class='form-control '  required="required">
                                 <option value=''>---- SELECT INVOICE TYPE ------</option>
                                 <?php echo $invoice_type_list;?>
                               </select>
                           </div>
                       </div>
                 </div>
                 <input type="hidden" name="transaction_id" value="<?php echo $transaction_id?>">
                 <div class="col-md-2">
                   
                   <div class="form-group">
                       <label class="col-md-4 control-label">Amount *</label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="transacted_amount" placeholder="Amount" required/>
                       </div>
                   </div>
                 </div>
                 <div class="col-md-3">
                   
                   <div class="form-group">
                       <label class="col-md-4 control-label">Description</label>

                       <div class="col-md-8">
                          <textarea class="form-control" name="transaction_description" placeholder="Transaction Description"></textarea>
                       </div>
                   </div>
                 </div>
                 <div class="col-md-1">
                   <div class="col-md-12">
                           <div class="text-center">
                               <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to submit this record ?')">Save record</button>
                           </div>
                       </div>
                 </div>
                 </div>


               </div>
               <?php echo form_close();?>


            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12">


			<?php
      $search = $this->session->userdata('search_landlord_payments');
			if(!empty($search))
			{
				?>
                <a href="<?php echo base_url().'finance/landlord/close_landlord_payments_search';?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
                <?php
			}


			$result =  '';

			// echo $result;

      $result = '';

  if($query_purchases->num_rows() > 0)
  {
    $count = $page;
    foreach ($query_purchases->result() as $key => $value) {
      // code...
      $transaction_number = $value->transaction_number;
      $created = $value->created;
      $property_name = $value->property_name;
      $transacted_amount = $value->transacted_amount;
      $invoice_type_name = $value->invoice_type_name;
      $income_id = $value->income_id;
      $transaction_description = $value->transaction_description;
     

      $balance = $transacted_amount;
      $count++;
      $result .='
                <tr>
                  <td>'.$count.'</td>
                  <td>'.$created.'</td>
                  <td>'.$transaction_number.'</td>
                  <td>'.$property_name.'</td>
                  <td>'.$invoice_type_name.'</td>
                  <td>'.$transaction_description.'</td>
                  <td>'.number_format($transacted_amount,2).'</td>
                  <td><a href="'.site_url().'delete-income/'.$income_id.'/'.$transaction_id.'"  class="btn btn-danger btn-sm"  onclick="return confirm(\'Are you sure you want to remove this record ? \')"><i class="fa fa-trash"></i></a></td>
              ';
    }
  }
?>
 <section class="panel panel-success">
    <header class="panel-heading">
      <h3 class="panel-title">All Incomes</h3>
       <div class="panel-tools pull-right" style="margin-top: -25px;">
                <?php

                if($transaction_id > 0)
                {
                  ?>
                   <a href="<?php echo site_url().'transactions'?>"  class="btn btn-info btn-sm" >Back to Transactions</a>
                  <?php
                }
                ?>
              </div>
    </header>
    <div class="panel-body">
    <?php
    $error = $this->session->userdata('error_message');
    $success = $this->session->userdata('success_message');

    if(!empty($error))
    {
      echo '<div class="alert alert-warning">'.$error.'</div>';
      $this->session->unset_userdata('error_message');
    }

    if(!empty($success))
    {
      echo '<div class="alert alert-success">'.$success.'</div>';
      $this->session->unset_userdata('success_message');
    }
    ?>
      <table class="table table-hover table-bordered ">
		 	<thead>
				<tr>
        		<th>#</th>
				  <th>Date</th>
				  <th>Ref Number</th>
       			 	<th>Property</th>
				  <th>Invoice Type</th>
           <th>Description</th>
				  <th>Amount Generated</th>
        			<th colspan="2">Action</th>
				</tr>
			 </thead>
		  	<tbody>
        		<?php echo $result;?>
		  	</tbody>
		</table>
      <?php echo $links?>
    </div>
</section>
</div>
</div>