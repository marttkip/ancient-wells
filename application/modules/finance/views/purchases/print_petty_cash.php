<?php
    $result =  '';

            // echo $result;
      $result = '';
      $count = 0;
      $balance = 0;
// get account opening balance
  
  // var_dump($opening_balance_query);die();
  $cr_amount = 0;
  $dr_amount = 0;

  if($transaction_id > 0)
  {

  }
  else
  {

    $opening_balance_query = $this->purchases_model->get_account_opening_balance($account_from_name);
     if($opening_balance_query->num_rows() > 0)
      {
        $row = $opening_balance_query->row();
        $cr_amount = $row->cr_amount;
        $dr_amount = $row->dr_amount;
      
        if($transaction_id > 0)
        {
            $cr_amount = 0;
            $dr_amount = 0;
          $result .='
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>Balance B/F</td>
                      <td >'.number_format(0,2).' </td>
                      <td >'.number_format(0,2).' </td>

                    </tr>
                    ';
        }
        else
        {
              $balance += $dr_amount;
            $balance -=  $cr_amount;

          $result .='
                    <tr>
                     <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>Balance B/F</td>
                      <td >'.number_format($dr_amount,2).' </td>
                      <td >'.number_format($cr_amount,2).' </td>
                      <td >'.number_format($balance,2).' </td>

                    </tr>
                    ';

        }
      }
      else {

        if($transaction_id > 0)
        {
          $result .='
                  <tr>
                    <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    <td>Balance B/F</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>0.00 </td>

                  </tr>
                  ';
        }
        else
        {
          $result .='
                  <tr>
                    <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    <td>Balance B/F</td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>0.00 </td>

                  </tr>
                  ';
        }
      }

  }
 



  if($query_purchases->num_rows() > 0)
  {
    foreach ($query_purchases->result() as $key => $value) {
      // code...
      $transactionClassification = $value->transactionClassification;

      $document_number = '';
      $transaction_number = '';
      $finance_purchase_description = '';
      $finance_purchase_amount = 0 ;


      $finance_purchase_payment_id = $value->transactionId;
      $referenceId = $value->payingFor;
     $document_number =$transaction_number = $value->referenceCode;
     $finance_purchase_description = $value->transactionName;
     $property_name = $value->property_name;
      $accountName = $value->accountName;
     $deleted = $value->deleted;
      if($transactionClassification == 'Purchase Payment')
      {
        $referenceId = $value->payingFor;

        // get purchase details
        $detail = $this->purchases_model->get_purchases_details($referenceId);
        $row = $detail->row();
        $document_number = $row->document_number;
        $transaction_number = $row->transaction_number;
        $finance_purchase_description = $row->finance_purchase_description;

        $account_name2 = $this->site_model->display_page_title();
        $account_name2 = strtolower($account_name2);
        $name = $this->site_model->create_web_name($account_name2);

        $checked = '  <td><a href="'.base_url().'print-vourcher/'.$finance_purchase_payment_id.'" target="_blank" class="btn btn-xs btn-success"><i class="fa fa-print"></i> Voucher</a></td>
                  <td><a href="'.base_url().'delete-petty-cash/'.$finance_purchase_payment_id.'/'.$name.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this record ? \')" ><i class="fa fa-trash"></i></a></td>';

      }else
      {
        $checked = '';
      }
      $cr_amount = $value->cr_amount;
      $dr_amount = $value->dr_amount;


      $transaction_date = $value->transactionDate;
      $transaction_date = date('jS M Y',strtotime($transaction_date));
      $creditor_name = $value->creditor_name;
      $creditor_id = 0;//$value->creditor_id;
      $account_name = '';//$value->account_name;


    


      if($deleted == 0)
      {
        $delete_status = '<span class="label label-xs label-default"> Active</span>';
      }
      else if($deleted == 1)
      {
        $delete_status = '<span class="label label-xs label-warning">Pending Delete Approval</span>';
      }
      else if($deleted == 2)
      {
        $delete_status = '<span class="label label-xs label-danger">Deleted</span>';
      }

      $count++;
      // var_dump($transaction_id);die();
      if($transaction_id > 0)
      {
        $balance +=  $cr_amount;

        $result .='
                  <tr>
                    <td>'.$count.'</td>
                    <td>'.$transaction_date.'</td>
                    <td>'.$transaction_number.'</td>
                    <td>'.$accountName.'</td>
                    <td>'.$finance_purchase_description.'</td>
                    <td>'.number_format($cr_amount,2).'</td>
                    <td>'.number_format($balance,2).'</td>

                  </tr>
                  ';

      }
      else
      {

          $balance += $dr_amount;
        $balance -=  $cr_amount;
        $result .='
                <tr>
                  <td>'.$count.'</td>
                  <td>'.$transaction_date.'</td>
                  <td>'.$transaction_number.'</td>
                  <td>'.$finance_purchase_description.'</td>
                  <td>'.$property_name.'</td>
                  <td>'.number_format($dr_amount,2).' </td>
                  <td>'.number_format($cr_amount,2).'</td>
                  <td>'.number_format($balance,2).'</td>
                  <td>'.$delete_status.'</td>
                  '.$checked.'

                </tr>
                ';
      }
      
    }


  }


  if($transaction_id > 0)
  {
    $result .='
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td colspan="2">Total</td>
              <td colspan="2" class="center-align"><strong>KES '.number_format($balance,2).' </strong></td>

            </tr>
            ';
  }
  else
  {
    $result .='
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td colspan="2">Total</td>
              <td colspan="2" class="center-align"><strong>KES '.number_format($balance,2).' </strong></td>

            </tr>
            ';
  }


?>

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Petty cash</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}

			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        <?php
        // $search_title = $this->session->userdata('accounts_search_title');
        ?>
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong>PETTY CASH <br> <?php echo $search_title?></strong>
            </div>
        </div>

    	<div class="row">
        	<div class="col-md-12">
        	<?php
        		// var_dump($account); die();


			?>
			<table class="table table-hover table-bordered ">
				 	<thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Ref Number</th>
              <th>Description</th>
              <th>Debit</th>
              <th>Credit</th>
              <th>Bal</th>
              <!-- <th>Action</th> -->
            </tr>
					 </thead>
				  	<tbody>
				  		<?php  echo $result;?>
					</tbody>
				</table>
            </div>
        </div>

    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong><?php echo $served_by;?>
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>
        	<div class="col-sm-12" style="margin-top:60px;">
                <div class="col-sm-2">
                	<strong>Checked by: </strong>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        	<div class="col-sm-12" style="margin-top:60px;">
                <div class="col-sm-2">
                	<strong>Approved by: </strong>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        </div>
    </body>

</html>
