<?php

$account_payments_rs = $this->purchases_model->get_petty_cash_details($finance_purchase_id);

// var_dump($account_payments_rs);die();
if($account_payments_rs->num_rows() > 0)
{
	foreach ($account_payments_rs->result() as $key => $value) {
		# code...
		$account_from_id = $value->account_from_id;
		$account_to_id = $value->account_to_id;
		$finance_purchase_description = $value->finance_purchase_description;
		$finance_purchase_amount = $value->finance_purchase_amount;
		$department_id = $value->department_id;
		$transaction_id = $value->transaction_id;
		// $location_id = $value->location_id;
		$transaction_date = $value->transaction_date;
		$transaction_number = $value->transaction_number;
		$finance_purchase_id = $value->finance_purchase_id;
		$property_beneficiary_idd = $value->property_beneficiary_id;



	}
}

$transactions_list = $this->purchases_model->get_all_opened_transactions();


$property_owners = $this->property_model->get_active_property_owners_with_beneficiaries();


$rs8 = $property_owners->result();

// var_dump($property_owners);die();
$property_beneficiaries_list = '';
$owner_id = 0;
foreach ($property_owners->result() as $property_rs =>$value) :


    $property_owner_id = $value->property_owner_id;
    $property_beneficiary_name = $value->property_beneficiary_name;
    $property_beneficiary_id = $value->property_beneficiary_id;
    $property_owner_name = $value->property_owner_name;

    if($property_owner_id != $owner_id)
    {
      $property_beneficiaries_list .= '<optgroup label="'.strtoupper($property_owner_name).'">';
    }


		if($property_beneficiary_idd == $property_beneficiary_id)
		{
			 $property_beneficiaries_list .= '<option value="'.$property_beneficiary_id.'#'.$property_owner_id.'" selected>'.strtoupper($property_beneficiary_name).'</option>';
		}  
		else
		{
			 $property_beneficiaries_list .= '<option value="'.$property_beneficiary_id.'#'.$property_owner_id.'">'.strtoupper($property_beneficiary_name).'</option>';
		} 

   


    


    if($property_owner_id != $owner_id AND $owner_id != 0)
    {
       $property_beneficiaries_list .= '</optgroup>';
    }
    $owner_id = $property_owner_id;


endforeach;
?>

<div class="col-md-12" style="margin-top:40px !important;">
	<section class="panel">
		<div class="panel-body">
			 <div class="padd">
		         <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form","id" => "edit-direct-payment"));?>


		         	<div class="row">
		                 <div class="col-md-12">
		                 <div class="col-md-6">
			                   <input type="hidden" class="form-control" name="account_from_id" placeholder="Account" value="<?php echo $account_from_id?>" required/>
			                   <input type="hidden" class="form-control" name="department_id" placeholder="Account" value="<?php echo $department_id?>" required/>
			                   <input type="hidden" class="form-control" name="finance_purchase_id" placeholder="Account" value="<?php echo $finance_purchase_id?>" required/>
			                   <input type="hidden" class="form-control" name="location_id" id="location_id" placeholder="Location" value="<?php echo $location_id?>" required/>
			                   <input type="hidden" name="redirect_url" id="redirect_url" value="<?php echo $this->uri->uri_string()?>">
		                       <div class="form-group">
		                           <label class="col-md-4 control-label">Transaction date: </label>

		                           <div class="col-md-8">
		                               <div class="input-group">
		                                   <span class="input-group-addon">
		                                       <i class="fa fa-calendar"></i>
		                                   </span>
		                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="transaction_date" placeholder="Transaction date" value="<?php echo $transaction_date;?>" id="datepicker2" required>
		                               </div>
		                           </div>
		                       </div>
		                       <div class="form-group">
		                           <label class="col-md-4 control-label">Transaction No *</label>

		                           <div class="col-md-8">
		                               <input type="text" class="form-control" value="<?php echo $transaction_number;?>" name="transaction_number" placeholder="Transaction Number" required/>
		                           </div>
		                       </div>
		                       
		                       <div class="form-group" >
		                           <label class="col-md-4 control-label">Expense Account *</label>

		                           <div class="col-md-8">
		                               <select class="form-control select2" name="account_to_id" id="account_to_id" required>
		                                 <option value="0">--- select an expense account - ---</option>
		                                 <?php
		                                 if($expense_accounts->num_rows() > 0)
		                                 {
		                                   foreach ($expense_accounts->result() as $key => $value) {
		                                     // code...
		                                     $account_id = $value->account_id;
		                                     $account_name = $value->account_name;
		                                     if($account_id == $account_to_id)
		                                    {
		                                    	echo "<option value=".$account_id." selected> ".$account_name."</option>";
		                                    }
		                                    else
		                                    {
		                                    	echo "<option value=".$account_id."> ".$account_name."</option>";
		                                    }
		                                   }
		                                 }
		                                 ?>
		                               </select>
		                           </div>
		                       </div>

		                       <div class="form-group">
			                         <label class="col-lg-4 control-label">Transaction Allocation</label>

			                         <div class="col-lg-8">

			                          <select class="form-control selectpicker" name="transaction_id">
			                            <!-- <option value="0">---- SELECT A TRANSACTION ---- </option> -->
			                            <?php
			                              if($transactions_list->num_rows() > 0)
			                              {
			                                foreach ($transactions_list->result() as $key => $value) {
			                                  # code...
			                                  $transaction_month = $value->transaction_month;
			                                  $transaction_year = $value->transaction_year;
			                                  $transaction_idd = $value->transaction_id;
			                                  $property_owner_name = $value->property_owner_name;
			                                  $date = '01-'.$transaction_month.'-'.$transaction_year;

			                                  if($transaction_id == $transaction_idd)
			                                  {
			                                  	 ?>

				                                    <option value="<?php echo $transaction_idd?>" selected><?php echo $property_owner_name.' : '.date('M Y',strtotime($date))?></option>
				                                  <?php

			                                  }
			                                  else
			                                  {
			                                  	 ?>

			                                    <option value="<?php echo $transaction_idd?>"><?php echo $property_owner_name.' : '.date('M Y',strtotime($date))?></option>
			                                  <?php
			                                  }


			                                 
			                                }
			                              }
			                            ?>
			                          </select>

			                            
			                         </div>
			                     </div>



			                     <div class="form-group">
			                         <label class="col-lg-4 control-label">Beneficairy</label>

			                         <div class="col-lg-8">

			                          <select class="form-control selectpicker" name="property_beneficiary_id">
			                            <!-- <option value="0">---- SELECT A BENEFICIARY ---- </option> -->
			                            <?php echo $property_beneficiaries_list;?>
			                          </select>

			                            
			                         </div>
			                     </div>


		                 </div>
		                 <div class="col-md-6">
		                   <div class="form-group">
		                       <label class="col-md-4 control-label">Type *</label>

		                       <div class="col-md-8">
		                           <div class="radio">
		                               <label>
		                                   <input  type="radio" checked value="0" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
		                                   Pure Expense
		                               </label>
		                              <!--  <label>
		                                   <input  type="radio"  value="1" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
		                                   Expense Linked to a creditor
		                               </label> -->
		                           </div>
		                       </div>
		                   </div>

		                   

		                   


		                   <div class="form-group">
		                       <label class="col-md-4 control-label">Description *</label>

		                       <div class="col-md-8">
		                           <textarea class="form-control" name="description" rows="5" required><?php echo $finance_purchase_description;?></textarea>
		                       </div>
		                   </div>

		                   <div class="form-group">
		                       <label class="col-md-4 control-label">Amount *</label>

		                       <div class="col-md-8">
		                           <input type="text" class="form-control" name="transacted_amount" placeholder="Amount" value="<?php echo $finance_purchase_amount?>" required/>
		                       </div>
		                   </div>


		                 </div>
		                 </div>



		               </div>
		               <div class="row" style="margin-top:5px;">
		                     <div class="col-md-12">
		                         <div class="text-center">
		                             <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to update this record ? ')">Update record</button>
		                         </div>
		                     </div>
		                 </div>



		            <?php echo form_close();?>
		        </div>
		</div>
	</section>
</div>
<br/>
<div class="row" style="margin-top: 5px;">
		<ul>
			<li style="margin-bottom: 5px;">
				<div class="row">
			        <div class="col-md-12 center-align">
				        <a  class="btn btn-sm btn-info" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR</a>
				        		
			               
			        </div>
			    </div>
				
			</li>
		</ul>
	</div>
