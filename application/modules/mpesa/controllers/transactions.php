<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/real_estate_administration/controllers/property.php";
// require_once "./application/modules/mpesa/controllers/cron.php";
// header('Content-type: application/json;');
error_reporting(0);
class Transactions extends property {
	var $csv_path;
	var $invoice_path;
	var $statement_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('real_estate_administration/rental_unit_model');
		$this->load->model('real_estate_administration/leases_model');
		$this->load->model('real_estate_administration/property_owners_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('admin/email_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('administration/reports_model');
    	$this->load->model('mpesa/mpesa_model');
    	$this->load->model('financials/ledgers_model');
		// $this->load->controller('mpesa/cron');
		// $this->load->model('accounting/salary_advance_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->invoice_path = realpath(APPPATH . '../invoices/');
		$this->statement_path = realpath(APPPATH . '../statements/');
		 $this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}




  	public function unreconcilled_payments()
	{



		$where = 'mpesa_status = 0 AND lease_id is null AND transaction_type = 0 AND recon_status = 0';
		$table = 'mpesa_transactions';

		$search = $this->session->userdata('search_mpesa_received');
		if(!empty($search))
		{
			$where .= $search;
		}
		// var_dump($where);die();
				$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/unallocated-transactions';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='mpesa_transactions.completion_time', $order_method='ASC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();
		// $v_data['payments_transactions'] = $this->mpesa_model->get_all_payments();
		$data['title'] = 'All Unreconcilled Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('unreconcilled_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function reconcile_payment($mpesa_id)
	{

		$where = 'mpesa_status = 0 AND mpesa_transactions.account_id = account.account_id  AND mpesa_id = '.$mpesa_id;
		$table = 'mpesa_transactions,account';

		$this->db->where($where);
		$query = $this->db->get('mpesa_transactions,account');
		$data['title'] = 'All Unreconcilled Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$data['content'] = $this->load->view('reconcile_payment', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function reconcile_payment_item($tenant_unit_id , $lease_id, $mpesa_id)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_amount', 'Water amount', 'trim|xss_clean');
		$this->form_validation->set_rules('service_charge_amount', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('penalty_fee', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'trim|xss_clean');



		$payment_method = $this->input->post('payment_method');
		$water_amount = $this->input->post('water_amount');
		$rent_amount = $this->input->post('rent_amount');
		$service_charge_amount = $this->input->post('service_charge_amount');
		$penalty_fee = $this->input->post('penalty_fee');
		$amount_paid = $this->input->post('amount_paid');
		$fixed_charge = $this->input->post('fixed_charge');
		$deposit_charge = $this->input->post('deposit_charge');

		$insurance = $this->input->post('insurance');
		$sinking_funds = $this->input->post('sinking_funds');
		$bought_water = $this->input->post('bought_water');
		$painting_charge = $this->input->post('painting_charge');
		$legal_fees = $this->input->post('legal_fees');


		if(empty($water_amount))
		{
			$water_amount = 0;
		}
		if(empty($service_charge_amount))
		{
			$service_charge_amount = 0;
		}
		if(empty($rent_amount))
		{
			$rent_amount = 0;
		}
		if(empty($penalty_fee))
		{
			$penalty_fee = 0;
		}
		if(empty($fixed_charge))
		{
			$fixed_charge = 0;
		}

		if(empty($deposit_charge))
		{
			$deposit_charge = 0;
		}
		if(empty($insurance))
		{
			$insurance = 0;
		}
		if(empty($sinking_funds))
		{
			$sinking_funds = 0;
		}
		if(empty($bought_water))
		{
			$bought_water = 0;
		}
		if(empty($painting_charge))
		{
			$painting_charge = 0;
		}
		if(empty($legal_fees))
		{
			$legal_fees = 0;
		}
		//  add all this items
		$total = $service_charge_amount + $rent_amount + $water_amount + $penalty_fee + $fixed_charge + $deposit_charge + $insurance + $sinking_funds + $bought_water + $painting_charge+ $legal_fees;

		// var_dump($total); die();
		// Normal


		if($payment_method == 1)
		{
			// check for cheque number if inserted
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');

		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
		}

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($amount_paid == $total)
			{
				$this->mpesa_model->receipt_payment($lease_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';

				// update payments
				$update_array['lease_id'] = $lease_id;
				$update_array['mpesa_status'] = 1;
				$this->db->where('mpesa_id',$mpesa_id);
				$this->db->update('mpesa_transactions',$update_array);

				$sender_phone = $this->input->post('sender_phone');
				$account_number = $this->input->post('account_number');
				// $tenant_phone_number = '+'.$sender_phone;
				$tenant_phone_number = '+254734808007';
				if(!empty($tenant_phone_number))
				{
					$message = 'We have received Ksh. '.$amount_paid.' for account '.$account_number.'. Thank you';
					// $this->mpesa_model->sms($tenant_phone_number,$message,$tenant_name);

					// save the message sent out
					// $insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
					// $this->db->insert('sms',$insert_array);
					// save the message sent out

				}

				redirect('accounts/payments/'.$tenant_unit_id.'/'.$lease_id);
			}
			else
			{
				$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				$response['status'] = 'fail';
				$response['message'] = 'The amounts of payment do not add up to the total amount paid';
				$redirect_url = $this->input->post('redirect_url');
				redirect($redirect_url);
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);

		}

		// echo json_encode($response);

  }

  public function completed_mpesa_payments()
	{
		$where = 'mpesa_status = 0 AND lease_id is null AND transaction_type = 0 AND recon_status = 1';
		$table = 'mpesa_transactions';

		$search = $this->session->userdata('search_completed_mpesa');
		if(!empty($search))
		{
			$where .= $search;
		}
				$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/completed-transactions';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

		$data['title'] = 'Completed Mpesa Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('completed_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_completed_mpesa_transactions()
	{
		$serial_number = $this->input->post('serial_number');
		$sender_name = $this->input->post('sender_name');
		$account_id = $this->input->post('account_id');
		$transaction_date_from = $this->input->post('transaction_date_from');
		$transaction_date_to = $this->input->post('transaction_date_to');
		// var_dump($_POST);die();
		$search_title = '';

		if(!empty($serial_number))
		{
			$serial_number = ' AND mpesa_transactions.receipt_number = "'.$serial_number.'"';
		}

		if(!empty($account_id))
		{
			$account_id = ' AND mpesa_transactions.account_id = "'.$account_id.'"';
		}

		if(!empty($sender_name))
		{
			$search_title .= $sender_name;

			$counter = explode(' ', $sender_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$sender_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$sender_name .= ') ';
			
			// $sender_name = ' AND tenants.sender_name LIKE \'%'.$sender_name.'%\'';
		
		
		}
		else
		{
			$sender_name = '';
			$search_title .= '';
		}
		


		
		if(!empty($transaction_date_from) && !empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) >= \''.$transaction_date_from.'\' AND DATE(completion_time) <= \''.$transaction_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else if(!empty($transaction_date_from))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
		}

		else if(!empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else
		{
			$transaction_date = '';
		}


		$search = $serial_number.$sender_name.$transaction_date.$account_id;

		$this->session->set_userdata('search_completed_mpesa', $search);
		redirect('mpesa-transactions/completed-transactions');
	}

	public function close_completed_mpesa_search()
	{
		$this->session->unset_userdata('search_completed_mpesa');
		redirect('mpesa-transactions/completed-transactions');
	}
  public function reversed_mpesa_payments()
  {
    $where = 'mpesa_status = 1 AND lease_id is null';
		$table = 'mpesa_transactions';

		$search = $this->session->userdata('search_reversed_mpesa');
		if(!empty($search))
		{
			$where .= $search;
		}
		$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/reversed-transactions';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

		$data['title'] = 'Reversed Mpesa Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reversed_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
  }
	public function search_reversed_mpesa_transactions()
	{
		$serial_number = $this->input->post('serial_number');
		$sender_name = $this->input->post('sender_name');
		$transaction_date_from = $this->input->post('transaction_date_from');
		$transaction_date_to = $this->input->post('transaction_date_to');
		// var_dump($_POST);die();
		$search_title = '';

		if(!empty($serial_number))
		{
			$serial_number = ' AND mpesa_transactions.receipt_number = "'.$serial_number.'"';
		}
		if(!empty($sender_name))
		{
			$search_title .= $sender_name;

			$counter = explode(' ', $sender_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$sender_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$sender_name .= ') ';
			
			// $sender_name = ' AND tenants.sender_name LIKE \'%'.$sender_name.'%\'';
		
		
		}
		else
		{
			$sender_name = '';
			$search_title .= '';
		}
		if(!empty($transaction_date_from) && !empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) >= \''.$transaction_date_from.'\' AND DATE(completion_time) <= \''.$transaction_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else if(!empty($transaction_date_from))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
		}

		else if(!empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else
		{
			$transaction_date = '';
		}


		$search = $serial_number.$sender_name.$transaction_date;

		$this->session->set_userdata('search_reversed_mpesa', $search);
		redirect('mpesa-transactions/reversed-transactions');
	}

	public function close_reversed_mpesa_search()
	{
		$this->session->unset_userdata('search_reversed_mpesa');
		redirect('mpesa-transactions/reversed-transactions');
	}
    public function all_mpesa_transactions_payments()
  	{
  		$where = 'mpesa_id > 0';
  		$table = 'mpesa_transactions';

  		$search = $this->session->userdata('search_all_mpesa');
  		if(!empty($search))
  		{
  			$where .= $search;
  		}
  				$this->session->unset_userdata('lease_number_searched');
  		$segment = 3;
  		//pagination

  		$this->load->library('pagination');
  		$config['base_url'] = base_url().'mpesa-transactions/all-mpesa-transactions';
  		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
  		$config['uri_segment'] = $segment;
  		$config['per_page'] = 20;
  		$config['num_links'] = 5;

  		$config['full_tag_open'] = '<ul class="pagination pull-right">';
  		$config['full_tag_close'] = '</ul>';

  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';

  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';

  		$config['next_tag_open'] = '<li>';
  		$config['next_link'] = 'Next';
  		$config['next_tag_close'] = '</span>';

  		$config['prev_tag_open'] = '<li>';
  		$config['prev_link'] = 'Prev';
  		$config['prev_tag_close'] = '</li>';

  		$config['cur_tag_open'] = '<li class="active">';
  		$config['cur_tag_close'] = '</li>';

  		$config['num_tag_open'] = '<li>';
  		$config['num_tag_close'] = '</li>';
  		$this->pagination->initialize($config);

  		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
          $v_data["links"] = $this->pagination->create_links();
  		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

  		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

  		$data['title'] = 'All Mpesa Payments';
  		$v_data['title'] = $data['title'];
  		$v_data['query'] = $query;
  		$v_data['page'] = $page;
  		$data['content'] = $this->load->view('all_mpesa_transactions', $v_data, true);

  		$this->load->view('admin/templates/general_page', $data);



	}

	public function search_all_mpesa_transactions()
	{
		$serial_number = $this->input->post('serial_number');
		$sender_name = $this->input->post('sender_name');
		$transaction_date_from = $this->input->post('transaction_date_from');
		$transaction_date_to = $this->input->post('transaction_date_to');
		// var_dump($_POST);die();
		$search_title = '';

		if(!empty($serial_number))
		{
			$serial_number = ' AND mpesa_transactions.receipt_number = "'.$serial_number.'"';
		}
		if(!empty($sender_name))
		{
			$search_title .= $sender_name;

			$counter = explode(' ', $sender_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$sender_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$sender_name .= ') ';
			
			// $sender_name = ' AND tenants.sender_name LIKE \'%'.$sender_name.'%\'';
		
		
		}
		else
		{
			$sender_name = '';
			$search_title .= '';
		}
		if(!empty($transaction_date_from) && !empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) >= \''.$transaction_date_from.'\' AND DATE(completion_time) <= \''.$transaction_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else if(!empty($transaction_date_from))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
		}

		else if(!empty($transaction_date_to))
		{
			$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else
		{
			$transaction_date = '';
		}


		$search = $serial_number.$sender_name.$transaction_date;

		$this->session->set_userdata('search_all_mpesa', $search);
		redirect('mpesa-transactions/all-mpesa-transactions');
	}

	public function close_all_mpesa_search()
	{
		$this->session->unset_userdata('search_all_mpesa');
		redirect('mpesa-transactions/all-mpesa-transactions');
	}


  		public function cancel_mpesa_payment($mpesa_id)
  		{
  			$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
  			$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
  			$redirect_url = $this->input->post('redirect_url');
  			//if form conatins invalid data
  			if ($this->form_validation->run())
  			{
  				// end of checker function
  				if($this->mpesa_model->cancel_mpesa_payment($mpesa_id))
  				{
  					$this->session->set_userdata("success_message", "Transaction successfully cancelled");
  				}
  				else
  				{
  					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
  				}
  			}
  			else
  			{
  				$this->session->set_userdata("error_message", validation_errors());

  			}
  			redirect($redirect_url);
  		}

		public function search_mpesa_transactions()
		{
			$serial_number = $this->input->post('serial_number');
			$sender_name = $this->input->post('sender_name');
			$account_id = $this->input->post('account_id');
			$transaction_date_from = $this->input->post('transaction_date_from');
			$transaction_date_to = $this->input->post('transaction_date_to');
			// var_dump($_POST);die();
			$search_title = '';

			if(!empty($serial_number))
			{
				$serial_number = ' AND mpesa_transactions.receipt_number = "'.$serial_number.'"';
			}

			if(!empty($account_id))
			{
				$account_id = ' AND mpesa_transactions.account_id = "'.$account_id.'"';
			}
			if(!empty($sender_name))
			{
				$search_title .= $sender_name;

				$counter = explode(' ', $sender_name);

				$total = count($counter);
				// var_dump($count);die();
				$count = 1;
				$sender_name = ' AND (';
				for($r = 0; $r < $total; $r++)
				{
					if($count == $total)
					{
						$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\'';
					}

					else
					{
						$sender_name .= ' mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\' AND ';
					}
					$count++;
				}
				$sender_name .= ') ';
				
				// $sender_name = ' AND tenants.sender_name LIKE \'%'.$sender_name.'%\'';
			
			
			}
			else
			{
				$sender_name = '';
				$search_title .= '';
			}
			if(!empty($transaction_date_from) && !empty($transaction_date_to))
			{
				$transaction_date = ' AND DATE(completion_time) >= \''.$transaction_date_from.'\' AND DATE(completion_time) <= \''.$transaction_date_to.'\'';
				$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else if(!empty($transaction_date_from))
			{
				$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_from.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
			}

			else if(!empty($transaction_date_to))
			{
				$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_to.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else
			{
				$transaction_date = '';
			}


			$search = $serial_number.$sender_name.$transaction_date.$account_id;
			// var_dump($search);die();
			$this->session->set_userdata('search_mpesa_received', $search);
			redirect('mpesa-transactions/unallocated-transactions');
		}

		public function close_mpesa_search()
		{
			$this->session->unset_userdata('search_mpesa_received');
			redirect('mpesa-transactions/unallocated-transactions');
		}


		public function update_mpesa_list()
		{
			$test_url = site_url().'mpesa/cron/pull_transactions';
			// var_dump($test_url);die();
			$patient_details['id'] = 1;
			$data_string = json_encode($patient_details);

			try{

				$ch = curl_init($test_url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);
			}
			catch(Exception $e)
			{
				$response = "something went wrong";
				echo json_encode($response.' '.$e);
			}
			redirect('mpesa-transactions/unallocated-transactions');
		}
		public function reverse_payment_detail($payment_id,$tenant_unit_id,$lease_id)
		{
			$this->db->where('lease_id = '.$lease_id.' AND payment_id ='.$payment_id);
			$query = $this->db->get('payments');

			if($query->num_rows() > 0)
			{
				$value = $query->row();
				$array = json_decode(json_encode($value), true);
				$amount_paid = $value->amount_paid;
				unset($array['payment_id']);
				unset($array['amount_paid']);
				$array['amount_paid'] = -$amount_paid;
				// var_dump($payment_id);die();
				$this->db->insert('payments',$array);
				$payment_idd = $this->db->insert_id();

				$this->db->where('payment_id ='.$payment_id);
				$query_two = $this->db->get('payment_item');

				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value_two) {
						// code...
						$array_two = json_decode(json_encode($value_two), true);
						$amount_paid_two = $value_two->amount_paid;
						unset($array_two['payment_item_id']);
						unset($array_two['payment_id']);
						unset($array_two['remarks']);
						unset($array_two['amount_paid']);
						$array_two['amount_paid'] = -$amount_paid_two;
						$array_two['remarks'] = 'Payment Reversal';
						$array_two['payment_id'] = $payment_idd;
						$this->db->insert('payment_item',$array_two);
					}
				}
			}

			redirect('mpesa-transactions/unallocated-transactions');
		}
		public function print_mpesa_transactions($status = null)
		{

			if($status = 1)
			{
					$where = 'mpesa_status = 0 AND lease_id is null AND recon_status = 1';
			}
			else {
					$where = 'mpesa_status = 0 AND lease_id is null';
			}


			$table = 'mpesa_transactions';

			$search = $this->session->userdata('search_mpesa_received');
			if(!empty($search))
			{
				$where .= $search;
			}

			$data['contacts'] = $this->site_model->get_contacts();
			$data['query'] = $this->mpesa_model->get_unreconcilled_payments($table, $where,null,null,'mpesa_transactions.transaction_date');

			$this->load->view('print_mpesa_transactions', $data);
		}



		public function add_payment_item($tenant_unit_id , $lease_id, $paying_account,$close_page = NULL)
		{
			// var_dump($lease_id); die();
			$this->form_validation->set_rules('invoice_type_id', 'Invoice Item', 'trim|required|xss_clean');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_month', 'Month', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_year', 'Year', 'trim|required|xss_clean');

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				$mpesa_id = $this->input->post('mpesa_id');

				if($mpesa_id > 0)
				{
					// check if the value is  greater that the one placed
						$actual_balance = $this->input->post('actual_balance');
						$amount = $this->input->post('amount');

						if($amount > $actual_balance)
						{
							$response['status'] = 'error';
							$response['message'] = 'Payment successfully added';
							$this->session->set_userdata('error_message', 'Sorry the amount placed cannot be more that the amount of money paid by the customer');
						}
						else {
							$this->mpesa_model->payment_item_add($lease_id,$paying_account);
							$this->session->set_userdata("success_message", 'Payment successfully added');
							$response['status'] = 'success';
							$response['message'] = 'Payment successfully added';
							$this->session->set_userdata('success_message', 'You have added the payment item successfully');
						}



				}
				else {
					$this->mpesa_model->payment_item_add($lease_id,$paying_account);
					$this->session->set_userdata("success_message", 'Payment successfully added');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
					$this->session->set_userdata('success_message', 'You have added the payment item successfully');
				}

			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
				$response['status'] = 'fail';
				$response['message'] = validation_errors();
				$this->session->set_userdata('error_message', validation_errors());
			}
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);
		}

		public function delete_payment_items_id($payment_item_id,$mpesa_id)
		{

			$this->db->where('payment_item_id',$payment_item_id);
			if($this->db->delete('payment_item'))
			{
				$this->session->set_userdata('success_message', 'you have successfully removed the payment item');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please check the details and try again ');
			}

			redirect('reconcile-payment/'.$mpesa_id);
		}

		public function confirm_payment($tenant_unit_id , $lease_id, $paying_account,$close_page = NULL)
		{
			$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
			$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
			$payment_method = $this->input->post('payment_method');
			if(!empty($payment_method))
			{
				if($payment_method == 2 || $payment_method == 3 || $payment_method == 5)
				{
					// check for cheque number if inserted
					$this->form_validation->set_rules('bank_id', 'Bank Id', 'trim|required|xss_clean');
					$this->form_validation->set_rules('reference_number', 'Amount', 'trim|required|xss_clean');
				}

				$response['status'] = 'fail';
				$response['message'] = 'Please select a payment method';
			}

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				$mpesa_id = $this->input->post('mpesa_id');
					$payment_id = $this->mpesa_model->confirm_payment($lease_id,$paying_account);
					$total_amount = $this->input->post('total_amount');

					if($mpesa_id > 0 AND $payment_id > 0)
					$this->mpesa_model->update_mpesa_recon_status($mpesa_id,$total_amount,$payment_id);

					$this->session->set_userdata("success_message", 'Payment successfully added');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
				$response['status'] = 'fail';
				$response['message'] = validation_errors();

			}

			// echo json_encode($response);


			$redirect_url = $this->input->post('redirect_url');
			redirect('mpesa-transactions/unallocated-transactions');
		}

		public function update_payments_item($payment_item_id,$lease_id,$paying_account)
		{
			$this->form_validation->set_rules('invoice_item_amount'.$payment_item_id, 'Payment Method', 'trim|required|xss_clean');

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				if($paying_account == 1)
				{
					$array['amount_paid'] = $this->input->post('invoice_item_amount'.$payment_item_id);
					$this->db->where('payment_item_id',$payment_item_id);
					$this->db->update('payment_item',$array);

				}
				else
				{
					$array['amount_paid'] = $this->input->post('invoice_item_amount'.$payment_item_id);
					$this->db->where('payment_item_id',$payment_item_id);
					$this->db->update('water_payment_item',$array);
				}
					

					$this->session->set_userdata("success_message", 'Successfully removed the payment item');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
			}

			redirect('mpesa-transactions/unallocated-transactions');
		}


		public function update_mpesa_list_cron()
		{
			$test_url = site_url().'mpesa/cron/pull_transactions';

			$patient_details['id'] = 1;
			$data_string = json_encode($patient_details);

			try{

				$ch = curl_init($test_url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);
			}
			catch(Exception $e)
			{
				$response = "something went wrong";
				echo json_encode($response.' '.$e);
			}
		}


	public function unreconcilled_withdrawals()
	{
		$where = 'mpesa_status = 0  AND transaction_type = 1 AND withdrawn <> recon_amount';
		$table = 'v_withdrawals_transactions';

		$search = $this->session->userdata('search_mpesa_transfered');
		if(!empty($search))
		{
			$where .= $search;
		}
				$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/unallocated-transactions';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 100;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_withdrawals($table, $where, $config["per_page"], $page, $order='completion_time', $order_method='DESC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

		$data['title'] = 'Unreconcilled Withdrawals';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('payments_out', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function reconcile_withdrawal($mpesa_id)
	{

		$where = 'mpesa_status = 0 AND mpesa_transactions.account_id = account.account_id  AND mpesa_id = '.$mpesa_id;
		$table = 'mpesa_transactions,account';

		// $this->db->select('mpesa_transactions.*');
		$this->db->where($where);
		$query = $this->db->get('mpesa_transactions,account');
		$data['title'] = 'Unreconcilled Payment';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['mpesa_id'] = $mpesa_id;
		$data['content'] = $this->load->view('reconcile_withdrawal', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function confirm_transfer($mpesa_id,$account_from_id)
	{
		$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
	
		

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$mpesa_id = $this->input->post('mpesa_id');
			$this->mpesa_model->confirm_withdrawal($mpesa_id);

			$this->session->set_userdata("success_message", 'Payment successfully added');
			$response['status'] = 'success';
			$response['message'] = 'Payment successfully added';
			redirect('mpesa-transactions/unreconcilled-withdrawals');
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);

		}

		// echo json_encode($response);


		
	}

	public function search_unreconcilled_withdrawals()
	{
			$serial_number = $this->input->post('serial_number');
			$sender_name = $this->input->post('sender_name');
			$account_id = $this->input->post('account_id');
			$transaction_date_from = $this->input->post('transaction_date_from');
			$transaction_date_to = $this->input->post('transaction_date_to');
			// var_dump($_POST);die();
			$search_title = '';

			if(!empty($serial_number))
			{
				$serial_number = ' AND v_mpesa_transactions.receipt_number = "'.$serial_number.'"';
			}

			if(!empty($account_id))
			{
				$account_id = ' AND v_mpesa_transactions.account_id = "'.$account_id.'"';
			}
			if(!empty($sender_name))
			{
				$search_title .= $sender_name;

				$counter = explode(' ', $sender_name);

				$total = count($counter);
				// var_dump($count);die();
				$count = 1;
				$sender_name = ' AND (';
				for($r = 0; $r < $total; $r++)
				{
					if($count == $total)
					{
						$sender_name .= ' v_mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\'';
					}

					else
					{
						$sender_name .= ' v_mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\' AND ';
					}
					$count++;
				}
				$sender_name .= ') ';
				
				// $sender_name = ' AND tenants.sender_name LIKE \'%'.$sender_name.'%\'';
			
			
			}
			else
			{
				$sender_name = '';
				$search_title .= '';
			}
			if(!empty($transaction_date_from) && !empty($transaction_date_to))
			{
				$transaction_date = ' AND DATE(completion_time) >= \''.$transaction_date_from.'\' AND DATE(completion_time) <= \''.$transaction_date_to.'\'';
				$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else if(!empty($transaction_date_from))
			{
				$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_from.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
			}

			else if(!empty($transaction_date_to))
			{
				$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_to.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else
			{
				$transaction_date = '';
			}


			$search = $serial_number.$sender_name.$transaction_date.$account_id;
			// var_dump($search);die();
			$this->session->set_userdata('search_mpesa_transfered', $search);
			redirect('mpesa-transactions/unreconcilled-withdrawals');
	}
	
	public function close_mpesa_withdrawals_search()
	{
		$this->session->unset_userdata('search_mpesa_transfered');
		redirect('mpesa-transactions/unreconcilled-withdrawals');
	}
	public function reconcilled_withdrawals()
	{
		$where = 'transaction_type = 1 AND withdrawn = recon_amount';
		$table = 'v_withdrawals_transactions';

		$search = $this->session->userdata('search_mpesa_reconcilled_transfered');
		if(!empty($search))
		{
			$where .= $search;
		}
		$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/reconcilled-withdrawals';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 100;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_withdrawals($table, $where, $config["per_page"], $page, $order='completion_time', $order_method='DESC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

		$data['title'] = 'Reconcilled Withdrawals';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('payments_out_reconcilled', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_reconcilled_withdrawals()
	{
			$serial_number = $this->input->post('serial_number');
			$sender_name = $this->input->post('sender_name');
			$account_id = $this->input->post('account_id');
			$transaction_date_from = $this->input->post('transaction_date_from');
			$transaction_date_to = $this->input->post('transaction_date_to');
			// var_dump($_POST);die();
			$search_title = '';

			if(!empty($serial_number))
			{
				$serial_number = ' AND v_mpesa_transactions.receipt_number = "'.$serial_number.'"';
			}

			if(!empty($account_id))
			{
				$account_id = ' AND v_mpesa_transactions.account_id = "'.$account_id.'"';
			}
			if(!empty($sender_name))
			{
				$search_title .= $sender_name;

				$counter = explode(' ', $sender_name);

				$total = count($counter);
				// var_dump($count);die();
				$count = 1;
				$sender_name = ' AND (';
				for($r = 0; $r < $total; $r++)
				{
					if($count == $total)
					{
						$sender_name .= ' v_mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\'';
					}

					else
					{
						$sender_name .= ' v_mpesa_transactions.sender_name LIKE \'%'.$counter[$r].'%\' AND ';
					}
					$count++;
				}
				$sender_name .= ') ';
				
				// $sender_name = ' AND tenants.sender_name LIKE \'%'.$sender_name.'%\'';
			
			
			}
			else
			{
				$sender_name = '';
				$search_title .= '';
			}
			if(!empty($transaction_date_from) && !empty($transaction_date_to))
			{
				$transaction_date = ' AND DATE(completion_time) >= \''.$transaction_date_from.'\' AND DATE(completion_time) <= \''.$transaction_date_to.'\'';
				$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else if(!empty($transaction_date_from))
			{
				$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_from.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
			}

			else if(!empty($transaction_date_to))
			{
				$transaction_date = ' AND DATE(completion_time) = \''.$transaction_date_to.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else
			{
				$transaction_date = '';
			}


			$search = $serial_number.$sender_name.$transaction_date.$account_id;
			// var_dump($search);die();
			$this->session->set_userdata('search_mpesa_reconcilled_transfered', $search);
			redirect('mpesa-transactions/reconcilled-withdrawals');
	}
	
	public function close_reconcilled_mpesa_withdrawals_search()
	{
		$this->session->unset_userdata('search_mpesa_reconcilled_transfered');
		redirect('mpesa-transactions/reconcilled-withdrawals');
	}


	public function delete_water_payment_items_id($payment_item_id,$mpesa_id)
	{

		$this->db->where('payment_item_id',$payment_item_id);
		if($this->db->delete('water_payment_item'))
		{
			$this->session->set_userdata('success_message', 'you have successfully removed the payment item');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}

		redirect('reconcile-payment/'.$mpesa_id);
	}



	public function reconcile_mpesa_transactions()
	{


		$where = 'mpesa_status = 0 AND lease_id is null AND transaction_type = 0 AND recon_status = 0 and payment_date IS NOT NULL ';
		$table = 'mpesa_transactions';
		// $this->db->limit(1);
		$this->db->where($where);
		$query = $this->db->get($table);
		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...
				$sender_phone = $value->sender_phone;
				$mpesa_id = $value->mpesa_id;
				$account_number = $value->account_number;
				$receipt_number = $value->receipt_number;
				$completion_time = $value->completion_time;
				$transaction_status = $value->transaction_status;
				$paid_in = $value->paid_in;
				$sender_name = $value->sender_name;
				$payment_date = $value->payment_date;

				$account_id = $value->account_id;

				$sender_phone = rtrim($sender_phone);
				// get the last three digits of the sender phone
				$sender_phone = $this->getLastThreeDigits($sender_phone);
				// get the sender name
				$sender_name = str_replace('*', '', $sender_name);
				$sender_name = rtrim($sender_name);

				$account_number = str_replace(' ', '', $account_number); // removing the spaces
				$account_number = strtoupper(strtolower($account_number));
			

				$where_two = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id AND leases.lease_deleted = 0 AND rental_unit.rental_unit_name = "'.$account_number.'"';
				$table_two = 'tenants,tenant_unit,rental_unit,leases,property';		
				$this->db->where($where_two);
				$query_two = $this->db->get($table_two);
						// var_dump($query_two);die();
				if($query_two->num_rows() == 1)
				{
					foreach ($query_two->result() as $key_two => $value_two) {
						// code...

						$lease_id = $value_two->lease_id;
						$tenant_name = strtoupper(strtolower($value_two->tenant_name));
						$property_id = $value_two->property_id;
						$tenant_phone_number = $value_two->tenant_phone_number;

				
							// Example usage
							$haystack = $tenant_name;
							$needle = ltrim(strtoupper(strtolower($sender_name)));
							$needle = strtoupper(strtolower(ltrim($needle)));
							$name_exisits = $this->stringExists($haystack, $needle);
					
							if ($name_exisits)  /// confirm if the name exisits on the property listing
							{

									// need to check if the tenants phone number is the same as the one being paid by
									$tenant_phone_number_last = $this->getLastThreeDigits($tenant_phone_number);
										// var_dump($sender_phone);die();
									if($sender_phone == $tenant_phone_number_last) // if both numbers matches then this payment good to be processed as a payment
									{
											
											if(!empty($account_number) AND $account_id != 29)
											$this->update_tenant_account($account_number,$mpesa_id,$paid_in,$completion_time,$receipt_number,$sender_name,$lease_id,$account_id,$payment_date);
											else if(!empty($account_number) AND $account_id == 29)
											$this->update_water_tenant_account($account_number,$mpesa_id,$paid_in,$completion_time,$receipt_number,$sender_name,$lease_id,$account_id,$payment_date);
										
									}
									else
									{
										// nothing should be done for this payment since the name of the payer is not the same, or the last digits of the tenant is not the same.
									}

									// var_dump($sender_phone);die();

							    // echo "'$needle' exists in the string.";die();
							} else {
							    // echo "'$needle' does not exist in the string<br>";
							}


					}
				}



			}
		}
		redirect('mpesa-transactions/unallocated-transactions');
	}

	function getLastThreeDigits($string) {
	    // Use regex to match the last three digits
	    if (preg_match('/(\d{1,3})$/', $string, $matches)) {
	        return $matches[1]; // Return the last one to three digits found
	    }
	    return null; // Return null if no digits found
	}

	function stringExists($haystack, $needle) {
	    // Check if the substring exists in the string
	    return strpos($haystack, $needle) !== false; // Returns true if found, false otherwise
	}


	public function update_tenant_account($account_number,$mpesa_id,$total_mpesa_paid,$transaction_time,$transaction_code,$paid_by,$lease_id,$account_id,$recon_date)
	{


		// convert the time to date time

		$date  = date('Y-m-d',strtotime($transaction_time));
		$month  = date('m',strtotime($transaction_time));
		$year  = date('Y',strtotime($transaction_time));

		// var_dump($date);die();

		// check the format of the account number and lease that is active account_number is the tenant number HXXXXXX
	


		if($lease_id > 0) // check if the value returns a lease id
		{
			// check if the account has a balance
			$all_leases = $this->leases_model->get_lease_detail($lease_id);
			foreach ($all_leases->result() as $leases_row)
			{
				$tenant_unit_id = $leases_row->tenant_unit_id;
				$property_name = $leases_row->property_name;
				$property_id = $leases_row->property_id;
				$rental_unit_name = $leases_row->rental_unit_name;
				$rental_unit_id = $leases_row->rental_unit_id;
				$tenant_name = $leases_row->tenant_name;
				$tenant_email = $leases_row->tenant_email;
				$lease_start_date = $leases_row->lease_start_date;
				$lease_duration = $leases_row->lease_duration;
				$tenant_id = $leases_row->tenant_id;
				$lease_number = $leases_row->lease_number;
				$lease_status = $leases_row->lease_status;
				$tenant_status = $leases_row->tenant_status;
				$tenant_number = $leases_row->tenant_number;
				$tenant_phone_number = $leases_row->tenant_phone_number;
			}

        
			$total_arrears = $this->accounts_model-> get_tenants_statements($lease_id,1);
			// $total_arrears = $tenants_response['balance'];


			if($total_arrears > 0 and $total_arrears <= $total_arrears) // if the account has a balance  and also the total_mpesa_paid
			{
				// get all the invoice types based on the priority of the balances

				$item_list = $this->accounts_model->get_all_invoice_items($lease_id);

		// var_dump($item_list);die();

				if($item_list->num_rows() > 0)
				{
						$amount_paid_balance = $total_mpesa_paid;
						// foreach ($item_list->result() as $key => $value) 
						// {
						// 	// code...
						// 		$invoice_type_name = $value->invoice_type_name;
						// 		$invoice_type_id = $value->invoice_type_id;
						// 		$total_amount = $value->total_amount;



						// 		if($total_amount > 0 AND $total_amount <= $amount_paid_balance)
						// 		{

						// 			// allocate the payment items based on step 3

						// 			$amount_to_pay = $total_amount;

						// 			$amount = $amount_to_pay;
						//   		$invoice_type_id=$invoice_type_id;
						//   		// $invoice_id=$this->input->post('invoice_id');
						//   		$payment_month=$month;
						//   		$payment_year=$year;
						//   		$lease_number=$lease_number;
						//   		$rental_unit_id=$rental_unit_id;
						//   		$tenant_id=$tenant_id;

						//   		$item_date = date('M Y',strtotime($date));

						//   		$remarks = 'Payment for  '.$invoice_type_name;

						//   		$service = array(
						//   							'payment_id'=>0,
						//   							'amount_paid'=> $amount,
						//   							'payment_month'=> $payment_month,
						//   							'rental_unit_id'=> $rental_unit_id,
						//   							'tenant_id'=> $tenant_id,
						//   							'lease_number'=> $lease_number,
						//   							'payment_year'=> $payment_year,
						//   							'invoice_type_id' => $invoice_type_id,
						//   							'payment_item_status' => 0,
						//   							'lease_id' => $lease_id,
						//   							'mpesa_id' => $mpesa_id,
						//   							'personnel_id' => $this->session->userdata('personnel_id'),
						//   							'payment_item_created' => $date,
						//   							'remarks'=>$remarks
						//   						);	

						  

						//   		// var_dump($service); die();
						//   		$this->db->insert('payment_item',$service);

						// 			// reduce the total amount to the balance


						// 			$amount_paid_balance = $amount_paid_balance - $amount_to_pay;




						// 		}else if($total_amount > 0 AND $total_amount > $amount_paid_balance)
						// 		{
						// 			// pay the difference 
						// 			$amount_to_pay = $amount_paid_balance; 



						// 			$amount = $amount_to_pay;
						// 			$invoice_type_id=$invoice_type_id;
						// 			// $invoice_id=$this->input->post('invoice_id');
						// 			$payment_month=$month;
						// 			$payment_year=$year;
						// 			$lease_number=$lease_number;
						// 			$rental_unit_id=$rental_unit_id;
						// 			$tenant_id=$tenant_id;

						// 			$item_date = date('M Y',strtotime($date));

						// 			$remarks = 'Payment for  '.$invoice_type_name;

						// 			$service = array(
						// 							'payment_id'=>0,
						// 							'amount_paid'=> $amount,
						// 							'payment_month'=> $payment_month,
						// 							'rental_unit_id'=> $rental_unit_id,
						// 							'tenant_id'=> $tenant_id,
						// 							'lease_number'=> $lease_number,
						// 							'payment_year'=> $payment_year,
						// 							'invoice_type_id' => $invoice_type_id,
						// 							'payment_item_status' => 0,
						// 							'lease_id' => $lease_id,
						// 							'mpesa_id' => $mpesa_id,
						// 							'personnel_id' => $this->session->userdata('personnel_id'),
						// 							'payment_item_created' => $date,
						// 							'remarks'=>$remarks
						// 						);

						// 			// var_dump($service); die();
						// 			$this->db->insert('payment_item',$service);

						// 			// reduce the total amount to the balance

						// 			$amount_paid_balance = $amount_paid_balance - $amount_to_pay;


						// 		}

						// 		// Create payment and update the payment items with the payment id

						// }


					$amount_paid = $total_mpesa_paid;
					$payment_method=9;
					$type_of_account=1;
					$rental_unit_id = $rental_unit_id;
					$lease_number=$lease_number;
					$tenant_id=$tenant_id;

					$transaction_code = $transaction_code;
					$bank_id = $account_id;


					// calculate the points to get
					$payment_date = $recon_date;

					$date_check = explode('-', $payment_date);
					$month = $date_check[1];
					$year = $date_check[0];

						$this->db->where('transaction_code ="'.$transaction_code.'" AND cancel = 0');
						$query_transactions = $this->db->get('payments');	
						// var_dump($query_transactions);die();
						if($query_transactions->num_rows() == 0)
						{

				  		$receipt_number = $this->accounts_model->create_receipt_number();




				  		$data = array(
									  			'payment_method_id'=>$payment_method,
									  			'bank_id'=>$bank_id,
									  			'amount_paid'=>$amount_paid,
									  			'personnel_id'=>$this->session->userdata("personnel_id"),
									  			'transaction_code'=>$transaction_code,
									  			'payment_date'=>$payment_date,
									  			'receipt_number'=>$transaction_code,
									  			'document_number'=>$receipt_number,
									  			'paid_by'=>$paid_by,
									  			'payment_created'=>date("Y-m-d"),
									  			'rental_unit_id'=>$rental_unit_id,
									  			'year'=>$year,
									  			'month'=>$month,
										      'account_id'=>$lease_number,
										      'tenant_id'=>$tenant_id,
									  			'confirm_number'=> $receipt_number,
									  			'confirmation_status'=>0,
									  			'payment_created_by'=>$this->session->userdata("personnel_id"),
									  			'approved_by'=>$this->session->userdata("personnel_id"),
									  			'date_approved'=>date('Y-m-d'),
	        								'transaction_date'=> $transaction_time
									  		);

				  			$data['lease_id'] = $lease_id;

				  			if($this->db->insert('payments', $data))
				  			{
				  				$payment_id = $this->db->insert_id();



				  					foreach ($item_list->result() as $key => $value) 
										{
											// code...
												$invoice_type_name = $value->invoice_type_name;
												$invoice_type_id = $value->invoice_type_id;
												$total_amount = $value->total_amount;



												if($total_amount > 0 AND $total_amount <= $amount_paid_balance)
												{

													// allocate the payment items based on step 3

													$amount_to_pay = $total_amount;

													$amount = $amount_to_pay;
										  		$invoice_type_id=$invoice_type_id;
										  		// $invoice_id=$this->input->post('invoice_id');
										  		$payment_month=$month;
										  		$payment_year=$year;
										  		$lease_number=$lease_number;
										  		$rental_unit_id=$rental_unit_id;
										  		$tenant_id=$tenant_id;

										  		$item_date = date('M Y',strtotime($date));

										  		$remarks = 'Payment for  '.$invoice_type_name;

										  		$service = array(
										  							'payment_id'=>0,
										  							'amount_paid'=> $amount,
										  							'payment_month'=> $payment_month,
										  							'rental_unit_id'=> $rental_unit_id,
										  							'tenant_id'=> $tenant_id,
										  							'lease_number'=> $lease_number,
										  							'payment_year'=> $payment_year,
										  							'invoice_type_id' => $invoice_type_id,
										  							'lease_id' => $lease_id,
										  							'mpesa_id' => $mpesa_id,
										  							'personnel_id' => $this->session->userdata('personnel_id'),
										  							'payment_item_created' => $date,
										  							'remarks'=>$remarks,
																		'payment_id'=>$payment_id,
																		'payment_item_created' =>$date,
																		'payment_item_status'=>1,
																		'document_no'=>$receipt_number
										  						);	

										  

										  		// var_dump($service); die();
										  		$this->db->insert('payment_item',$service);

													// reduce the total amount to the balance


													$amount_paid_balance = $amount_paid_balance - $amount_to_pay;




												}else if($total_amount > 0 AND $total_amount > $amount_paid_balance)
												{
													// pay the difference 
													$amount_to_pay = $amount_paid_balance; 



													$amount = $amount_to_pay;
													$invoice_type_id=$invoice_type_id;
													// $invoice_id=$this->input->post('invoice_id');
													$payment_month=$month;
													$payment_year=$year;
													$lease_number=$lease_number;
													$rental_unit_id=$rental_unit_id;
													$tenant_id=$tenant_id;

													$item_date = date('M Y',strtotime($date));

													$remarks = 'Payment for  '.$invoice_type_name;

													$service = array(
																	'payment_id'=>0,
																	'amount_paid'=> $amount,
																	'payment_month'=> $payment_month,
																	'rental_unit_id'=> $rental_unit_id,
																	'tenant_id'=> $tenant_id,
																	'lease_number'=> $lease_number,
																	'payment_year'=> $payment_year,
																	'invoice_type_id' => $invoice_type_id, 
																	'lease_id' => $lease_id,
																	'mpesa_id' => $mpesa_id,
																	'personnel_id' => $this->session->userdata('personnel_id'),
																	'payment_item_created' => $date,
																	'remarks'=>$remarks,
																	'payment_id'=>$payment_id,
							  									'payment_item_created' =>$date,
							  									'payment_item_status'=>1,
							  									'document_no'=>$receipt_number
																);

													// var_dump($service); die();
													$this->db->insert('payment_item',$service);

													// reduce the total amount to the balance

													$amount_paid_balance = $amount_paid_balance - $amount_to_pay;


												}
												else if($total_amount <= 0)
												{
													 // pay the difference 
													$amount_to_pay = $amount_paid_balance; 



													$amount = $amount_to_pay;
													$invoice_type_id=$invoice_type_id;
													// $invoice_id=$this->input->post('invoice_id');
													$payment_month=$month;
													$payment_year=$year;
													$lease_number=$lease_number;
													$rental_unit_id=$rental_unit_id;
													$tenant_id=$tenant_id;

													$item_date = date('M Y',strtotime($date));

													$remarks = 'Payment for  '.$invoice_type_name;

													$service = array(
																	'payment_id'=>0,
																	'amount_paid'=> $amount,
																	'payment_month'=> $payment_month,
																	'rental_unit_id'=> $rental_unit_id,
																	'tenant_id'=> $tenant_id,
																	'lease_number'=> $lease_number,
																	'payment_year'=> $payment_year,
																	'invoice_type_id' => $invoice_type_id, 
																	'lease_id' => $lease_id,
																	'mpesa_id' => $mpesa_id,
																	'personnel_id' => $this->session->userdata('personnel_id'),
																	'payment_item_created' => $date,
																	'remarks'=>$remarks,
																	'payment_id'=>$payment_id,
							  									'payment_item_created' =>$date,
							  									'payment_item_status'=>1,
							  									'document_no'=>$receipt_number
																);

													// var_dump($service); die();
													$this->db->insert('payment_item',$service);

													// reduce the total amount to the balance

													$amount_paid_balance = $amount_paid_balance - $amount_to_pay;
												}

												// Create payment and update the payment items with the payment id

										}
				  				// $service = array(
				  				// 					'payment_id'=>$payment_id,
				  				// 					'payment_item_created' =>$date,
				  				// 					'payment_item_status'=>1,
				  				// 					'document_no'=>$receipt_number
				  				// 				);
				  				// $this->db->where('payment_item_status = 0 AND lease_id = '.$lease_id.' and mpesa_id = '.$mpesa_id);
				  				// $this->db->update('payment_item',$service);


				  				$this->mpesa_model->update_mpesa_recon_status($mpesa_id,$total_mpesa_paid,$payment_id);
				  				// $add_mpesa['local_id'] = $payment_id;
				  				// $this->db->where('mpesa_id = '.$mpesa_id);
				  				// $this->db->update('mpesa_transactions',$add_mpesa);
				  				// send receipt to the tenant for the transaction 
				  				

				  				// return TRUE;
				  			}
				  			else{
				  				// return FALSE;
				  			}
				  		}
			  		

		  	}
			}

			

		}


	}

	public function update_water_tenant_account($account_number,$mpesa_id,$total_mpesa_paid,$transaction_time,$transaction_code,$paid_by,$lease_id,$account_id,$recon_date)
	{


		// convert the time to date time

		$date  = date('Y-m-d',strtotime($transaction_time));
		$month  = date('m',strtotime($transaction_time));
		$year  = date('Y',strtotime($transaction_time));

		// var_dump($date);die();

		// check the format of the account number and lease that is active account_number is the tenant number HXXXXXX
	


		if($lease_id > 0) // check if the value returns a lease id
		{
			// check if the account has a balance
			$all_leases = $this->leases_model->get_lease_detail($lease_id);
			foreach ($all_leases->result() as $leases_row)
			{
				$tenant_unit_id = $leases_row->tenant_unit_id;
				$property_name = $leases_row->property_name;
				$property_id = $leases_row->property_id;
				$rental_unit_name = $leases_row->rental_unit_name;
				$rental_unit_id = $leases_row->rental_unit_id;
				$tenant_name = $leases_row->tenant_name;
				$tenant_email = $leases_row->tenant_email;
				$lease_start_date = $leases_row->lease_start_date;
				$lease_duration = $leases_row->lease_duration;
				$tenant_id = $leases_row->tenant_id;
				$lease_number = $leases_row->lease_number;
				$lease_status = $leases_row->lease_status;
				$tenant_status = $leases_row->tenant_status;
				$tenant_number = $leases_row->tenant_number;
				$tenant_phone_number = $leases_row->tenant_phone_number;
			}

        
			$total_arrears = $this->accounts_model->get_tenants_statements($lease_id,5);
			// $total_arrears = $tenants_response['balance'];

	

		// var_dump($total_arrears);die();

			if($total_arrears > 0 and $total_arrears <= $total_arrears) // if the account has a balance  and also the total_mpesa_paid
			{
				// get all the invoice types based on the priority of the balances

				$item_list = $this->accounts_model->get_all_invoice_items($lease_id,null,2);

		// var_dump($item_list->num_rows());die();

				if($item_list->num_rows() > 0)
				{
						$amount_paid_balance = $total_mpesa_paid;
						// foreach ($item_list->result() as $key => $value) 
						// {
						// 	// code...
						// 		$invoice_type_name = $value->invoice_type_name;
						// 		$invoice_type_id = $value->invoice_type_id;
						// 		$total_amount = $value->total_amount;



						// 		if($total_amount > 0 AND $total_amount <= $amount_paid_balance)
						// 		{

						// 			// allocate the payment items based on step 3

						// 			$amount_to_pay = $total_amount;

						// 			$amount = $amount_to_pay;
						//   		$invoice_type_id=$invoice_type_id;
						//   		// $invoice_id=$this->input->post('invoice_id');
						//   		$payment_month=$month;
						//   		$payment_year=$year;
						//   		$lease_number=$lease_number;
						//   		$rental_unit_id=$rental_unit_id;
						//   		$tenant_id=$tenant_id;

						//   		$item_date = date('M Y',strtotime($date));

						//   		$remarks = 'Payment for  '.$invoice_type_name;

						//   		$service = array(
						//   							'payment_id'=>0,
						//   							'amount_paid'=> $amount,
						//   							'payment_month'=> $payment_month,
						//   							'rental_unit_id'=> $rental_unit_id,
						//   							'tenant_id'=> $tenant_id,
						//   							'lease_number'=> $lease_number,
						//   							'payment_year'=> $payment_year,
						//   							'invoice_type_id' => $invoice_type_id,
						//   							'payment_item_status' => 0,
						//   							'lease_id' => $lease_id,
						//   							'mpesa_id' => $mpesa_id,
						//   							'personnel_id' => $this->session->userdata('personnel_id'),
						//   							'payment_item_created' => $date,
						//   							'remarks'=>$remarks
						//   						);	

						  

						//   		// var_dump($service); die();
						//   		$this->db->insert('water_payment_item',$service);

						// 			// reduce the total amount to the balance


						// 			$amount_paid_balance = $amount_paid_balance - $amount_to_pay;




						// 		}else if($total_amount > 0 AND $total_amount > $amount_paid_balance)
						// 		{
						// 			// pay the difference 
						// 			$amount_to_pay = $amount_paid_balance; 



						// 			$amount = $amount_to_pay;
						// 			$invoice_type_id=$invoice_type_id;
						// 			// $invoice_id=$this->input->post('invoice_id');
						// 			$payment_month=$month;
						// 			$payment_year=$year;
						// 			$lease_number=$lease_number;
						// 			$rental_unit_id=$rental_unit_id;
						// 			$tenant_id=$tenant_id;

						// 			$item_date = date('M Y',strtotime($recon_date));

						// 			$remarks = 'Payment for  '.$invoice_type_name;

						// 			$service = array(
						// 							'payment_id'=>0,
						// 							'amount_paid'=> $amount,
						// 							'payment_month'=> $payment_month,
						// 							'rental_unit_id'=> $rental_unit_id,
						// 							'tenant_id'=> $tenant_id,
						// 							'lease_number'=> $lease_number,
						// 							'payment_year'=> $payment_year,
						// 							'invoice_type_id' => $invoice_type_id,
						// 							'payment_item_status' => 0,
						// 							'lease_id' => $lease_id,
						// 							'mpesa_id' => $mpesa_id,
						// 							'personnel_id' => $this->session->userdata('personnel_id'),
						// 							'payment_item_created' => $date,
						// 							'remarks'=>$remarks
						// 						);

						// 			// var_dump($service); die();
						// 			$this->db->insert('water_payment_item',$service);

						// 			// reduce the total amount to the balance

						// 			$amount_paid_balance = $amount_paid_balance - $amount_to_pay;


						// 		}

						// 		// Create payment and update the payment items with the payment id

						// }


					$amount_paid = $total_mpesa_paid;
					$payment_method=9;
					$type_of_account=1;
					$rental_unit_id = $rental_unit_id;
					$lease_number=$lease_number;
					$tenant_id=$tenant_id;

					$transaction_code = $transaction_code;
					$bank_id = $account_id;


					// calculate the points to get
					$payment_date = $recon_date;

					$date_check = explode('-', $payment_date);
					$month = $date_check[1];
					$year = $date_check[0];


			  		$receipt_number = $this->accounts_model->create_receipt_number();


			  			$this->db->where('transaction_code ="'.$transaction_code.'" AND cancel = 0');
						$query_transactions = $this->db->get('water_payments');	
						// var_dump($query_transactions);die();
						if($query_transactions->num_rows() == 0)
						{

						

				  		$data = array(
									  			'payment_method_id'=>$payment_method,
									  			'bank_id'=>$bank_id,
									  			'amount_paid'=>$amount_paid,
									  			'personnel_id'=>$this->session->userdata("personnel_id"),
									  			'transaction_code'=>$transaction_code,
									  			'payment_date'=>$payment_date,
									  			'receipt_number'=>$transaction_code,
									  			'document_number'=>$receipt_number,
									  			'paid_by'=>$paid_by,
									  			'payment_created'=>date("Y-m-d"),
									  			'rental_unit_id'=>$rental_unit_id,
									  			'year'=>$year,
									  			'month'=>$month,
										      'account_id'=>$lease_number,
										      'tenant_id'=>$tenant_id,
									  			'confirm_number'=> $receipt_number,
									  			'confirmation_status'=>0,
									  			'payment_created_by'=>$this->session->userdata("personnel_id"),
									  			'approved_by'=>$this->session->userdata("personnel_id"),
									  			'date_approved'=>date('Y-m-d'),
	        								'transaction_date'=> $transaction_time

									  		);

				  			$data['lease_id'] = $lease_id;

				  			if($this->db->insert('water_payments', $data))
				  			{
				  				$payment_id = $this->db->insert_id();

				  					// $amount_to_pay = $amount_paid_balance; 



										$amount = $total_mpesa_paid;
										$invoice_type_id=2;
										// $invoice_id=$this->input->post('invoice_id');
										$payment_month=$month;
										$payment_year=$year;
										$lease_number=$lease_number;
										$rental_unit_id=$rental_unit_id;
										$tenant_id=$tenant_id;

										$item_date = date('M Y',strtotime($recon_date));

										$remarks = 'Payment for  Water';

										$service = array(
														'payment_id'=>0,
														'amount_paid'=> $amount,
														'payment_month'=> $payment_month,
														'rental_unit_id'=> $rental_unit_id,
														'tenant_id'=> $tenant_id,
														'lease_number'=> $lease_number,
														'payment_year'=> $payment_year,
														'invoice_type_id' => $invoice_type_id,
														'lease_id' => $lease_id,
														'mpesa_id' => $mpesa_id,
														'personnel_id' => $this->session->userdata('personnel_id'),
														'payment_item_created' => $date,
														'remarks'=>$remarks,
														'payment_id'=>$payment_id,
				  									'payment_item_created' =>$date,
				  									'payment_item_status'=>1,
				  									'document_no'=>$receipt_number
													);

										// var_dump($service); die();
										$this->db->insert('water_payment_item',$service);


				  				// $service = array(
				  				// 					'payment_id'=>$payment_id,
				  				// 					'payment_item_created' =>$date,
				  				// 					'payment_item_status'=>1,
				  				// 					'document_no'=>$receipt_number
				  				// 				);
				  				// $this->db->where('payment_item_status = 0 AND lease_id = '.$lease_id.' and mpesa_id = '.$mpesa_id);
				  				// $this->db->update('water_payment_item',$service);


				  				$this->mpesa_model->update_mpesa_recon_status($mpesa_id,$total_mpesa_paid,$payment_id);
				  				// $add_mpesa['local_id'] = $payment_id;
				  				// $this->db->where('mpesa_id = '.$mpesa_id);
				  				// $this->db->update('mpesa_transactions',$add_mpesa);
				  				// send receipt to the tenant for the transaction 
				  				

				  				// return TRUE;
				  			}
				  			else{
				  				// return FALSE;
				  			}
				  	}
			  		

		  	}
			}

			

		}


	}

}
?>
