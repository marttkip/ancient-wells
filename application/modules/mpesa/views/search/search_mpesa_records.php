<section class="panel panel-info">
    <header class="panel-heading">
      <h3 class="panel-title">Search unreconcilled Amount</h3>
    </header>
    <div class="panel-body">
           <?php
           echo form_open("search-mpesa-transactions", array("class" => "form-horizontal"));
           ?>
           <div class="row">
            <div class="col-md-4">
                   <div class="form-group">
                        <label class="col-md-4 control-label">Account: </label>
                        <div class="col-md-8">
                            <select class="form-control" name="account_id" required="">
                                <option value="">-- Select account --</option>
                                <?php
                                 $query = $this->accounts_model->get_bank_accounts_types(1);

                                $options2 = $query;
                                $bank_list = '';
                                $bank_total = 0;
                                foreach($options2->result() AS $key_old) 
                                { 

                                    $account_id = $key_old->account_id;
                                    $account_name = $key_old->account_name;
                                    ?>
                                    <option value="<?php echo $account_id;?>"><?php echo $account_name;?></option>
                                    <?php
                                    
                                }
                                ?>
                            </select>
                        </div>
                    </div>
              </div>

              <div class="col-md-4">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Name: </label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="sender_name" placeholder="Name" autocomplete="off"/>
                       </div>
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Serial: </label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="serial_number" placeholder="Serial" autocomplete="off"/>
                       </div>
                   </div>
               </div>
            </div>
            <br>
            <div class="row">
               <div class="col-md-4">
                   <div class="form-group">
                       <label class="col-md-4 control-label">FROM: </label>

                       <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" id='datetimepicker1' data-plugin-datepicker class="form-control" name="transaction_date_from" placeholder="Date from">
                            </div>
                       </div>
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="form-group">
                       <label class="col-md-4 control-label">TO : </label>

                       <div class="col-md-8">
                          
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" id='datetimepicker1' data-plugin-datepicker class="form-control" name="transaction_date_to" placeholder="Date from">
                            </div>
                       </div>
                   </div>
               </div>


               <div class="col-md-4">
                   <div class="center-align">
                       <button type="submit" class="btn btn-info btn-sm">Search</button>
                   </div>
               </div>
           </div>


           <?php
           echo form_close();
           ?>
       </div>
  </section>
