<section class="panel panel-success">
    <header class="panel-heading">
      <h3 class="panel-title">Search Reconcilled Withdrawals</h3>
    </header>
    <div class="panel-body">
           <?php
           echo form_open("search-reconcilled-withdrawals", array("class" => "form-horizontal"));
           ?>
           <div class="row">
                
               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Serial: </label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="serial_number" placeholder="Serial" autocomplete="off"/>
                       </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">FROM: </label>

                       <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" id='datetimepicker1' data-plugin-datepicker class="form-control" name="transaction_date_from" placeholder="Date from">
                            </div>
                       </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">TO : </label>

                       <div class="col-md-8">
                          
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" id='datetimepicker1' data-plugin-datepicker class="form-control" name="transaction_date_to" placeholder="Date from">
                            </div>
                       </div>
                   </div>
               </div>

               <div class="col-md-2">
                   <div class="center-align">
                       <button type="submit" class="btn btn-info btn-sm">Search</button>
                   </div>
               </div>
           </div>


           <?php
           echo form_close();
           ?>
       </div>
  </section>
