<?php

class Company_financial_model extends CI_Model
{

	public function get_income_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(cr_amount) AS total_amount,transactionName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'"');
		$this->db->group_by('transactionCode');
		$query = $this->db->get();

		return $query;
	}

	public function get_cog_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'" AND projectId > 0');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_operational_cost_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'" AND (projectId IS NULL OR projectId = 0)');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_payables_aging_report()
	{
		//retrieve all users
		$this->db->from('v_aged_payables');
		$this->db->select('*');
		// $this->db->where('');
		// $this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}

	public function get_receivables_aging_report()
	{
		//retrieve all users
		$this->db->from('v_aged_receivables');
		$this->db->select('*');
		// $this->db->where('');
		// $this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}



	public function get_account_value()
	{
		//retrieve all users
		$this->db->from('v_account_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount,accountName');
		$this->db->where('accountParentId = 1');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;

	}


	public function get_accounts_receivables()
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount) -SUM(dr_amount)) AS total_amount');
		$this->db->where('projectId > 0 AND (transactionCategory = "Revenue" OR transactionCategory = "Tenants Payments")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_credit_note');
		$this->db->where('projectId > 0 AND transactionCategory = "Tenants Credit Notes"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_credit_note = $query_credit_row->total_credit_note;

		return $total_invoices_balance - $total_credit_note;

	}
	public function get_accounts_payable()
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId > 0 AND (transactionClassification = "Creditors Invoices" OR transactionClassification = "Purchases")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_payments');
		$this->db->where('recepientId > 0 AND transactionCategory = "Expense Payment"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_payments = $query_credit_row->total_payments;

		return $total_invoices_balance - $total_payments;
	}


	public function get_total_wht_tax()
	{
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 2 AND vat_amount > 0');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}

	public function get_total_vat_tax()
	{
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 1 AND vat_amount > 0');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;




		return $total_invoices_balance;

	}

	public function get_all_vendors()
	{
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id > 0');
		$query = $this->db->get();

		return $query;
	}

	public function get_creditor_expenses($creditor_id)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId = '.$creditor_id.' AND (transactionClassification = "Creditors Invoices" OR transactionClassification = "Purchases")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_payments');
		$this->db->where('recepientId = '.$creditor_id.'  AND transactionCategory = "Expense Payment"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_payments = $query_credit_row->total_payments;

		return $total_invoices_balance - $total_payments;
	}



	public function get_all_properties()
	{
		$this->db->from('property');
		$this->db->select('*');
		$this->db->where('property_id > 0');
		$query = $this->db->get();

		return $query;
	}

	public function get_property_balances($property_id)
	{


		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount) -SUM(dr_amount)) AS total_amount');
		$this->db->where('projectId = '.$property_id.' AND (transactionCategory = "Revenue" OR transactionCategory = "Tenants Payments")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_credit_note');
		$this->db->where('projectId = '.$property_id.' AND transactionCategory = "Tenants Credit Notes"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_credit_note = $query_credit_row->total_credit_note;

		return $total_invoices_balance - $total_credit_note;

	}

	public function get_total_receivable_wht_tax()
	{
		//retrieve all users
		$this->db->from('lease_invoice,invoice');
		$this->db->select('SUM(tax_amount) AS total_amount');
		$this->db->where('tax_amount > 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}

	/*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_creditor($creditor_id)
	{
		//retrieve all users
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id = '.$creditor_id);
		$query = $this->db->get();

		return $query;
	}


	public function get_creditor_statement($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
		
			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_creditor_ledger_by_date,creditor');
		$this->db->select('*');
		$this->db->where('creditor.creditor_id = v_creditor_ledger_by_date.recepientId AND v_creditor_ledger_by_date.transactionDate >= creditor.start_date AND recepientId = '.$creditor_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}



 	public function get_unallocated_funds($creditor_id)
   {
    $selected_statement = "
                  SELECT 
                      SUM(cr_amount) AS cr_amount

                  FROM
                  (
                  SELECT
                  `creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
                  `creditor_payment`.`creditor_payment_id` AS `referenceId`,
                  `creditor_payment`.`reference_number` AS `referenceCode`,
                  `creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
                  `creditor_payment`.`document_number` AS `transactionCode`,
                  '' AS `patient_id`,
                  `creditor_payment`.`creditor_id` AS `recepientId`,
                  `account`.`parent_account` AS `accountParentId`,
                  `account_type`.`account_type_name` AS `accountsclassfication`,
                  `creditor_payment`.`account_from_id` AS `accountId`,
                  `account`.`account_name` AS `accountName`,
                  `creditor_payment_item`.`description` AS `transactionName`,
                  CONCAT('Payment on account')  AS `transactionDescription`,
                      0 AS `department_id`,
                  0 AS `dr_amount`,
                  `creditor_payment_item`.`amount_paid` AS `cr_amount`,
                  `creditor_payment`.`transaction_date` AS `transactionDate`,
                  `creditor_payment`.`created` AS `createdAt`,
                  `creditor_payment`.`created` AS `referenceDate`,
                  `creditor_payment_item`.`creditor_payment_item_status` AS `status`,
                  'Expense Payment' AS `transactionCategory`,
                  'Creditors Invoices Payments' AS `transactionClassification`,
                  'creditor_payment' AS `transactionTable`,
                  'creditor_payment_item' AS `referenceTable`
                FROM
                  (
                    (
                      (
                        `creditor_payment_item`
                        JOIN `creditor_payment` ON(
                          (
                            creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
                          )
                        )
                      )
                      JOIN account ON(
                        (
                          account.account_id = creditor_payment.account_from_id
                        )
                      )
                    )
                    JOIN `account_type` ON(
                      (
                        account_type.account_type_id = account.account_type_id
                      )
                    )
                    JOIN `creditor` ON(
                      (
                        creditor.creditor_id = creditor_payment_item.creditor_id
                      )
                    )
                  )
                  WHERE creditor_payment_item.invoice_type = 3) AS data WHERE data.recepientId = ".$creditor_id;
          $query = $this->db->query($selected_statement);
          $checked = $query->row();

          $dr_amount = $checked->cr_amount;

          return $dr_amount;
  	}


  	public function get_creditor_statement_balance($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';

		// var_dump($search_status);die();
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate < \''.$date_from.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate < \''.$date_to.'\'';
			}
		}
		else
		{
		
			$search_invoice_add =  '';

		}
		//retrieve all users
		$this->db->from('v_general_ledger_by_date,creditor');
		$this->db->select('SUM(`dr_amount`) AS dr_amount,SUM(cr_amount) AS cr_amount');
		$this->db->where('creditor.creditor_id = v_general_ledger_by_date.recepientId AND v_general_ledger_by_date.transactionDate >= creditor.start_date AND recepientId = '.$creditor_id.$search_invoice_add);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}

	public function get_creditor_amount_paid($creditor_invoice_id,$creditor_id)
	{
		 $this->db->where('creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id AND creditor_payment.creditor_payment_status = 1 AND creditor_payment_item.creditor_invoice_id ='.$creditor_invoice_id.' AND creditor_payment_item.creditor_id = '.$creditor_id);
		 $this->db->select('SUM(creditor_payment_item.amount_paid) AS total_paid');
		 $query = $this->db->get('creditor_payment,creditor_payment_item');


		 $total_paid = 0;
		 if($query->num_rows() > 0)
		 {
		  foreach ($query->result() as $key => $value) {
		    # code...
		    $total_paid = $value->total_paid;
		  }
		 }
		 return $total_paid;
	}

	public function get_type()		
	{
		//retrieve all users
		$this->db->from('account_type');
		$this->db->select('*');
		$this->db->where('account_type_id > 0 ');
		$query = $this->db->get();
		
		return $query;    	
 
    }
    public function get_parent_accounts()		
	{
		//retrieve all users
		$this->db->from('account');
		$this->db->select('*');
		$this->db->where('parent_account = 0');
		$query = $this->db->get();
		
		return $query;    	
 
    }
    public function get_all_cash_accounts($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all accounts
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}

	public function deactivate_account($account_id)
	{
		$this->db->where('account_id = '.$account_id);
		if($this->db->update('account',array('account_status'=>0)))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function activate_account($account_id)
	{
		$this->db->where('account_id = '.$account_id);
		if($this->db->update('account',array('account_status'=>1)))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_account($account_id)
	{
		$this->db->select('*');
		$this->db->where('account_id = '.$account_id);
		$query = $this->db->get('account');
		
		return $query->row();
	}

	public function update_account($account_id)
		{
			$account_data = array(
						'account_name'=>$this->input->post('account_name'),
						'account_type_id'=>$this->input->post('account_type_id'),
						'parent_account'=>$this->input->post('parent_account'),
						'account_opening_balance'=>$this->input->post('account_balance'),
						'start_date'=>$this->input->post('start_date'),
						'paying_account'=>$this->input->post('paying_account'),
						'transfer_charge'=>$this->input->post('transfer_charge')
						);
			$this->db->where('account_id = '.$account_id);
			if($this->db->update('account', $account_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		public function add_account()
		{
			$account = array(
						'account_name'=>$this->input->post('account_name'),
						'account_opening_balance'=>$this->input->post('account_balance'),
						'parent_account'=>$this->input->post('parent_account'),
						'account_type_id'=>$this->input->post('account_type_id'),
	                    'account_status'=>$this->input->post('account_status'),
						'start_date'=>$this->input->post('start_date'),
						'paying_account'=>$this->input->post('paying_account'),
						'transfer_charge'=>$this->input->post('transfer_charge')
						);
			if($this->db->insert('account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}

		public function get_account_id($account_name)
		{
			$account_id = 0;
			
			$this->db->select('account_id');
			$this->db->where('account_name = "'.$account_name.'"');
			$query = $this->db->get('account');
			
			$bal = $query->row();
			$account_id = $bal->account_id;
			// var_dump($account_id); die();
			return $account_id;
			
		}
		public function get_account_opening_bal($account)
		{
			$opening_bal = 0;
			
			$this->db->select('account_opening_balance');
			$this->db->where('account_id = '.$account);
			$query = $this->db->get('account');
			
			$bal = $query->row();
			$opening_bal = $bal->account_opening_balance;

			return $opening_bal;
			
		}
		public function get_total_opening_bal()
		{
			$opening_bal = 0;
			
			$this->db->select('SUM(account_opening_balance) AS total_opening_bal');
			$query = $this->db->get('account');
			
			$bal = $query->row();
			$opening_bal = $bal->total_opening_bal;

			return $opening_bal;
		}

	    public function get_parent_account($parent_account)
	    {
	    	$this->db->from('account');
			$this->db->select('*');
			$this->db->where('account_id = '.$parent_account);
			$query = $this->db->get();
			$account_name = '';
			if($query->num_rows() > 0)  
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$account_name = $value->account_name;
				}
			}

			return $account_name;
	    }

}
?>
