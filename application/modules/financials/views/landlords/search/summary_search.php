        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
			$error = $this->session->userdata('error_message');
			if(!empty($error))
			{
				?>
				<div class="alert alert-danger">
				<?php echo $error;?>
                </div>
                <?php
				$this->session->unset_userdata('error_message');
			}
            echo form_open("reports/search-landlord-summaries", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Owner</label>
                        
                        <div class="col-lg-8">
                           <select id='property_owner_id' name='property_owner_id' class='form-control' required="required">
                              <option value=''>Select a property owner</option>
                              <?php echo $property_list;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                   <div class="form-group">
                        <label class="col-lg-4 control-label">Date From: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transaction_date_from" placeholder="Date From" required="required">
                            </div>
                        </div>
                    </div>

                </div>
                 <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label"> Date To: </label>
                        
                        <div class="col-lg-8">
                        	<div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transaction_date_to" placeholder="Date To" required="required">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="center-align">
                                <button type="submit" class="btn btn-sm btn-info">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>

        