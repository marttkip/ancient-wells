<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('site/site_model');
	}
    
	/*
	*
	*	Default action is to go to the home page
	*
	*/
	public function index() 
	{
		redirect('home');
	}
    
	/*
	*
	*	Home Page
	*
	*/
	public function home_page() 
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$data['class'] = 'home';
		$data['content'] = $this->load->view("home", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}
    
	/*
	*
	*	FAQs
	*
	*/
	public function faqs() 
	{	
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("faqs", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}
    
	/*
	*
	*	terms
	*
	*/
	public function terms() 
	{	
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("terms", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}
    
	/*
	*
	*	privacy
	*
	*/
	public function privacy() 
	{	
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("privacy", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}
    
	/*
	*
	*	about
	*
	*/
	public function about() 
	{	
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$v_data['items'] = $this->site_model->get_front_end_items();
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("about", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}

	/*
	*
	*	about
	*
	*/
	public function services() 
	{	
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$v_data['service_location'] = $this->service_location;
		$data['title'] = $this->site_model->display_page_title();
		$v_data['services'] = $this->site_model->get_active_departments('Services');
		$services = $v_data['services'];
		
		if($services->num_rows() > 0)
		{
			$result = $services->result();
			$service_name = $result[0]->service_name;
			$web_name = $this->site_model->create_web_name($service_name);
			redirect('services/'.$web_name);
		}
		/*$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("services", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);*/
	}


   /*
	*
	*	about
	*
	*/
	public function gallery() 
	{	
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['gallery_location'] = $this->gallery_location;
		$v_data['gallery_images'] = $this->site_model->get_active_gallery();
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("gallery", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}
	
	public function service_item($service_web_name = NULL) 
	{
		$table = "service";
		$where = 'service.service_status = 1';
		$v_data['service_location'] = $this->service_location;
		
		if($service_web_name != NULL)
		{
			$service_name = $this->site_model->decode_web_name($service_web_name);
			$where .= ' AND service.service_name = \''.$service_name.'\'';//echo $where; die();
			$v_data['services_item'] = $this->site_model->get_services($table, $where, NULL);
			$data['title'] = $service_name;
			$v_data['title'] = $service_name;
		}
		
		else
		{
			$data['title'] = 'Our Services';
			$v_data['title'] = 'Our Services';
			$v_data['services_item'] = $this->site_model->get_services($table, $where, 12);
		}
		$v_data['service_location'] = $this->service_location;
		$v_data['services'] = $this->site_model->get_active_departments('Services');

		$contacts = $this->site_model->get_contacts();
		$v_data['service_web_name'] = $service_web_name;
		$v_data['contacts'] = $contacts;
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("single_service", $v_data, TRUE);
		
		$this->load->view("site/templates/general_page", $data);
	}
	
	/*
	*
	*	Contact
	*
	*/
	public function contact()
	{
		$v_data['sender_name_error'] = '';
		$v_data['sender_email_error'] = '';
		$v_data['sender_phone_error'] = '';
		$v_data['message_error'] = '';
		
		$v_data['sender_name'] = '';
		$v_data['sender_email'] = '';
		$v_data['sender_phone'] = '';
		$v_data['message'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('sender_name', 'Your Name', 'required');
		$this->form_validation->set_rules('sender_email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('sender_phone', 'phone', 'xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'required');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$response = $this->site_model->contact();
			$this->session->set_userdata('success_message', 'Your message has been sent successfully. We shall get back to you as soon as possible');
		}
		else
		{
			$validation_errors = validation_errors();
			
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['sender_name_error'] = form_error('sender_name');
				$v_data['sender_email_error'] = form_error('sender_email');
				$v_data['sender_phone_error'] = form_error('sender_phone');
				$v_data['message_error'] = form_error('message');
				
				//repopulate fields
				$v_data['sender_name'] = set_value('sender_name');
				$v_data['sender_email'] = set_value('sender_email');
				$v_data['sender_phone'] = set_value('sender_phone');
				$v_data['message'] = set_value('message');
			}
		}
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$v_data['items'] = $this->site_model->get_front_end_items();
		
		$data['title'] = $v_data['title'] = $this->site_model->display_page_title();
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view('contact', $v_data, true);
		
		$this->load->view("site/templates/general_page", $data);
	}
	
	/*
	*
	*	Request Quote
	*
	*/
	public function request_quote()
	{
		//form validation rules
		$this->form_validation->set_rules('fname', 'Your Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('tel', 'phone', 'xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'required');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$response = $this->site_model->request_quote();
			$this->session->set_userdata('quote_success_message', 'Your message has been sent successfully. We shall get back to you as soon as possible');
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata('quote_error_message', validation_errors());
		}
		
		redirect('home');
	}
	
	public function about_us($about_name)
	{
		$about_title = $this->site_model->decode_web_name($about_name);
		$title = $about_title ;

		$v_data['title'] = $title;
		$service_id = $this->site_model->get_service_id($title);
		$about = $this->site_model->get_about_item($service_id);
		$contacts = $this->site_model->get_contacts();

		$v_data['contacts'] = $contacts;
		$v_data['services'] = $about;
		$data['content'] = $this->load->view('single_about', $v_data, true);
		$this->load->view("site/templates/general_page", $data);
	}
	
}
?>