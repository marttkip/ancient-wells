<?php
    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $facebook = $contacts['facebook'];
        $twitter = $contacts['twitter'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        $address = $contacts['address'];
        $post_code = $contacts['post_code'];
        $city = $contacts['city'];
        $building = $contacts['building'];
        $floor = $contacts['floor'];
        $location = $contacts['location'];

        $working_weekday = $contacts['working_weekday'];
        $working_weekend = $contacts['working_weekend'];

        $mission = $contacts['mission'];
        $vision = $contacts['vision'];
        $about = $contacts['about'];
        $core_values = $contacts['core_values'];
    }
?>
			<!-- inner banner -->
            <section class="bg-white inner-banner pages">

                <div class="container pad-container">
                    <div class="col-md-12">
                        <h1><?php echo $title?></h1>
                    </div>
                </div>

            </section>
            <!-- / inner banner -->
            <!-- intro section -->
            <section class="bg-white">
                <div class="container pad-container2">

                    <div class="row t-pad20">

                        <div class="col-md-4">

                            <h3>Who We Are?</h3>
                            <?php echo $about;?>

                        </div>

                        <div class="col-md-4">

                            <h3>Mission</h3>
                            <?php echo $mission;?>

                        </div>
                        
                        <div class="col-md-4">

                            <h3>Vision</h3>
                            <?php echo $vision;?>

                        </div>


                    </div>

                </div>
            </section>
            <!--/ intro section -->
            <!-- skills section -->
            <section class="bg-gray">

                <div class="container container-pad">

                    <div class="col-sm-6">
                        <h2>Our Core Value</h2>
                        <?php echo $core_values?>

                    </div>

                    <div class="col-sm-6">
						<img src="<?php echo base_url().'assets/images/dark-logo.png';?>"  alt="">
                    </div>

                </div>

            </section>
            <!--/ skills section -->

