<?php

	$result = '';
	
	//if users exist display them

	if ($latest_posts->num_rows() > 0)
	{	
		//get all administrators
		$administrators = $this->users_model->get_all_administrators();
		if ($administrators->num_rows() > 0)
		{
			$admins = $administrators->result();
		}
		
		else
		{
			$admins = NULL;
		}
		
		foreach ($latest_posts->result() as $row)
		{
			$post_id = $row->post_id;
			$blog_category_name = '';//$row->blog_category_name;
			$blog_category_web_name = $this->site_model->create_web_name($blog_category_name);
			$blog_category_id = $row->blog_category_id;
			$post_title = $row->post_title;
			$web_name = $this->site_model->create_web_name($post_title);
			$post_status = $row->post_status;
			$post_views = $row->post_views;
			$image = base_url().'assets/images/posts/'.$row->post_image;
			$created_by = $row->created_by;
			$modified_by = $row->modified_by;
			$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
			$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
			$description = $row->post_content;
			$mini_desc = implode(' ', array_slice(explode(' ', strip_tags($description)), 0, 30));
			$created = $row->created;
			$day = date('j',strtotime($created));
			$month = date('M',strtotime($created));
			//$created_on = date('jS M Y',strtotime($row->created));
			$created_on = date('M Y',strtotime($row->created));
			
			$categories = '';
			$count = 0;
			
			foreach($categories_query->result() as $res)
			{
				$count++;
				$category_name = $res->blog_category_name;
				$category_id = $res->blog_category_id;
				
				if($count == $categories_query->num_rows())
				{
					$categories .= '<li><a href="'.site_url().'blog/category/'.$category_id.'">'.$category_name.'</a></li>';
				}
				
				else
				{
					$categories .= '<li><a href="'.site_url().'blog/category/'.$category_id.'">'.$category_name.'</a></li>';
				}
			}
			$comments_query = $this->blog_model->get_post_comments($post_id);
			//comments
			$comments = 'No Comments';
			$total_comments = $comments_query->num_rows();
			if($total_comments == 1)
			{
				$title = 'comment';
			}
			else
			{
				$title = 'comments';
			}
			
			if($comments_query->num_rows() > 0)
			{
				$comments = '';
				foreach ($comments_query->result() as $row)
				{
					$post_comment_user = $row->post_comment_user;
					$post_comment_description = $row->post_comment_description;
					$date = date('jS M Y H:i a',strtotime($row->comment_created));
					
					$comments .= 
					'
						<div class="user_comment">
							<h5>'.$post_comment_user.' - '.$date.'</h5>
							<p>'.$post_comment_description.'</p>
						</div>
					';
				}
			}
			$result .= '
			
				<!-- blog posts 1-->
				<div class="item">

					<div class="post-content">
						<div class="img">
							<div class="date">'.$created_on.'</div>
							<img src="'.$image.'" class="pic" alt="'.$post_title.'">  
						</div>                              
						<h2><a href="news-details.html">'.$post_title.'</a></h2>
						<div class="meta">
							<ul>
								<li><a href="#">Posted by Admin</a></li>
								'.$categories.'
								<li><a href="#">'.$total_comments.' '.$title.'</a></li>
							</ul>
						</div>    
						<p>'.$mini_desc.'</p>
						<p class="t-mgr10"><a href="'.site_url().'blog/'.$web_name.'">read more &rsaquo;</a></p>
						

					</div>

				</div>
			';
		}
	}
	else
	{
		$result = '<div class="item"><div class="post-content"><p>There are no posts :-(</p></div></div>';
	}
	   
	  ?>
				
				<!-- blog posts -->
				<section class="bg-gray">

					<div class="container pad-container">

						<div class="row col-md-12 text-center">
							<h2 class="heading">Latest News Updates </h2>
						</div>

						<div class="row">
							
							<div class="col-md-12">

								<div class="owl-carousel owlcarousel-full">
									<?php echo $result;?>
								</div>

							</div>

						</div>                    

					</div>

				</section>
				<!-- / blog posts -->
