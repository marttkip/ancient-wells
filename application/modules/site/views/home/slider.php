
        <!-- Set the URL of your banner background image to data-image-src="YOUR IMAGE URL" attribute. -->
        <section id="banner" class="banner-wrapper" data-parallax="scroll" data-image-src="<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/banner_background.jpg">
            <div class="section-content">
                <div class="container"> 
                    <div class="banner clearfix">
                        
                        <!-- START BANNER DECORATION -->
                        <div class="banner-decoration reveal reveal-left" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/banner_decoration.png');">
                            <!-- Set banner image on the phone here. The image sets from remote server -->
                             <div class="image-holder">
                                <img src="<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/banner_mobile.jpg" alt="banner image"/>
                            </div>
                        </div>
                        <!-- END BANNER DECORATION -->
                        
                        <!-- START BANNER CONTENT -->
                        <div class="banner-content reveal reveal-right">
                            <h1>Ubiker</h1>
                            <div class="banner-description">
                                Lorem ipsum dolor sit amet, consectetur elit, sed do eiu mod tempor incididunt ut 
                                labore et dolore magna aliqua. Ut enim ad minim veniam.
                            </div>
                            <button type="button" class="btn btn-primary">Get app</button>
                            <button type="button" class="btn btn-default">Take a tour</button>
                        </div>
                        <!-- END BANNER CONTENT -->
                        
                    </div>
                </div>
            </div>
        </section>
        