<?php
$routes = '';
if($routes_query->num_rows() > 0)
{
	foreach ($routes_query->result() as $key) {
		# code...
		$route_start = $key->route_start;
		$route_end = $key->route_end;
		$route_id = $key->route_id;

		$routes .= '
					<div class="col-50">
                      <label class="label-radio item-content">
                      <span class="mylabel">'.$route_start.' - '.$route_end.'</span>
                        <input class="styledinput" type="radio" name="route_id" placeholder="" value="'.$route_id.'">
                        <div class="item-after"><i class="icon-form-radio"></i></div>
                      </label>
                    </div>
				   ';
	}
}
echo $routes;
?>