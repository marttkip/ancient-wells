<?php
$routes = '';
if($properties->num_rows() > 0)
{
	foreach ($properties->result() as $key) {
		# code...
		$property_name = $key->property_name;
		$property_id = $key->property_id;

		// balance b/f

		// property invoices


		// $total_credit_note = $this->accounts_model->get_rent_credit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);
		// $total_debit_note =$this->accounts_model->get_rent_debit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);


		// $rent_payment_amount = $this->accounts_model->get_rent_payments_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

		// $rent_invoice_amount =$this->accounts_model->get_rent_invoices_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

		$brought_forward = 0;//($rent_invoice_amount+$total_debit_note) - ($rent_payment_amount+$total_credit_note);

		$rent_current_invoice =  $this->reporting_model->get_current_month_invoice($property_id,date('m'),date('Y'),1);
		$rent_current_payment = $this->reporting_model->get_current_months_payments($property_id,date('m'),date('Y'),1);

		// $total_current_variance = $rent_current_invoice - $rent_current_payment;

		// $total_balance = $brought_forward + $total_current_variance;

		// property payments

		// balance

		if($rent_current_invoice > $rent_current_payment)
		{
			$percentage = number_format(($rent_current_payment/$rent_current_invoice) *100,0);
		}
		else
		{
			$percentage = 100;
		}


		$routes .= '<li>
					  <a href="" class="item-link item-content" >
					    <div class="item-media">
					      <div class="list-date">
					        <span class="list-dayname">'.$percentage.'%</span>
					        <span class="list-daynumber"></span>
					    </div>
					     </div>
					    <div class="item-inner">
					      <div class="item-title-row">
					        <div class="item-title">'.ucfirst($property_name).' </div>
					      </div>
					       <div class="item-subtitle"> <i class="fa fa-user"></i> '.ucfirst($property_name).' </div>
					       <div class="item-text"><span><i class="fa fa-arrow-up"></i> B/F :</span> 0.00 <span><i class="fa fa-arrow-up"></i> INV :</span> '.number_format($rent_current_invoice,0).' <span><i class="fa fa-arrow-down"></i> PYT :</span> '.number_format($rent_current_payment,0).' </div>
					    </div>
					  </a>
					</li>';
	}
}


echo $routes;
?>