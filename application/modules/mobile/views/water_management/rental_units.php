<?php
$routes = '';

$rental_units = $this->leases_model->get_property_rental_units($property_id);


if($property_invoice_id > 0)
{


	// $property_invoice_id = $pro;
	$property_invoice_rs = $this->water_management_model->get_property_invoice_detail($property_invoice_id);


	if($property_invoice_rs->num_rows() > 0)
	{
		foreach ($property_invoice_rs->result() as $key => $value) {
			# code...
			$invoice_year = $value->year;
			$invoice_month = $value->month;
			$property_invoice_date = $value->property_invoice_date;

			// var_dump($invoice_year);die();
		}
	}

	if($rental_units->num_rows() > 0)
	{
		foreach ($rental_units->result() as $leases_row) {
			# code...
			// $rental_unit_id = $key->rental_unit_id;
			// $rental_unit_name = $key->rental_unit_name;
			// $tenant_name = $key->tenant_name;
			// $lease_id = $key->lease_id;
			
			$year = date('Y');
			$month = date('m');




			
			$lease_id = $leases_row->lease_id;
			$tenant_id = $leases_row->tenant_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$rental_unit_id = $leases_row->rental_unit_id;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$initial_reading = 0;// $leases_row->initial_reading;

			// var_dump($initial_reading);die();
			$rent_calculation = $leases_row->rent_calculation;
			// $property_id = $leases_row->property_id;
			$deposit = $leases_row->deposit;
			$water_charges = 0;//$leases_row->billing_amount;
			$deposit_ext = $leases_row->deposit_ext;
			$lease_status = $leases_row->lease_status;
			$points = 0;//$leases_row->points;
			$created = $leases_row->created;

			$todays_date = date('Y-m-d');
			$todays_month = date('m');
			$todays_year = date('Y');

			

		
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

			$invoice_id= $this->water_management_model->get_max_invoice_id($lease_id);
			// var_dump($invoice_id); die();
			$prev_reading = '';
			$current_reading = 0;
			$units_consumed = 0;
			$arrears_bf = 0;
			$property_invoice_status = 0;
		
			if(!empty($invoice_id))
			{

				// var_dump($invoice_id);die();
				$water_readings = $this->water_management_model->get_water_readings($invoice_id);
				if($water_readings->num_rows() > 0)
				{
					foreach ($water_readings->result() as $key) {
						# code...
						$prev_reading = $key->prev_reading;
						$arrears_bf = $key->arrears_bf;
					}
				}
			}

			if(empty($arrears_bf))
			{
				$prev_reading = $initial_reading;
			}
			else
			{
				$prev_reading = $arrears_bf;

			}
			$current_response = $this->water_management_model->get_current_active_meter_number($lease_id);
			$current_meter_number = $current_response['property_billing_id'];
			$initial_reading = $current_response['initial_reading'];
			$billing_amount = $current_response['billing_amount'];

			$previous_meter_number = $this->water_management_model->get_previous_active_meter_number($lease_id);

			if($current_meter_number == $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
			{
				$used_meter = $previous_meter_number;
			}
			else if($current_meter_number != $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
			{

				if($property_invoice_status == 1 OR $property_invoice_status == 0)
				{
					$used_meter = $current_meter_number;
				}
				else
				{

					$used_meter = $previous_meter_number;
				}
				
			}

			$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
			$current_reading = 0;

		

			if($water_readings_current['status'])
			{
				$current_reading = $water_readings_current['current_reading'];
				$prev_reading = $water_readings_current['prev_reading'];

				if($current_reading == 0)
				{
					$units_consumed = 0;
				}
				else
				{
					$units_consumed = $current_reading - $prev_reading;
				}
			}
			else
			{
				$prev_reading = $initial_reading;
				$current_reading = 0;
				$units_consumed = 0;
			}
			

			$todays_date = date('Y-m-d');
			$todays_month = date('m');
			$todays_year = date('Y');

			if($current_reading == 0)
			{
				$color = 'orange';
			}
			else
			{
				$color = 'green';
			}

			// this is the end of the added section


			// allocated units
			$total_units = 0;//$this->leases_model->get_total_units($rental_unit_id);
			$occupied_units = 0;//$this->leases_model->get_occipied_units($rental_unit_id,$year,$month);
			$unoccipied_units = $total_units - $occupied_units;
			$routes .= '
						<li class="list-benefit" onclick="open_pop_up('.$rental_unit_id.','.$lease_id.','.$property_invoice_id.')">
						      <a class="item-link item-content" >
						       	<div class="item-inner">
						          <div class="item-title-row">
						            <div class="item-title"><span><i class="fa fa-home"></i> </span> '.strtoupper($rental_unit_name).'   </div>
						            <div class="item-after">
						            	<span class="badge color-'.$color.'">Units: '.$units_consumed.'</span>
						            </div>
						          </div>
						          <div class="item-subtitle"> <i class="fa fa-user"></i>  '.strtoupper($tenant_name).'  </div>
						          <div class="item-text">
						          	 PR : <span> '.$prev_reading.' units</span>
						          	 CR: <span> '.$current_reading.' units</span>
						          </div>
						        </div>
						      </a>
					    </li>
					   ';
		}
	}
}
else
{
	$routes .= '
					<li class="list-benefit" >
					      <a class="item-link item-content" >
					       	<div class="item-inner">
					          <div class="item-title-row">
					            <div class="item-title"><span>Sorry billing has not been opened </span> </div>
					            
					          </div>
					          <div class="item-subtitle"> </div>
					          <div class="item-text">
					          </div>
					        </div>
					      </a>
					</li>
					   ';
}
echo $routes;
?>
