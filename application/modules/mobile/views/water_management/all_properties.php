<?php
$routes = '';
if($all_properties->num_rows() > 0)
{
	foreach ($all_properties->result() as $key) {
		# code...
		$property_id = $key->property_id;
		$property_name = $key->property_name;
		$year = date('Y');
		$month = date('m');

		// allocated units
		$total_units = 0;//$this->leases_model->get_total_units($property_id);
		$occupied_units = 0;//$this->leases_model->get_occipied_units($property_id,$year,$month);
		$unoccipied_units = $total_units - $occupied_units;
		$routes .= '
					<li class="list-benefit" onclick="get_property_units('.$property_id.')">
				      <a class="item-link item-content" >
				       	<div class="item-inner">
				          <div class="item-title-row">
				            <div class="item-title"><span><i class="fa fa-home"></i> </span> '.strtoupper($property_name).' </div>
				            <div class="item-after"></div>
				          </div>
				          <div class="item-text">
				          	Occupied Units : <span> '.$occupied_units.' units</span>
				          	 Vacant Units: <span> '.$unoccipied_units.' units</span>
				          </div>
				        </div>
				      </a>
				    </li>
				   ';
	}
}
echo $routes;
?>
