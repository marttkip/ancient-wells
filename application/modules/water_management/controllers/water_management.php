<?php
class Water_management extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('water_management_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/dashboard_model');
		$this->load->model('administration/reports_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('real_estate_administration/tenants_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('real_estate_administration/leases_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
	
	public function index()
	{
		$where = 'property_invoice.property_id = property.property_id';
		$table = 'property,property_invoice';

		$accounts_search = $this->session->userdata('all_water_property');
		
		if(!empty($accounts_search))
		{
			$where .= $accounts_search;	
			
		}

		$segment = 2;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'property-readings';
		$config['total_rows'] = $this->water_management_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();

		$query = $this->water_management_model->get_all_water_records($table, $where, $config["per_page"], $page);

		$data['title'] = $this->site_model->display_page_title();
		$v_data['properties'] = $this->property_model->get_active_property();
		$v_data['invoice_types'] = $this->water_management_model->get_invoice_types();
		// $v_data['property_invoices'] = $this->water_management_model->get_property_invoices();
		$v_data['title'] = $data['title'];
		$v_data['page'] = $page;
		$v_data['property_invoices'] = $query;
		$data['content'] = $this->load->view('water_reporting', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);	
	}
	
	public function import_water_readings_template()
	{
		$this->water_management_model->import_template();
	}
	function do_water_readings_import()
	{
		$this->form_validation->set_rules('property_invoice_id', 'Invoice', 'required|xss_clean');
		//$this->form_validation->set_rules('invoice_type_id', 'Type', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if(isset($_FILES['import_csv']))
			{
				// var_dump($message_category_id); die();
				if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
				{
					//import products from excel 
	
					$response = $this->water_management_model->import_csv_charges($this->csv_path);
					
					
					if($response == FALSE)
					{
	
					}
					
					else
					{
						if($response['check'])
						{
							$v_data['import_response'] = $response['response'];
						}
						
						else
						{
							$v_data['import_response_error'] = $response['response'];
						}
					}
				}
				
				else
				{
					$v_data['import_response_error'] = 'Please select a file to import.';
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('cash-office/property-readings');
	}
	
	public function print_water_readings($document_number)
	{
		$data['document_number'] = $document_number;
		$data['document_details'] = $this->water_management_model->get_document_details($document_number);
		$data['rental_units'] = $this->water_management_model->get_rental_units();
		$data['invoice_types'] = $this->water_management_model->get_invoice_types();
		$data['contacts'] = $this->site_model->get_contacts();
		$this->load->view('reading_reports', $data);
	}
	
	// public function send_invoices($record_id)
 //    {
 //    	if($this->water_management_model->send_invoices($record_id))
 //    	{
	// 		$this->session->set_userdata('success_message', 'Reminder was send successfully successfully');	
	// 	}
	// 	else
	// 	{
	// 		$this->session->set_userdata('error_message', 'Sorry something has gone wrong. PLease try again to send the message');
	// 	}
	// 	redirect('cash-office/property-readings');
	// }
	
	public function add_property_invoice() 
	{
		//form validation rules
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Type', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_amount', 'Amount', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_units', 'Units', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_date', 'Date', 'required|xss_clean');
		$this->form_validation->set_rules('property_invoice_number', 'Invoice Number', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$response = $this->water_management_model->add_property_invoice();
			if($response)
			{
				$this->session->set_userdata("success_message", "Property invoice added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message", "Could not add property invoice. Please try again");
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('cash-office/property-readings');
	}
	
	public function edit_property_invoice($property_invoice_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Type', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_amount', 'Amount', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_units', 'Units', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_date', 'Date', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$response = $this->water_management_model->edit_property_invoice($property_invoice_id);
			if($response)
			{
				$this->session->set_userdata("success_message", "Property invoice updated successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message", "Could not update property invoice. Please try again");
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('cash-office/property-readings');
	}
	
	public function delete_property_invoice($property_invoice_id) 
	{
		if($this->water_management_model->delete_roperty_invoice($property_invoice_id))
		{
			$this->session->set_userdata("success_message", "Property invoice deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message", "Could not delete property invoice. Please try again");
		}
		redirect('cash-office/property-readings');
	}

	public function generate_report()
	{


		$this->form_validation->set_rules('property_id', 'Property', 'required|xss_clean');
		$this->form_validation->set_rules('year', 'Year', 'required|xss_clean');
		$this->form_validation->set_rules('month', 'Month', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$response = $this->water_management_model->generate_property_reading_report($property_invoice_id);
			if($response)
			{
				$this->session->set_userdata("success_message", "Property invoice updated successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message", "Could not update property invoice. Please try again");
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('property-readings');
	}


	public function water_tenants($property_invoice_id, $property_idd) 
	{
			
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id AND leases.lease_deleted = 0 AND property.property_id = '.$property_idd;
		$table = 'tenants,tenant_unit,rental_unit,leases,property';		


		$accounts_search = $this->session->userdata('all_water_accounts_search');
		
		if(!empty($accounts_search))
		{
			$where .= $accounts_search;	
			
		}
		$segment = 4;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'view-reading-details/'.$property_invoice_id.'/'.$property_idd;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->water_management_model->get_all_water_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_name,rental_unit.rental_unit_id,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		$property_names = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;
			if($property_idd == $property_id)
			{
				$property_names = $property_name;
				$property_list .="<option value='".$property_id."' selected='selected'>".$property_name." Location: ".$property_location."</option>";
			}
		endforeach;

		$v_data['property_list'] = $property_list;
		$v_data['property_invoice_id'] = $property_invoice_id;
		$data['title'] = $property_names.' Units';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['property_id'] = $property_idd;
		$data['content'] = $this->load->view('water_tenants', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function update_invoice($tenant_unit_id,$lease_id,$rental_unit_id)
	{
		$this->form_validation->set_rules('current_reading'.$lease_id, 'current reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('previous_reading'.$lease_id, 'previous reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_charges'.$lease_id, 'Water Charge', 'trim|required|xss_clean');
		$this->form_validation->set_rules('meter_number'.$lease_id, 'Meter', 'trim|required|xss_clean');

		$this->form_validation->set_rules('billing_amount'.$lease_id, 'Meter', 'trim|required|xss_clean');

		// var_dump($_POST);die();
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$current_reading = $this->input->post('current_reading'.$lease_id);
			$previous_reading = $this->input->post('previous_reading'.$lease_id);
			$water_charges = $this->input->post('water_charges'.$lease_id);

			if($current_reading < $previous_reading)
			{
				$this->session->set_userdata("error_message", "Sorry you the current reading is not accurate");
			}

			else
			{
				if($this->water_management_model->update_water_invoice($lease_id,$rental_unit_id))
				{

					$this->session->set_userdata("success_message", "You have successfully billed for water");
				}
				
				else
				{
					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");

				}
			}
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function water_tenant_payments($tenant_unit_id,$lease_id)
	{
		$v_data = array('tenant_unit_id'=>$tenant_unit_id,'lease_id'=>$lease_id);
		$v_data['title'] = 'Tenant payments';
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// get lease payments for this year
		$v_data['lease_payments'] = $this->water_management_model->get_lease_payments($lease_id);
		$v_data['lease_pardons'] = $this->water_management_model->get_lease_pardons($lease_id);

		$data['content'] = $this->load->view('water_tenant_payments', $v_data, true);
		
		$data['title'] = 'Tenant payments';
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function close_property_invoice($property_invoice_id,$property_id,$property_invoice_status)
	{
		$data['property_invoice_status'] = $property_invoice_status;
		$this->db->where('property_invoice_id',$property_invoice_id);
		$this->db->update('property_invoice',$data);
		redirect('property-readings');
	}

	public function print_water_bills($property_invoice_id,$property_id)
	{

		$where = 'tenants.tenant_id > 0 
					AND tenants.tenant_id = tenant_unit.tenant_id 
					AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  
					AND tenant_unit.tenant_unit_id = leases.tenant_unit_id 
					AND leases.lease_status = 1 
					AND property.property_id = rental_unit.property_id 
					AND property.property_id = '.$property_id.' 
					AND property_invoice.property_id = property.property_id 
					AND leases.lease_id = water_management.lease_id
					AND property_invoice.property_invoice_id = water_management.property_invoice_id 
					AND property_invoice.property_invoice_id = '.$property_invoice_id;
		$table = 'tenants,tenant_unit,rental_unit,leases,property,property_invoice,water_management';	


		$this->db->where($where);
		$this->db->order_by('rental_unit.rental_unit_name','ASC');
		$query = $this->db->get($table);
		// var_dump($query);die();
		$data['rental_units'] = $this->water_management_model->get_rental_units();
		$data['contacts'] = $this->site_model->get_contacts();
		$data['query'] = $query;
		$this->load->view('reading_reports', $data);

	}

	public function search_water_tenants()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$rental_unit_name = $this->input->post('rental_unit_name');
		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}

			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
			
		}
		
		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name.' ';
			$tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';

			
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
			
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();
		
		// $this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('all_water_accounts_search', $search);
		$this->session->set_userdata('accounts_search_title', $search_title);

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function close_accounts_search($property_invoice_id,$property_id)
	{
		$this->session->unset_userdata('all_water_accounts_search');
		$this->session->unset_userdata('accounts_search_title');	
		
		redirect('view-reading-details/'.$property_invoice_id.'/'.$property_id);
	}

	public function search_water_property()
	{
		$property_id = $this->input->post('property_id');		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}

			$property_id = ' AND property_invoice.property_id = '.$property_id.' ';
			
		}
		
	


		$property_search = $property_id;
		// var_dump($search); die();
		
		// $this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('all_water_property', $property_search);
		$this->session->set_userdata('accounts_search_title', $search_title);

		redirect('property-readings');
	}
	public function close_water_search()
	{
		$this->session->unset_userdata('all_water_property');
		$this->session->unset_userdata('accounts_search_title');

		redirect('property-readings');
	}

	public function export_readings($property_invoice_id,$property_id)
	{
		$this->water_management_model->export_readings($property_invoice_id,$property_id);
	}

	public function import_water_reading_view($property_invoice_id,$property_id)
	{

		$v_data['title'] = 'Import Water Readings';
		$v_data['property_invoice_id'] = $property_invoice_id;
		$v_data['property_id'] = $property_id;
		$v_data['property_invoices'] = $this->water_management_model->get_property_invoices();
		$data['content'] = $this->load->view('import_readings', $v_data, true);
		
		$data['title'] = 'Import water readings';
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function import_readings_template()
	{
		$this->water_management_model->import_water_template();
	}
	function do_readings_import($property_invoice_id,$property_id)
	{
		$this->form_validation->set_rules('property_invoice_id', 'Invoice', 'required|xss_clean');
		//$this->form_validation->set_rules('invoice_type_id', 'Type', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if(isset($_FILES['import_csv']))
			{
				// var_dump($message_category_id); die();
				if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
				{
					//import products from excel 
	
					$response = $this->water_management_model->import_csv_charges_water($this->csv_path);
					
					
					if($response == FALSE)
					{
	
					}
					
					else
					{
						if($response['check'])
						{
							$v_data['import_response'] = $response['response'];
						}
						
						else
						{
							$v_data['import_response_error'] = $response['response'];
						}
					}
				}
				
				else
				{
					$v_data['import_response_error'] = 'Please select a file to import.';
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		redirect('view-reading-details/'.$property_invoice_id.'/'.$property_id);
	}

	public function water_tenants_list()
	{
		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';		


		$accounts_search = $this->session->userdata('all_water_accounts_search');
		
		if(!empty($accounts_search))
		{

			// var_dump($accounts_search);die();
			$where .= $accounts_search;	
			
		}
		$segment = 2;
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'water-tenants-accounts';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		
		// $this->accounts_model->update_invoices();
		

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('water_tenants_list', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	
	}

	public function search_water_accounts()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$rental_unit_name = $this->input->post('rental_unit_name');
		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
			
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
			
			
		}
		
		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name.' ';
			$tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';

			
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
			
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();
		
		$this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('all_water_accounts_search', $search);
		$this->session->set_userdata('accounts_search_title', $search_title);
		
		redirect('water-tenants-accounts');
	}

	public function close_water_accounts_search()
	{
		$this->session->unset_userdata('all_water_accounts_search');
		$this->session->unset_userdata('search_title');
		
		
		redirect('water-tenants-accounts');
	}

	public function make_water_payments($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_amount', 'Water amount', 'trim|xss_clean');
		$this->form_validation->set_rules('service_charge_amount', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('penalty_fee', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'trim|xss_clean');	


		$payment_method = $this->input->post('payment_method');
		$water_amount = $this->input->post('water_amount');
		$rent_amount = $this->input->post('rent_amount');
		$service_charge_amount = $this->input->post('service_charge_amount');
		$penalty_fee = $this->input->post('penalty_fee');
		$amount_paid = $this->input->post('amount_paid');
		$fixed_charge = $this->input->post('fixed_charge');
		$deposit_charge = $this->input->post('deposit_charge');

		$insurance = $this->input->post('insurance');
		$sinking_funds = $this->input->post('sinking_funds');
		$bought_water = $this->input->post('bought_water');
		$painting_charge = $this->input->post('painting_charge');
		$legal_fees = $this->input->post('legal_fees');


		if(empty($water_amount))
		{
			$water_amount = 0;
		}
		if(empty($service_charge_amount))
		{
			$service_charge_amount = 0;
		}
		if(empty($rent_amount))
		{
			$rent_amount = 0;
		}
		if(empty($penalty_fee))
		{
			$penalty_fee = 0;
		}
		if(empty($fixed_charge))
		{
			$fixed_charge = 0;
		}

		if(empty($deposit_charge))
		{
			$deposit_charge = 0;
		}
		if(empty($insurance))
		{
			$insurance = 0;
		}
		if(empty($sinking_funds))
		{
			$sinking_funds = 0;
		}
		if(empty($bought_water))
		{
			$bought_water = 0;
		}
		if(empty($painting_charge))
		{
			$painting_charge = 0;
		}
		if(empty($legal_fees))
		{
			$legal_fees = 0;
		}
		//  add all this items 
		$total = $service_charge_amount + $rent_amount + $water_amount + $penalty_fee + $fixed_charge + $deposit_charge + $insurance + $sinking_funds + $bought_water + $painting_charge+ $legal_fees;

		// var_dump($total); die();
		// Normal
		
		if(!empty($payment_method))
		{
			if($payment_method == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
			
			}
			else if($payment_method == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[water_payments.transaction_code]|trim|required|xss_clean');
			}
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			$transaction_code = $this->input->post('mpesa_code');
			if($payment_method == 5)
			{
				$check_transaction_code = $this->water_management_model->check_transaction_code($transaction_code);
			}
			else
			{
				$check_transaction_code = TRUE;
			}
			
			if($amount_paid == $total AND $check_transaction_code)
			{
				$this->water_management_model->receipt_water_payment($lease_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
			}
			else
			{
				if($check_transaction_code == FALSE)
				{
					$this->session->set_userdata("error_message", 'Sorry seems like the transaction code is not unique');
				}
				else
				{
					$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				}
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function print_water_receipt($payment_id, $tenant_unit_id, $lease_id)
	{
		$data = array('payment_id' => $payment_id, 'tenant_unit_id' => $tenant_unit_id, 'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->water_management_model->get_lease_payments($lease_id);
		
		$data['lease_payments'] = $this->water_management_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];

		$this->load->view('receipts/water_tenant_receipt', $data);
	}

	public function send_tenants_receipt($payment_id, $tenant_unit_id, $lease_id)
	{
		$data = array('payment_id' => $payment_id, 'tenant_unit_id' => $tenant_unit_id, 'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $lease_payments = $this->accounts_model->get_lease_payments($lease_id);
		
		$data['payment_details'] = $this->accounts_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];


		$html = $this->load->view('receipts/water_tenant_receipt', $data, TRUE);

		$this->load->library('mpdf');
		
		$document_number = date("ymdhis");
		$receipt_month_date = date('Y-m-d');
		$receipt_month = date('M',strtotime($receipt_month_date));
		$receipt_year = date('Y',strtotime($receipt_month_date));
		$title = $document_number.'-REC.pdf';
		$invoice = $title;
		// echo $invoice;die();
		
		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}

		if($lease_payments->num_rows() > 0)
		{
			$y = 0;
			foreach ($lease_payments->result() as $key) 
			{
				$payment_id = $key->payment_id;
				$receipt_number = $key->receipt_number;
				$amount_paid = $key->amount_paid;
				$paid_by = $key->paid_by;
				$payment_date = $date_of_payment = $key->payment_date;
				$payment_created = $key->payment_created;
				$payment_created_by = $key->payment_created_by;
				$transaction_code = $key->transaction_code;
				$invoice_month_number = $key->month;
				
				$payment_date = date('jS M Y',strtotime($payment_date));
				$payment_created = date('jS M Y',strtotime($payment_created));
				$y++;
			}
		}
		// var_dump($tenant_phone_number);die();
		// $tenant_email = 'marttkip@gmail.com';
		if(!empty($tenant_email))
		{
			$message['subject'] =  $document_number.' Receipt';
			$message['text'] = ' <p>Dear '.$tenant_name.',<br>
									Please find attached receipt
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email']; 
			
			$button = '';
			$sender['email'] = $contacts['email']; 
			$sender['name'] = $contacts['company_name'];
			$receiver['email'] = $tenant_email;
			$receiver['name'] = $tenant_name;
			$payslip = $title;
			// var_dump($tenant_email);die();
			
			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
			
			// save the invoice sent out on the database

			$insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'type_of_account'=>1,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'email_type'=>2);
			$this->db->insert('email',$insert_array);
		}
		// $tenant_phone_number = 704808007;
		// var_dump($tenant_phone_number);die();
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $tenant_array = explode(' ', $tenant_name);
			// $tenant_name = $tenant_array[0];

			
			$message = 'Dear '.$tenant_name.', Your payment of  KES. '.$amount_paid.' for '.$rental_unit_name.' has been successfully received '.$message_prefix;
			$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

			// save the message sent out 
			$insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
			$this->db->insert('sms',$insert_array);
			// save the message sent out
			
		}

		redirect('cash-office/payments/'.$tenant_unit_id.'/'.$lease_id.'');
	}

	public function create_water_pardon($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('pardon_amount', 'Amount', 'numeric|trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_date', 'Date ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_reason', 'Reason', 'required|trim|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			if($this->water_management_model->receipt_pardon($lease_id))
			{
				$this->session->set_userdata("success_message", 'Pardon created successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message", 'Sorry. Please try again');
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}

	public function cancel_pardon($pardon_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d H:i:s"),
			"cancel" => 1,
			"pardon_delete" => 1
		);
		
		$this->db->where('pardon_id', $pardon_id);
		if($this->db->update('water_pardon_payments', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}

	public function cancel_payment($payment_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->water_management_model->cancel_payment($payment_id))
			{
				$this->session->set_userdata("success_message", "Payment action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		redirect($redirect_url);
	}


	public function send_water_arrears($lease_id,$invoice_month,$invoice_year,$property_invoice_id,$property_id,$page)
	{

		$month = date('m');
		$year = date('Y');

		$this->send_water_invoices($lease_id,$invoice_month,$invoice_year,$property_invoice_id);

		$this->session->set_userdata('success_message', 'Message has been send successfully');	


		redirect('view-reading-details/'.$property_invoice_id.'/'.$property_id.'/'.$page);
	}


	public function send_water_invoices($lease_id,$invoice_month,$invoice_year,$property_invoice_id)
	{
		// var_dump($lease_id);die();
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_invoice'] = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year);

		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		
		$all_leases = $this->leases_model->get_lease_detail($lease_id);


		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;
			$message_prefix = $leases_row->message_prefix;
			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			
			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
			
		}
		
		$contacts = $data['contacts'];

		$data['rental_unit_id'] = $lease_id;

		// $html = $this->load->view('cash_office/water_charge_invoice', $data, TRUE);
		// $html = $this->load->view('cash_office/invoice_three', $data, TRUE);

		// $invoice_template_name = $this->accounts_model->get_invoice_template(1,$property_id);
		
		// $html = $this->load->view('cash_office/'.$invoice_template_name.'', $data, TRUE);
	


		$property_invoice_rs = $this->water_management_model->get_property_invoice_detail($property_invoice_id);


		if($property_invoice_rs->num_rows() > 0)
		{
			foreach ($property_invoice_rs->result() as $key => $value) {
				# code...
				$invoice_year = $value->year;
				$invoice_month = $value->month;
				$property_invoice_date = $value->property_invoice_date;

				
			}
		}

		// var_dump($property_invoice_id);die();

		
		$date_today = date('Y-m-d');
		$date_exploded = explode('-', $date_today);
		$_year = $date_exploded[0];
		$_month = $date_exploded[1];
		


		$todays_date = date('Y-m-d');
		$todays_month = date('m');
		$todays_year = date('Y');

		$total_water_arrears = $this->water_management_model->get_initial_opening_balance($lease_id,$rental_unit_id);


		// this is the added section
		$bal_b_f = $this->water_management_model->get_water_arrears($lease_id,$invoice_year,$invoice_month,$property_invoice_id,$property_invoice_date);
		if(empty($bal_b_f))
		{
			$bal_b_f = 0;
		}

		$bal_b_f +=$total_water_arrears;

		
		
		$current_response = $this->water_management_model->get_current_active_meter_number($lease_id);
		$current_meter_number = $current_response['property_billing_id'];
		$initial_reading = $current_response['initial_reading'];
		$billing_amount = $current_response['billing_amount'];

		// $current_meter_number = $this->water_management_model->get_current_active_meter_number($lease_id);


		$previous_meter_number = $this->water_management_model->get_previous_active_meter_number($lease_id);

		if($current_meter_number == $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
		{
			$used_meter = $previous_meter_number;
		}
		else if($current_meter_number != $previous_meter_number AND ($previous_meter_number != 0 OR $current_meter_number != 0))
		{

			if($property_invoice_status == 1 OR $property_invoice_status == 0)
			{
				$used_meter = $current_reading;
			}
			else
			{

				$used_meter = $previous_meter_number;
			}
			
		}
	
		
		$water_readings_current = $this->water_management_model->get_current_reading($lease_id,$property_invoice_id,$used_meter);
		$current_reading = 0;

		

		if($water_readings_current['status'])
		{
			$current_reading = $water_readings_current['current_reading'];
			$prev_reading = $water_readings_current['prev_reading'];

			$units_consumed = $current_reading - $prev_reading;

			// var_dump($prev_reading);die();
			if($current_reading == 0)
			{
				$units_consumed = 0;
			}
			else
			{
				$units_consumed = $current_reading - $prev_reading;
			}
		}
		else
		{
			$prev_reading = $initial_reading;
			$current_reading = 0;
			$units_consumed = 0;
		}

		$bal_b_f = $this->water_management_model->get_water_arrears($lease_id,$invoice_year,$invoice_month,$property_invoice_id,$property_invoice_date);
		if(empty($bal_b_f))
		{
			$bal_b_f = 0;
		}


		$current_payment = $this->water_management_model->report_current_payment($lease_id,$invoice_year,$invoice_month,$property_invoice_id,$property_invoice_date);
		if(empty($current_payment))
		{
			$current_payment = 0;
		}
		$current_invoice = $units_consumed * $billing_amount;

		// var_dump($current_invoice);die();

		$balance = $bal_b_f + $current_invoice - $current_payment;

		$tenant_name = ucfirst($tenant_name);
		$tenant_explode = explode(' ', $tenant_name);

		$tenant_name = $tenant_explode[0];
		// $tenant_phone_number = '254720465220';
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$phone_array = explode('/', $tenant_phone_number);
			$total_rows = count($phone_array);

			for($r = 0; $r < $total_rows; $r++)
			{
				$tenant_phone_number = $phone_array[$r];


				$message = "Dear ".$tenant_name.", Your  current water balance leading upto the month of ".$invoice_month." ".$invoice_year."  is KES. ".number_format($bal_b_f + $current_invoice - $current_payment).".\nBal B/F KES. ".number_format($bal_b_f,0)."\nInitial reading: ".$prev_reading."\nCurrent Reading: ".$current_reading."\nUnits Consumed: ".$units_consumed." @".$billing_amount." per unit.\nReach us on 0702032278. Water Paybill: 846783 A/C: Hse No. Water supply is for domestic use not for consumption.";


				// if($this->accounts_model->check_if_sms_sent($rental_unit_id,$tenant_phone_number,$todays_year,$todays_month,1,4))
				// {
				// }
				// else
				// {
				// var_dump($tenant_phone_number);die();
					$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);
					$insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'rental_unit_id'=>$rental_unit_id, 'sms_invoice_year'=>$_year, 'sms_invoice_month'=>$_month, 'sms_type'=> 4, 'sms_sent'=>1);
					$this->db->insert('sms',$insert_array);
				// }

				// save the message sent out 
				
			}			
			// save the message sent out		
		}
	}

	public function print_water_report($property_invoice_id,$month,$year,$property_id)
{

		//get property data
		// $property_array = explode('_', $property);
		// $property_id = $property_array[0];
		// $property_name = $property_array[1];

		$this->db->where('property_id',$property_id);
		$query_result = $this->db->get('property');
		$return_date = '01';
		if($query_result->num_rows() > 0)
		{
			foreach ($query_result->result() as $key => $value) {
				# code...
				$return_date = $value->return_date;
				$property_name = $value->property_name;
			}
		}
		if(empty($return_date))
		{
			$return_date = '01';
			$property_name = '';
		}


		$this->db->where('property_invoice_id',$property_invoice_id);
		$query_result_property = $this->db->get('property_invoice');
		$return_date = '01';
		if($query_result_property->num_rows() > 0)
		{
			foreach ($query_result_property->result() as $key => $value_two) {
				# code...
				$property_invoice_date = $value_two->property_invoice_date;
			}
		}
		else
		{
			$property_invoice_date = date('Y-m-d');
		}

		
		//get month data
		// $month_array = explode('_', $month);
		// $month_id = $month_array[0];
		// $month_name = $month_array[1];



		$this->session->set_userdata('month',$month);
		$this->session->set_userdata('year',$year);
		$this->session->set_userdata('return_date',$return_date);
		$this->session->set_userdata('property_id',$property_id);
		
		$title = $property_name.' Water Report for '.$month.' '.$year;
		// var_dump($where);die();

		$where = 'rental_unit.property_id = property.property_id AND rental_unit.property_id = '.$property_id;
		$segment = 3;
		

		$dashboard_where = 'property_id = '.$property_id;
		$dashboard_table = 'property';
		$dashboard_select = '*';
		$query_list  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select);
		$property_type_id = 0;
		if($query_list->num_rows()  == 1)
		{
			foreach ($query_list->result() as $key => $value) {
				# code...
				$property_type_id = $value->property_type_id;
			}
		}


		if($property_type_id == 2)
		{
			// put the query for grouping per block

			$dashboard_where = 'property_id = '.$property_id;
			$dashboard_table = 'block';
			$dashboard_select = '*';
			$query  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select,'block.block_id',null,'block_name','ASC');


		}
		else
		{
			$dashboard_where = 'rental_unit.property_id = property.property_id AND rental_unit.property_id = '.$property_id;
			$dashboard_table = 'rental_unit,property';
			$dashboard_select = '*';
			$query  = $this->dashboard_model->get_content($dashboard_table, $dashboard_where,$dashboard_select,NULL,NULL,'rental_unit.rental_unit_name','ASC');
		}


		$data['property_type_id'] =  $property_type_id;
		$data['property_id'] =  $property_id;
		$data['property_invoice_id'] =  $property_invoice_id;
		$data['property_invoice_date'] =  $property_invoice_date;
		// var_dump($property_type_id);die();
		// $this->accounts_model->update_invoices();
		$data['contacts'] = $this->site_model->get_contacts();
		$data['title'] = $v_data['title'] = $title;
		$v_data['query'] = $data['query'] =  $query;

		// $where = 'tenants.tenant_id > 0 
		// 			AND tenants.tenant_id = tenant_unit.tenant_id 
		// 			AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  
		// 			AND tenant_unit.tenant_unit_id = leases.tenant_unit_id 
		// 			AND leases.lease_status = 1 
		// 			AND property.property_id = rental_unit.property_id 
		// 			AND property.property_id = '.$property_id.' 
		// 			AND property_invoice.property_id = property.property_id 
		// 			AND leases.lease_id = water_management.lease_id
		// 			AND property_invoice.property_invoice_id = water_management.property_invoice_id 
		// 			AND property_invoice.property_invoice_id = '.$property_invoice_id;
		// $table = 'tenants,tenant_unit,rental_unit,leases,property,property_invoice,water_management';	


		// $this->db->where($where);
		// $this->db->order_by('rental_unit.rental_unit_name','ASC');
		// $query = $this->db->get($table);
		// var_dump($query);die();
		$data['rental_units'] = $this->water_management_model->get_rental_units();
		$data['contacts'] = $this->site_model->get_contacts();
		$data['query'] = $query;
		$this->load->view('water_return', $data);

	}


	public function update_used_meters()
	{
		$this->db->where('record_id > 0 AND meter_number = 0');
		$query = $this->db->get('water_management');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$record_id = $value->record_id;
				$lease_id = $value->lease_id;


				$meter_number = $this->water_management_model->get_current_active_meter_number($lease_id);
				if(is_array($meter_number))
				{
					$array['meter_number'] = $meter_number['property_billing_id'];

					$this->db->where('record_id',$record_id);
					$this->db->update('water_management',$array);

				}
				
			}
		}
	}


}


?>