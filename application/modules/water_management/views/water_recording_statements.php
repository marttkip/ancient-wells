<?php
	$result = '';
	$action_point = '';
	//if users exist display them

	// var_dump($query->num_rows()); die();
	if ($query->num_rows() > 0)
	{
		$count = $page;
		
		$result .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Document Number</th>
					<th>Property Name</th>
					<th colspan="1">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($query->result() as $row)
		{
			$record_id = $row->record_id;
			$property_id = $row->property_id;
			$property_name = $row->property_name;
			$document_number = $row->document_number;	
			
			$count++;
			$result .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$document_number.'</td>
					<td>'.$property_name.'</td>
					<td><a href="'.site_url().'send-water-invoices/'.$record_id.'"  class="btn btn-sm btn-warning" ><i class="fa fa-inbox"></i> Send reminders</a></td>
					<td><a href="'.site_url().'print-water-readings/'.$document_number.'" target="_blank" class="btn btn-sm btn-danger" ><i class="fa fa-print"></i> Print Documents</a></td>
	
				</tr> 
			';
	
			// $action_point = '<div class="pull-right"><a class="btn btn-success btn-sm" href="'.site_url().'messaging/send-messages" style="margin-top: -5px;" onclick="return confirm(\'Do you want to send the messages ?\');" title="Send Message">Send Unsent Messages</a></div>';
		}
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result .= "There are no messages";
	}
	
	$result2 = '';
	//if users exist display them
	if ($property_invoices->num_rows() > 0)
	{
		$count = 0;
		
		$result2 .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Invoice Date</th>
					<th>Property</th>
					<th>Type</th>
					<th>Invoice Number</th>
					<th>Invoice Amount</th>
					<th>Invoice Units</th>
					<th>Created</th>
					<th colspan="1">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($property_invoices->result() as $row)
		{
			$property_invoice_id = $row->property_invoice_id;
			$invoice_type_name = $row->invoice_type_name;
			$property_name = $row->property_name;
			$property_invoice_number = $row->property_invoice_number;
			$property_invoice_date = date('jS M Y',strtotime($row->property_invoice_date));
			$property_invoice_amount = $row->property_invoice_amount;
			$property_invoice_units = $row->property_invoice_units;
			$created = date('jS M Y H:i a',strtotime($row->created));
			
			$count++;
			$result2 .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$property_invoice_date.'</td>
					<td>'.$property_name.'</td>
					<td>'.$invoice_type_name.'</td>
					<td>'.$property_invoice_number.'</td>
					<td>'.number_format($property_invoice_amount, 2).'</td>
					<td>'.$property_invoice_units.'</td>
					<td>'.$created.'</td>
					<td><a href="'.site_url().'delete-property-invoices/'.$property_invoice_id.'"  class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this invoice\')"><i class="fa fa-trash"></i> Delete</a></td>
	
				</tr> 
			';
		}
		
		$result2 .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result2 .= "There are no invoices";
	}
?>

<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title">Readings Reports</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
            	<?php
					$success = $this->session->userdata('success_message');
				
					if(!empty($success))
					{
						echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
						$this->session->unset_userdata('success_message');
					}
				
					$error = $this->session->userdata('error_message');
				
					if(!empty($error))
					{
						echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
						$this->session->unset_userdata('error_message');
					}
				?>
				
				<?php
					if(isset($import_response))
					{
						if(!empty($import_response))
						{
							echo $import_response;
						}
					}
					
					if(isset($import_response_error))
					{
						if(!empty($import_response_error))
						{
							echo '<div class="center-align alert alert-danger">'.$import_response_error.'</div>';
						}
					}

					echo $result;
				?>
                
                                                
            </div>
        </div>
    </div>
</section>