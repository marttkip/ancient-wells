<?php
$property_id = set_value('property_id');
$creditor_account_date = set_value('creditor_account_date');
$year = $this->reports_model->generate_financial_year();


if(!empty($this->session->userdata('financial_year_search')))
{
	$current_year = $this->session->userdata('financial_year_search');
	$financial_year =  $current_year;

	$next_financial_year = $current_year + 1;
    $previous_financial_year = $current_year - 1;
}
else
{
	$financial_year =  date('Y');

	$next_financial_year = date('Y') + 1;
    $previous_financial_year = date('Y') - 1;
}
// $financial_year = 2016;
// $next_financial_year = 2017;

// get balance of the previous year 
$previous_creditor_amount = $this->reports_model->get_creditor_previous_service_amounts($financial_year);
$previous_tenants_inflow_amount = $this->reports_model->get_tenants_previous_inflow_amounts($financial_year);
$previous_owners_inflow_amount = $this->reports_model->get_owners_previous_inflow_amounts($financial_year);


$balance  =  $previous_owners_inflow_amount + $previous_tenants_inflow_amount - $previous_creditor_amount;

$months = '';
$count = $invoice_count= 0;
$total_months =count($year);
$totals_array = array();
for($r=0;$r<$total_months;$r++)
{
	$month_id = $year[$r];
	$totals_array[$month_id]  =0;

	if($month_id === 1 OR $month_id === 2 OR $month_id === 3)
	{
		$financial_year = $next_financial_year;
	}

	$month_name  = $this->reports_model->get_month_name($month_id);
	$months .='<th>'.$financial_year.' '.$month_name.'</th>'; 
}

// bank balances 

$bank_balance = 0;

// end of bank balance

//display al creditors 
$result ='';
if($all_creditor_services->num_rows() > 0)
{
	//var_dump($all_creditor_services);die();
	foreach($all_creditor_services->result() as $creditors_services)
	{
		$creditor_service_id = $creditors_services->creditor_service_id;//echo $creditor_service_id;die();
		$creditor_service_name = $creditors_services->creditor_service_name;
		$count++;
		$result .='<tr>
						<td>'.$count.'</td>
						<td>'.$creditor_service_name.'</td>
					';
		$creditor_result = '';

		if(!empty($this->session->userdata('financial_year_search')))
		{
			$current_year = $this->session->userdata('financial_year_search');
			$financial_year_two =  $current_year;

			$next_financial_year_two = $current_year + 1;
		}
		else
		{
			$financial_year_two =  date('Y');

			$next_financial_year_two = date('Y') + 1;
		}

        // var_dump($next_financial_year_two); die();

		for($r=0;$r<$total_months;$r++)
		{
			$month_id = $original_month_id = $year[$r];
			if(strlen($month_id)!=2)
			{
				$month_id = '0'.$month_id;
			}
			$total_amount = $service_total = 0;
			if($original_month_id === 1 OR $original_month_id === 2 OR $original_month_id === 3)
			{
				$financial_year_two = $next_financial_year_two;
			}
			$total_amount = $this->reports_model->get_creditor_service_amounts($creditor_service_id,$month_id,$financial_year_two);

			
			$totals_array[$original_month_id] += $total_amount;
			$result .='
				<td>'.number_format($total_amount).'</td>
			';

		}
		
		$result .= '</tr>
					';
	}
	
	$result.='<tr class="info">
			<th colspan="2"><strong>Total Outflow</strong></th>';
	for($r=0;$r<$total_months;$r++)
	{
		
		$month_id = $year[$r];
		$month_amount = $totals_array[$month_id];
		$result .='
					<td>'.number_format($month_amount).'</td>'; 
	}
	$result.='</tr>';
	
}
//inflows 
$income_result ='';
$total_incomes = array();
for($r=0;$r<$total_months;$r++)
{
	$month_id = $original_month_id = $year[$r];
	$total_incomes[$month_id] = 0;
}
if($all_invoice_types->num_rows() > 0)
{
	foreach($all_invoice_types->result() as $invoice_types)
	{
		$invoice_type_id = $invoice_types->invoice_type_id;
		$invoice_type_name = $invoice_types->invoice_type_name;
		$invoice_count++;
		$income_result .='
		<tr>
			<td>'.$invoice_count.'</td>
			<td>'.$invoice_type_name.'</td>
		';
		$creditor_result = '';
		// var_dump($total_months); die();
		$month_item=0;

		if(!empty($this->session->userdata('financial_year_search')))
		{
			$current_year = $this->session->userdata('financial_year_search');
			$financial_year_two =  $current_year;

			$next_financial_year_two = $current_year + 1;
		}
		else
		{
			$financial_year_two =  date('Y');

			$next_financial_year_two = date('Y') + 1;
		}
		for($r=0;$r<$total_months;$r++)
		{
			$month_id = $month_item = $year[$r];
			if(strlen($month_id)!= 2)
			{
				$month_id = '0'.$month_id;
			}
			$total_amount = 0;


			// get total_amount made on the invoice type

			if($month_item === 1 OR $month_item === 2 OR $month_item === 3)
			{
				$financial_year_two = $next_financial_year_two;
			}

			$total_tenant_inflow_amt = $this->reports_model->get_amount_collected_invoice_type($invoice_type_id,$month_item,$financial_year_two);

			$total_owner_inflow_amt = $this->reports_model->get_amount_owners_collected_invoice_type($invoice_type_id,$month_item,$financial_year_two);
           // var_dump($invoice_type_id."sdhakjda");

			$total_inflow_amt = $total_tenant_inflow_amt + $total_owner_inflow_amt;

			$income_result .='
				<td>'.number_format($total_inflow_amt).'</td>
			';
			$total_incomes[$month_item] += $total_inflow_amt;
			

		}
		$income_result .= '</tr>';
	}
	$income_result .='<tr class="info">
					<th colspan="2">Total Inflows</th>';
	for($r=0;$r<$total_months;$r++)
	{
		$month_id = $year[$r];
		$month_payment_amount = $total_incomes[$month_id];
        // openning balance from the previous



		$income_result .='
					<th>'.number_format($month_payment_amount+$balance).'</th>'; 
		
	}
	$income_result.='</tr>';
	/*
	 $income_result .='   	
    </tbody>
</table>
';*/



	
}

// var_dump($total_incomes);die();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Income Statement</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	 <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        
		            <div class="col-xs-12 ">
		              <?php
		              if(!empty($this->session->userdata('search_title_data')))
                        {
                            echo $this->session->userdata('search_title_data');
                        }
                        else
                        {
                            echo 'Showing report for all properties Financial Year '.date('Y');
                        }
		              ?>
		               
		            </div>
		        </div>

		        <div class="row">
			       	<div class="col-xs-12 ">
							<tr>
			                    <td colspan="14">INFLOWS</td>
			                </tr>
				            <?php
				                 $income_result_changed ='
				                    <table class="table table-condensed table-striped table-hover" id="customers">
				                        <thead>
				                            <tr>
				                                <th>#</th>
				                                <th>Invoice Type</th>
				                                '.$months.'
				                            </tr>
				                        </thead>
				                        <tbody>
				                        ';
				        
				                    $income_result_changed .= '<tr>
				                                        <td colspan="2">Opening Balance</td>';


				                                        $changed_bal_amount = $balance;
				                                        $cummulative_amount_account_balance = 0;
				                                        $total_months = $total_months -1;

				                                        $r = 0;

				                                        foreach ($year as $key) {

				                                            $cummulative_amount_account = $total_incomes[$key];
				                                            $cummulative_amount_account_balance = $totals_array[$key];


				                                            $income_result_changed .='
				                                                    <td>'.number_format($changed_bal_amount ).'</td>'; 

				                                            $changed_bal_amount = $changed_bal_amount+$cummulative_amount_account - $cummulative_amount_account_balance;
				                                        }

				                    $income_result_changed.='</tr>';
				        
				                    echo $income_result_changed;
				        
				                ?>
				                <?php echo $income_result;?>
				                <tr>
				                    <td colspan="14">OUTFLOWS</td>
				                </tr>
				                <?php echo $result;?>
				        
				                <?php
				        
				                $income_result_changed_bottom = '<tr class="primary">
				                                        <td colspan="2">Bank Balance</td>';
				                                        $changed_amount = $balance;
				                                        $changed_amount_balance = 0;
				                                        $total_months = 12;

				                                         
				                                        for($r=0;$r<$total_months;$r++)
				                                        {
				                                            $month_id = $year[$r];
				                                                $cummulative_amount_account = $total_incomes[$month_id];
				                                                $cummulative_amount_account_balance = $totals_array[$month_id];
				                                                $changed_amount = $changed_amount+$cummulative_amount_account - $cummulative_amount_account_balance;

				                                                $income_result_changed_bottom .='
				                                                        <td><strong>'.number_format($changed_amount).'</strong></td>'; 
				                                            
				                                        }
				                    $income_result_changed_bottom .='</tr>';
				        
				                    echo $income_result_changed_bottom;
				                ?>
				        
				            </tbody>
				        </table>
			       	</div>
			   	</div>
		       </div>
		    </div>
		</body>
    </html>