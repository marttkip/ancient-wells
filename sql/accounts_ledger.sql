CREATE OR REPLACE VIEW v_account_ledger AS

SELECT
	`account`.`account_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
	'' AS `property_id`,
	'' AS `property_name`,
  '' AS `recepientId`,
  '' AS `transaction`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	account.account_id AS `accountId`,
	account.account_name AS `accountName`,
	CONCAT('Opening Balance as from',' ',`account`.`start_date`) AS `transactionName`,
	CONCAT('Opening Balance as from',' ',' ',`account`.`start_date`) AS `transactionDescription`,
	`account`.`account_opening_balance` AS `dr_amount`,
	'0' AS `cr_amount`,
	`account`.`start_date` AS `transactionDate`,
	`account`.`start_date` AS `createdAt`,
	`account`.`account_status` AS `status`,
	0 AS `deleted`,
	'Income' AS `transactionCategory`,
	'Account Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE account.parent_account = 1



UNION ALL

SELECT
  	`payments`.`payment_id` AS `transactionId`,
  	`payments`.`lease_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`payments`.`transaction_code` AS `referenceCode`,
  	`payments`.`transaction_code` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`payments`.`bank_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	'Money Received' AS `transactionName`,
  	 CONCAT('Amount Received from ',account.account_name,' Ref. ', `payments`.`transaction_code`) AS `transactionDescription`,
  	`payments`.`amount_paid` AS `dr_amount`,
     0 AS `cr_amount`,
  	`payments`.`transaction_date` AS `transactionDate`,
  	`payments`.`payment_created` AS `createdAt`,
  	`payments`.`cancel` AS `status`,
  	`payments`.cancel AS deleted,
  	'Income' AS `transactionCategory`,
  	'Income' AS `transactionClassification`,
  	'payments' AS `transactionTable`,
  	'accounts' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`payments`,payment_method,account
  				
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)
  WHERE payments.payment_method_id = payment_method.payment_method_id
   AND payments.bank_id = account.account_id 
   AND payments.cancel = 0 AND payments.confirmation_status = 0 AND DATE(`payments`.`transaction_date`) >= account.start_date


  UNION ALL

SELECT
  	`payments`.`payment_id` AS `transactionId`,
  	`payments`.`lease_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`payments`.`transaction_code` AS `referenceCode`,
  	`payments`.`transaction_code` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`payment_method`.`account_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	'Money Received' AS `transactionName`,
  	 CONCAT('Direct Deposit to ',account.account_name,' Ref. ', `payments`.`transaction_code`) AS `transactionDescription`,
  	`payments`.`amount_paid` AS `dr_amount`,
     0 AS `cr_amount`,
  	CONCAT(`payments`.`payment_date`,' 0000-00-00') AS `transactionDate`,
  	`payments`.`payment_created` AS `createdAt`,
  	`payments`.`cancel` AS `status`,
  	`payments`.cancel AS deleted,
  	'Income' AS `transactionCategory`,
  	'Income' AS `transactionClassification`,
  	'payments' AS `transactionTable`,
  	'accounts' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`payments`,payment_method,account
  				
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)
  WHERE payments.payment_method_id = payment_method.payment_method_id
   AND payment_method.account_id = account.account_id 
   AND payments.cancel = 0 AND payments.confirmation_status = 0 AND DATE(`payments`.`payment_date`) >= account.start_date

 UNION ALL

SELECT
  	`water_payments`.`payment_id` AS `transactionId`,
  	`water_payments`.`lease_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`water_payments`.`transaction_code` AS `referenceCode`,
  	`water_payments`.`transaction_code` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`water_payments`.`bank_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	'Money Received' AS `transactionName`,
  	 CONCAT('Amount Received from ',account.account_name,' Ref. ', `water_payments`.`transaction_code`) AS `transactionDescription`,
  	`water_payments`.`amount_paid` AS `dr_amount`,
     0 AS `cr_amount`,
  	`water_payments`.`transaction_date` AS `transactionDate`,
  	`water_payments`.`payment_created` AS `createdAt`,
  	`water_payments`.`cancel` AS `status`,
  	`water_payments`.cancel AS deleted,
  	'Income' AS `transactionCategory`,
  	'Income' AS `transactionClassification`,
  	'payments' AS `transactionTable`,
  	'accounts' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`water_payments`,payment_method,account
  				
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)
  WHERE water_payments.payment_method_id = payment_method.payment_method_id
   AND water_payments.bank_id = account.account_id 
   AND water_payments.cancel = 0 AND water_payments.confirmation_status = 0 AND  DATE(`water_payments`.`transaction_date`) >= account.start_date

UNION ALL


SELECT
  	`water_payments`.`payment_id` AS `transactionId`,
  	`water_payments`.`lease_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`water_payments`.`transaction_code` AS `referenceCode`,
  	`water_payments`.`transaction_code` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`payment_method`.`account_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	'Money Received' AS `transactionName`,
  	 CONCAT('Direct Deposit to ',account.account_name,' Ref. ', `water_payments`.`transaction_code`) AS `transactionDescription`,
  	`water_payments`.`amount_paid` AS `dr_amount`,
     0 AS `cr_amount`,
  	CONCAT(`water_payments`.`payment_date`,' 0000-00-00') AS `transactionDate`,
  	`water_payments`.`payment_created` AS `createdAt`,
  	`water_payments`.`cancel` AS `status`,
  	`water_payments`.cancel AS deleted,
  	'Income' AS `transactionCategory`,
  	'Income' AS `transactionClassification`,
  	'payments' AS `transactionTable`,
  	'accounts' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`water_payments`,payment_method,account
  				
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)
  WHERE water_payments.payment_method_id = payment_method.payment_method_id
   AND payment_method.account_id = account.account_id 
   AND water_payments.cancel = 0 AND water_payments.confirmation_status = 0 AND  DATE(`water_payments`.`payment_date`) >= account.start_date

UNION ALL

SELECT
  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`finance_transfer`.`reference_number` AS `referenceCode`,
  	`finance_transfer`.`document_number` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`finance_transfered`.`account_to_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	`finance_transfered`.`remarks` AS `transactionName`,
  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
  	`finance_transfered`.`finance_transfered_amount` AS `dr_amount`,
     0 AS `cr_amount`,
  	`finance_transfer`.`transaction_date` AS `transactionDate`,
  	`finance_transfer`.`created` AS `createdAt`,
  	`finance_transfer`.`finance_transfer_status` AS `status`,
	finance_transfer.finance_transfer_deleted AS `deleted`,
  	'Transfer' AS `transactionCategory`,
  	'Transfer' AS `transactionClassification`,
  	'finance_transfer' AS `transactionTable`,
  	'finance_transfered' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`finance_transfered`
  				JOIN `finance_transfer` ON(
  					(
  						finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id
  					)
  				)
  			)
  			JOIN account ON(
  				(
  					account.account_id = finance_transfered.account_to_id 
  				)
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)
  WHERE finance_transfer.finance_transfer_deleted = 0 

UNION ALL


SELECT
	`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	`finance_purchase`.`finance_purchase_id` AS `payingFor`,
	`finance_purchase`.`transaction_number` AS `referenceCode`,
	`finance_purchase`.`document_number` AS `transactionCode`,
	`finance_purchase`.`property_id` AS `property_id`,
  	`property`.`property_name` AS `property_name`,
  	finance_purchase.creditor_id AS `recepientId`,
    finance_purchase.transaction_id AS `transaction`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`,' Ref. ', `finance_purchase`.`transaction_number`) AS `transactionDescription`,
	0 AS `dr_amount`,
	`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
	`finance_purchase`.`finance_purchase_deleted` AS `deleted`,
	'Expense Payment' AS `transactionCategory`,
	'Purchase Payment' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'finance_purchase_payment' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase_payment`
				JOIN `finance_purchase` ON(
					(
						finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id 
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = finance_purchase_payment.account_from_id
				)
			)
			LEFT JOIN `property` ON(
		      (
		        property.property_id = finance_purchase.property_id
		      )
		    )
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)
	WHERE finance_purchase.finance_purchase_deleted = 0

UNION ALL


SELECT
	`finance_purchase`.`finance_purchase_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	`finance_purchase`.`transaction_number` AS `referenceCode`,

	`finance_purchase`.`document_number` AS `transactionCode`,
	`finance_purchase`.`property_id` AS `property_id`,
  	`property`.`property_name` AS `property_name`,
	`finance_purchase`.`creditor_id` AS `recepientId`,
  finance_purchase.transaction_id AS `transaction`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`,' Ref. ', `finance_purchase`.`transaction_number`) AS `transactionDescription`,
	`finance_purchase`.`finance_purchase_amount` AS `dr_amount`,
	0 AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase`.`finance_purchase_status` AS `status`,
	`finance_purchase`.`finance_purchase_deleted` AS `deleted`,
	'Expense' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase`
				JOIN account ON(
					(
						account.account_id = finance_purchase.account_to_id
					)
				)
			)

		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
		LEFT JOIN `property` ON(
		      (
		        property.property_id = finance_purchase.property_id
		      )
		    )
	)
	WHERE finance_purchase.finance_purchase_deleted = 0
	
UNION ALL



  SELECT
  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`finance_transfer`.`reference_number` AS `referenceCode`,
  	`finance_transfer`.`document_number` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`finance_transfer`.`account_from_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	`finance_transfer`.`remarks` AS `transactionName`,
  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `transactionDescription`,
  	0 AS `dr_amount`,
  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
  	`finance_transfered`.`transaction_date` AS `transactionDate`,
  	`finance_transfered`.`created` AS `createdAt`,
  	`finance_transfer`.`finance_transfer_status` AS `status`,
  	finance_transfer.finance_transfer_deleted AS `deleted`,
  	'Transfer' AS `transactionCategory`,
  	'Transfer' AS `transactionClassification`,
  	'finance_transfered' AS `transactionTable`,
  	'finance_transfer' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`finance_transfer`
  				JOIN `finance_transfered` ON(
  					(
  						finance_transfered.finance_transfer_id = finance_transfer.finance_transfer_id
  					)
  				)
  			)
  			JOIN account ON(
  				(
  					account.account_id = finance_transfer.account_from_id
  				)
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)

  	WHERE finance_transfer.finance_transfer_deleted = 0
  UNION ALL
 

  SELECT
	`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
	`creditor_payment`.`creditor_payment_id` AS `referenceId`,
	`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
	`creditor_payment`.`reference_number` AS `referenceCode`,
	`creditor_payment`.`document_number` AS `transactionCode`,
	'' AS `property_id`,
	'' AS `property_name`,
  	`creditor_payment`.`creditor_id` AS `recepientId`,
    '' AS `transaction`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_payment_item`.`description` AS `transactionName`,
	CONCAT('Payment for invoice of ',' ',`creditor_invoice`.`invoice_number`,' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
	0 AS `dr_amount`,
	`creditor_payment_item`.`amount_paid` AS `cr_amount`,
	`creditor_payment`.`transaction_date` AS `transactionDate`,
	`creditor_payment`.`created` AS `createdAt`,
	`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
	`creditor_payment_item`.`creditor_payment_item_status` AS `deleted`,
	'Expense Payment' AS `transactionCategory`,
	'Creditors Invoices Payments' AS `transactionClassification`,
	'creditor_payment' AS `transactionTable`,
	'creditor_payment_item' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_payment_item`,creditor_payment,creditor
				
			)
			JOIN account ON(
				(
					account.account_id = creditor_payment.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
		JOIN `creditor_invoice` ON(
			(
				creditor_invoice.creditor_invoice_id = creditor_payment_item.creditor_invoice_id
			)
		)
	)
	WHERE creditor_payment_item.invoice_type = 0
	AND creditor.creditor_id = creditor_payment.creditor_id 
	AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
	AND creditor_payment.creditor_payment_status = 1 AND creditor_payment.transaction_date >= creditor.start_date

UNION ALL


SELECT
`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
`creditor_payment`.`creditor_payment_id` AS `referenceId`,
`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
`creditor_payment`.`reference_number` AS `referenceCode`,
`creditor_payment`.`document_number` AS `transactionCode`,
'' AS `property_id`,
'' AS `property_name`,
`creditor_payment`.`creditor_id` AS `recepientId`,
'' AS `transaction`,
`account`.`parent_account` AS `accountParentId`,
`account_type`.`account_type_name` AS `accountsclassfication`,
`creditor_payment`.`account_from_id` AS `accountId`,
`account`.`account_name` AS `accountName`,
`creditor_payment_item`.`description` AS `transactionName`,
CONCAT('Payment for creditor invoice',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
0 AS `dr_amount`,
`creditor_payment_item`.`amount_paid` AS `cr_amount`,
`creditor_payment`.`transaction_date` AS `transactionDate`,
`creditor_payment`.`created` AS `createdAt`,
`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
`creditor_payment_item`.`creditor_payment_item_status` AS `deleted`,
'Expense Payment' AS `transactionCategory`,
'Creditors Invoices Payments' AS `transactionClassification`,
'creditor_payment' AS `transactionTable`,
'creditor_payment_item' AS `referenceTable`
FROM
(
	(
		(
			`creditor_payment_item`,creditor_payment,creditor
		)
		JOIN account ON(
			(
				account.account_id = creditor_payment.account_from_id
			)
		)
	)
	JOIN `account_type` ON(
		(
			account_type.account_type_id = account.account_type_id
		)
	)
)
WHERE creditor_payment_item.invoice_type = 2
AND creditor.creditor_id = creditor_payment.creditor_id 
AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
AND creditor_payment.creditor_payment_status = 1 AND creditor_payment.transaction_date >= creditor.start_date


UNION ALL

SELECT
`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
`creditor_payment`.`creditor_payment_id` AS `referenceId`,
`creditor_payment_item`.`creditor_invoice_id` AS `payingFor`,
`creditor_payment`.`reference_number` AS `referenceCode`,
`creditor_payment`.`document_number` AS `transactionCode`,
'' AS `property_id`,
'' AS `property_name`,
`creditor_payment`.`creditor_id` AS `recepientId`,
'' AS `transaction`,
`account`.`parent_account` AS `accountParentId`,
`account_type`.`account_type_name` AS `accountsclassfication`,
`creditor_payment`.`account_from_id` AS `accountId`,
`account`.`account_name` AS `accountName`,
`creditor_payment_item`.`description` AS `transactionName`,
CONCAT('Payment on account',' Ref. ', `creditor_payment`.`reference_number`)  AS `transactionDescription`,
0 AS `dr_amount`,
`creditor_payment_item`.`amount_paid` AS `cr_amount`,
`creditor_payment`.`transaction_date` AS `transactionDate`,
`creditor_payment`.`created` AS `createdAt`,
`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
`creditor_payment_item`.`creditor_payment_item_status` AS `deleted`,
'Expense Payment' AS `transactionCategory`,
'Creditors Invoices Payments' AS `transactionClassification`,
'creditor_payment' AS `transactionTable`,
'creditor_payment_item' AS `referenceTable`
FROM
(
	(
		(
			`creditor_payment_item`,creditor_payment,creditor
		)
		JOIN account ON(
			(
				account.account_id = creditor_payment.account_from_id
			)
		)
	)
	JOIN `account_type` ON(
		(
			account_type.account_type_id = account.account_type_id
		)
	)
)
WHERE creditor_payment_item.invoice_type = 3
AND creditor.creditor_id = creditor_payment.creditor_id 
AND creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id 
AND creditor_payment.creditor_payment_status = 1 AND creditor_payment.transaction_date >= creditor.start_date


UNION ALL

SELECT
  	`landlord_transactions`.`landlord_transaction_id` AS `transactionId`,
  	`landlord_transactions`.`property_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`landlord_transactions`.`chequeno` AS `referenceCode`,
  	`landlord_transactions`.`chequeno` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`landlord_transactions`.`bank_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	'Money Paid to Landlord' AS `transactionName`,
  	 CONCAT('Payment to ',property_owners.property_owner_name,' for ',property.property_name, '<br> -' ,`landlord_transactions`.`chequeno`) AS `transactionDescription`,
  	 0 AS `dr_amount`,
     `landlord_transactions`.`landlord_transaction_amount` AS `cr_amount`,
  	`landlord_transactions`.`transaction_date` AS `transactionDate`,
  	`landlord_transactions`.`created` AS `createdAt`,
  	`landlord_transactions`.`landlord_transaction_deleted` AS `status`,
  	`landlord_transactions`.`landlord_transaction_deleted` AS `deleted`,
  	'Landlord Payment' AS `transactionCategory`,
  	'Landlord Payment' AS `transactionClassification`,
  	'landlord_transactions' AS `transactionTable`,
  	'accounts' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`landlord_transactions`,account
  				
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  		JOIN `property_owners` ON(
  			(
  				landlord_transactions.landlordid = property_owners.property_owner_id
  			)
  		)
  		JOIN `property` ON(
  			(
  				landlord_transactions.property_id = property.property_id
  			)
  		)
  	)
  WHERE landlord_transactions.bank_id = account.account_id 
   AND landlord_transactions.landlord_transaction_deleted = 0


   UNION ALL

   SELECT
	`mpesa_transfers`.`mpesa_transfer_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	mpesa_transfers.transaction_number AS `transactionCode`,
	'' AS `property_id`,
	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	mpesa_transfers.account_from_id AS `accountId`,
	account.account_name AS `accountName`,
	'Mpesa Transfer' AS `transactionName`,
	CONCAT('Mpesa Transfer') AS `transactionDescription`,
	0 AS `dr_amount`,
	`mpesa_transfers`.`transacted_amount` AS `cr_amount`,
	`mpesa_transfers`.`transaction_date` AS `transactionDate`,
	`mpesa_transfers`.`payment_date` AS `createdAt`,
	`mpesa_transfers`.`mpesa_transfer_status` AS `status`,
	0 AS `deleted`,
	'Account Expense' AS `transactionCategory`,
	'Account Expense' AS `transactionClassification`,
	'' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
mpesa_transfers,account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE mpesa_transfers.account_from_id  = account.account_id AND mpesa_transfers.mpesa_transfer_status = 0
AND  DATE(`mpesa_transfers`.`transaction_date`) >= account.start_date


UNION ALL

   SELECT
	`mpesa_transfers`.`mpesa_transfer_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	mpesa_transfers.transaction_number AS `transactionCode`,
	'' AS `property_id`,
	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	mpesa_transfers.account_to_id AS `accountId`,
	account.account_name AS `accountName`,
	'Mpesa Transfered' AS `transactionName`,
	CONCAT('Mpesa Transfer') AS `transactionDescription`,
	`mpesa_transfers`.`transacted_amount` AS `dr_amount`,
	0 AS `cr_amount`,
	`mpesa_transfers`.`transaction_date` AS `transactionDate`,
	`mpesa_transfers`.`payment_date` AS `createdAt`,
	`mpesa_transfers`.`mpesa_transfer_status` AS `status`,
	0 AS `deleted`,
	'Account Expense' AS `transactionCategory`,
	'Account Expense' AS `transactionClassification`,
	'' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
mpesa_transfers,account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE mpesa_transfers.account_to_id  = account.account_id AND mpesa_transfers.mpesa_transfer_status = 0 
AND  DATE(`mpesa_transfers`.`transaction_date`) >= account.start_date

UNION ALL 

SELECT
	`account_payments`.`account_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	account_payments.receipt_number AS `transactionCode`,
	'' AS `property_id`,
	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	account_payments.account_from_id AS `accountId`,
	account.account_name AS `accountName`,
	'Direct payments for landlord ' AS `transactionName`,
	CONCAT('Direct Payment to ', ' ',(SELECT property_owner_name FROM property_owners WHERE property_owner_id = account_payments.payment_to )) AS `transactionDescription`,
	0 AS `dr_amount`,
	`account_payments`.`amount_paid` AS `cr_amount`,
	`account_payments`.`payment_date` AS `transactionDate`,
	`account_payments`.`payment_date` AS `createdAt`,
	`account_payments`.`account_payment_status` AS `status`,
	0 AS `deleted`,
	'Account Expense' AS `transactionCategory`,
	'Account Expense' AS `transactionClassification`,
	'' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
account_payments,account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE account_payments.account_from_id  = account.account_id AND account_payments.account_payment_deleted = 0 
AND account_payments.account_to_type = 3
AND  DATE(`account_payments`.`payment_date`) >= account.start_date

UNION ALL 

SELECT
	`account_payments`.`account_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	account_payments.receipt_number AS `transactionCode`,
	'' AS `property_id`,
	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	account_payments.account_from_id AS `accountId`,
	account.account_name AS `accountName`,
	'Direct Expense Payment ' AS `transactionName`,
	CONCAT('Direct Payment ',(SELECT account_name FROM account WHERE account_id = account_payments.account_to_id )) AS `transactionDescription`,
	0 AS `dr_amount`,
	`account_payments`.`amount_paid` AS `cr_amount`,
	`account_payments`.`payment_date` AS `transactionDate`,
	`account_payments`.`payment_date` AS `createdAt`,
	`account_payments`.`account_payment_status` AS `status`,
	0 AS `deleted`,
	'Direct Expense Payment' AS `transactionCategory`,
	'Direct Expense Payment' AS `transactionClassification`,
	'' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
account_payments,account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE account_payments.account_from_id  = account.account_id AND account_payments.account_payment_deleted = 0 
AND account_payments.account_to_type = 4
AND  DATE(`account_payments`.`payment_date`) >= account.start_date

UNION ALL

SELECT
	`account_payments`.`account_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	account_payments.receipt_number AS `transactionCode`,
	'' AS `property_id`,
	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
	account.parent_account AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	account_payments.account_to_id AS `accountId`,
	account.account_name AS `accountName`,
	'Direct Expense Payment ' AS `transactionName`,
	CONCAT('Direct Payment from',(SELECT account_name FROM account WHERE account_id = account_payments.account_from_id )) AS `transactionDescription`,
	`account_payments`.`amount_paid`  AS `dr_amount`,
	0 AS `cr_amount`,
	`account_payments`.`payment_date` AS `transactionDate`,
	`account_payments`.`payment_date` AS `createdAt`,
	`account_payments`.`account_payment_status` AS `status`,
	0 AS `deleted`,
	'Direct Expense Payment' AS `transactionCategory`,
	'Direct Expense Payment' AS `transactionClassification`,
	'' AS `transactionTable`,
	'account' AS `referenceTable`
FROM
account_payments,account
LEFT JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
WHERE account_payments.account_to_id  = account.account_id AND account_payments.account_payment_deleted = 0 
AND account_payments.account_to_type = 4
AND  DATE(`account_payments`.`payment_date`) >= account.start_date
;




CREATE OR REPLACE VIEW v_account_ledger_by_date AS select * from v_account_ledger ORDER BY transactionDate;