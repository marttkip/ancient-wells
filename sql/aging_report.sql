CREATE OR REPLACE VIEW v_aged_receivables AS
SELECT
  property.property_id,
  property.property_name as receivables,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) )  = 0, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) )  = 0, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) )  = 0, invoice.invoice_amount, 0 ))
      -
        ( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 )
          FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id)
        - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 )
          FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)

        END
  ) AS `coming_due`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 1 AND 30, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 1 AND 30, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 1 AND 30, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
    END
  ) AS `thirty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 31 AND 60, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 31 AND 60, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 31 AND 60, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
    END
  ) AS `sixty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 61 AND 90, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 61 AND 90, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 61 AND 90, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
    END
  ) AS `ninety_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) >90, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) >90, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) >90, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
    END
  ) AS `over_ninety_days`,

  (

    (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) )  = 0, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) )  = 0, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) )  = 0, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
      END
    )-- Getting the Value for 0 Days
    + (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 1 AND 30, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 1 AND 30, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 1 AND 30, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
      END
    ) --  AS `1-30 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 31 AND 60, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 31 AND 60, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 31 AND 60, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
      END
    ) -- AS `31-60 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 61 AND 90, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 61 AND 90, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) BETWEEN 61 AND 90, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
      END
    ) -- AS `61-90 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) >90, invoice.invoice_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) >90, invoice.invoice_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( invoice.invoice_date ) ) >90, invoice.invoice_amount, 0 ))-( SELECT COALESCE ( SUM( payment_item.amount_paid ), 0 ) FROM payment_item WHERE payment_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id) - ( SELECT COALESCE ( SUM( credit_note_item.credit_note_amount ), 0 ) FROM credit_note_item WHERE credit_note_item.invoice_id = invoice.invoice_id)
      END
    ) -- AS `>90 Days`
  ) AS `Total`

  FROM
    invoice
    LEFT JOIN invoice ON invoice.invoice_id = invoice.invoice_id
    LEFT JOIN rental_unit ON invoice.rental_unit_id = rental_unit.rental_unit_id
    LEFT JOIN property ON property.property_id = rental_unit.property_id
GROUP BY property.property_id;
