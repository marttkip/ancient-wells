CREATE OR REPLACE VIEW v_petty_cash AS 

SELECT 

	account.account_id AS `transactionId`,
  	account.account_id AS `referenceId`,
  	'' AS `payingFor`,
  	account.account_id AS `referenceCode`,
  	account.account_id AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	account.account_id AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	'Opening Balance' AS `transactionName`,
  	'Opening Balance' AS `transactionDescription`,
  	`account`.`account_opening_balance` AS `dr_amount`,
     0 AS `cr_amount`,
  	`account`.`start_date` AS `transactionDate`,
  	`account`.`start_date` AS `createdAt`,
  	`account`.`account_status` AS `status`,
		`account`.`account_status` AS `deleted`,
  	'Account' AS `transactionCategory`,
  	'Account' AS `transactionClassification`,
  	'account' AS `transactionTable`,
  	'account' AS `referenceTable`

 FROM 

 account,account_type

 WHERE account_status = 1 AND account.account_type_id = account_type.account_type_id

 UNION ALL

SELECT
    `finance_transfered`.`finance_transfered_id` AS `transactionId`,
    `finance_transfer`.`finance_transfer_id` AS `referenceId`,
    '' AS `payingFor`,
    `finance_transfer`.`reference_number` AS `referenceCode`,
    `finance_transfer`.`document_number` AS `transactionCode`,
    '' AS `property_id`,
    '' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
    `account`.`parent_account` AS `accountParentId`,
    `account_type`.`account_type_name` AS `accountsclassfication`,
    `finance_transfered`.`account_to_id` AS `accountId`,
    `account`.`account_name` AS `accountName`,
    `finance_transfered`.`remarks` AS `transactionName`,
     CONCAT('Amount transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
    0 AS `dr_amount`,
    `finance_transfered`.`finance_transfered_amount` AS `cr_amount`,
    `finance_transfer`.`transaction_date` AS `transactionDate`,
    `finance_transfer`.`created` AS `createdAt`,
    `finance_transfer`.`finance_transfer_status` AS `status`,
  finance_transfer.finance_transfer_deleted AS `deleted`,
    'Transfer' AS `transactionCategory`,
    'Transfer' AS `transactionClassification`,
    'finance_transfer' AS `transactionTable`,
    'finance_transfered' AS `referenceTable`
  FROM
 

finance_transfer,`finance_transfered`,account,account_type
          
        
WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
AND account.account_id = finance_transfer.account_from_id 
AND finance_transfer.transaction_date >= account.start_date
AND account_type.account_type_id = account.account_type_id
AND finance_transfer.finance_transfer_deleted = 0



UNION ALL

SELECT
  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
  	'' AS `payingFor`,
  	`finance_transfer`.`reference_number` AS `referenceCode`,
  	`finance_transfer`.`document_number` AS `transactionCode`,
  	'' AS `property_id`,
  	'' AS `property_name`,
    '' AS `recepientId`,
    '' AS `transaction`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`finance_transfered`.`account_to_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	`finance_transfered`.`remarks` AS `transactionName`,
  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id ),' Ref. ', `finance_transfer`.`reference_number`) AS `transactionDescription`,
  	`finance_transfered`.`finance_transfered_amount` AS `dr_amount`,
     0 AS `cr_amount`,
  	`finance_transfer`.`transaction_date` AS `transactionDate`,
  	`finance_transfer`.`created` AS `createdAt`,
  	`finance_transfer`.`finance_transfer_status` AS `status`,
	finance_transfer.finance_transfer_deleted AS `deleted`,
  	'Transfer' AS `transactionCategory`,
  	'Transfer' AS `transactionClassification`,
  	'finance_transfer' AS `transactionTable`,
  	'finance_transfered' AS `referenceTable`
  FROM
 

finance_transfer,`finance_transfered`,account,account_type
  				
  			
WHERE finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id 
AND account.account_id = finance_transfered.account_to_id 
AND finance_transfer.transaction_date >= account.start_date
AND account_type.account_type_id = account.account_type_id
AND finance_transfer.finance_transfer_deleted = 0

UNION ALL


SELECT
	`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
	'' AS `referenceId`,
	`finance_purchase`.`finance_purchase_id` AS `payingFor`,
	`finance_purchase`.`transaction_number` AS `referenceCode`,
	`finance_purchase`.`document_number` AS `transactionCode`,
	'' AS `property_id`,
  	`property_owners`.`property_owner_name` AS `property_name`,
  	finance_purchase.property_owner_id AS `recepientId`,
    finance_purchase.transaction_id AS `transaction`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`,' Ref. ', `finance_purchase`.`transaction_number`) AS `transactionDescription`,
	0 AS `dr_amount`,
	`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
	`finance_purchase`.`finance_purchase_deleted` AS `deleted`,
	'Expense Payment' AS `transactionCategory`,
	'Purchase Payment' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'finance_purchase_payment' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase_payment`,finance_purchase,account,account_type
				
			)
			
			LEFT JOIN `property_owners` ON(
		      (
		        finance_purchase.property_owner_id = property_owners.property_owner_id
		      )
		    )
		)
		
	)
	WHERE finance_purchase.finance_purchase_deleted = 0 
	AND account.account_id = finance_purchase_payment.account_from_id 
	AND account_type.account_type_id = account.account_type_id
	AND finance_purchase.transaction_date >= account.start_date
	AND finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id
