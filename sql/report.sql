


CREATE OR REPLACE VIEW  v_tenant_leases AS  select `tenants`.`tenant_name` AS `tenant_name`,`tenants`.`tenant_phone_number` AS `tenant_phone_number`,
`tenants`.`tenant_id` AS `tenant_id`,
`leases`.`rental_unit_id` AS `rental_unit_id`,
`leases`.`lease_id` AS `lease_id`,
`property_billing`.`property_billing_deleted`,
`property_billing`.`property_billing_id`,
`property_billing`.`billing_amount` AS `billing_amount`,
`tenants`.`tenant_number` AS `tenant_number`,
`leases`.`lease_number` AS `lease_number`,
`leases`.`notice_date` AS `notice_date`,
`leases`.`vacated_on` AS `vacated_on`
from (((`leases`,property_billing)
left join `tenants` on((`tenants`.`tenant_id` = `leases`.`tenant_id`)))
left join `tenant_unit` on((`tenant_unit`.`tenant_unit_id` = `leases`.`tenant_unit_id`)))
WHERE `property_billing`.`lease_id` = `leases`.`lease_id` AND property_billing.invoice_type_id = 1 AND property_billing.property_billing_deleted = 0
group by `leases`.`lease_id`;


CREATE OR REPLACE VIEW v_active_leases AS SELECT `tenants`.tenant_code,tenants.tenant_id,tenants.tenant_name,tenants.tenant_national_id,tenants.tenant_phone_number,tenants.created AS tenant_created_on, `rental_unit`.`rental_unit_id` AS unit_id,
`rental_unit`.rental_unit_name, rental_unit.created as rental_unit_created_on,rental_unit_code,
`property`.property_id,property.property_name,property.created as property_created_on,property_code,
leases.lease_number,`leases`.lease_id,leases.notice_date,leases.created AS lease_created_on,leases.lease_status,lease_start_date,lease_end_date
FROM
(`tenants`, `tenant_unit`, `rental_unit`, `leases`, `property`)
WHERE
tenants.tenant_id = tenant_unit.tenant_id
AND leases.rental_unit_id = rental_unit.rental_unit_id
AND tenant_unit.tenant_unit_id = leases.tenant_unit_id
AND leases.lease_status < 4
AND property.property_id = rental_unit.property_id
GROUP BY leases.lease_id
ORDER BY `rental_unit`.`rental_unit_id`, `rental_unit`.`rental_unit_name`, `property`.`property_name` ASC;




CREATE OR REPLACE VIEW v_tenants_tickets AS select `tenants`.`tenant_code` AS `tenant_code`,`tenants`.`tenant_id` AS `tenant_id`,`tenants`.`tenant_name` AS `tenant_name`,
`tenants`.`tenant_national_id` AS `tenant_national_id`,`tenants`.`tenant_phone_number` AS `tenant_phone_number`,
`tenants`.`created` AS `tenant_created_on`,`rental_unit`.`rental_unit_id` AS `unit_id`,`rental_unit`.`rental_unit_name` AS `rental_unit_name`,`rental_unit`.`created`
AS `rental_unit_created_on`,`rental_unit`.`rental_unit_code` AS `rental_unit_code`,`property`.`property_id` AS `property_id`,`property`.`property_name` AS `property_name`,
`property`.`created` AS `property_created_on`,`property`.`property_code` AS `property_code`,`leases`.`lease_number` AS `lease_number`,`leases`.`lease_id` AS `lease_id`,`leases`.`notice_date`
 AS `notice_date`,`leases`.`created` AS `lease_created_on`,`leases`.`lease_status` AS `lease_status`,`leases`.`lease_start_date` AS `lease_start_date`,`leases`.`lease_end_date` AS `lease_end_date`,
ticket.created as ticket_created,ticket.ticket_description,ticket_status,ticket.ticket_number,ticket.ticket_status_id,ticket.ticket_id
from (((((`ticket` join `tenants`) join `tenant_unit`) join `rental_unit`) join `leases`) join `property`)
where ((`tenants`.`tenant_id` = `tenant_unit`.`tenant_id`)
 and (`tenant_unit`.`rental_unit_id` = `rental_unit`.`rental_unit_id`)
and (`tenant_unit`.`tenant_unit_id` = `leases`.`tenant_unit_id`)
and (`ticket`.`lease_id` = `leases`.`lease_id`)
and (`property`.`property_id` = `rental_unit`.`property_id`))
order by ticket.ticket_number ASC;
