CREATE OR REPLACE VIEW  v_lease_total_invoice as 
select sum(`invoice`.`invoice_amount`) AS `total_invoice`,`invoice`.`lease_id` AS `lease_id` 
			from (`invoice`) 
			where (`invoice`.`invoice_status` = 0)
			group by `invoice`.`lease_id`;

CREATE OR REPLACE VIEW v_lease_total_payments as 
select sum(`payments`.`amount_paid`) AS `total_amount_paid`,`payments`.`lease_id` AS `lease_id` 
	from `payments` 
	where ((`payments`.`payment_status` = 1) 
		and (`payments`.`cancel` = 0)) 
	group by `payments`.`lease_id`;

CREATE OR REPLACE VIEW v_lease_total_pardon as 
select sum(`pardon_payments`.`pardon_amount`) AS `total_pardon`,`pardon_payments`.`lease_id` AS `lease_id` 
	from `pardon_payments` 
	where ((`pardon_payments`.`pardon_status` = 1) 
		and (`pardon_payments`.`pardon_delete` = 0)) 
	group by `pardon_payments`.`lease_id`;


CREATE OR REPLACE VIEW  v_lease_account AS 
			select 
			ifnull(`v_lease_total_invoice`.`total_invoice`,0) AS `total_invoice_amount`,
			ifnull(`v_lease_total_payments`.`total_amount_paid`,0) AS `total_paid_amount`,
			ifnull(`v_lease_total_pardon`.`total_pardon`,0) AS `total_waived_amount`,
			`leases`.`lease_number` AS `lease_number` ,
			`leases`.`lease_id` AS `lease_id` 
			from ((`leases` left join `v_lease_total_payments` on((`v_lease_total_payments`.`lease_id` = `leases`.`lease_id`))) 
				left join `v_lease_total_invoice` on((`v_lease_total_invoice`.`lease_id` = `leases`.`lease_id`))) 
				left join `v_lease_total_pardon` on((`v_lease_total_pardon`.`lease_id` = `leases`.`lease_id`))
			where (`leases`.`lease_deleted` = 0);

CREATE OR REPLACE VIEW v_lease_balances as 
select 
	`v_lease_account`.`total_invoice_amount` AS `total_invoice_amount`,
	`v_lease_account`.`total_paid_amount` AS `total_paid_amount`,
	`v_lease_account`.`total_waived_amount` AS `total_waived_amount`,
	((`v_lease_account`.`total_invoice_amount`) - (`v_lease_account`.`total_paid_amount` + `v_lease_account`.`total_waived_amount`)) AS `balance`,
	`v_lease_account`.`lease_number` AS `lease_number`, 
	`v_lease_account`.`lease_id` AS `lease_id` 
	from `v_lease_account`;


CREATE OR REPLACE VIEW v_payments_unreconcilled_view AS
SELECT 
  payment_item.payment_item_id AS payment_item_id, 
  payment_item.payment_id AS payment_id, 
  payment_item.amount_paid AS amount_paid, 
    payment_item.invoice_type_id AS invoice_type_id,
    '' AS bank_id,
payment_item.mpesa_id AS mpesa_id,
    '' AS transaction_code,
    '' AS transaction_date,
    '' AS payment_date,
    'Rental Income' AS payment_type,
    'rental_income' AS checked_type,
    payment_item.payment_year AS year,
    payment_item.payment_month AS month,
    payment_item.lease_id AS lease_id,
    payment_item.remarks AS remarks
FROM payment_item
WHERE 
  payment_item.payment_id = 0

UNION ALL

SELECT 
  water_payment_item.payment_item_id AS payment_item_id, 
  water_payment_item.payment_id AS payment_id, 
  water_payment_item.amount_paid AS amount_paid, 
    water_payment_item.invoice_type_id AS invoice_type_id,
    '' AS bank_id,
    water_payment_item.mpesa_id AS mpesa_id,
    '' AS transaction_code,
    '' AS transaction_date,
    '' AS payment_date,
    'Water Income' AS payment_type,
    'water_income' AS checked_type,
    water_payment_item.payment_year AS year,
    water_payment_item.payment_month AS month,
    water_payment_item.lease_id AS lease_id,
    water_payment_item.remarks AS remarks
FROM water_payment_item
WHERE 
  water_payment_item.payment_id = 0;

CREATE OR REPLACE VIEW v_payments_view AS
SELECT 
	payment_item.payment_item_id AS payment_item_id, 
	payment_item.payment_id AS payment_id, 
	payment_item.amount_paid AS amount_paid, 
    payment_item.invoice_type_id AS invoice_type_id,
    payments.bank_id AS bank_id,
    payment_item.mpesa_id AS mpesa_id,
    payments.transaction_code AS transaction_code,
    payments.transaction_date AS transaction_date,
    payments.payment_date AS payment_date,
    'Rental Income' AS payment_type,
    'rental_income' AS checked_type,
    payments.year AS year,
    payments.month AS month,
    payments.lease_id AS lease_id
FROM payments,payment_item
WHERE 
	payments.payment_id = payment_item.payment_id 
	AND payments.cancel = 0 

UNION ALL

SELECT 
	water_payment_item.payment_item_id AS payment_item_id, 
	water_payment_item.payment_id AS payment_id, 
	water_payment_item.amount_paid AS amount_paid, 
    water_payment_item.invoice_type_id AS invoice_type_id,
    water_payments.bank_id AS bank_id,
    water_payment_item.mpesa_id AS mpesa_id,
    water_payments.transaction_code AS transaction_code,
    water_payments.transaction_date AS transaction_date,
    water_payments.payment_date AS payment_date,
    'Water Income' AS payment_type,
    'water_income' AS checked_type,
    water_payments.year AS year,
    water_payments.month AS month,
    water_payments.lease_id AS lease_id
FROM water_payments,water_payment_item
WHERE 
	water_payments.payment_id = water_payment_item.payment_id 
	AND water_payments.cancel = 0 ;


CREATE OR REPLACE VIEW v_mpesa_transactions AS
SELECT mpesa_transactions.*,COALESCE(SUM(v_payments_view.amount_paid),0) AS recon_amount from mpesa_transactions
LEFT JOIN v_payments_view ON v_payments_view.mpesa_id = mpesa_transactions.mpesa_id 
WHERE mpesa_transactions.mpesa_id > 0
GROUP BY mpesa_transactions.mpesa_id;


CREATE OR REPLACE VIEW v_withdrawals_transactions AS
SELECT mpesa_transactions.*,COALESCE(SUM(transacted_amount),0) AS recon_amount from mpesa_transactions
LEFT JOIN mpesa_transfers ON mpesa_transfers.mpesa_id = mpesa_transactions.mpesa_id 
WHERE mpesa_transactions.mpesa_id > 0 AND mpesa_transactions.transaction_type = 1
GROUP BY mpesa_transactions.mpesa_id;



-- SELECT `property_billing`.`lease_id` AS `transactionId`,
--        concat('RECP', '-', `property_billing`.`lease_id`) AS `referenceId`,
--        `property_billing`.`lease_id` AS `lease_id`,
--        'Opening Balance' AS `accountsclassfication`,
--        `property_billing`.`invoice_type_id` AS `accountId`,
--        `invoice_type`.`invoice_type_name` AS `accountName`,
--        concat('Payment for ', `invoice_type`.`invoice_type_name`) AS `transactionDescription`,
--        property_billing.arrears_bf  AS `dr_amount`,
--        0 AS `cr_amount`,
--        leases.lease_start_date AS `transactionDate`,
--        leases.lease_start_date AS `createdAt`,
--        1 AS `status`,
--        MONTH(leases.lease_start_date) AS `transaction_year`,
--        YEAR(leases.lease_start_date) AS `transaction_month`,
--        'Opening Balance' AS `transactionCategory`,
--        2 AS `transactionType`,
--        'payment_item' AS `transactionTable`,
--        'payments' AS `referenceTable`
-- FROM property_billing,leases,invoice_type
-- WHERE  property_billing.property_billing_deleted = 0
--   AND invoice_type.invoice_type_id = property_billing.invoice_type_id
--    AND leases.lease_id = property_billing.lease_id AND property_billing.invoice_type_id = 2



-- UNION ALL

-- SELECT `property_billing`.`lease_id` AS `transactionId`,
--        concat('RECP', '-', `property_billing`.`lease_id`) AS `referenceId`,
--        `property_billing`.`lease_id` AS `lease_id`,
--        'Opening Balance' AS `accountsclassfication`,
--        `property_billing`.`invoice_type_id` AS `accountId`,
--        `invoice_type`.`invoice_type_name` AS `accountName`,
--        concat('Payment for ', `invoice_type`.`invoice_type_name`) AS `transactionDescription`,
--        property_billing.arrears_bf  AS `dr_amount`,
--        0 AS `cr_amount`,
--        leases.lease_start_date AS `transactionDate`,
--        leases.lease_start_date AS `createdAt`,
--        1 AS `status`,
--        MONTH(leases.lease_start_date) AS `transaction_year`,
--        YEAR(leases.lease_start_date) AS `transaction_month`,
--        'Opening Balance' AS `transactionCategory`,
--        1 AS `transactionType`,
--        'payment_item' AS `transactionTable`,
--        'payments' AS `referenceTable`
-- FROM property_billing,leases,invoice_type
-- WHERE  property_billing.property_billing_deleted = 0
--   AND invoice_type.invoice_type_id = property_billing.invoice_type_id
--    AND leases.lease_id = property_billing.lease_id AND property_billing.invoice_type_id <> 2

-- UNION ALL

CREATE OR REPLACE VIEW v_transactions AS



SELECT `invoice`.`invoice_id` AS `transactionId`,
       concat('INV', '-', `invoice`.`invoice_id`) AS `referenceCode`,
       `invoice`.`lease_id` AS `lease_id`,
       'Payable' AS `accountsclassfication`,
       `invoice`.`invoice_type` AS `accountId`,
       `invoice_type`.`invoice_type_name` AS `accountName`,
       concat('Invoice for ', `invoice`.`invoice_month`, '-', `invoice`.`invoice_year`) AS `transactionDescription`,
       `invoice`.`invoice_amount` AS `dr_amount`,
       '0' AS `cr_amount`,
       invoice.invoice_date AS `transactionDate`,
       `invoice`.`invoice_date` AS `createdAt`,
       `invoice`.`invoice_status` AS `status`,
       `invoice`.`invoice_year` AS `transaction_year`,
       `invoice`.`invoice_month` AS `transaction_month`,
       'Rent Invoice' AS `transactionCategory`,
       1 AS `transactionType`,
       'invoice' AS `transactionTable`,
       'invoice' AS `referenceTable`
FROM ((`invoice`
       JOIN `leases`)
      JOIN `invoice_type`)
WHERE ((`invoice`.`invoice_status` = 1)
       AND (`invoice`.`invoice_type` = `invoice_type`.`invoice_type_id`)
       AND (`invoice`.`lease_id` = `leases`.`lease_id`))
UNION ALL
SELECT `payments`.`payment_id` AS `transactionId`,
       concat('RECP', '-', `payment_item`.`payment_id`) AS `referenceId`,
       `payments`.`lease_id` AS `lease_id`,
       'Invoice Payments' AS `accountsclassfication`,
       `payment_item`.`invoice_type_id` AS `accountId`,
       `invoice_type`.`invoice_type_name` AS `accountName`,
       concat('Payment for ', `invoice_type`.`invoice_type_name`) AS `transactionDescription`,
       '0' AS `dr_amount`,
       `payment_item`.`amount_paid` AS `cr_amount`,
       payments.transaction_date AS `transactionDate`,
       `payments`.`payment_created` AS `createdAt`,
       `payment_item`.`payment_item_status` AS `status`,
       `payments`.`year` AS `transaction_year`,
       `payments`.`month` AS `transaction_month`,
       'Rental Payments' AS `transactionCategory`,
       1 AS `transactionType`,
       'payment_item' AS `transactionTable`,
       'payments' AS `referenceTable`
FROM ((`payment_item`
       JOIN `payments` on((`payment_item`.`payment_id` = `payments`.`payment_id`)))
      JOIN `invoice_type` on((`payment_item`.`invoice_type_id` = `invoice_type`.`invoice_type_id`)))
WHERE (`payments`.`cancel` = 0 AND  payments.confirmation_status = 0)
UNION ALL
SELECT `pardon_payments`.`pardon_id` AS `transactionId`,
       concat('CR', '-', `pardon_payments`.`pardon_id`) AS `referenceId`,
       `pardon_payments`.`lease_id` AS `lease_id`,
       'Credit Note' AS `accountsclassfication`,
       `invoice_type`.`invoice_type_id` AS `accountId`,
       `invoice_type`.`invoice_type_name` AS `accountName`,
       concat('Credit Note for ', `invoice_type`.`invoice_type_name`) AS `transactionDescription`,
       '0' AS `dr_amount`,
       `pardon_payments`.`pardon_amount` AS `cr_amount`,
       `pardon_payments`.`pardon_date` AS `transactionDate`,
       `pardon_payments`.`pardon_date` AS `createdAt`,
       `pardon_payments`.`pardon_status` AS `status`,
       year(`pardon_payments`.`pardon_date`) AS `transaction_year`,
       month(`pardon_payments`.`pardon_date`) AS `transaction_month`,
       'Rental Pardons' AS `transactionCategory`,
       1 AS `transactionType`,
       'pardon_payments' AS `transactionTable`,
       'pardon_payments' AS `referenceTable`
FROM (`pardon_payments`,invoice_type)
WHERE ((`pardon_payments`.`pardon_delete` = 0 AND `invoice_type`.`invoice_type_id` = pardon_payments.invoice_type_id))


UNION ALL 

SELECT `water_invoice`.`invoice_id` AS `transactionId`,
       concat('INV', '-', `water_invoice`.`invoice_id`) AS `referenceCode`,
       `water_invoice`.`lease_id` AS `lease_id`,
       'Payable' AS `accountsclassfication`,
       `water_invoice`.`invoice_type` AS `accountId`,
       `invoice_type`.`invoice_type_name` AS `accountName`,
       concat('Invoice for ', `water_invoice`.`invoice_month`, '-', `water_invoice`.`invoice_year`) AS `transactionDescription`,
       `water_invoice`.`invoice_amount` AS `dr_amount`,
       '0' AS `cr_amount`,
       water_invoice.invoice_date AS `transactionDate`,
       `water_invoice`.`invoice_date` AS `createdAt`,
       `water_invoice`.`invoice_status` AS `status`,
       `water_invoice`.`invoice_year` AS `transaction_year`,
       `water_invoice`.`invoice_month` AS `transaction_month`,
       'Water Invoice' AS `transactionCategory`,
       2 AS `transactionType`,
       'water_invoice' AS `transactionTable`,
       'water_invoice' AS `referenceTable`
FROM ((`water_invoice`
       JOIN `leases`)
      JOIN `invoice_type`)
WHERE ((`water_invoice`.`invoice_status` = 1)
       AND (`water_invoice`.`invoice_type` = `invoice_type`.`invoice_type_id`)
       AND (`water_invoice`.`lease_id` = `leases`.`lease_id`))
UNION ALL

SELECT `water_payments`.`payment_id` AS `transactionId`,
       concat('RECP', '-', `water_payment_item`.`payment_id`) AS `referenceId`,
       `water_payments`.`lease_id` AS `lease_id`,
       'Invoice Payments' AS `accountsclassfication`,
       `water_payment_item`.`invoice_type_id` AS `accountId`,
       `invoice_type`.`invoice_type_name` AS `accountName`,
       concat('Payment for ', `invoice_type`.`invoice_type_name`) AS `transactionDescription`,
       '0' AS `dr_amount`,
       `water_payment_item`.`amount_paid` AS `cr_amount`,
       water_payments.transaction_date AS `transactionDate`,
       `water_payments`.`payment_created` AS `createdAt`,
       `water_payment_item`.`payment_item_status` AS `status`,
       `water_payments`.`year` AS `transaction_year`,
       `water_payments`.`month` AS `transaction_month`,
       'Rental Payments' AS `transactionCategory`,
       2 AS `transactionType`,
       'payment_item' AS `transactionTable`,
       'payments' AS `referenceTable`
FROM ((`water_payment_item`
       JOIN `water_payments` on((`water_payment_item`.`payment_id` = `water_payments`.`payment_id`)))
      JOIN `invoice_type` on((`water_payment_item`.`invoice_type_id` = `invoice_type`.`invoice_type_id`)))
WHERE (`water_payments`.`cancel` = 0  AND  payments.confirmation_status = 0)

UNION ALL


SELECT `water_pardon_payments`.`pardon_id` AS `transactionId`,
       concat('CR', '-', `water_pardon_payments`.`pardon_id`) AS `referenceId`,
       `water_pardon_payments`.`lease_id` AS `lease_id`,
       'Credit Note' AS `accountsclassfication`,
       `invoice_type`.`invoice_type_id` AS `accountId`,
       `invoice_type`.`invoice_type_name` AS `accountName`,
       concat('Credit Note for ', `invoice_type`.`invoice_type_name`) AS `transactionDescription`,
       '0' AS `dr_amount`,
       `water_pardon_payments`.`pardon_amount` AS `cr_amount`,
       `water_pardon_payments`.`pardon_date` AS `transactionDate`,
       `water_pardon_payments`.`pardon_date` AS `createdAt`,
       `water_pardon_payments`.`pardon_status` AS `status`,
       year(`water_pardon_payments`.`pardon_date`) AS `transaction_year`,
       month(`water_pardon_payments`.`pardon_date`) AS `transaction_month`,
       'Rental Pardons' AS `transactionCategory`,
       2 AS `transactionType`,
       'pardon_payments' AS `transactionTable`,
       'pardon_payments' AS `referenceTable`
FROM (`water_pardon_payments`,invoice_type)
WHERE ((`water_pardon_payments`.`pardon_delete` = 0 AND `invoice_type`.`invoice_type_id` = water_pardon_payments.invoice_type_id))
;
-- end of payments

CREATE OR REPLACE VIEW v_transactions_by_date AS SELECT * FROM v_transactions   ORDER BY transactionDate;

CREATE OR REPLACE VIEW v_rent_balances AS 
SELECT 
v_transactions.lease_id,
SUM(v_transactions.dr_amount) AS total_debits, 
SUM(v_transactions.cr_amount) AS total_credits,
(SUM(v_transactions.dr_amount) - SUM(v_transactions.cr_amount)) AS total_balance,
property_billing.billing_amount AS billing_amount,
((SUM(v_transactions.dr_amount) - SUM(v_transactions.cr_amount))/property_billing.billing_amount) AS total_units

FROM v_transactions,property_billing
WHERE v_transactions.accountId <> 2 
AND property_billing.invoice_type_id = 1
 AND property_billing.property_billing_deleted = 0 
 AND property_billing.lease_id = v_transactions.lease_id
GROUP BY v_transactions.lease_id;

CREATE OR REPLACE VIEW v_water_balances AS 
SELECT 
v_transactions.lease_id,
SUM(v_transactions.dr_amount) AS total_debits, 
SUM(v_transactions.cr_amount) AS total_credits,
(SUM(v_transactions.dr_amount) - SUM(v_transactions.cr_amount)) AS total_balance

FROM v_transactions
WHERE v_transactions.accountId = 2
GROUP BY v_transactions.lease_id;


